#!/usr/bin/env bash
#git reset --hard HEAD~
git pull
echo "git pull ok";

composer update --no-dev
echo "composer ok";

php artisan cache:clear
php artisan view:clear
php artisan route:clear
php artisan cache:clear
php artisan config:clear
php artisan optimize:clear
composer dump-autoload
php artisan clear-compiled
php artisan vendor:publish --all

echo "artisan ok";

chmod -R 775 bootstrap/cache/
chmod -R 777 storage/
echo "chmod ok";
