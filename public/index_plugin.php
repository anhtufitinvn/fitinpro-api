<?php

function fitin3dscript($html) {
    return <<<HTML
{$html}
HTML;
}
    
function pushCss($uri) {
    header("Link: <{$uri}>; rel=preload; as=style", false);
    return <<<HTML
<link href="{$uri}" rel=stylesheet>
HTML;
}

function pushJs($uri) {
    header("Link: <{$uri}>; rel=preload; as=script", false);
    return <<<HTML
<script src="{$uri}"></script>
HTML;
}

function preload($uri, $type){
    header("Link: <{$uri}>; rel=preload; as=$type", false);
}

function getExtension($path)
{
    $ar = explode('.', $path);
    return $ar[count($ar) - 1];
}

function mapping_preload($manifest, &$array){
    foreach($manifest as $file){
        
        $ext = getExtension($file);
        switch ($ext) {
            case 'js' : 
                $array[] = pushJs($file);
                break;
            case 'css' : 
                $array[] = pushCss($file);
                break;    
            case 'svg' : 
                $array[] = preload($file, 'image');
                break;   
            case 'json' : 
                $array[] = preload($file, 'fetch');
                break;  
            default : 
                $array[] = preload($file, 'font');
                break;
    
        }
    }

    return $array;
}

//Frontend Script
$asset_manifest = file_get_contents('asset-manifest.json');
$asset_manifest = json_decode($asset_manifest, true);

$fitin3d_manifest = file_get_contents('fitin3d-manifest.json');
$fitin3d_manifest = json_decode($fitin3d_manifest, true);

$asset_push = [];

$fitin3d_push = [];

$asset_push = mapping_preload($asset_manifest, $asset_push);
$fitin3d_push = mapping_preload($fitin3d_manifest, $fitin3d_push);


?>
<!DOCTYPE html>
<html lang=en>

<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <link rel=icon href=/designs/favicon.ico> <title>Fitin3d Editor</title>
    <link href=/designs/static/css/chunk-9c50cd2c.09a19154.css rel=prefetch>
    <link href=/designs/static/js/chunk-1f5b8117.b7f9660a.js rel=prefetch>
    <link href=/designs/static/js/chunk-2418f85c.1696a800.js rel=prefetch>
    <link href=/designs/static/js/chunk-2d0d2fce.0d47cf6a.js rel=prefetch>
    <link href=/designs/static/js/chunk-3e57555f.3e12d245.js rel=prefetch>
    <link href=/designs/static/js/chunk-7da138e8.ece16630.js rel=prefetch>
    <link href=/designs/static/js/chunk-9c50cd2c.550022c6.js rel=prefetch>
    <link href=/designs/static/css/app.2e985fe7.css rel=preload as=style>
    <link href=/designs/static/css/chunk-vendors.c456727f.css rel=preload as=style>
    <link href=/designs/static/js/app.3cb4442c.js rel=preload as=script>
    <link href=/designs/static/js/chunk-vendors.794439b8.js rel=preload as=script>

    <!--<link href=/designs/static/css/chunk-vendors.c456727f.css rel=stylesheet>
    <link href=/designs/static/css/app.2e985fe7.css rel=stylesheet> -->
    <?php foreach($asset_push as $script) { echo fitin3dscript($script); }?>
    <?php foreach($fitin3d_push as $script) { echo fitin3dscript($script); } ?>
</head>

<body>
    <noscript><strong>We're sorry but fitin3d-editor-threejs doesn't work properly without JavaScript enabled. Please
            enable it to continue.</strong></noscript>
    <div id=app></div>
    

    
    <!--<script src=/designs/static/js/chunk-vendors.794439b8.js> </script> 
    <script src=/designs/static/js/app.3cb4442c.js></script> -->
    
</body> 
</html>