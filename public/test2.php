<?php
$params = [
        'vendor3d' => $_GET['vendor3d'] ?? null,
        'fitin3d' => $_GET['fitin3d'] ?? null,
        'script' => $_GET['script'] ?? null
];

function fitin3dscript($html) {
    return <<<HTML
{$html}
HTML;
}
    
function pushScript($uri) {
    header("Link: <{$uri}>; rel=preload; as=script", false);
    return <<<HTML
<script src="{$uri}"></script>
HTML;
}

$script_vendor = pushScript($params['vendor3d']);
$script_fitin = pushScript($params['fitin3d']);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />
    <link rel="icon" href="/favicon.ico" />
    <link href="css/fontawesome.min.css" rel="stylesheet"> <!--load all styles -->
    <link href="css/custom.css" rel="stylesheet"> <!--load all styles -->
    <title>fitin3d-editor-threejs</title>
    <script src="../libs/lodash.min.js"></script>
    <style>
      body{
        position: absolute;
        width: 100%;
        height: 100%;
        display: block;
        margin: 0;
      }
    </style>

    <?php
        echo fitin3dscript($script_vendor);
        echo fitin3dscript($script_fitin);
    ?>

    <script>
      function getParamsUrl() {
        var pairs = window.location.search.substring(1).split("&"),
            obj = {},
            pair,
            i;
        for (i in pairs) {
            if (pairs[i] === "") {
                continue;
            }
            pair = pairs[i].split("=");
            obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
        }
        return obj;
      }
      var params = getParamsUrl();

      var head = document.getElementsByTagName('HEAD')[0]; 
  
      // Create new link Element 
      //var vendor = document.createElement('script'); 
      //var fitin3d = document.createElement('script'); 
      //vendor.src = params.vendor3d;  
      //fitin3d.src = params.fitin3d;
      
      var messageWrap = document.createElement('script'); 
      messageWrap.src = "./messageWrap.js";
      var actionContext = document.createElement('script'); 
      actionContext.src = "./actionContext.js";
      var script = document.createElement('script'); 
      script.src = "./"+ <?php echo $params['script'] ?> +".js";


    // vendor.addEventListener("load", function () {
    //    head.appendChild(fitin3d);
    //  });
    //   fitin3d.addEventListener("load", function () {
    //   head.appendChild(messageWrap);
    //    head.appendChild(actionContext);
    //  });
      messageWrap.addEventListener("load", function () {
        head.appendChild(script);
      });

      // Append link element to HTML head 
      head.appendChild(vendor);
      
    </script>
  </head>
  <body>
    <noscript>
      <strong
        >We're sorry but fitin3d-editor-threejs doesn't work properly without
        JavaScript enabled. Please enable it to continue.</strong
      >
    </noscript>
    <div id="id-menu-context" class="btn-feature">
      <div class="interaction-btns--expanded">
        <button id="menu-context-info" class="default-btn "><i class="fas fa-info"></i></button>
        <button id="menu-context-home" class="default-btn "><i class="fas fa-home"></i></button>
        <button id="menu-context-pen" class="default-btn "><i class="fas fa-pen"></i></button>
        <button id="menu-context-clone" class="default-btn "><i class="fas fa-clone"></i></button>
        <button id="menu-context-undo" class="default-btn "><i class="fas fa-undo"></i></button>
        <button id="menu-context-delete" class="default-btn "><i class="fas fa-trash-alt"></i></button>
        <button id="menu-context-close" class="default-btn "><i class="fas fa-times"></i></button>
        <button id="menu-context-rotation" class="default-btn "><i class="fas fa-sync"></i></button>
      </div>
    </div>
  </body>
</html>
