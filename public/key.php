<?php
$cache = new \Memcached();
$cache->addServer('memcached', '11211');

// get all stored memcached items

$keys = $cache->getAllKeys();
$data = [];
foreach ($keys as $key){
	$data[$key] = $cache->get($key);
}

echo json_encode($data);