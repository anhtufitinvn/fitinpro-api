

chrome.extension.onMessage.addListener(function(message, sender, sendResponse) {
    
    switch(message.command) {
        case "crawl":
            //crawlParent();
            crawlChild();
        break;
    }
    sendResponse('success');
});


function crawlParent() {
    var url = 'http://localhost:9020/api/cms/v2/color-groups';

    var select_group_element = $(".select2-results__option");
    console.log(select_group_element.children('.colour-text-wrap'));
    select_group_element.each(function(){
        var e = $(this);
        var name = e.find('.colour-text-wrap').text();
        var data = {
            name : name
        }
        jqueryPost(url, data);
    });

}

function jqueryPost(url, data){
    $.post( url, data, function(response) {
        console.log(response);
      });
}

function crawlChild() {
    var pathname = window.location.pathname;
    var arr =  pathname.split("/");
    var group = arr[arr.length - 1];
    var url = 'http://localhost:9020/api/cms/v2/colors';

    var select_group_element = $(".colour-code-page .colour-wrap");
    select_group_element.each(function(){
        var e = $(this);
        var li_sub = e;
        var name = li_sub.find('h3').text();
        var color = li_sub.data('colour');
        var data = {
            name : name,      
            group: group
        }

        if(li_sub.find('.field-field-colour-identify').length){
            data.code = li_sub.find('.field-field-colour-identify').text();
        }else{
            name_arr = name.split("_");
            var flag1 = name_arr[0];
            var flag1_arr = flag1.split(" ");
            var code1 = flag1_arr[flag1_arr.length - 1];
            var flag2= name_arr[1];
            var flag2_arr = flag2.split(" ");
            var code2 = flag2_arr[0];
            data.code = code1 + '-' + code2;
        }

        if(color.length > 7){
            data.externalImage = color;
        }else{
            data.hex = color;
        }

        jqueryPost(url, data);
    });

}


