<?php 
    ini_set('max_execution_time', 0);


    $fitin_hash = '536535c7524841ec843e6d42ddd9add8';
    $fitin_secret_key = 'fitin3d.com';
    $api_mode = 'dev';
    $api_url = "https://api-3d.fitin.vn/v2/objects/video/";
    //require_once "Mobile_Detect.php";


    function streamfile($link, $size){
        
        $v = $link;
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_VERBOSE, 1);
        // curl_setopt($ch, CURLOPT_TIMEOUT, 222222);
        // curl_setopt($ch, CURLOPT_URL, $v);
        // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        // curl_setopt($ch, CURLOPT_HEADER, true);
        // curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    
        // curl_setopt($ch, CURLOPT_NOBODY, true);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $info = curl_exec($ch);
        // $size2 = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        
        // $filesize = $size2;
        // $offset = 0;
        // $length = $filesize;
        
        header("Content-Type: video/mp4");
        header('Accept-Ranges: bytes');
        header("Content-length: ".$size);
    
    
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 222222);
        curl_setopt($ch, CURLOPT_URL, $v);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    
        curl_setopt($ch, CURLOPT_NOBODY, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
        curl_exec($ch);
    }

    
    function validate($objectId, $key, $sid){
        global  $fitin_hash, $fitin_secret_key;
        $objectId = strtolower($objectId);
        if (strpos($objectId, '_') !== false) {
            $item_array = explode('_', $objectId);
            $brand  = $item_array[0];
        }else{
            return false;
        }
        
        $hash = hash_hmac('md5', $sid . $brand . $objectId, $fitin_secret_key);
        if($hash !== $key && $fitin_hash !== $key){
            return false;
        }
        return $objectId;
        
    }

    if(!$_GET['objectId'] || !$_GET['key'] ){
        return false;
    };
    $sid = $_GET['sid'];
    $objectId = validate($_GET['objectId'], $_GET['key'], $_GET['sid']);
    if(!$objectId) {
        return false;
    }
    
    
    try{
        // $url = 'https://render-'.$api_mode.'.fitin3d.com/v1/assets/transform/v360/' . $objectId;
        $url = $api_url . $objectId;
        $result = false;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url
        ));

        $result = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($result, true);
        
        if($data['code'] == 0){
            $result = $data['data']['url'];
            $size = $data['data']['size'];
        }
        else{
            $result = NULL;
        }
        
       
    }catch(Exception $e) {
        echo 'Message: ' .$e->getMessage();
        $result = NULL;
    }
    

    if(!$result){
        exit;
    }
    
    //$remote = "https://cdn.fitin.vn/stream/index.php?key=jysk_bedside_cabinet_vedde";
    //$link = "https://cdn.fitin.vn/video/$objectId/$objectId.mp4";
    streamfile($result, $size);
?>   

