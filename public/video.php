
<?php 
    $fitin_hash = '536535c7524841ec843e6d42ddd9add8';
    $fitin_secret_key = 'fitin3d.com';
    $api_mode = 'dev';
    $api_url = "https://api-3d.fitin.dev/v2/objects/video/";
    //require_once "Mobile_Detect.php";


    function validate($objectId, $key, $sid){
        global  $fitin_hash, $fitin_secret_key;
        $objectId = strtolower($objectId);
        if (strpos($objectId, '_') !== false) {
            $item_array = explode('_', $objectId);
            $brand  = $item_array[0];
        }else{
            return false;
        }
        
        $hash = hash_hmac('md5', $sid . $brand . $objectId, $fitin_secret_key);
        if($hash !== $key && $fitin_hash !== $key){
            return false;
        }
        return $objectId;
        
    }

    if(!$_GET['objectId'] || !$_GET['key'] ){
        return false;
    };
    $sid = $_GET['sid'];
    $objectId = validate($_GET['objectId'], $_GET['key'], $_GET['sid']);
    if(!$objectId) {
        return false;
    }
    

    try{
        // $url = 'https://render-'.$api_mode.'.fitin3d.com/v1/assets/transform/v360/' . $objectId;
        $url = $api_url . $objectId;
        $result = false;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL => $url
        ));

        $result = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($result, true);
        if($data['code'] == 0){
            $result = $data['data']['url'];
        }
        else{
            $result = NULL;
        }
       
    }catch(Exception $e) {
        echo 'Message: ' .$e->getMessage();
        $result = false;
    }
    

    if(!$result){
        exit;
    }
    //$detect = new Mobile_Detect;
    //$isMobile = $detect->isMobile();
    $remote = "https://cdn.fitin.vn/stream/index.php?key=jysk_bedside_cabinet_vedde";
   
   
?>   
<!DOCTYPE html>
<!--[if IE 8]><html lang="en" class="no-js ie ie8"><![endif]-->
<!--[if IE 9]><html lang="en" class="no-js ie ie9"><![endif]-->
<!--[if gt IE 9]><!-->
 
<html lang="en" class="no-js">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1"><![endif]-->
        <title>FitIn view video 360</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="FitIn3D - Plugin View 360">
        <meta name="keywords" content="js360">
        <meta name="robots" content="index, follow">
        <meta name="author" content="FitIn3D">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
        <style>
            #view-video {
                width: 100%;
                max-width: 720px;
                display: block;
                margin: 0 auto;
            }
            #view-video video {
                width: 100%;
            }
            #miss-object-id {
                display: none;
            }
            #miss-object-video {
                display: none;
            }
            .text-center {
                text-align: center;
            }
        </style>
        
    </head>
    <body>
        <script type="text/javascript">
            var dataVideo = '<?php echo $result;?>';
            var objectId = '<?php echo $objectId;?>';
        
        </script>

        <div id="miss-object-id">
            <h1 class="text-center">Please Add Object ID in Url! </h1>
        </div>

        <div id="miss-object-video">
            <h1 class="text-center">Sản phảm không có hỗ trợ video 360!</h1>
        </div>
        
        <div id="view-video">
            <video controls preload="metadata">
                <source src="/stream.php?key=<?php echo $objectId; ?>#t=0.5" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>

        <script>
            if (objectId === '') {
                document.getElementById('miss-object-id').style.display = 'block';
                document.getElementById('view-video').style.display = 'none';
            } else  if (dataVideo === '') {
                document.getElementById('miss-object-video').style.display = 'block';
                document.getElementById('view-video').style.display = 'none';
            }
        </script>
        <?php

        ?>
        
    </body>
</html>
