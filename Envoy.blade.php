{{-- [1] composer global require laravel/envoy --}}
{{-- [2] envoy run deploy --server=live --}}

@include('vendor/autoload.php');

@setup
    $now = new DateTime();
    $server = isset($server) ? $server : "local";
    $app_path = "/var/www/cms.fitin3d.com/enterprise/";
@endsetup

@servers(['local' => 'local', 'live' => 'root@45.124.94.203'])

@task('get_file',['on' => [$server]])
    uname -a
    ls -la
@endtask

@task('info_debug',['on' => [$server]])
    echo {{ $server }}
    echo {{ $app_path }}
@endtask


@task('composer',['on' => [$server]])
    @if($server == 'live')
        cd {{$app_path}}
        composer update --no-dev
        composer dump-autoload
    @else
        composer update
    @endif
@endtask

@task('artisan_cache',['on' => [$server]])
    @if($server == 'live')
        cd {{$app_path}}
    @endif
    php artisan view:clear
    php artisan cache:clear
    php artisan clear-compiled
    php artisan route:cache
    php artisan config:clear
    php artisan config:cache
    php artisan optimize --force
@endtask

@task('chown',['on' => [$server]])
    @if($server == 'live')
        cd {{$app_path}}
        sudo chown -R root:root .
        sudo chmod -R 777:77 storage/
    @endif
@endtask

@task('down',['on' => [$server]])
@if($server == 'live')
    cd {{$app_path}}
    php artisan down --message="Website đang bảo trì"
@endif
@endtask

@task('npm',['on' => [$server]])
    npm i
    npm run watch-poll
@endtask

@task('git_pull',['on' => [$server]])
    @if($server == 'live')
        echo
        cd {{$app_path}}
        ls -la
        git config --get remote.origin.url
        git status
        git fetch
        git pull
    @endif
@endtask

@macro('deploy')
    info_debug
    git_pull
    {{--composer--}}
    {{--artisan_cache--}}
@endmacro

@finished
    @slack('https://hooks.slack.com/services/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', '#notifications', 'Deploy Successfully!')
@endfinished

