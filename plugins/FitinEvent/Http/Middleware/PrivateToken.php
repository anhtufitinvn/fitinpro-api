<?php

namespace Plugin\FitinEvent\Http\Middleware;

use Closure;

class PrivateToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(request()->header('fitin-token') &&  in_array(request()->header('fitin-token'), config('external-token') )){         
            return $next($request);
        }else{
            return response()->json([
                'code' => -1,
                'message' => 'Unauthorized!',
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        }
        
    }
}
