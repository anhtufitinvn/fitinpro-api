<?php

namespace Plugin\FitinEvent\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'code' => $this->code,
            'title' => $this->title,
            'content' => $this->content,
            'mediaUrl' => $this->mediaUrl,
            'meta' => $this->meta,
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
