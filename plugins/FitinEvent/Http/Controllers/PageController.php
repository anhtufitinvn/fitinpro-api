<?php

namespace Plugin\FitinEvent\Http\Controllers;

use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Plugin\FitinEvent\Models\Page;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Plugin\FitinEvent\Http\Resources\PageResource;
use Plugin\FitinEvent\Http\Resources\PageResourceCollection;

use Plugin\FitinEvent\Http\Resources\PageMetaResource;

use Plugin\FitinEvent\Repositories\PageRepository;

class PageController extends Controller
{

    protected $repository;

    public function __construct(PageRepository $repository){
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $code = $request->get('code');
        $exists = $this->repository->find($code);
        if($exists){
            return response()->json([
                'code' => -1,
                'message' => 'Page code already exists!'
            ]);
        }
        $input = $request->all();
        $page = $this->repository->create($input);

        $file = $request->file('file');
        if($request->hasFile('file') && $file->isValid()){
            $page = $this->repository->uploadMedia($page, $file);
        }
        
        return new PageResource($page);
    }
  

    /**
     * Display the specified resource.
     *
     * @param Brand $category
     * @return BrandResource
     */
    public function show($key)
    {
        $page = $this->repository->find($key);

        if(!$page){
            return response()->json([
                'code' => -1,
                'message' => 'Page not found!'
            ]);
        }
        return new PageResource($page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param 
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code']);
        $page = $this->repository->update($key, $input);
        if(!$page){
            return response()->json([
                'code' => -1,
                'message' => 'Page not found!'
            ]);
        }

        $file = $request->file('file');
        if($request->hasFile('file') && $file->isValid()){
            $page = $this->repository->uploadMedia($page, $file);
        }
        
        return new PageResource($page);
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param 
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        $page = $this->repository->delete($key);
        if(!$page){
            return response()->json([
                'code' => -1,
                'message' => 'Page not found!'
            ]);
        }
        return response()->json([
            'code' => 0,
            'message' => 'Success'
        ]);
    }

    public function storeMeta(Request $request){
        $code = $request->get('code');
        $exists = $this->repository->find($code);
        if(!$exists){
            return response()->json([
                'code' => -1,
                'message' => 'Page not exists!'
            ]);
        }
        $input = $request->all();
        $page_meta = $this->repository->createMeta($input);

        $file = $request->file('file');
        if($request->hasFile('file') && $file->isValid()){
            $page_meta = $this->repository->uploadMetaMedia($page_meta, $file);
        }
        
        return new PageMetaResource($page_meta);
    }

    public function updateMeta(Request $request, $key)
    {
        $input = $request->except(['code']);
        $page_meta = $this->repository->updateMeta($key, $input);
        if(!$page_meta){
            return response()->json([
                'code' => -1,
                'message' => 'Page metadata not found!'
            ]);
        }

        $file = $request->file('file');
        if($request->hasFile('file') && $file->isValid()){
            $page_meta = $this->repository->uploadMetaMedia($page_meta, $file);
        }
        
        return new PageMetaResource($page_meta);
    }

    public function destroyMeta($key)
    {
        $page_meta = $this->repository->deleteMeta($key);
        if(!$page_meta){
            return response()->json([
                'code' => -1,
                'message' => 'Page metadata not found!'
            ]);
        }
        return response()->json([
            'code' => 0,
            'message' => 'Success'
        ]);
    }
}
