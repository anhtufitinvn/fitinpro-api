<?php

namespace Plugin\FitinEvent\Http\Controllers;


use Plugin\CS\Http\Resources\OrderResource;
use Plugin\CS\Http\Resources\OrderResourceCollection;
use Illuminate\Support\Facades\Validator;
use Plugin\CS\Models\Order;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\Controller;

class EventController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $orders = $this->repository->getAll();
        return new OrderResourceCollection($orders);
    }

    public function test()
    {
        dd('dmtest');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $order = $this->repository->create($input);
        return new OrderResource($order);
    }
  

    /**
     * Display the specified resource.
     *
     * @param Brand $category
     * @return BrandResource
     */
    public function show($key)
    {
        $order = $this->repository->find($key);
        if(!$order){
            return $this->responseError('Order not found');
        }
        return new OrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        $input = $request->all();
        $order = $this->repository->update($key, $input);
        if(!$order){
            return $this->responseError('Order not found');
        }
        return new OrderResource($order);
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        $order = $this->repository->delete($key);
        if(!$order){
            return $this->responseError('Order not found');
        }
        return $this->respondSuccess();
    }
}
