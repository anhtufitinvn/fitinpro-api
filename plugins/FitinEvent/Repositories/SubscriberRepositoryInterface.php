<?php


namespace Plugin\FitinEvent\Repositories;

use App\Repositories\RepositoryInterface;

interface SubscriberRepositoryInterface extends RepositoryInterface {

}