<?php

namespace Plugin\CS\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Storage;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Plugin\CS\Models\Customer;
use Plugin\CS\Models\Order;

class SyncFromExternalWebhookJob //implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
        * Create a new job instance.
        *
        * @return void
        */
    public $tries = 5;

    protected $input;

    public function __construct($input)
    {
        //
        $this->input = $input;
    }

    /**
        * Execute the job.
        *
        * @return void
        */
    public function handle()
    {
        $input = $this->input;
        $type = $input['type'];
        $action = $input['action'];
        $data = $input['data'];    

        Log::channel('cs')->info($input);
        
        if($type == 'order'){
            $this->syncOrder($action, $data);
        }

        if($type == 'customer'){
            $this->syncCustomer($action, $data);
        }
    }

    public function syncOrder($action, $data){
        if(empty($data['order_code'])){
            return false;
        }
        $customer = $data['customer'] ?? NULL;
        if(!$customer){
            return false;
        }
        if($action == 'create'){
            $order = Order::firstOrCreate([
                "userId" => $data['customer_id'] ?? NULL,
                "orderCode" => $data['order_code'] ?? NULL,
                "status" => $data['stage'] ?? NULL,
                "dataJson" => $data
            ]);

            if( !empty($customer['phone'])){
                $customer = Customer::updateOrCreate(['phone' => $customer['phone']], [
                    'phone' => $customer['phone'],
                    'type' => 'customer',
                    'name' => $customer['name'] ?? NULL,
                    'email' => $customer['email'] ?? NULL,
                    'userId' => $customer['id'] ?? NULL
                ]);
            }
            
        }

        if($action == 'update'){
            if(empty($data['order_code'])){
                return false;
            }
            $customer = $data['customer'] ?? NULL;
            if(!$customer){
                return false;
            }
            $order = Order::updateOrCreate(["orderCode" => $data['order_code']],[
                "userId" => $data['customer_id'] ?? NULL,
                "orderCode" => $data['order_code'],
                "status" => $data['stage'] ?? NULL,
                "dataJson" => $data
            ]);

            if( !empty($customer['phone'])){
                $customer = Customer::updateOrCreate(['phone' => $customer['phone']], [
                    'phone' => $customer['phone'],
                    'type' => 'customer',
                    'name' => $customer['name'] ?? NULL,
                    'email' => $customer['email'] ?? NULL,
                    'userId' => $customer['id'] ?? NULL
                ]);
            }
        }
    }

    public function syncCustomer($action, $customer){
        if(empty($customer['phone'])){
            return false;
        }
        
        if($action == 'create'){
           
                $customer = Customer::updateOrCreate(['phone' => $customer['phone']], [
                    'phone' => $customer['phone'],
                    'type' => 'customer',
                    'name' => $customer['name'] ?? NULL,
                    'email' => $customer['email'] ?? NULL,
                    'userId' => $customer['id'] ?? NULL
                ]);
            
            
        }

        if($action == 'update'){ 
            
                $customer = Customer::updateOrCreate(['phone' => $customer['phone']], [
                    'phone' => $customer['phone'],
                    'type' => 'customer',
                    'name' => $customer['name'] ?? NULL,
                    'email' => $customer['email'] ?? NULL,
                    'userId' => $customer['id'] ?? NULL
                ]);
            
        }
    }
}
