<?php 

namespace Plugin\CS\Traits;

trait SearchTrait
{
    /**
     * Scope a query with search of term.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $term
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch($query, $term)
    {
        if(!$term){
            return $query->where('_id', false);
        }
        foreach ($this->searchable as $search_field){
            $query->orWhere($search_field, $term);
        }
        return $query;
    }
}