<?php
namespace Plugin\CS\Repositories;

use App\Repositories\EloquentRepository;
use Plugin\CS\Models\Customer;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Plugin\CS\Services\FitinResourceService;

class CustomerRepository extends EloquentRepository implements CustomerRepositoryInterface {
    
    const CACHE_TIME = 60 * 5;

    protected $service;

    public function __construct(FitinResourceService $service)
    {
        $this->model = app($this->model());
        $this->service = $service;
    }

	public function model()
	{
		return Customer::class;
	}
    
    public function getAll()
    {
        $cache_key = "index";
        $search =  request()->get('search');
        $type =  strtolower(request()->get('type'));
        $page = (int) (request()->get('page'));
        $limit = (int) (request()->get('limit', 30));
        $concat = concat(":", $search, $type, $page, $limit);
        $cache_key = $cache_key . $concat;
        $collections = Cache::tags(Customer::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use ($search, $type,  $page, $limit) {
            $query = $this->model;
            if($search){
                $query = $query->where('phone', 'like', '%'. $search. '%')->orWhere('email', 'like', '%'. $search. '%');
            }
            if($type){
                $query = $query->where('type', $type);
            }
            return $query->latest()->paginate($limit);
        });
        return $collections;
    }

    
    
	public function create($input)
	{
        $customer = $this->model->firstOrCreate(
            [
                'phone' => $input['phone']
            ], 
            $input
        ); 
        
        
        if(!isset($input['reference'])){
            $param = [
                'phone' => $input['phone']
            ];
        }else{
            $param = [
                'phone' => $input['reference'],
                'email' => $input['email'],
                'name' => $input['name']
            ];
        }
        $fitinCustomer = $this->service->getCustomer($param);
        if(!empty($fitinCustomer) && !empty($fitinCustomer['id'])){
                // $customer->userId = $fitinCustomer['id'];
                // $customer->save();
            if(!empty($fitinCustomer['phone'])){
                $c = $this->model->where('phone', $fitinCustomer['phone'])->first();
                if(!$c){
                    $customer = $this->model->create([
                        'name' => $fitinCustomer['name'] ?? null,
                        'phone' => $fitinCustomer['phone'] ?? null,
                        'email' => $fitinCustomer['email'] ?? null,
                       'userId' => $fitinCustomer['id'],
                        'type' => 'customer'
                    ]);
                }
            }
        }
        
        return $customer;
    }


    public function find($key){
        $cache_key = "show:$key";
        $customer = Cache::tags(Customer::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('phone', $key)->first();
        });
        
        return $customer;   
    }


    public function update($key, $input){
        $customer = $this->model->where('_id', $key)->orWhere('phone', $key)->first();
        if(!$customer){
            return false;
        }
        $customer->update($input);

        return $customer;
    }

    public function delete($key){
        $customer = $this->model->where('_id', $key)->orWhere('phone', $key)->first();
        
        if(!$customer){
            return false;
        }
        $customer->delete();
        return $customer;
    }

}