<?php
namespace Plugin\CS\Repositories;

use App\Repositories\EloquentRepository;
use Plugin\CS\Models\Order;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;


class OrderRepository extends EloquentRepository implements OrderRepositoryInterface {
    
    const CACHE_TIME = 60 * 5;

    public function __construct()
    {
        $this->model = app($this->model());
       
    }

	public function model()
	{
		return Order::class;
	}
    
    public function getAll()
    {
        $cache_key = "index";
        $collections = Cache::tags(Order::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function ()  {
            return $this->model->all();
        });
        return $collections;
    }

    
    
	public function create($input)
	{
		$order = $this->model->create($input);
        return $order;
    }

    public function firstOrCreate($input)
	{
        $data['orderCode'] = $input['order_code'] ?? null;
        $data['userId'] = $input['customer_id'] ?? null;
        $data['status'] = $input['stage'] ?? null;
        $data['dataJson'] = $input;
		$order = $this->model->firstOrCreate(['orderCode' => $input['order_code']], $data);
        return $order;
    }


    public function find($key){
        $cache_key = "show:$key";
        $order = Cache::tags(Order::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('orderCode', $key)->first();
        });
        
        return $order;   
    }


    public function update($key, $input){
        $order = $this->model->where('_id', $key)->orWhere('orderCode', $key)->first();
        if(!$order){
            return false;
        }
        $order->update($input);

        return $order;
    }

    public function delete($key){
        $order = $this->model->where('_id', $key)->orWhere('orderCode', $key)->first();
        
        if(!$order){
            return false;
        }
        $order->delete();
        return $order;
    }

}