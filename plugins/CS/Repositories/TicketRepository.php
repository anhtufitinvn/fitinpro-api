<?php
namespace Plugin\CS\Repositories;

use App\Repositories\EloquentRepository;
use Plugin\CS\Models\Ticket;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;


class TicketRepository extends EloquentRepository implements TicketRepositoryInterface {
    
    const CACHE_TIME = 60 * 5;

    public function __construct()
    {
        $this->model = app($this->model());
       
    }

	public function model()
	{
		return Ticket::class;
	}
    
    public function getAll()
    {
        $cache_key = "index";
        $search =  request()->get('search');
        $type = strtolower(request()->get('type'));
        $page = (int) (request()->get('page'));
        $limit = (int) (request()->get('limit', 30));
        $concat = concat(":", $search, $type, $page, $limit);
        $cache_key = $cache_key . $concat;
        $collections = Cache::tags(Ticket::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($search, $type, $page, $limit)  {
            $query = $this->model;
            if($search){
                $query = $query->where('phone', 'like', '%'. $search. '%');
            }
            if($type){
                $query = $query->where('type', $type);
            }
            return $query->latest()->paginate($limit);
        });
        return $collections;
    }

    
    
	public function create($input)
	{
        if(empty($input['creator']) && $user = Auth::guard('cs')->user()){
            $input['creator'] = $user->username;
        }
        $input['status'] = Ticket::TO_DO;
        $ticket = $this->model->create($input);
        return $ticket;
    }


    public function find($key){
        $cache_key = "show:$key";
        $ticket = Cache::tags(Ticket::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('ticketId', $key)->first();
        });
        
        return $ticket;   
    }


    public function update($key, $input){
        $ticket = $this->model->where('_id', $key)->orWhere('ticketId', $key)->first();
        if(!$ticket){
            return false;
        }
        $ticket->update($input);

        return $ticket;
    }

    public function delete($key){
        $ticket = $this->model->where('_id', $key)->orWhere('ticketId', $key)->first();
        
        if(!$ticket){
            return false;
        }
        $ticket->delete();
        return $ticket;
    }

}