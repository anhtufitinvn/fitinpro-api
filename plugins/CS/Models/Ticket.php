<?php

namespace Plugin\CS\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;
use Plugin\CS\Traits\SearchTrait;

class Ticket extends Eloquent {

    use CacheTrait, SearchTrait;

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->setTicketIdAttribute();
        });

        self::created(function ($model) {
            self::flushCache();
        });

        self::updating(function ($model) {

        });

        self::updated(function ($model) {
            self::flushCache();
        });

        self::deleting(function ($model) {

        });

        self::deleted(function ($model) {
            self::flushCache();
        });
    }
    
    protected $collection = 'CSTickets';


    protected $fillable = [
        "ticketId",
        "phone",
        "callReference",
        "type",
        "title",
        "description",
        "orderIds",
        "creator",
        "resolver",
        "status"
    ];

    public const CACHE_TAG = 'cs-ticket';
    public const ADDITIONAL_CACHE_TAG = 'cs-search';

    public const TO_DO = 'to-do';
    public const IN_PROGRESS = 'in-progress';
    public const RESOLVED = 'resolved';

    protected $searchable = [
        'phone' , 'ticketId'
    ];
    

    public function customer(){
        return $this->belongsTo(Customer::class, 'phone', 'phone');
    }

    public function _creator(){
        return $this->belongsTo(User::class, 'creator', 'username');
    }
    public function _resolver(){
        return $this->belongsTo(User::class, 'resolver', 'username');
    }

    public function setPhoneAttribute($value){
        $this->attributes['phone'] = (string) $value;
    }

    
    public function setTicketIdAttribute(){
        $prefix = \Carbon\Carbon::now()->format("ymd");
        
        $last = Ticket::where("ticketId", "like", "CS$prefix-%")->latest()->first();
        if(!$last){
            $id = "CS$prefix-00001";
        }else{
            $idArray = explode('-',$last->ticketId);
            $num = (int) $idArray[1];
            $id = "CS$prefix-" . str_pad($num + 1, 5, "0" , STR_PAD_LEFT);
        }
        $this->attributes['ticketId'] = $id;
    }
}