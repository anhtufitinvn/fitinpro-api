<?php

namespace Plugin\CS\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;
use Plugin\CS\Traits\SearchTrait;

class Order extends Eloquent {

    use CacheTrait, SearchTrait;
    
    protected $collection = 'CSOrders';


    protected $fillable = [
        "userId",
        "orderCode",
        "status",
        "dataJson"
    ];

    public const CACHE_TAG = 'cs-ticket';
    public const ADDITIONAL_CACHE_TAG = 'cs-search';
    
    protected $searchable = [
        'orderId'
    ];
    

    public function customer(){
        return $this->belongsTo(Customer::class, 'userId', 'userId');
    }

}