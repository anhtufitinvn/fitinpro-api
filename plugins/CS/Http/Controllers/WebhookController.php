<?php

namespace Plugin\CS\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Plugin\CS\Models\Ticket;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Plugin\CS\Jobs\SyncFromExternalWebhookJob;

class WebhookController extends ApiController
{

   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function get(Request $request)
    {
        

        return response()->json(
            [
                "code" => 0,
                "message" => "success"
            ]
        );

    }


}
