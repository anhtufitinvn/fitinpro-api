<?php

namespace Plugin\CS\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Helpers\Utils;
use Illuminate\Support\Facades\Cache;

use \Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{

    /**
     * Return generic json response with the given data.
     *
     * @param $data
     * @param int $statusCode
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respond($data, $statusCode = 200, $headers = [])
    {
        return response()->json($data, $statusCode, $headers);
    }

    /**
     * Respond with success.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondSuccess($message = 'Sucess')
    {
        return $this->respond([
            'code' => 0,
            'data' => [
                   
            ],
            'message' => 'Success'
        ]);
    }

    /**
     * Respond with created.
     *
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondCreated($data)
    {
        return $this->respond($data, 200);
    }

    /**
     * Respond with no content.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondNoContent()
    {
        return $this->respond(null, 204);
    }

    /**
     * Respond with error.
     *
     * @param $message
     * @param $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondError($message, $statusCode)
    {
        return $this->respond([
            'errors' => [
                'message' => $message,
                'status_code' => $statusCode
            ]
        ], $statusCode);
    }

    /**
     * Respond with unauthorized.
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondUnauthorized($message = 'Unauthorized')
    {
        return $this->respondError($message, -401);
    }

  

    /**
     * Sends a response that the object has been deleted, and also indicates
     * the id of the object that has been deleted.
     *
     * @param  int $id
     * @return JsonResponse
     */
    public function respondObjectDeleted($id)
    {
        return response()->json([
            'code' => 0,
            'message' => 'success',
            'data' => [
                'id' => $id,
                'deleted' => true
            ]
        ]);
    }



    public function responseData($data, $code = 0){
        
        return response()->json([
            'code' => $code,
            'message' => 'success',
            'data' => [
                'list' => $data
            ]
        ]);
    }

    public function responseObjectData($data, $code = 0){
        
        return response()->json([
            'code' => $code,
            'message' => 'success',
            'data' => $data
        ]);
    }

    public function responseError($message, $error_code = 'NOT_FOUND', $code = -1){
        return response()->json([        
            'code' => $code,
            'message' => $message,
            'data' => [
                'error_code' => $error_code
            ]
        ]);
    }

    public function clearCache($class){
        Cache::tags($class::CACHE_TAG)->flush();
    }

}
