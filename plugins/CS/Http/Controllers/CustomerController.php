<?php

namespace Plugin\CS\Http\Controllers;


use Plugin\CS\Http\Resources\CustomerResource;
use Plugin\CS\Http\Resources\CustomerResourceCollection;
use Illuminate\Support\Facades\Validator;
use Plugin\CS\Models\Customer;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

use Plugin\CS\Repositories\CustomerRepository;

class CustomerController extends ApiController
{

    protected $repository;

    public function __construct(CustomerRepository $repository){
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $customers = $this->repository->getAll();
        return new CustomerResourceCollection($customers);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'phone'   => 'required',
            'name' => 'required',
            'email' => 'required_with:reference'
        ]);
        if($validator->fails()){
            return $this->responseError('name, phone, email required', "INVALID");   
        }
        // $exists = $this->repository->find((string) $input['phone']);
        // if($exists){
        //     return $this->responseError('Phone number duplicated', 'DATA_EXISTS');
        // }
        $customer = $this->repository->create($input);
        return new CustomerResource($customer);
    }
  

    /**
     * Display the specified resource.
     *
     * @param Brand $category
     * @return BrandResource
     */
    public function show($key)
    {
        $customer = $this->repository->find($key);
        if(!$customer){
            return $this->responseError('Customer not found');
        }
        return new CustomerResource($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        $input = $request->only(['type', 'name', '2ndphone']);
        $customer = $this->repository->update($key, $input);
        if(!$customer){
            return $this->responseError('Customer not found');
        }
        return new CustomerResource($customer);
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        $customer = $this->repository->delete($key);
        if(!$customer){
            return $this->responseError('Customer not found');
        }
        return $this->respondSuccess();
    }
}
