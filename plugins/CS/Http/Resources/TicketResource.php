<?php

namespace Plugin\CS\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ticketId' => $this->ticketId,
            'phone' => $this->phone,
            'callReference' => $this->callReference,
            'creator' => $this->_creator,
            'resolver' => $this->_resolver,
            'type' => $this->type,
            'title' => $this->title,
            'description' => $this->description,
            'customer' => $this->customer,
            'status' => $this->status
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
