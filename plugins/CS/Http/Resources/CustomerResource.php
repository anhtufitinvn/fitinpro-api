<?php

namespace Plugin\CS\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'name' => $this->name,
            'phone' => $this->phone,
            '2ndphone' => $this->{'2ndphone'},
            'type' => $this->type,
            'email' => $this->email,
            'tickets' => $this->tickets,
            'orders' => $this->orders
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
