<?php

namespace Plugin\CS\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'customers' => new CustomerResourceCollection($this['customers']),
            'tickets' => new TicketResourceCollection($this['tickets']),
            'orders' => new OrderResourceCollection($this['orders']),
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
