<?php
Route::group([
    'prefix' => 'v1',
], function(){

    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/logout', 'AuthController@logout');

});


Route::group([
    'prefix' => 'v1',
    'middleware' => ['auth:cs']
], function () {
    Route::ApiResource('customers', 'CustomerController');
    Route::ApiResource('tickets', 'TicketController');
    Route::ApiResource('orders', 'OrderController');

    Route::get('search', 'ResourceController@search');
});

Route::post('/v1/webhook', 'WebhookController@get')->middleware('private-token');
Route::post('/v1/fitin/tickets', 'TicketController@createTicketFromFitin')->middleware('private-token');  