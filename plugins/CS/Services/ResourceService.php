<?php
namespace Plugin\CS\Services;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;

use Plugin\CS\Models\Customer;
use Plugin\CS\Models\Ticket;
use Plugin\CS\Models\Order;

class ResourceService
{

    const CACHE_TIME = 60 * 24;


    public function search($query){
        $key = $query ?? "noquery";
        $result = Cache::tags('cs-search')->remember($key, self::CACHE_TIME, function () use($query)  {
            $customers = Customer::search($query)->get();
            $customers_tickets = $customers->pluck('tickets')->collapse();
            $customers_orders = $customers->pluck('orders')->collapse();
            $tickets = Ticket::search($query)->get();
            $result_tickets = $customers_tickets->merge($tickets);

            $orders = Order::search($query)->get();
            $result_orders = $customers_orders->merge($orders);

            $ticket_customers = $tickets->pluck('customer')->unique();
            $order_customers = $orders->pluck('customer')->unique();

            if(count($ticket_customers)){
                $customers = $customers->merge($ticket_customers);
            }
            if(count($order_customers)){
                $customers = $customers->merge($order_customers);
            }

            $data = [
                'customers' => $customers,
                'tickets' => $result_tickets,
                'orders' => $result_orders
            ];
            return $data;
        });
        
        return $result;
    }
}
