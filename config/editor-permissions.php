<?php

return [

    'category' => [
        'index' => 'Xem danh sách category',
        'store' => 'Tạo category',
        'show' => 'Xem thông tin category',
        'update'  => 'Chỉnh sửa category',
        'destroy' => 'Xóa category',
        'upload_thumb' => 'Upload ảnh thumb'     
    ],

    'room_category' => [
        'index' => 'Xem danh sách  room category',
        'store' => 'Tạo room category',
        'show' => 'Xem thông tin room category',
        'update'  => 'Chỉnh sửa room category',
        'destroy' => 'Xóa room category',
        'upload_thumb' => 'Upload ảnh thumb'     
    ],
    
    'brand' => [
        'index' => 'Xem danh sách brand',
        'store' => 'Tạo brand',
        'show' => 'Xem thông tin brand',
        'update'  => 'Chỉnh sửa brand',
        'destroy' => 'Xóa brand',
        'upload_thumb' => 'Upload ảnh thumb'       
    ],

    'project' => [
        'index' => 'Xem danh sách project',
        'store' => 'Tạo project',
        'show' => 'Xem thông tin project',
        'update'  => 'Chỉnh sửa project',
        'destroy' => 'Xóa project',
        'upload_thumb' => 'Upload ảnh thumb'      
    ],

    'page' => [
        'index' => 'Xem danh sách page',
        'store' => 'Tạo page',
        'show' => 'Xem thông tin page',
        'update'  => 'Chỉnh sửa page',
        'destroy' => 'Xóa page'     
    ],

    'feedback' => [
        'index' => 'Xem danh sách feedback',
        'store' => 'Tạo feedback',
        'show' => 'Xem thông tin feedback',
        'update'  => 'Chỉnh sửa feedback',
        'destroy' => 'Xóa feedback'     
    ],

    'version' => [
        'upload' => 'Upload bản Build Homestyler',
        'update' => 'Update thông tin bản build',
        'index' => 'Xem danh sách version'
    ],

    'user' => [
        'index' => 'Xem danh sách user',
        'store' => 'Tạo user',
        'show' => 'Xem thông tin user',
        'update'  => 'Chỉnh sửa user',
        'destroy' => 'Xóa user'     
    ],

    'cms_user' => [
        'index' => 'Xem danh sách cms_user',
        'store' => 'Tạo cms_user',
        'show' => 'Xem thông tin cms_user',
        'update'  => 'Chỉnh sửa cms_user',
        'destroy' => 'Xóa cms_user' ,
        'change_password' => 'Đổi password cms_user'    
    ],

    'role' => [
        'index' => 'Xem danh sách role',
        'store' => 'Tạo role',
        'show' => 'Xem thông tin role',
        'update'  => 'Chỉnh sửa role',
        'destroy' => 'Xóa role'     
    ],

    'permission' => [
        'index' => 'Xem danh sách permission',
        'store' => 'Tạo permission',
        'show' => 'Xem thông tin permission',
        'update'  => 'Chỉnh sửa permission',
        'destroy' => 'Xóa permission'     
    ],
    
    'layout' => [
        'index' => 'Xem danh sách layout',
        'store' => 'Tạo layout',
        'show' => 'Xem thông tin layout',
        'update'  => 'Chỉnh sửa layout',
        'destroy' => 'Xóa layout' ,
        'upload_asset_bundle' => 'Upload Asset bundle',
        'upload_thumb' => 'Upload ảnh thumb',
        'sync_data' => 'Sync data từ ecom',
        'approved' => 'Approve thiết kế',
        'rejected' => 'Reject thiết kế'

    ],

    'object' => [
        'index' => 'Xem danh sách object',
        'store' => 'Tạo object',
        'show' => 'Xem thông tin object',
        'update'  => 'Chỉnh sửa object',
        'destroy' => 'Xóa object',
        'upload_asset_bundle' => 'Upload Asset bundle',
        'upload_asset_zip' => 'Upload Asset zip',
        'upload_thumb' => 'Upload ảnh thumb'
    ],

    'material' => [
        'index' => 'Xem danh sách material',
        'store' => 'Tạo material',
        'show' => 'Xem thông tin material',
        'update'  => 'Chỉnh sửa material',
        'destroy' => 'Xóa material',
        'upload_asset_bundle' => 'Upload Asset bundle',
        'upload_thumb' => 'Upload ảnh thumb',
        'upload_excel' => 'Upload Excel để tạo material'
    ],

    'material_category' => [
        'index' => 'Xem danh sách material category',
        'store' => 'Tạo material category',
        'show' => 'Xem thông tin material category',
        'update'  => 'Chỉnh sửa material category',
        'destroy' => 'Xóa material category',
        'upload_thumb' => 'Upload ảnh thumb'  
    ],

    'material_brand' => [
        'index' => 'Xem danh sách material brand',
        'store' => 'Tạo material brand',
        'show' => 'Xem thông tin material brand',
        'update'  => 'Chỉnh sửa material brand',
        'destroy' => 'Xóa material brand',
        'upload_thumb' => 'Upload ảnh thumb' 
    ],

    'color' => [
        'index' => 'Xem danh sách color',
        'store' => 'Tạo color',
        'show' => 'Xem thông tin color',
        'update'  => 'Chỉnh sửa color',
        'destroy' => 'Xóa color'  ,
        'upload_excel' => 'Upload Excel để tạo color'   
    ],

    'color_group' => [
        'index' => 'Xem danh sách color group',
        'store' => 'Tạo color group',
        'show' => 'Xem thông tin color group',
        'update'  => 'Chỉnh sửa color group',
        'destroy' => 'Xóa color group'     
    ],

    'color_brand' => [
        'index' => 'Xem danh sách color brand',
        'store' => 'Tạo color brand',
        'show' => 'Xem thông tin color brand',
        'update'  => 'Chỉnh sửa color brand',
        'destroy' => 'Xóa color brand' ,
        'upload_thumb' => 'Upload ảnh thumb'     
    ],

    'bundle' => [
        'index' => 'Xem danh sách bundle',
        'store' => 'Tạo bundle',
        'show' => 'Xem thông tin bundle',
        'update'  => 'Chỉnh sửa bundle',
        'destroy' => 'Xóa bundle' ,
        'upload_thumb' => 'Upload ảnh thumb', 
        'approved' => 'Approve thiết kế',
        'rejected' => 'Reject thiết kế'     
    ],

    'room' => [
        'index' => 'Xem danh sách room',
        'store' => 'Tạo room',
        'show' => 'Xem thông tin room',
        'update'  => 'Chỉnh sửa room',
        'destroy' => 'Xóa room' ,
        'upload_asset_bundle' => 'Upload Asset bundle',
        'upload_thumb' => 'Upload ảnh thumb', 
        'approved' => 'Approve thiết kế',
        'rejected' => 'Reject thiết kế'     
    ],
];