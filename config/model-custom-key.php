<?php

return [
    // \App\Models\Ver2\Object3d::class,
    // \App\Models\Ver2\Bundle::class,
    // \App\Models\Ver2\Layout::class,
    // \App\Models\Ver2\Brand::class,
    // \App\Models\Ver2\Category::class,
    // \App\Models\Ver2\Color::class,
    // \App\Models\Ver2\ColorGroup::class,
    // \App\Models\Ver2\Vendor::class,
    // \App\Models\Ver2\Material::class,
    // \Plugin\CS\Models\Customer::class,

    \App\EditorPro\Models\Brand::class,
    \App\EditorPro\Models\Layout::class,
    \App\EditorPro\Models\Project::class,
    \App\EditorPro\Models\Object3d::class,
    \App\EditorPro\Models\Room::class,
    \App\EditorPro\Models\Category::class,
    \App\EditorPro\Models\Style::class,
    \App\EditorPro\Models\User::class,
    \App\EditorPro\Models\Material::class,
    \App\EditorPro\Models\Bundle::class,
    \App\EditorPro\Models\Color::class
];
