<?php

return [
    'layouts' => [
        'index' => 'Xem danh sách layout',
        'store' => 'Tạo layout',
        'show' => 'Xem thông tin layout',
        'update'  => 'Chỉnh sửa layout',
        'destroy' => 'Xóa layout',
        //more
        'template' => 'Thông tin templates của layout',
        'copy' => 'Copy layout',
        'sku.index' => 'Xem danh sách sku',
        'sku.remove' => 'Xóa sku',
        'unity' => 'Danh sách unity',
        'version.index' => 'Danh sách version',
        'version.create' => 'Tạo version',
        'version.detail' => 'Chi tiết version'
    ],

    'contact' => [
        'index' => 'Xem danh sách contact',
        'store' => 'Tạo contact',
        'show' => 'Xem thông tin contact',
        'update'  => 'Chỉnh sửa contact',
        'destroy' => 'Xóa contact'
        //more
        
    ],

    'users' => [
        'index' => 'Xem danh sách user',
        'store' => 'Tạo user',
        'show' => 'Xem thông tin user',
        'update'  => 'Chỉnh sửa user',
        'destroy' => 'Xóa user',
        //more
        'layout' => 'Danh sách layout từ user'
    ],

    'permissions' => [
        'index' => 'Xem danh sách permission',
        'store' => 'Tạo permission',
        'show' => 'Xem thông tin permission',
        'update'  => 'Chỉnh sửa permission',
        'destroy' => 'Xóa permission'
    ],

    'roles' => [
        'index' => 'Xem danh sách role',
        'store' => 'Tạo role',
        'show' => 'Xem thông tin role',
        'update'  => 'Chỉnh sửa role',
        'destroy' => 'Xóa role'
    ],

    'categories' => [
        'index' => 'Xem danh sách category',
        'store' => 'Tạo category',
        'show' => 'Xem thông tin category',
        'update'  => 'Chỉnh sửa category',
        'destroy' => 'Xóa category'
    ],

    'objects' => [
        'index' => 'Xem danh sách object',
        'store' => 'Tạo object',
        'show' => 'Xem thông tin object',
        'update'  => 'Chỉnh sửa object',
        'destroy' => 'Xóa object'
    ],

    'styles' => [
        'index' => 'Xem danh sách style',
        'store' => 'Tạo style',
        'show' => 'Xem thông tin style',
        'update'  => 'Chỉnh sửa style',
        'destroy' => 'Xóa style'
    ],

    'bundles' => [
        'index' => 'Xem danh sách bundle',
        'store' => 'Tạo bundle',
        'show' => 'Xem thông tin bundle',
        'update'  => 'Chỉnh sửa bundle',
        'destroy' => 'Xóa bundle',
        //more
        'user'  => 'Danh sách bundle theo user'
    ],

    'sku' => [
        'index' => 'Xem danh sách sku',
        'store' => 'Tạo sku',
        'show' => 'Xem thông tin sku',
        'update'  => 'Chỉnh sửa sku',
        'destroy' => 'Xóa sku',
        //more
        'valid-ar' => 'Validate AR file',
        'exist' => 'Kiểm tra tồn tại sku',
        'product-info' => 'Lấy thông tin product',
        'unique' => 'Kiểm tra tính unique sku',
        'merchant' => 'Danh sách Sku theo merchant'
    ],

    'notification' => [
        'index' => 'Xem danh sách notification',
        'store' => 'Tạo notification',
        'show' => 'Xem thông tin notification',
        'update'  => 'Chỉnh sửa notification',
        'destroy' => 'Xóa notification'
    ],

    'projects' => [
        'index' => 'Xem danh sách project',
        'store' => 'Tạo project',
        'show' => 'Xem thông tin project',
        'update'  => 'Chỉnh sửa project',
        'destroy' => 'Xóa project'
    ],

    'products' => [
        'index' => 'Xem danh sách product',
        'show' => 'Xem thông tin product'
    ],
];