<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'storage' => [
            'driver' => 'local',
            'root' => storage_path('logs'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
        
        'static' => [
            'driver' => 'local',
            'root' => env('STATIC_PATH'),
            'url' => env('STATIC_URL'),
            #'visibility' => 'public',
        ],

        'unity' => [
            'driver' => 'local',
            'root' => env('UNITY_STATIC_PATH'),
            'url' => env('UNITY_STATIC_URL'),
            #'visibility' => 'public',
        ],

        'model3d' => [
            'driver' => 'local',
            'root' => env('MODEL3D_STATIC_PATH'),
            'url' => env('MODEL3D_STATIC_URL'),
            #'visibility' => 'public',
        ],

        'video' => [
            'driver' => 'local',
            'root' => env('VIDEO_STATIC_PATH'),
            'url' => env('VIDEO_STATIC_URL'),
            #'visibility' => 'public',
        ],

        'asset' => [
            'driver' => 'local',
            'root' => env('ASSET_STATIC_PATH'),
            'url' => env('ASSET_STATIC_URL'),
            #'visibility' => 'public',
        ],

        'v360' => [
            'driver' => 'local',
            'root' => env('V360_STATIC_PATH'),
            'url' => env('V360_STATIC_URL'),
            #'visibility' => 'public',
        ],

        'editor' => [
            'driver' => 'local',
            'root' => env('EDITOR_STATIC_PATH'),
            'url' => env('EDITOR_STATIC_URL'),
            #'visibility' => 'public',
        ],

        'event' => [
            'driver' => 'local',
            'root' => env('EVENT_STATIC_PATH', storage_path('app/public')),
            'url' => env('EVENT_STATIC_URL', env('APP_URL').'/storage'),
            #'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
        ],

    ],

];
