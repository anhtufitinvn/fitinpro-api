<?php

return [
    "admin" => [
        "email" => "admin@fitin.vn",
        "password" => env("ID_ADMIN_PASSWORD", "123456")
    ],
    'mail' => [
        'api_endpoint' => env('MAIL_SERVICE_ENDPOINT', 'https://api-mail.fitin.vn'),
        'access_token' => env('MAIL_SERVICE_ACCESS_TOKEN', 'VaX8pHGLBw'),
    ],

    "auth" => [
        "api_endpoint" => env("AUTH_ID_APP_ENDPOINT","https://id-dev.fitin3d.com"),
        "app_id" => env('AUTH_ID_APP_ID','cms'),
        "app_secret" => env('AUTH_ID_APP_SECRET','3e78e28daed60b2df405fe4976fc490084b97744'),
        "editor_app_id" => env('EDITOR_AUTH_ID_APP_ID','fitin-development'),
        "editor_app_secret" => env('EDITOR_AUTH_ID_APP_SECRET','c8741b06be83925606603cccc678164bdb26bcad'),
        "user_active_redirect_url" => "string",
        "user_forgot_password_redirect_url" => "string",
        "user_register_redirect_url" => "string",
    ],
    "cms" => [
        "api_endpoint" => env("CMS_APP_ENDPOINT","https://api-dev.fitin3d.com"),
        "access_token" => env('CMS_ACCESS_TOKEN',"9OAMUJHalcpJcR0CohSNTXDirqA"),
    ],
    "ecom" => [
        "api_endpoint" => env('ECOM_API_ENDPOINT',"https://api.fitin.dev"),
        "api_cms_endpoint" => env('ECOM_API_CMS_ENDPOINT',"https://api2-cms.fitin.dev"),
        "production_api_endpoint" => env('PRODUCTION_ECOM_API_ENDPOINT', env('ECOM_API_ENDPOINT',"https://api.fitin.dev")),
        "domain" => env('ECOM_DOMAIN',"https://dev.fitin.vn"),
        "webhook_endpoint" =>  env('ECOM_WEBHOOK_ENDPOINT'),
        "private_token" => env('FITIN_PRIVATE_TOKEN',"HuTVLp3uJ4NRLV2E12x5K1He"),
        "tuvan_api_endpoint" => env('TUVAN_API_ENDPOINT', "https://tuvan.fitin.dev"),
    ],

    "threejs" => [
        "api_endpoint" => env('THREEJS_OBJECT_API_ENDPOINT',"https://api-3d.fitin.vn")
    ],
    
    "partner_token" =>  env('PARTNER_PRIVATE_TOKEN',"9OAMUJHalcpJcR0CohSNTXDirqA"),
    "fitin_token" => env('FITIN_PRIVATE_TOKEN',"HuTVLp3uJ4NRLV2E12x5K1He"),
    "render_token" => env('RENDER_PRIVATE_TOKEN',"FHkuB3RmRQ7kvuoWIwwr"),
    "editor_token" => "NT8SL3uWc1VBIpseZqv2",
    "default_sku_image" => "https://www.gravatar.com/avatar/00000000000000000000000000000000",
    "default_asset_image" => env('STATIC_DEFAULT_IMAGE', "https://cdn.fitin.vn/images/3d-thumb.png") ,
    "ar" => [
        'AR_FILE_API' => env('AR_FILE_API'),
        'AR_FILE_API_TOKEN' => env('AR_FILE_API_TOKEN'),
    ],
    'CMS_APP_ENDPOINT' => env('CMS_APP_ENDPOINT'),
    "unity" => [
        'filesystem_driver' => env('UNITY_FILESYSTEM_DRIVER', 'unity'),
        'static_path' => env('UNITY_STATIC_PATH', ''),
        'static_url'  => env('UNITY_STATIC_URL', '')
    ],

    "model3d" => [
        'filesystem_driver' => env('MODEL3D_FILESYSTEM_DRIVER', 'model3d'),
        'static_path' => env('MODEL3D_STATIC_PATH', ''),
        'static_url'  => env('MODEL3D_STATIC_URL', '')
    ],

    "asset" => [
        'filesystem_driver' => env('ASSET_FILESYSTEM_DRIVER', 'asset'),
        'static_path' => env('ASSET_STATIC_PATH', ''),
        'static_url'  => env('ASSET_STATIC_URL', ''),
        'default_fbx' => "default/cube.fbx",
        'default_thumb' => "default/cube-default.jpg",
    ],

    "v360" => [
        'filesystem_driver' => env('V360_FILESYSTEM_DRIVER', 'v360'),
        'static_path' => env('V360_STATIC_PATH', ''),
        'static_url'  => env('V360_STATIC_URL', '')
    ],

    "video" => [
        'filesystem_driver' => env('VIDEO_FILESYSTEM_DRIVER', 'video'),
        'static_path' => env('VIDEO_STATIC_PATH', ''),
        'static_url'  => env('VIDEO_STATIC_URL', '')
    ],

    "editor" => [
        'filesystem_driver' => env('EDITOR_FILESYSTEM_DRIVER', 'editor'),
        'static_path' => env('EDITOR_STATIC_PATH', ''),
        'static_url'  => env('EDITOR_STATIC_URL', ''),
        'private_token' => 'NT8SL3uWc1VBIpseZqv2'
    ],

    'enabled_sync_to_external' => env('ENABLED_SYNC_TO_EXTERNAL', false),
    'enabled_sync_from_external' => env('ENABLED_SYNC_FROM_EXTERNAL', false),
    'enable_gzip' => env('GZIP_ENABLED', false),

    'enabled_encrypt_file' => env('ENCRYPT_ENABLED', false),

    'static_user_agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36',

    'render_environment' => [
       1 => 'dev' , 
       2 => 'stagging', 
       3 => 'production', 
       4 => 'cloud'
    ],

    'errors' => [
        'auth' => [
            'inactive' => 10,
            'invalid' => 11
        ]
    ]
];
