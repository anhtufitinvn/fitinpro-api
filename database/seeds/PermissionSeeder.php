<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
        DB::table('role_has_permissions')->truncate();
        DB::table('model_has_roles')->truncate();
        DB::table('model_has_permissions')->truncate();
        DB::table('roles')->truncate();
        DB::table('permissions')->truncate();
        
        $arrAbilities = [
            [
                'name' => 'layouts',
                'title' => 'Management layouts'
            ],
            [
                'name' => 'objects',
                'title' => 'Management 3d-model objects'
            ],
            [
                'name' => 'bundles',
                'title' => 'Management bundles'
            ],
            [
                'name' => 'projects',
                'title' => 'Management projects'
            ],
            [
                'name' => 'products',
                'title' => 'Management products'
            ],
            [
                'name' => 'styles',
                'title' => 'Management styles'
            ],
            [
                'name' => 'categories',
                'title' => 'Management categories'
            ],
            [
                'name' => 'users',
                'title' => 'Management users'
            ],
            [
                'name' => 'requests',
                'title' => 'Management requests'
            ],
            [
                'name' => 'user-permissions',
                'title' => 'Management user permissions'
            ],
            [
                'name' => 'user-roles',
                'title' => 'Management user roles'
            ],
            [
                'name' => 'contacts',
                'title' => 'Management contacts'
            ],
            [
                'name' => 'sku',
                'title' => 'Management sku'
            ],
            [
                'name' => 'notifications',
                'title' => 'Management notifications'
            ],
        ];
        

        foreach ($arrAbilities as $item) {
            Permission::create([
                'name' => $item['name'],
                'title' => $item['title'] ?? NULL,
                'guard_name' => 'api'
            ]);

        }
        
        
        $arrRoles = [
            [
                'name' => 'super_admin',
                'title' => 'Super Admin',
                'permissions' => [
                    'user-permissions',
                    'user-roles',
                    'users'
                ]
            ],
            [
                'name' => 'admin',
                'title' => 'Admin'
            ],
            [
                'name' => 'manager',
                'title' => 'Manager'
            ],
            [
                'name' => 'editor',
                'title' => 'Editor'
            ],
            [
                'name' => 'merchant',
                'title' => 'Merchant'
            ]
        ];

        foreach ($arrRoles as $item) {
            $role = Role::create([
                'name' => $item['name'],
                'title' => $item['title'] ?? NULL,
                'guard_name' => 'api'
            ]);

            if(!empty($item['permissions'])){
                $role->givePermissionTo($item['permissions']);
            }
        }
        
        $user = \App\Models\User::where('email','loi.tran@fitin.vn')->first();
        if($user){
            $user->syncRoles('super_admin');
        }
        
        $user = \App\Models\User::where('email','admin@fitin.vn')->first();
        if($user){
            $user->syncRoles('super_admin');
        }

        $user = \App\Models\User::where('email','dang.nguyen@fitin.vn')->first();
        if($user){
            $user->syncRoles('manager');
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
