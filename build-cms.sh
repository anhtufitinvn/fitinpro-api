#!/bin/bash

echo 'cd to WorkPlace'
cd client/
echo 'Done cd to WorkPlace'

echo 'Build FE'
yarn build:dev
echo 'Done Build FE'

echo 'cd to Build'
cd build-dev
echo 'Done cd to Unity'

echo 'zip file'
# now=$(date +"%S-%M-%H-%m-%d-%y")
tar cvf build.tar ./
echo 'zip file'

echo 'cp to Server'
scp -i ~/.ssh/fitin_dev build.tar root@45.124.94.203:/var/www/cms.fitin3d.com
echo 'cp to Server'

echo 'unzip on Server'
ssh -i ~/.ssh/fitin_dev -t 'root@45.124.94.203' 'cd "/var/www/cms.fitin3d.com"; tar xf build.tar ./; chmod 755 -R ./*; chown root.root -R ./*'


# content of your script
