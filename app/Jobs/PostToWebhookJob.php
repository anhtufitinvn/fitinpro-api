<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

class PostToWebhookJob implements ShouldQueue
{
   use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

   /**
    * Create a new job instance.
    *
    * @return void
    */
   public $tries = 5;

   protected $action, $data, $webhook_url;

   public function __construct($action, $data)
   {
      //
      $this->action = $action;
      $this->data = $data;
      $this->webhook_url = config('fitin.ecom.webhook_endpoint');
   }

   /**
    * Execute the job.
    *
    * @return void
    */
   public function handle()
   {
      $action = $this->action;
      $data = $this->data;
      $webhook_url = $this->webhook_url;

      if (!$webhook_url) {
         return false;
      }
      $type = explode('.', $action)[0];
      $method = explode('.', $action)[1];
      $postData = [
         'type' => $type,
         'action' => $method,
         'data' => $data
      ];

      try {
         $arrData = [
             'headers' => [
                 'Access-Key' => config('fitin.partner_token'),
                 'User-Agent' => config('fitin.static_user_agent')
             ],
             'json' => $postData 
         ];
         $client = new Client();
         $response = $client->request('POST',  $webhook_url , $arrData);
         if ($response->getStatusCode() == 200) {
             $jsonResult = $response->getBody()->getContents();
             $result = json_decode($jsonResult, 1);
             
             Log::channel('webhook')->debug($result);
         }
         Log::channel('webhook')->debug($postData);
     }catch(\Exception $e){
         Log::info($e->getMessage());
     }
   }
}
