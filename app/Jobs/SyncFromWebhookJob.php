<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Storage;
use App\Models\Ver2\Category;
use App\Models\Ver2\Brand;
use App\Models\Ver2\Room;
use App\Models\Ver2\Project;
use App\Models\Ver2\Style;
use App\Models\Ver2\Layout;

use App\Models\Ver2\Attachment;

use Illuminate\Support\Facades\Log;
class SyncFromWebhookJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $tries = 5;

    protected $data;
    
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        return null;
        // $data = $this->data;
        
        // $type = $data['type'];
        // $action = $data['action'];

        // if($type == 'category'){
        //     $this->syncCategory($action, $data['data']);
        // }

        // if($type == 'product_brand'){
        //     $this->syncBrand($action, $data['data']);
        // }


        // if($type == 'layout_style'){
        //     $this->syncLayoutStyle($action, $data['data']);
        // }

        // if($type == 'layout_project'){
        //     $this->syncLayoutProject($action, $data['data']);
        // }

        // if($type == 'layout'){
        // //    $this->syncLayout($action, $data['data']);
        // }

        // return 1;
    }

    /********************************************************************************
     * CATEGORY 
     ********************************************************************************/
    protected function syncCategory($action, $data){
        $instance = Category::where('ecom_id', $data['id'])->first();

        if($action == 'delete'){
            if($instance){
                $instance->delete();
                return true;
            }
            return true;
        }else{
            $data['ecom_id'] = $data['id'];         

            if($instance){        
                $instance->update($data);    
            }else {
                $instance = Category::create($data);
            }

            if(isset($data['image'])){
                $image = $this->syncImage($instance, $data['image']);                              
            }  

            if($image){
                $instance->attachment()->save($image);
            }
            
            return true;
        }

    }


    /********************************************************************************
     * BRAND 
     ********************************************************************************/
    protected function syncBrand($action, $data){
        $instance = Brand::where('ecom_id', $data['id'])->first();

        if($action == 'delete'){
            if($instance){
                $instance->delete();
                return true;
            }
            return true;
        }else{
            $data['ecom_id'] = $data['id'];
            
            if($instance){        
                $instance->update($data);    
            }else {
                $instance = Brand::create($data);
            }

            if(isset($data['image'])){
                $image = $this->syncImage($instance, $data['image']);                              
            }  

            if($image){
                $instance->attachment()->save($image);
            }
            
            return true;
        }

    }


    
    /********************************************************************************
     * ROOM 
     ********************************************************************************/
    protected function syncRoom($action, $data){
        $instance = Room::where('ecom_id', $data['id'])->first();

        if($action == 'delete'){
            if($instance){
                $instance->delete();
                return true;
            }
            return true;
        }else{
            $data['ecom_id'] = $data['id'];          

            if($instance){        
                $instance->update($data);    
            }else {
                $instance = Room::create($data);
            }

            if(isset($data['image'])){
                $image = $this->syncImage($instance, $data['image']);                              
            }  

            if($image){
                $instance->attachment()->save($image);
            }
            
            return true;
        }

    }



    /********************************************************************************
     * PROJECT 
     ********************************************************************************/
    protected function syncLayoutProject($action, $data){
        $instance = Project::where('ecom_id', $data['id'])->first();

        if($action == 'delete'){
            if($instance){
                $instance->delete();
                return true;
            }
            return true;
        }else{
            $data['ecom_id'] = $data['id'];          

            if($instance){        
                $instance->update($data);    
            }else {
                $instance = Project::create($data);
            }

            if(isset($data['image'])){
                $image = $this->syncImage($instance, $data['image']);                              
            }  

            if($image){
                $instance->attachment()->save($image);
            }
            
            return true;
        }

    }



    /********************************************************************************
     * STYLE 
     ********************************************************************************/
    protected function syncLayoutStyle($action, $data){
        $instance = Style::where('ecom_id', $data['id'])->first();

        if($action == 'delete'){
            if($instance){
                $instance->delete();
                return true;
            }
            return true;
        }else{
            $data['ecom_id'] = $data['id'];          

            if($instance){        
                $instance->update($data);    
            }else {
                $instance = Style::create($data);
            }

            if(isset($data['image'])){
                $image = $this->syncImage($instance, $data['image']);                              
            }  

            if($image){
                $instance->attachment()->save($image);
            }
            
            return true;
        }

    }



    /********************************************************************************
     * Layout 
     ********************************************************************************/
    protected function syncLayout($action, $data){
        $instance = Layout::where('ecom_id', $data['id'])->first();

        if($action == 'delete'){
            if($instance){
                $instance->delete();
                return true;
            }
            return true;
        }else{
            $data['ecom_id'] = $data['id'];          

            $data = $this->mappingDataWith3DId($data);
            if($instance){        
                $instance->update($data);    
            }else {
                $instance = Layout::create($data);
            }

            if(isset($data['image'])){
                $image = $this->syncImage($instance, $data['image']);                              
            }  

            if($image){          
                $instance->attachment()->save($image);
            }
            
            return true;
        }

    }


    /**
     * 
     */

    protected function syncImage($instance, $image_url){
        $attachment = Attachment::where('reference', $image_url)->first();
        if($attachment){
            return $attachment;
        }
        
        if ($image_url){
            $class_name = strtolower(class_basename($instance));
            $date = date('y/m/d');
            $pathinfo = pathinfo($image_url);
            $basename = $pathinfo['basename'];
            $base_path = "images/$class_name/$date";
            $path = "images/$class_name/$date/$basename";
            try {
                $contents = file_get_contents($image_url);
                $upload = Storage::put($path, $contents);
            
                $att = [
                    'reference' =>  $image_url,
                    'path' => $base_path,
                    'size' => Storage::size($path),
                    'mime_type' => $pathinfo['extension'], 
                    'name' => $basename,
                    'origin_name' => $basename,
                    'type' => 'image'
                ];
                
                if($upload){
                    $attachment = Attachment::create($att);
                    return $attachment;
                }else{
                    Log::info('Storage failed');
                    return false;
                }
                
            }catch (\Exception $e){
                Log::info($e->getMessage());
                return false;
            }   
            
        }

    }


    protected function mappingDataWith3DId($data){
        if(!empty($data['parent'])){
            $parent_layout = Layout::where('ecom_id', $data['parent'])->first();
            if($parent_layout){
                $data['parent_id'] = $parent_layout->_id;
            }
        }

        if(!empty($data['project_id'])){
            $project = Project::where('ecom_id', $data['project_id'])->first();
            if($project){
                $data['project_id'] = $project->_id;
            }
        }

        if(!empty($data['style_id'])){
            $style = Style::where('ecom_id', $data['style_id'])->first();
            if($style){
                $data['style_id'] = $style->_id;
            }
        }

        return $data;
    }
}
