<?php

namespace App\Exceptions;

use Exception;

/**
 * Class GeneralException.
 */
class ValidFieldException extends Exception
{
    /**
     * message.
     *
     * @var string
     */
    public $message;

    /**
     * dontHide.
     *
     * @var bool
     */
    public $dontHide;


    public $field;

    /**
     * Constructor function.
     *
     * @param string $message
     * @param bool   $dontHide
     */
    public function __construct($message, $field = '')
    {
        parent::__construct();

        $this->message = $message;

        $this->field = $field;
    }

    public function getField(){
        return $this->field;
    }
}
