<?php

namespace App\Exceptions;

use Exception;

/**
 * Class GeneralException.
 */
class GeneralException extends Exception
{
    /**
     * message.
     *
     * @var string
     */
    public $message;

    /**
     * dontHide.
     *
     * @var bool
     */
    public $dontHide;

    /**
     * Constructor function.
     *
     * @param string $message
     * @param bool   $dontHide
     */
    public function __construct($message, $code = 0)
    {
        parent::__construct();

        $this->message = $message;
        $this->code = $code;



    }
}
