<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

use \Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Log;

use GuzzleHttp\Exception\ClientException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        GeneralException::class,
    ];
    
    protected $disableReport = [
        'AuthenticationException',
        'AuthorizationException',
        'HttpException',
        'TokenMismatchException',
        'ValidationException',
        'GeneralException',
        'PermissionException',
        'NotFoundHttpException',
        'UnauthorizedHttpException',
        'ValidFieldException'
    ];
    /**
     * Report or log an exception.
     *
     * @param Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        if (config('app.env') != 'local' && !in_array(class_basename($exception), $this->disableReport)) {
            $arrError = [
                'api' => config('app.url'),
                'error' => class_basename($exception) . ' | ' .$exception->getMessage(),
                'stack' => mb_substr($exception->getTraceAsString(), 0, 150)  
            ];
            $channel = env('LOG_CHANNEL', 'telegram');
            Log::channel($channel)->critical($arrError);
        }
        
    //    parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     * @return Response
     */
    public function render($request, Exception $exception)
    {
        $isDebug = config('app.debug', false);

        if ($exception instanceof ValidationException) {
            $keys = $exception->validator->errors()->keys();
            return response()->json([
                'code' => -1,        
                'message' =>  $exception->validator->errors()->first(),
                'data' => [
                    'error_code' => 'VALIDATION_ERROR'
                ],
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        } else if ($exception instanceof NotFoundHttpException) {
            return response()->json([ 
                'code' => -1,            
                'message' => 'API not found!',
                'data' => [],
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
                
            ]);
        } else if ($exception instanceof GeneralException) {
            return response()->json([
                'code' => -1,        
                'message' => $exception->getMessage(),
                'data' => [
                    'error_code' => $exception->getCode()
                ],
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        } else if ($exception instanceof ValidFieldException) {
            return response()->json([
                'code' => -1, 
                'message' => $exception->getMessage(),
                'data' => [
                    'error_code' => 'VALIDATION_ERROR',
                ],
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        }
        else if ($exception instanceof UnauthorizedHttpException) {

            if ($exception->getPrevious() instanceof TokenExpiredException) {

                return response()->json([
                    'code' => -401,
                    'data' => [
                        'error_code' => 'TOKEN_EXPIRED',
                    ],
                    'message' => __('Phiên đăng nhập đã hết hạn. Vui lòng đăng nhập lại!'),    
                    "errors" => [
                        [
                            "error" => 1,
                            "message" => "",
                            "stack" => []
                        ]
                    ]           
                ]);
            } else if ($exception->getPrevious() instanceof TokenInvalidException) {

                return response()->json([
                    'code' => -401,
                    'message' => __('Mã token không hợp lệ. Vui lòng đăng nhập lại!'),
                    'data' => [
                        'error_code' => 'TOKEN_INVALID',
                    ]            ,
                    "errors" => [
                        [
                            "error" => 1,
                            "message" => "",
                            "stack" => []
                        ]
                    ]
                ]);

            } else {
                return response()->json([      
                    'code' => -401,
                    'message' => __('Vui lòng đăng nhập để truy cập tài nguyên!'),
                    'data' => [
                        'error_code' => 'UNAUTHORIZED_REQUEST',
                    ] ,
                    "errors" => [
                        [
                            "error" => 1,
                            "message" => "",
                            "stack" => []
                        ]
                    ]
                ]);
            }

        }
        else if ($exception instanceof TokenExpiredException) {

            return response()->json([
                'code' => -401,
                'message' => __('Phiên đăng nhập đã hết hạn. Vui lòng đăng nhập lại!'),
                'data'  => [
                    'error_code' => 'TOKEN_EXPIRED'
                ],
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
                
            ]);
        }
        else if ($exception instanceof TokenInvalidException) {
            return response()->json([
                'code' => -401,
                'message' => __('Mã token không hợp lệ. Vui lòng đăng nhập lại!'),       
                'data' => [
                    'error_code' => 'TOKEN_INVALID'
                ], 
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);

        } else if ($exception instanceof TokenBlacklistedException) {

            return response()->json([
                'message' => __('Mã token hết hiệu lực. Vui lòng đăng nhập lại!'),
                'code' => -401,
                'data' => [
                    'error_code' => 'TOKEN_BLACKLISTED'
                ], 
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);

        } else if ($exception instanceof ThrottleRequestsException) {
            return response()->json([
                'message' => __('Máy chủ đang bận. Vui lòng thử lại sau!'),
                'code' => -1,
                'data' => [
                    'error_code' => 'BAN_IP'
                ],
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);

        } else if ($exception instanceof AuthenticationException) {
            return response()->json([
                'message' => 'Người dùng không xác thực. Vui lòng đăng nhập lại để truy cập',
                'code' => -401,
                'data' => [
                    'error_code' => class_basename($exception),
                ],
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);

        }else if ($exception instanceof ClientException) {
            return response()->json([
                'code' => -1,
                'message' => $exception->getMessage(),   
                "error_code" => class_basename($exception),
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);

        }else if ($request->isJson()) {
            $json = [                
                'code' => -1,
                'message' => $exception->getMessage(),   
                
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        'stack' => $isDebug ? $exception->getTraceAsString() : '',
                    ]
                ]
            ];

            return response()->json($json);
        }

        $json = [
            'code' => -1,
            'message' => $exception->getMessage(),            
            'data' => [
                'error_code' => class_basename($exception),
                
            ],
            "errors" => [
                [
                    "error" => 1,
                    "message" => "",
                    'stack' => $isDebug ? $exception->getTraceAsString() : '',
                ]
            ]
        ];

        return response()->json($json);
        //return parent::render($request, $exception);
    }
}
