<?php

namespace App\Rules;

use App\FitIn\AuthID;
use Illuminate\Contracts\Validation\Rule;

class PhoneExist implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $authID = new AuthID();
        return $authID->hasUniquePhone($value) === false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Số điện thoại đã tồn tại.';
    }
}
