<?php

namespace App\Rules;

use App\FitIn\AuthID;
use Illuminate\Contracts\Validation\Rule;

class PhoneNotExist implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $authID = new AuthID();
        return $authID->hasUniqueEmail($value) === true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Số điện thoại không tồn tại trong hệ thống');
    }
}
