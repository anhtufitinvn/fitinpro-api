<?php

function concat($separate, ...$str){
    $arr = [];
    for ($i = 0; $i < count($str); $i++){
        $tmp = $str[$i];
        if($tmp === NULL){
            $tmp = "null";
        }
        if($tmp === false){
            $tmp = "false";
        }
        if($tmp === ''){
            $tmp = "_";
        }

        $arr[] = $tmp;
    }
    return implode($separate, $arr);
}

function uploadDriver($data, $file, $driver, $path, $url , $instance = NULL){
    $event = event(new \App\EditorPro\Events\UploadEvent($data, $file, $driver, $path, $url, $instance));
    return $event[0];
}

function file_valid($url){
    $header_response = get_headers($url);
    if ($header_response) {
        if ( strpos( $header_response[0], "404" ) !== false ){
          return false;
        }else{
          return true;
        }
    }else {
        return false;
    }
}

function slug($str, $separate = '-'){
    return \Illuminate\Support\Str::slug($str, $separate);
}

function space_replace($str, $separate = '-'){
    return str_replace(' ', $separate, $str);
}

function saveUserDownload($file, $driver, $path, $url , $instance = NULL){
    $user = \Illuminate\Support\Facades\Auth::user();
    $ip = request()->ip();
    event(new \App\Events\DownloadEvent($user, $ip, $file, $driver, $path, $url, $instance));
    return true;
}

function saveFileHistory($data, $driver = NULL, $obj = NULL){
    $data['owner'] = \Illuminate\Support\Facades\Auth::user()->email;
    //$file = \App\Models\Ver2\File::create($data);

    if($driver){
        uploadDriver($data, $data['name'] ?? NULL, $driver, $data['path'], \Illuminate\Support\Facades\Storage::disk($driver)->url($data['path']),$obj );
    }
    return true;
}

function parseLayoutFromUnity($json){
    $data = [
        'layout_id' => $json['RoomId'] ?? null,
        'tts'=> []
    ];

    foreach ($json['TTS'] as $t){
        if(is_array($t['ListItems']) && count ($t['ListItems']) ){
            $item = $t['ListItems'][0];
            $new_data = [
                'name' => $t['Name'],
                'item_id' => gen_uuid(),
                'fbxUrl' => null,
                'position' => parseUnityPosition($item['Position']),
                'scale' => [
                    "x" => $item['Scale']['x'],
                    "y" => $item['Scale']['y'],
                    "z" => $item['Scale']['z']
                ],
                'rotation' => [
                    "x" => $item['Rotate']['x'],
                    "y" => $item['Rotate']['y'],
                    "z" => $item['Rotate']['z']
                ],
            ];
            $data['tts'][] = $new_data;
        }
    }
    
    return $data;
}

function parseUnityPosition ($p){
    $data = [
        "x" => -100 * $p['x'],
        "y" => 100 * $p['y'],
        "z" => 100 * $p['z']
    ];
    return $data;
}

function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

function editorApiResource($path, $key ,$controller){
    //Index
    \Illuminate\Support\Facades\Route::get($path , [
        'middleware' => "editor_role_or_permission:admin|$key.index",
        'as' => "$key.index",
        'uses' => "$controller@index"
    ]);
    //Store    
    \Illuminate\Support\Facades\Route::post($path , [
        'middleware' => "editor_role_or_permission:admin|$key.store",
        'as' => "$key.store",
        'uses' => "$controller@store"
    ]);
    //Show    
    \Illuminate\Support\Facades\Route::get("$path/{key}" , [
        'middleware' => "editor_role_or_permission:admin|$key.show",
        'as' => "$key.show",
        'uses' => "$controller@show"
    ]);
    //Update    
    \Illuminate\Support\Facades\Route::put("$path/{key}" , [
        'middleware' => "editor_role_or_permission:admin|$key.update",
        'as' => "$key.update",
        'uses' => "$controller@update"
    ]);
    //Destroy    
    \Illuminate\Support\Facades\Route::delete("$path/{key}", [
        'middleware' => "editor_role_or_permission:admin|$key.destroy",
        'as' => "$key.destroy",
        'uses' => "$controller@destroy"
    ]);
    
}

//Role List
function getRoles($params = []){
    $roles = \Illuminate\Support\Facades\Cache::rememberForever(\App\Models\Ver2\Role::CACHE_KEY, function () {
        return \App\Models\Ver2\Role::all();
    });

    foreach ($params as $attr => $value) {
        $roles = $roles->where($attr, $value);
    }

    return $roles;
}

function forgetCacheRoles(){
    \Illuminate\Support\Facades\Cache::forget(\App\Models\Ver2\Role::CACHE_KEY);
}

function forgetCacheUserRoles(){
    \Illuminate\Support\Facades\Cache::tags(\App\Models\Ver2\Role::CACHE_TAG)->flush();
}

function forgetCacheEditorRoles(){
    \Illuminate\Support\Facades\Cache::forget(\App\EditorPro\Models\Role::CACHE_KEY);
}

function forgetCacheEditorUserRoles(){
    \Illuminate\Support\Facades\Cache::tags(\App\EditorPro\Models\Role::CACHE_TAG)->flush();
}

function clearCache($array_class){
    foreach ($array_class as $class){
        \Illuminate\Support\Facades\Cache::tags($class::CACHE_TAG)->flush();
    }
}

function forgetCacheEditorUsersRolesPermissions(){
    \Illuminate\Support\Facades\Cache::tags(\App\EditorPro\Models\Role::CACHE_TAG)->flush();
    \Illuminate\Support\Facades\Cache::tags(\App\EditorPro\Models\Permission::CACHE_TAG)->flush();
    \Illuminate\Support\Facades\Cache::tags(\App\EditorPro\Models\CMSUser::CACHE_TAG)->flush();
}

function isJSON($string){
    return is_string($string) && is_array(json_decode($string, true)) ? true : false;
 }

function checkEditorOwner($instance){
    if(\Illuminate\Support\Facades\Auth::guard('editor')->check()){
        $user = \Illuminate\Support\Facades\Auth::user();
        if($user->auth_id !== $instance->getOwnerId()){
            throw new \Exception('You are not owner of '.get_class($instance));
        }
    }
}