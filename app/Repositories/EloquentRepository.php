<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;

abstract class EloquentRepository implements RepositoryInterface
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;
    
    /**
     * EloquentRepository constructor.
     */
    public function __construct()
    {
        $this->setModel();
    }

    /**
     * get model
     * @return string
     */
    abstract public function model();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->model = app()->make(
            $this->model()
        );
    }

    /**
     * Get All
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll($params)
    {
        return $this->model->all();
    }

    /**
     * Get All By User Id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
     public function getByUser($id)
     {
         return $this->model->where('user_id', $id)->orderBy('id', 'DESC')->get();
     }

     
    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $result = $this->model->find($id);
        return $result;
    }
    
    /**
     * Create
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    /**
     * Update
     * @param $id
     * @param array $attributes
     * @return bool|mixed
     */
    public function update($key, array $attributes)
    {
        $result = $this->find($key);
        if($result) {
            $result->update($attributes);
            return $result;
        }
        return false;
    }

    /**
     * Delete
     * 
     * @param $id
     * @return bool
     */
    public function delete($key)
    {
        $result = $this->find($key);
        if($result) {
            $result->delete();
            return true;
        }

        return false;
    }

    /**
     * paginate
     * 
     * @param $perPage, $column
     * @return mixed
     */
    public function paginate($perPage = 15, $columns = array('*'), $method = "paginate") {
        /* $this->newQuery()->eagerLoadRelations();
        $this->applyScope(); */
        return $this->model->orderBy('id', 'DESC')->{$method}($perPage, $columns);
    }
} 