<?php
namespace App\Repositories\Project;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\Project;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;

class ProjectRepository extends EloquentRepository implements ProjectRepositoryInterface {
    
    const CACHE_TIME = 60 * 5;

    protected $upload_service;

    public function __construct(UploadService $upload_service)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
    }

	public function model()
	{
		return Project::class;
	}
    
    public function getAll($params = [])
    {
        $key = "index";
        $collections = Cache::tags(Project::CACHE_TAG)->remember($key, self::CACHE_TIME, function () {
            return $this->model->all();
        });
        return $collections;
    }

    
	public function create($input)
	{
        $input['status'] = Project::PUBLISHED;
		$project = $this->model->create($input);
        
        if(!empty($input['image64'])){
            $image_data = $this->upload_service->uploadThumbGroup($project, $input['image64'], [$project->code]);
            
            if(!empty($image_data['file_path'])){
                $project->image = $image_data['file_path'];
                $project->save();
            }    
        }
        
        return $project;
    }
    
    public function find($key){
        $cache_key = "show:$key";
        $project = Cache::tags(Project::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('code', $key)->first();
        });
        
        return $project;
    }


    public function update($key, $input){
        $project = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$project){
            return false;
        }
        $project->update($input);

        if(request()->get('image64')){
            $image_data = $this->upload_service->uploadThumbGroup($project, $input['image64'], [$project->code]);
            
            if(!empty($image_data['file_path'])){
                $project->image = $image_data['file_path'];
                $project->save();
            }    
        }
        return $project;
    }

    public function delete($key){
        $project = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$project){
            return false;
        }

        $project->delete();
        return $project;
    }
}