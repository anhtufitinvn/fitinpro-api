<?php
namespace App\Repositories\Material;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\Material;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;

class MaterialRepository extends EloquentRepository implements MaterialRepositoryInterface {
    
    const CACHE_TIME = 60 * 5;

    protected $upload_service;

    public function __construct(UploadService $upload_service)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
    }

	public function model()
	{
		return Material::class;
	}
    
    public function getAll($params = [])
    {
        $key = "index";

        $limit =  (int) request()->get('limit', 40);
        $page =  (int) request()->get('page');    
        $name = strtolower(request()->get('name'));
        $type = strtolower(request()->get('type'));
        $concat = concat(":", $name, $type, $limit, $page);
        $key = $key . $concat;
        $collections = Cache::tags(Material::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use ($name, $type, $limit, $page) {
            $query = $this->model;
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }

            if($type){
                $query = $query->where('attrs.type',  $type);
            }

            return $query->latest()->paginate($limit);
        });
        return $collections;
    }
    
	public function create($input)
	{
		$material = $this->model->create($input);
        
        return $material;
    }
    
    public function find($key){
        $cache_key = "show:$key";
        $material = Cache::tags(Material::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('code', $key)->first();
        });
        
        return $material;   
    }


    public function update($key, $input){
        $material = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$material){
            return false;
        }
        if($material->code && isset($input['code'])){
            unset($input['code']);
        }
        $material->update($input);
        
        return $material;
    }

    public function delete($key){
        $material = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$material){
            return false;
        }
        $material->delete();
        return $material;
    }
}