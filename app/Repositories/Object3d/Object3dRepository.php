<?php
namespace App\Repositories\Object3d;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\Object3d;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;
use App\FitIn\Asset;

class Object3dRepository extends EloquentRepository implements Object3dRepositoryInterface {
    
    const CACHE_TIME = 60 * 60 * 24;

    protected $upload_service, $asset;

    public function __construct(UploadService $upload_service, Asset $asset)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
        $this->asset = $asset;
    }

	public function model()
	{
		return Object3d::class;
	}
    
    public function getAll($params = [])
    {
        $type = $params['type'] ?? null;
        $vendor = request()->get('vendor');
        $brand = request()->get('brand');
        $category = request()->get('category');
        $limit =  (int) request()->get('limit', 40);
        $page =  (int) request()->get('page');    
        $name = strtolower(request()->get('name'));
        $start = strtolower(request()->get('start'));
        $layout = request()->get('layout');
        $gltf = request()->get('gltf');
        $published = $params['published'] ?? 0;
        $modeltype = $params['model-type'] ?? '3d';
        $concat = concat(":", $vendor, $brand, $layout, $start, $name, $category, $gltf, $limit, $page, $published, $modeltype);

        if($type == 'item'){
            $key = "indexItem:";
        }elseif($type == 'tts'){
            $key = "indexTTS:";
        }else{
            $key = "index:";
        }
        $key = $key . $concat;
        
        $collections = Cache::tags(Object3d::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($vendor, $name, $brand, $layout, $start, $category, $gltf, $limit, $page, $type, $published, $modeltype) {
            $query = $this->model;
            if($brand){
                $query = $query->where('brand', $brand);
            }
            if($category){
                $query = $query->where('category', $category);
            }
            if($vendor){
                $query = $query->where('vendor', $vendor);
            }
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }
            if($start){
                if($type == 'tts'){
                    $query = $query->where('name', 'regexp', "/^[a-z0-9]+_[a-z0-9]+_$start./");
                }
                else {
                    $query = $query->where('name', 'regexp', "/^[a-z0-9]+_$start./");
                }
            }

            if($layout){
                if(strtolower($layout) == 'isnull'){
                    $query = $query->whereNull('layout');
                }else{
                    $query = $query->where('layout', $layout);
                }
            }

            if($type == 'item'){
               $query = $query->withoutTTS();
            }
            elseif($type == 'tts'){
                $query = $query->withTTS();
            }
            if($gltf !== null){
                if($gltf){
                    $query = $query->whereNotNull('gltf');
                }
                else{
                    $query = $query->whereNull('gltf');
                }
            }

            if($published){
                $query = $query->published();
            }

            if($modeltype){
                $query = $query->where('attributes.type', $modeltype);
            }

            return $query->orderBy('created_at', "DESC")->paginate($limit);
        });
        return $collections;
    }

    public function getAllForPlugin($params = []){
       
        $vendor = request()->get('vendor');
        $brand = request()->get('brand');
        $category = request()->get('category');
        $limit =  (int) request()->get('limit', 40);
        $page =  (int) request()->get('page');    
        $name = strtolower(request()->get('name'));
        $start = strtolower(request()->get('start'));
        $layout = request()->get('layout');
        $gltf = request()->get('gltf');
        $published = $params['published'] ?? 0;
        $concat = concat(":", $vendor, $brand, $layout, $start, $name, $category, $gltf, $limit, $page, $published);

        $key = "index:";
        $key = $key . $concat;
        
        $collections = Cache::tags(Object3d::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($vendor, $name, $brand, $layout, $start, $category, $gltf, $limit, $page, $published) {
            
            
            // $array = [
            //     "jysk_kitchen_cabinet_kalby",
            //     "jysk_dinner_table_2m_gammelgab",
            //     "library_light_livingroom_b",
            //     "jysk_chair_kastrup",
            //     "jysk_chair_kastrup_black",
            //     "jysk_dinner_table_stege",
            //     "library_light_b",
            //     "jysk_cabinet_gadeskov",
            //     "jysk_dinner_table_2m_gammelgab",
            //     "library_light_livingroom_b",
            //     "jysk_chair_jonstrup",
            //     "alpha2_carpet_a",
            //     "jysk_table_lamp_herluf",
            //     "jysk_coffee_table_lejre",
            //     "jysk_tvcabinet_aarup",
            //     "jysk_coffee_table_bakkebjerg",
            //     "jysk_chair_silas",
            //     "sunwah_tree_a",
            //     "jysk_sofa_florence",
            //     "jysk_table_caffe_stege",
            //     "jysk_tvcabinet_kalby",
                
            //     "sunwah_tree_b",
            //     "jysk_stool_gedved",
            //     "jysk_sofa_3sgedved",
            //     "jysk_armchair_gedved",
            //     "jysk_shelf_stavanger",
            //     "jysk_shelf_stavanger",
            //     "library_picture_b",
            //     "jysk_wardrobe_favrbo",
            //     "jysk_desk_abbetved",
            //     "jysk_chair_dima",
            //     "jysk_bed_favrbo",
            //     "jysk_bedside_favrbo",
            //     "jysk_bedside_favrbo",
            //     "jysk_tvcabinet_favrbo",
            //     "jysk_shelf_stavanger",
            //     "jysk_shelf_stavanger",
            //     "jysk_bedside_cabinet_vedde",
            //     "jysk_bedside_cabinet_vedde",
            //     "jysk_wardrobe_vedde",
            //     "jysk_bed_vedde",
            //     "jysk_tvcabinet_kalby",
            //     "jysk_wardrobe_sattrup",
            //     "jysk_desk_stege",
            //     "jysk_bedside_cabinet_broby",
            //     "jysk_bedside_cabinet_broby",
            //     "sunwah_tree_b",
            //     "jysk_chair_dima",
            //     "jysk_bed_tinglev",
            //     "jysk_desk_tamholt",
            //     "jysk_bedside_table_grindsted",
            //     "jysk_bedside_table_grindsted",
            //     "jysk_chair_abildholt",
            //     "jysk_bed_edith",
            //     "jysk_chair_ejerslev",
            //     "jysk_cabinet_gadeskov",
            //     "jysk_bedside_cabinet_limfjorden",
            //     "jysk_bedside_cabinet_limfjorden",
            //     "jysk_cabinet_skals",
            //     "jysk_bed_tinglev",
            //     "jysk_chair_lola",
            //     "jysk_wardrobe_favrbo",
            // ];
            $array = $this->asset->getEcomItems();
            //$array = array_intersect($ecom_lists, $array);
            //$array = array_values(array_unique($array));
            $query = $this->model->whereIn('name', $array);
            if($brand){
                $query = $query->where('brand', $brand);
            }
            if($category){
                $query = $query->where('category', $category);
            }
            if($vendor){
                $query = $query->where('vendor', $vendor);
            }
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }
            if($start){
                $query = $query->where('name', 'regexp', "/^[a-z0-9]+_$start./");
            }

            if($layout){
                if(strtolower($layout) == 'isnull'){
                    $query = $query->whereNull('layout');
                }else{
                    $query = $query->where('layout', $layout);
                }
            }

            if($gltf !== null){
                if($gltf){
                    $query = $query->whereNotNull('gltf');
                }
                else{
                    $query = $query->whereNull('gltf');
                }
            }
            
            // if($published){
            //     $query = $query->published();
            // }
            
            return $query->orderBy('created_at', "DESC")->paginate($limit);
        });
        return $collections;
    }
    // public function getAllItems()
    // {
    //     $vendor = request()->get('vendor');
    //     $brand = request()->get('brand');
    //     $category = request()->get('category');
    //     $limit =  (int) request()->get('limit', 40);
    //     $page =  (int) request()->get('page');    
    //     $name = strtolower(request()->get('name'));
    //     $start = strtolower(request()->get('start'));
    //     $layout = request()->get('layout');
        
    //     $key = "indexItem:" . concat(":", $vendor, $brand, $layout, $start, $name, $category, $limit, $page);

    //     $collections = Cache::tags(Object3d::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($vendor, $name, $brand, $layout, $start, $category, $limit, $page) {
    //         $query = $this->model;
    //         if($brand){
    //             $query = $query->where('brand', $brand);
    //         }
    //         if($category){
    //             $query = $query->where('category', $category);
    //         }
    //         if($vendor){
    //             $query = $query->where('vendor', $vendor);
    //         }
    //         if($name){
    //             $query = $query->where('name', 'like', '%'. $name . '%');
    //         }
    //         if($start){
    //             $query = $query->where('name', 'like', $start . '%');
    //         }
    //         if($layout){
    //             if(strtolower($layout) == 'isnull'){
    //                 $query = $query->whereNull('layout');
    //             }else{
    //                 $query = $query->where('layout', $layout);
    //             }
    //         }
    //         return $query->withoutTTS()->orderBy('created_at', "DESC")->paginate($limit);
    //     });
    //     return $collections;
    // }

    // public function getAllTTS()
    // {
    //     $vendor = request()->get('vendor');
    //     $brand = request()->get('brand');
    //     $category = request()->get('category');
    //     $limit =  (int) request()->get('limit', 40);
    //     $page =  (int) request()->get('page');    
    //     $name = strtolower(request()->get('name'));
    //     $start = strtolower(request()->get('start'));
    //     $layout = request()->get('layout');

    //     $key = "indexTTS:" . concat(":", $vendor, $brand, $layout, $start, $name, $category, $limit, $page);
    //     $collections = Cache::tags(Object3d::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($vendor, $name, $brand, $layout, $start, $category, $limit, $page) {
    //         $query = $this->model;
    //         if($brand){
    //             $query = $query->where('brand', $brand);
    //         }
    //         if($category){
    //             $query = $query->where('category', $category);
    //         }
    //         if($vendor){
    //             $query = $query->where('vendor', $vendor);
    //         }
    //         if($name){
    //             $query = $query->where('name', 'like', '%'. $name . '%');
    //         }
    //         if($start){
    //             $query = $query->where('name', 'like', $start . '%');
    //         }
    //         if($layout){
    //             if(strtolower($layout) == 'isnull'){
    //                 $query = $query->whereNull('layout');
    //             }else{
    //                 $query = $query->where('layout', $layout);
    //             }
    //         }
    //         return $query->withTTS()->orderBy('created_at', "DESC")->paginate($limit);
    //     });
    //     return $collections;
    // }
    
    // public function getMyObjects()
    // {
    //     $user = Auth::user();
    //     $vendor = request()->get('vendor');
    //     $brand = request()->get('brand');
    //     $category = request()->get('category');
    //     $limit =  (int) request()->get('limit', 40);
    //     $page =  (int) request()->get('page');  
    //     $name = strtolower(request()->get('name'));
    //     $start = strtolower(request()->get('start'));
    //     $layout = request()->get('layout');

    //     $key = "index:" . $user->auth_id . ":" . concat(":", $vendor, $brand, $layout, $start, $name, $category, $limit, $page);
    //     $collections = Cache::tags(Object3d::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use ($user, $vendor, $name, $brand, $layout, $start, $category, $limit, $page){
    //         $query = $this->model;
    //         if($brand){
    //             $query = $query->where('brand', $brand);
    //         }
    //         if($category){
    //             $query = $query->where('category', $category);
    //         }
    //         if($vendor){
    //             $query = $query->where('vendor', $vendor);
    //         }
    //         if($name){
    //             $query = $query->where('name', 'like', '%'. $name . '%');
    //         }
    //         if($start){
    //             $query = $query->where('name', 'like', $start . '%');
    //         }
    //         if($layout){
    //             if(strtolower($layout) == 'isnull'){
    //                 $query = $query->whereNull('layout');
    //             }else{
    //                 $query = $query->where('layout', $layout);
    //             }
    //         }
    //         return $query->where('ownerID', $user->auth_id)->orderBy('created_at', "DESC")->paginate($limit);
    //     });
    //     return $collections;
    // }
    
	public function create($input)
	{
        if(!isset($input['attributes']) || (is_array($input['attributes']) && !isset($input['attributes']['type'])) || (is_array($input['attributes']) && isset($input['attributes']['type']) && $input['attributes']['type'] == '2d')){
            $input['fbx'] = config('fitin.asset.default_fbx');
        }
        if(isset($input['thumb']) && strpos($input['thumb'], 'data:image') !== false){
            if(empty($input['thumb64'])){
                $input['thumb64'] = $input['thumb'];
            }
            unset($input['thumb']);
        }
		$object = $this->model->create($input);
        
        if(!empty($input['thumb64'])){
            $image_data = $this->upload_service->uploadThumbGroup($object, $input['thumb64'], [$object->brand, $object->name]);
            
            if(!empty($image_data['file_path'])){
                $object->thumb = $image_data['file_path'];
                $object->save();
            }    
        }
        
        return $object;
    }
    
    public function find($key){
        $cache_key = "show:$key";
        $object = Cache::tags(Object3d::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('name', $key)->first();
        });
        return $object;
    }


    public function update($key, $input){
        $object = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        if(!$object){
            return false;
        }
        //$input = $this->model->buildAttributeStructure( $input);
        if(isset($input['thumb']) && strpos($input['thumb'], 'data:image') !== false){
            if(empty($input['thumb64'])){
                $input['thumb64'] = $input['thumb'];
            }
            unset($input['thumb']);
        }
        $object->update($input);

        if(!empty($input['thumb64'])){
            $image_data = $this->upload_service->uploadThumbGroup($object, $input['thumb64'], [$object->brand, $object->name]);
            
            if(!empty($image_data['file_path'])){
                $object->thumb = $image_data['file_path'];
                $object->save();
            }    
        }
        return $object;
    }

    public function delete($key){
        $object = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        if(!$object){
            return false;
        }

        $object->delete();
        return $object;
    }

    public function check_linked($key){
        $asset = new Asset();
        $keys = $asset->getEcomItems();
        if(in_array($key, $keys)){
            return true;
        }else{
            return false;
        }
    }
}