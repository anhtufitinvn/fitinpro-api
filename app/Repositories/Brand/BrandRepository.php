<?php
namespace App\Repositories\Brand;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\Brand;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;

class BrandRepository extends EloquentRepository implements BrandRepositoryInterface {
    
    const CACHE_TIME = 60 * 5;

    protected $upload_service;

    public function __construct(UploadService $upload_service)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
    }

	public function model()
	{
		return Brand::class;
	}
    
    public function getAll($params = [])
    {
        $key = "index";
        $collections = Cache::tags(Brand::CACHE_TAG)->remember($key, self::CACHE_TIME, function () {
            return $this->model->all();
        });
        return $collections;
    }

    
    
	public function create($input)
	{
		$instance = $this->model->create($input);
        
        if(!empty($input['image64'])){
            $image_data = $this->upload_service->uploadThumbGroup($instance, $input['image64'], [$instance->code]);
            
            if(!empty($image_data['file_path'])){
                $instance->image = $image_data['file_path'];
                $instance->save();
            }    
        }
        
        return $instance;
    }
    
    public function find($key){
        $cache_key = "show:$key";
        $instance = Cache::tags(Brand::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('code', $key)->first();
        });
        
        return $instance;
    }


    public function update($key, $input){
        $instance = $this->model->where('code', $key)->first();
        if(!$instance){
            return false;
        }
        $instance->update($input);

        if(request()->get('image64')){
            $image_data = $this->upload_service->uploadThumbGroup($instance, $input['image64'], [$instance->code]);
            
            if(!empty($image_data['file_path'])){
                $instance->image = $image_data['file_path'];
                $instance->save();
            }    
        }
        return $instance;
    }

    public function delete($key){
        $instance = $this->model->where('code', $key)->first();
        if(!$instance){
            return false;
        }

        $instance->delete();
        return $instance;
    }
}