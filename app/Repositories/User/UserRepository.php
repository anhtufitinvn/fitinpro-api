<?php
namespace App\Repositories\User;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\User;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class UserRepository extends EloquentRepository implements UserRepositoryInterface {
    
    const CACHE_TIME = 60 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return User::class;
    }
    
    public function getAll($params = [])
    {
        $cache_key = "index:";
        $page = (int) (request()->get('page'));
        $limit = (int) (request()->get('limit', 30));
        $concat = concat(":", $page, $limit);
        $cache_key = $cache_key . $concat;
        $collections = Cache::tags(User::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($page, $limit)  {
            $query = $this->model;
            
            return $query->latest()->paginate($limit);
        });
        return $collections;
    }

    public function getFitinUsers($params = [])
    {
        $cache_key = "fitin:";
        $page = (int) (request()->get('page'));
        $limit = (int) (request()->get('limit', 30));
        $concat = concat(":", $page, $limit);
        $cache_key = $cache_key . $concat;
        $collections = Cache::tags(User::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($page, $limit)  {
            $query = $this->model;
            $query = $query->where('email', 'like', "%@fitin.vn");
            return $query->latest()->paginate($limit);
        });
        return $collections;
    }
    
	public function create($input)
	{
       
        $user = $this->model->create($input);
        $roles = $input['roles'] ?? [];
        $user->syncRoles($roles);

        return $user;
    }


    public function find($key){
        $cache_key = "show:$key";
        $key = (int) $key;
        $user = Cache::tags(User::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('auth_id', $key)->first();
        });
        
        return $user;   
    }


    public function update($key, $input){
        $key = (int) $key;
        $user = $this->model->where('auth_id', $key)->first();
        if(!$user){
            return false;
        }
        $roles = $input['roles'] ?? [];

        $user->syncRoles($roles); 
        $user->update($input);
        return $user;
    }

    
    

    public function delete($key){
        $user = $this->model->where('auth_id', $key)->first();
        
        if(!$user){
            return false;
        }
        $user->delete();
        return $user;
    }

}