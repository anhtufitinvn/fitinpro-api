<?php
namespace App\Repositories\Role;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\Role;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;


class RoleRepository extends EloquentRepository implements RoleRepositoryInterface {
    
    const CACHE_TIME = 60 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
       
    }

	public function model()
	{
		return Role::class;
	}
    
    public function getAll($params = [])
    {
        $roles = getRoles();
        return $roles;
    }

    
    
	public function create($input)
	{
        $role = $this->model->create($input);
        return $role;
    }


    public function find($key){
        $cache_key = "show:$key";
        $role = Cache::tags(Role::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('name', $key)->first();
        });
        
        return $role;   
    }


    public function update($key, $input){
        $role = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        if(!$role){
            return false;
        }

        $role->update($input);
        return $role;
    }

    public function delete($key){
        $role = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        
        if(!$role){
            return false;
        }
        $role->delete();
        return $role;
    }

}