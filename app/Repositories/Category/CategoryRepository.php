<?php
namespace App\Repositories\Category;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\Category;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;

class CategoryRepository extends EloquentRepository implements CategoryRepositoryInterface {
    
    const CACHE_TIME = 60 * 5;

    protected $upload_service;

    public function __construct(UploadService $upload_service)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
    }

	public function model()
	{
		return Category::class;
	}
    
    public function getAll($params = [])
    {
        $key = "index:";
        $published = $params['published'] ?? 0;
        $concat = concat(":", $published);
        $key = $key . $concat;
        $collections = Cache::tags(Category::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use ($published) {
            $query = $this->model;
            if($published){
                $query = $query->published();
            }
            return $query->get();
        });
        return $collections;
    }

    
    
	public function create($input)
	{
        if(!isset($input['publishedEcom'])){
            $input['publishedEcom'] = false;
        }
		$cat = $this->model->create($input);
        
        if(!empty($input['image64'])){
            $image_data = $this->upload_service->uploadThumbGroup($cat, $input['image64'], [$cat->code]);
            
            if(!empty($image_data['file_path'])){
                $cat->image = $image_data['file_path'];
                $cat->save();
            }    
        }
        
        return $cat;
    }
    
    public function find($key){
        $cache_key = "show:$key";
        $cat = Cache::tags(Category::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('code', $key)->first();
        });
        
        return $cat;
    }


    public function update($key, $input){
        $cat = $this->model->where('code', $key)->first();
        if(!$cat){
            return false;
        }
        $cat->update($input);

        if(request()->get('image64')){
            $image_data = $this->upload_service->uploadThumbGroup($cat, $input['image64'], [$cat->code]);
            
            if(!empty($image_data['file_path'])){
                $cat->image = $image_data['file_path'];
                $cat->save();
            }    
        }
        return $cat;
    }

    public function delete($key){
        $cat = $this->model->where('code', $key)->first();
        if(!$cat){
            return false;
        }

        $cat->delete();
        return $cat;
    }
}