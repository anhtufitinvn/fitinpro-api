<?php
namespace App\Repositories\Room;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\Room;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;

class RoomRepository extends EloquentRepository implements RoomRepositoryInterface {
    
    const CACHE_TIME = 60 * 5;

    protected $upload_service;

    public function __construct(UploadService $upload_service)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
    }

	public function model()
	{
		return Room::class;
	}
    
    public function getAll($params = [])
    {
        $key = "index";
        $collections = Cache::tags(Room::CACHE_TAG)->remember($key, self::CACHE_TIME, function () {
            return $this->model->whereNull('parent_id')->get();
        });
        return $collections;
    }

    public function getMyRooms()
    {
        $user = Auth::user();
        $key = "index:". $user->auth_id;
        $collections = Cache::tags(Room::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use ($user) {
            return $this->model->whereNotNull('parent_id')->where('owner_id', $user->auth_id)->get();
        });
        return $collections;
    }
    
	public function create($input)
	{
		$room = $this->model->create($input);
        
        if(!empty($input['image64'])){
            $image_data = $this->upload_service->uploadThumbGroup($room, $input['image64'], [$room->code]);
            
            if(!empty($image_data['file_path'])){
                $room->image = $image_data['file_path'];
                $room->save();
            }    
        }
        
        return $room;
    }
    
    public function find($key){
        $cache_key = "show:$key";
        $room = Cache::tags(Room::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->first();
        });
        
        return $room;
    }


    public function update($key, $input){
        $room = $this->model->where('_id', $key)->first();
        if(!$room){
            return false;
        }
        $room->update($input);

        if(request()->get('image64')){
            $image_data = $this->upload_service->uploadThumbGroup($room, $input['image64'], [$room->code]);
            
            if(!empty($image_data['file_path'])){
                $room->image = $image_data['file_path'];
                $room->save();
            }    
        }
        return $room;
    }

    public function delete($key){
        $room = $this->model->where('_id', $key)->first();
        if(!$room){
            return false;
        }

        $room->delete();
        return $room;
    }
}