<?php
namespace App\Repositories\Color;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\Color;
use App\Models\Ver2\ColorGroup;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use App\Services\UploadService;

class ColorRepository extends EloquentRepository implements ColorRepositoryInterface {
    
    const CACHE_TIME = 60 * 60 * 24;

    public function __construct(UploadService $upload_service)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
    }

	public function model()
	{
		return Color::class;
	}
    
    public function getAll($params = [])
    {
        
        $group = strtolower(request()->get('group'));
        $name = strtolower(request()->get('name'));

        $key = "index:" .concat(':', $group, $name);
        
        $collections = Cache::tags(Color::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use ($group, $name) {
            //Jensseger MongoDB has, whereHas not working as expected. So use 2 queries below.
            $query = $this->model;
            $colorGroups = ColorGroup::published()->get()->pluck('code');
            if($group){
                $query = $query->where('group', $group);
            }
            if($name){
                $query = $query->where('name', 'like', '%'. $name. '%');
            }
            
            $query = $query->whereIn('group', $colorGroups)->orderBy('priority', 'DESC')->orderBy('usedCount', 'DESC');
            return $query->get();
        });

        return $collections;
    }

    public function getListForEcom()
    { 
        $limit = (int) request()->get('limit', 40);
        $page = (int) request()->get('page', 1);

        $colorGroups = ColorGroup::published()->get()->pluck('code');    
        $collections = $this->model->whereIn('group', $colorGroups)->paginate($limit);

        return $collections;
    }
    
	public function create($input)
	{
		$color = $this->model->create($input);
        if(!$color->hex && isset($input['externalImage'])){
            $data = $this->upload_service->syncImageFromUrl($color, $input['externalImage'], ['images']);
            if(!empty($data['file_path'])){
                $color->image = $data['file_path'];
                $color->save();
            }
        }
        return $color;
    }


    public function find($key){
        $cache_key = "show:$key";
        $color = Cache::tags(Color::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('code', $key)->first();
        });
        
        return $color;   
    }


    public function update($key, $input){
        $color = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$color){
            return false;
        }
        $color->update($input);

        return $color;
    }

    public function updateCount($key){
        $color = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$color){
            return false;
        }
        $count = $color->usedCount ?? 0;
        $color->usedCount = $count + 1;
        $color->save();
        return $color;
    }

    public function delete($key){
        $color = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        
        if(!$color){
            return false;
        }
        $color->delete();
        return $color;
    }

    public function getMostUsedList(){
        $limit = (int) request()->get('limit', 10);
        $colors = $this->model->orderBy('usedCount', 'DESC')->take($limit)->get();
        return $colors;
    }
}