<?php
namespace App\Repositories\Color;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\ColorGroup;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class ColorGroupRepository extends EloquentRepository implements ColorGroupRepositoryInterface {
    
    const CACHE_TIME = 60 * 5;

    public function __construct()
    {
        $this->model = app($this->model());

    }

	public function model()
	{
		return ColorGroup::class;
	}
    
    public function getAll($params = [])
    {
        $key = "index";
        $collections = Cache::tags(ColorGroup::CACHE_TAG)->remember($key, self::CACHE_TIME, function () {
            return $this->model->published()->get();
        });
        return $collections;
    }
    
	public function create($input)
	{
		$color = $this->model->create($input);
        
        return $color;
    }


    public function find($key){
        $cache_key = "show:$key";
        $color = Cache::tags(ColorGroup::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('code', $key)->first();
        });
        
        return $color;   
    }


    public function update($key, $input){
        $color = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$color){
            return false;
        }
        $color->update($input);

        return $color;
    }

    public function delete($key){
        $color = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        
        if(!$color){
            return false;
        }
        $color->delete();
        return $color;
    }
}