<?php
namespace App\Repositories\Layout;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\Layout;
use App\Models\Ver2\Object3d;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;

class LayoutRepository extends EloquentRepository implements LayoutRepositoryInterface {
    
    const CACHE_TIME = 60 * 5;

    protected $upload_service;

    public function __construct(UploadService $upload_service)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
    }

	public function model()
	{
		return Layout::class;
	}
    
    public function getAll($params = [])
    {
        $project = request()->get('project');
        
        $limit =  (int) request()->get('limit', 40);
        $page =  (int) request()->get('page');    
        $name = strtolower(request()->get('name'));
        $start = strtolower(request()->get('start'));
        $owner = (int) request()->get('owner', 0);
        $gltf = request()->get('gltf');
        $published = $params['published'] ?? 0;
        $concat = concat(":", $project, $start, $name, $owner, $gltf, $limit, $page, $published);

        $key = "index:". $concat;
        $collections = Cache::tags(Layout::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use ($project, $start, $name, $owner, $gltf, $limit, $page, $published) {
            $query = $this->model;
            if($project){
                $query = $query->where('project', $project);
            }
            if($owner){
                $query = $query->where('ownerId', $owner);
            }
            
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }
            if($start){
                $query = $query->where('name', 'like', "$start" . '%');
            }

            

            if($gltf !== null){
                if($gltf){
                    $query = $query->whereNotNull('gltf');
                }
                else{
                    $query = $query->whereNull('gltf');
                }
            }

            if($published){
                $query = $query->published();
            }
            
            return $query->latest()->paginate($limit);
        });
        return $collections;
    }

    public function getListName()
    {
        $key = "list";
        $collections = Cache::tags(Layout::CACHE_TAG)->remember($key, self::CACHE_TIME, function () {
            $data = $this->model->whereNotNull('name')->pluck('name');
            return $data->toArray();
        });
        return $collections;
    }
    
    public function getListObject($layout_name)
    {
        $key = "object:list";
        $collections = Cache::tags(Layout::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($layout_name) {
            $data = Object3d::where('layout', $layout_name)->get();
            return $data;
        });
        return $collections;
    }

	public function create($input)
	{
        if(isset($input['dataJson']) && ( !isset($input['no_format']) || !$input['no_format'])){
            
            $input['dataJson'] = parseLayoutFromUnity($input['dataJson']);
        }
        $layout = $this->model->create($input);
        $layout = $this->find($layout->getKey());
        
        if(!empty($input['requireDependencies']) && is_array($input['requireDependencies'])){
            $layout->buildDependencies($input['requireDependencies']);
        }
        if(!empty($input['image64'])){
            $image_data = $this->upload_service->uploadThumbGroup($layout, $input['image64'], [$layout->_id]);
            
            if(!empty($image_data['file_path'])){
                $layout->image = $image_data['file_path'];
                $layout->save();
            }    
        }
        
        return $layout;
    }

    public function duplicate($id)
	{
        $user = Auth::user();
        $parent = $this->model->find($id);
        if(!$parent){
            return false;
        }
        $newLayout = $parent->replicate();
        $newId = new \MongoDB\BSON\ObjectId();
        $newLayout->_id = $newId;
        $newLayout->push();
        $newLayout->update([
            'parentId' => $id,
            'ownerId' => $user->auth_id,
            'isTemplate' => false,
            'status' => Layout::PENDING
        ]);

        return $newLayout;
    }
    
    public function find($key){
        
        $requireDependencies = request()->get('requireDependencies') ? 1 : 0;
        $cache_key = "show:$key:$requireDependencies";
        $layout = Cache::tags(Layout::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key, $requireDependencies) {
            $q = $this->model->where('_id', $key)->orWhere('name', $key)->first();
            if($q && $requireDependencies){   
                
                $q->buildDependencies();
                
            }
            return $q;
        });
        
        return $layout;   
    }


    public function update($key, $input){
        $layout = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        if(!$layout){
            return false;
        }
        // if(isset($input['dataJson']) && ( !isset($input['no_format']) || !$input['no_format'])){
        //     $input['dataJson'] = parseLayoutFromUnity($input['dataJson']);
        // }
        $layout->update($input);
        if(!empty($input['requireDependencies']) && is_array($input['requireDependencies'])){
            $layout->buildDependencies($input['requireDependencies']);
        }
        if(request()->get('image64')){
            $image_data = $this->upload_service->uploadThumbGroup($layout, $input['image64'], [$layout->_id]);
            
            if(!empty($image_data['file_path'])){
                $layout->image = $image_data['file_path'];
                $layout->save();
            }    
        }
        return $layout;
    }

    public function delete($key){
        $layout = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        
        if(!$layout){
            return false;
        }
        $layout->delete();
        return $layout;
    }
}