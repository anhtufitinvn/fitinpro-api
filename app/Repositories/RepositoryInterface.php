<?php


namespace App\Repositories;


interface RepositoryInterface
{
    /**
     * Get all
     * @return mixed
     */
    public function getAll(array $params);

    /**
     * Get one
     * @param $id
     * @return mixed
     */
    public function find($key);

    /**
     * Create
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes);

    /**
     * Update
     * @param $id
     * @param array $attributes
     * @return mixed
     */
    public function update($key, array $attributes);



    /**
     * Delete
     * @param $id
     * @return mixed
     */
    public function delete($key);
}
