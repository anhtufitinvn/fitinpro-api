<?php
namespace App\Repositories\Bundle;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\Bundle;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;
use App\FitIn\Asset;

class BundleRepository extends EloquentRepository implements BundleRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    protected $upload_service;

    public function __construct(UploadService $upload_service)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
    }

	public function model()
	{
		return Bundle::class;
	}
    
    public function getAll($params = [])
    {
        $type = $params['type'] ?? NULL;
        $brand = request()->get('brand');
        
        $limit =  (int) request()->get('limit', 40);
        $page =  (int) request()->get('page');    
        $name = strtolower(request()->get('name'));
        $start = strtolower(request()->get('start'));
        $owner = (int) request()->get('owner', 0);
        $gltf = request()->get('gltf');
        $parent = request()->get('parent');
        $concat = concat(":",$parent, $brand, $start, $name, $owner, $gltf, $limit, $page);

        $key = "index:$type:" . $concat;
        $collections = Cache::tags(Bundle::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use ( $parent, $brand, $start, $name, $owner, $gltf, $limit, $page, $type) {
            
            $query = $this->model;
            if($brand){
                $query = $query->where('brand', $brand);
            }
            if($owner){
                $query = $query->where('ownerId', $owner);
            }
            
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }
            if($start){
                $query = $query->where('name', 'like', "$start" . '%');
            }

            

            if($gltf !== null){
                if($gltf){
                    $query = $query->whereNotNull('gltf');
                }
                else{
                    $query = $query->whereNull('gltf');
                }
            }

            if($type == 'published'){
                $query = $query->published();
            }else if($type == 'my-bundles'){
                $userId = Auth::user()->auth_id;
                $query = $query->where('ownerId', $userId);
            }

            if($parent){
                return $query->where('parent', $parent)->latest()->paginate($limit);
            }else{
                return $query->whereNull('parent')->latest()->paginate($limit);
            }
            
        });
        return $collections;
    }

    public function getAllKeys()
    {

        $key = "index:render:key";
        $collections = Cache::tags(Bundle::CACHE_TAG)->remember($key, self::CACHE_TIME, function () {   
            $data = $this->model->get()->pluck('key_name');
            
            return $data;
        });
        return $collections;
    }
    
    
	public function create($input)
	{
        if(Auth::user()){
            $input['ownerId'] = Auth::user()->auth_id;
            if(empty( $input['parentId'])){
                $input['isTemplate'] = 1;
            }else{
                $input['isTemplate'] = 0;
            }
        }
        else {
            $input['ownerId'] = "cms_1";
        }
        
        $input['status'] = $input['status'] ?? Bundle::PENDING;

        $input = $this->model->buildAttributeStructure($input);
		$bundle = $this->model->create($input);
        
        return $bundle;
    }
    
    public function duplicate($key)
	{
        $user = Auth::user();
        $parent = $this->model->find($key);
        if(!$parent){
            return false;
        }
        
        $newBundle = $parent->replicate();
        $newId = new \MongoDB\BSON\ObjectId();
        $newBundle->_id = $newId;
        $newBundle->key_name = $key . '_' . $newId;
        $newBundle->push();
        $newBundle->update([
            'parent' => $key,
            'ownerId' => $user->auth_id,
            'isTemplate' => 0
        ]);

        return $newBundle;
    }

    public function find($key){
        $cache_key = "show:$key";
        $bundle = Cache::tags(Bundle::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('key_name', $key)->first();
        });
        
        return $bundle;
    }


    public function update($key, $input){
        $bundle = $this->model->where('_id', $key)->orWhere('key_name', $key)->first();
        if(!$bundle){
            return false;
        }
        $input = $this->model->buildAttributeStructure($input);
        $bundle->update($input);

        return $bundle;
    }

    public function delete($key){
        $bundle = $this->model->where('_id', $key)->orWhere('key_name', $key)->first();
        if(!$bundle){
            return false;
        }

        $bundle->delete();
        return $bundle;
    }

    public function check_linked($key){
        $asset = new Asset();
        $keys = $asset->getEcomBundles();
        if(in_array($key, $keys)){
            return true;
        }else{
            return false;
        }
    }
}