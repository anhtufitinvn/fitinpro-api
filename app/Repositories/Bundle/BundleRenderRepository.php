<?php
namespace App\Repositories\Bundle;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\BundleRender;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;

class BundleRenderRepository extends EloquentRepository implements BundleRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    protected $upload_service;

    public function __construct(UploadService $upload_service)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
    }

	public function model()
	{
		return BundleRender::class;
	}
    
    public function getAll($params = [])
    {
    
        
        $limit =  (int) request()->get('limit', 40);
        $page =  (int) request()->get('page');    
        
        $concat = concat(":", $limit, $page);

        $key = "index:" . $concat;
        $collections = Cache::tags(BundleRender::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use ($limit, $page) {
            
            $query = $this->model;
            return $query->paginate($limit);
        });
        return $collections;
    }
    
	public function create($input)
	{
		$bundle = $this->model->create($input);
        
        return $bundle;
    }
    

    public function find($key){
        $cache_key = "show:$key";
        $bundle = Cache::tags(BundleRender::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('key_name', $key)->first();
        });
        
        return $bundle;
    }


    public function update($key, $input){
        $bundle = $this->model->where('_id', $key)->orWhere('key_name', $key)->first();
        if(!$bundle){
            return false;
        }
        $bundle->update($input);

        return $bundle;
    }

}