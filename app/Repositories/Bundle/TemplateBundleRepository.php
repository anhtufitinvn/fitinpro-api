<?php
namespace App\Repositories\Bundle;

use App\Repositories\EloquentRepository;
use App\Models\Ver2\TemplateBundle;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;

class TemplateBundleRepository extends EloquentRepository implements TemplateBundleRepositoryInterface {
    
    const CACHE_TIME = 60 * 10;

    protected $upload_service;

    public function __construct(UploadService $upload_service)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
    }

	public function model()
	{
		return TemplateBundle::class;
	}
    
    public function getAll($params = [])
    {
        $key = "index";
        $collections = Cache::tags(TemplateBundle::CACHE_TAG)->remember($key, self::CACHE_TIME, function () {
            $user = Auth::user();
            return $user->user_template_bundle;
        });
        return $collections;
    }
    
	public function create($input)
	{
        $user = Auth::user();
        $input['userId'] = $user->auth_id;
		$TemplateRoom = $this->model->create($input);
        
        if(!empty($input['image64'])){
            $image_data = $this->upload_service->uploadThumbGroup($TemplateRoom, $input['image64'], [$user->auth_id, 'template-rooms']);
            
            if(!empty($image_data['file_path'])){
                $TemplateRoom->imagePath = $image_data['file_path'];
                $TemplateRoom->save();
            }    
        }
        
        return $TemplateRoom;
    }
    
    public function find($key){
        $cache_key = "show:$key";
        $TemplateRoom = Cache::tags(TemplateBundle::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('code', $key)->first();
        });
        
        return $TemplateRoom;
    }


    public function update($key, $input){
        $TemplateRoom = $this->model->where('_id', $key)->first();
        if(!$TemplateRoom){
            return false;
        }
        $user = Auth::user();
        $TemplateRoom->update($input);

        if(request()->get('image64')){
            $image_data = $this->upload_service->uploadThumbGroup($TemplateRoom, $input['image64'], [$user->auth_id, 'template-rooms']);
            
            if(!empty($image_data['file_path'])){
                $TemplateRoom->imagePath = $image_data['file_path'];
                $TemplateRoom->save();
            }    
        }
        return $TemplateRoom;
    }

    public function delete($key){
        $TemplateRoom = $this->model->where('_id', $key)->first();
        if(!$TemplateRoom){
            return false;
        }

        $TemplateRoom->delete();
        return $TemplateRoom;
    }
}