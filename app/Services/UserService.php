<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Exceptions\ValidFieldException;
use App\FitIn\AuthID;
use App\Models\Ver2\User;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Carbon;

class UserService
{
    /**
     * @var AuthID
     */
    private $authID;

    public function __construct(AuthID $authID,
                                UploadService $uploadService)
    {
        $this->authID = $authID;
        $this->uploadService = $uploadService;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getProfile($user_auth_id)
    {
        //$auth_token = $this->getGuard()->getClaim('auth_token');
        //$profile = $this->authID->getProfile($auth_token);
        
        $user = User::where('auth_id', $user_auth_id)->first();
        return $user;
    }

    public function getProfileByEmail($email)
    {
        
        $user = User::where('email', $email)->first();
        return $user;
    }

    /**
     * @return mixed
     */
    public function getCurrentUser()
    {
        return $this->getGuard()->user();
    }

    public function getGuard()
    {
        return Auth()->guard('token');
    }

    public function updateProfile($id, $data)
    {
        unset($data['status'], $data['active'], $data['email'], $data['phone']);

        $user = User::findOrFail($id);
        $user->update($data);

        return $user;
    }

    /**
     * @param $arrUser
     * @return mixed
     * @throws GeneralException
     */
    public function register($arrUser)
    {
        $arrAuthResult = $this->authID->register($arrUser['email'], $arrUser['password'], $arrUser['phone']);

        $authData = $arrAuthResult['data'];

        $user = User::create([
            'auth_id' => $authData['id'],
            'app_id' => $authData['appId'],
            'name' => $arrUser['name'],
            'email' => $arrUser['email'],
            'phone' => !empty($arrUser['phone']) ? $arrUser['phone'] : null,
            'active' => 1
        ]);

        return $user->id;
    }


    private function createOrUpdateUser($resultAuth)
    {
        
        $user = User::where('auth_id', $resultAuth['id'])->first();

        if (!$user) {
            $userInfo = $this->authID->getInfo($resultAuth['token']);
            $arrEmail = explode('@', $resultAuth['email']);
            $user = User::create([
                'name' => $arrEmail[0],
                'email' => $userInfo['email'],
                'phone' => !empty($userInfo['phone']) ? $userInfo['phone'] : null,
                'auth_id' => $userInfo['id'],
                'app_id' => $userInfo['appId'] ?? null,
                'active' => $userInfo['status'] > 0 ? 1 : 0
            ]);
        }
       
        return $user;
    }

    /**
     * @param $email
     * @return bool
     * @throws GeneralException
     * @throws ValidFieldException
     */
    public function forgotPassword($email)
    {
        //$this->checkEmailPhone($email);

        return $this->authID->forgotPassword($email);
    }


    /**
     * @param $user
     * @param $authToken
     * @return array
     */
    private function loginByUser($user, $resultAuth, $expire)
    {
        // $authToken = $resultAuth['token'];

        // $this->getGuard()->login($user);

        // $user = $this->getGuard()->user();

        // $user->setAuthToken($authToken);
        
        // $token = $this->getGuard()->check() ? JWTAuth::fromUser($user) : false;

        // $arrToken = [
        //     'access_token' => $token,
        //     'auth_token' => $authToken
        // ];
        // return $this->getTokenJwt($arrToken);

        $cache_time = $this->getCacheExpire($expire);

        $this->setCacheLogin($user, $resultAuth['token'], $cache_time);

        $arrToken = [
            'access_token' => $resultAuth['token'],
            'expires_in' => $resultAuth['expire'],
            'error' => 0
        ];

        return $arrToken;
    }

    public function loginByToken( $token)
    {
    

        $resultAuth = $this->authID->loginByToken( $token);
        
        if (!$resultAuth) {
            throw new GeneralException(__('Token Invalid or expired!'), 'ACCESS_TOKEN_INVALID');
        }

        $user = $this->createOrUpdateUser($resultAuth);

        return $this->loginByUser($user, $resultAuth, $resultAuth['expire']);
    }

    public function setCacheLogin(User $user, $token, $expire_in_10min)
    {
        Cache::put('oauth:token:' . md5($token), serialize($user), $expire_in_10min);
    }

    public function getCacheExpire($expire){
        //Tinh time cho expire truoc 10phut
        $expire_in = (int)($expire / 1000);
        $expire_in_10min = $expire_in - 600;
        //Tinh second het han memcached
        $cache_time = $expire_in_10min - time();
        return $cache_time;
    }

    public function getCacheLogin($token)
    {
        //Cache
        $tokenCache = Cache::get('oauth:token:' . md5($token));
        
        if ($tokenCache) {
            $tokenCache = unserialize($tokenCache);
            return $tokenCache;
        }

        $oAuthID = AuthID::getInstance();
        $arrDataUser = $oAuthID->tokenVerify($token);

        if ($arrDataUser) {

            $cache_time = $this->getCacheExpire($arrDataUser['expire']);

            $oUser = $this->getProfile($arrDataUser['id']);
            if (!$oUser){
                $oUser = User::create([
                    'auth_id' => $arrDataUser['id'],
                    'app_id' => $arrDataUser['appId'],
                    'name' => $arrDataUser['name'] ?? null,
                    'email' => $arrDataUser['email'],
                    'phone' => !empty($arrDataUser['phone']) ? $arrDataUser['phone'] : null,
                    'active' => 1
                ]);
                
            }
            $this->setCacheLogin($oUser, $token, $cache_time);
            return $oUser;
        }

        return false;
    }

    /**
     * @param $email
     * @param $password
     * @return array
     * @throws GeneralException
     * @throws ValidFieldException
     */
    public function login($email, $password)
    {
        //$this->checkEmailPhone($email);

        if($email == config('fitin.admin.email')){
            $user = User::where('email', $email)->first();
            
            if($user && \Hash::check($password, $user->password)){
                return $this->loginAsAdmin($email);
            }else{
                return false;
            }

        }else{
            $resultAuth = $this->authID->login($email, $password);
        }
        

        if (!$resultAuth) {
            return false;
            
        }

        //$isConfirm = $this->authID->hasConfirmation($resultAuth['token']);

        // if (!$isConfirm) {
        //     $this->resendOtp($email);

        //     throw new GeneralException(__('Tài khoản chưa xác nhận. Vui lòng xác nhận tài khoản qua email hoặc điện thoại.'), 'ACCOUNT_NOT_ACTIVE');
        // }

        $user = $this->createOrUpdateUser($resultAuth);

        return $this->loginByUser($user, $resultAuth, $resultAuth['expire']);
    }


    public function loginAsAdmin($email){
        $token = gen_uuid();
        $user = $this->getProfileByEmail($email);
        
        $this->setCacheLogin($user, $token, 3600 * 24);

        $expire = \Carbon\Carbon::now()->addDays(1)->timestamp * 1000;
        
        $arrToken = [
            'access_token' => $token,
            'expires_in' =>  $expire,
            'error' => 0
        ];

        return $arrToken;
    }
    /**
     * @param $social
     * @param $access_token
     * @return array
     * @throws GeneralException
     */
    public function loginBySocial($social, $access_token)
    {
        $resultAuth = $this->authID->socialLogin($social, $access_token);
        
        if (!$resultAuth) {
            return false;
        }

        $user = $this->createOrUpdateUser($resultAuth);

        return $this->loginByUser($user, $resultAuth, $resultAuth['expire']);
    }

    /**
     * @param $arrToken
     * @return array
     */
    protected function getTokenJwt($arrToken)
    {
        return [
            'access_token' => $arrToken['access_token'],
            'auth_token' => $arrToken['auth_token'] ?? NULL,
            'token_type' => 'bearer',
            'expires_in' => $this->getGuard()->factory()->getTTL() * 60,
            'error' => 0
        ];
    }

    /**
     * @param $email
     * @param $new_password
     * @param $re_password
     * @return bool
     * @throws \App\Exceptions\GeneralException
     */
    public function changePassword($email, $new_password, $re_password)
    {
        return $this->authID->changePassword($email, $new_password, $re_password);
    }

    /**
     * @param $token
     * @param $email
     * @param $new_password
     * @param $retype_new_password
     * @return bool
     * @throws GeneralException
     * @throws ValidFieldException
     */
    public function resetNewPassword($token, $email, $new_password, $retype_new_password)
    {
        //$this->checkEmailPhone($email);


        return $this->authID->resetPassword($token, $email, $new_password, $retype_new_password);
    }

    /**
     * @param $id
     * @param $file
     * @return bool
     */
    public function updateAvatar($id, $file)
    {
        //TODO
//        $auth_token = $this->getGuard()->getClaim('auth_token');
//        $this->authID->setToken($auth_token);
//        $arrResultUpload = $this->authID->uploadAvatar($file);
//        dd($arrResultUpload);

        // $attachment = $this->uploadService->uploadAvatar($file);

        // if ($attachment) {
        //     $oUser = $this->getProfile($id);

        //     $oUser->attachmentAvatar()->where('_id', '!=', $attachment->id)->delete();
        //     $attachment->type = 'avatar';
        //     $oUser->attachmentAvatar()->save($attachment);
        //     return $attachment;
        // }

        return false;
    }


    /**
     * @param $email
     * @throws ValidFieldException
     */
    public function checkEmailPhone($email)
    {
        if (!$this->authID->hasExistEmail($email)) {
            return response()->json([
                'code' => -1,
                'message' => 'Email chưa đăng ký. Vui lòng thử lại',
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
           
        }

        if (!$this->authID->hasExistPhone($email)) {
            return response()->json([
                'code' => -1,
                'message' => 'Sdt chưa đăng ký. Vui lòng thử lại',
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
            
        }

    }

    /**
     * @param $arrData
     * @return bool
     * @throws GeneralException
     * @throws ValidFieldException
     */
    public function verifyAccount($email, $otp_code)
    {
        //$this->checkEmailPhone($email);

        return $this->authID->verifyAccount($email, $otp_code);
    }

    /**
     * @param $email
     * @param $otp_code
     * @return bool
     * @throws GeneralException
     */
    public function verifyOtp($email, $otp_code){
        return $this->authID->checkOtpValid($email, $otp_code);
    }

    /**
     * @param $emailOrPhone
     * @return mixed
     */
    public function activeAccount($emailOrPhone){

        $user = User::where('email', $emailOrPhone)
            ->orWhere('phone', $emailOrPhone)
            ->first();

        $user->active = 1;
        $user->save();

        return $user;
    }

    /**
     * @return array
     */
    public function refreshToken()
    {
        $arrToken = [
            'access_token' => $this->getGuard()->refresh()
        ];

        //$this->authID->refreshToken('abc');

        return $this->getTokenJwt($arrToken);
    }

    public function existEmail($email)
    {
        return $this->authID->hasUniqueEmail($email);
    }

    /**
     * @param $email
     * @throws GeneralException
     */
    public function removeAccount($email)
    {
        try {
            $this->authID->removeAccount($email);

            $user = User::where('email', $email)->first();

            if($user){
                $user->delete();
            }


            return true;
        } catch (\Exception $exception) {
            $arrError = [
                'api' => config('app.url'),
                'error' => class_basename($exception) . ' | ' .$exception->getMessage(),
                'stack' => mb_substr($exception->getTraceAsString(), 0, 150)  
            ];
            $channel = env('LOG_CHANNEL', 'telegram');
            Log::channel($channel)->debug($arrError);
            return false;
        }
    }

    public function resendOtp($email){
        return $this->authID->confirmationResend($email);
    }
}
