<?php
namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\Jobs\PostToWebhookJob;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
class Webhook
{
    protected $action, $data, $webhook_url;

    public function __construct($action , $data){
        $this->action = $action;
        $this->data = $data;
        $this->webhook_url = config('fitin.ecom.webhook_endpoint');
    }

    public function post(){
        
    }

}
