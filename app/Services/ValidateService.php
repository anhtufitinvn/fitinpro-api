<?php

namespace App\Services;


use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Validator;



class ValidateService
{
    public function valid($inputs,$rules,$error_code = 'VALIDATION_ERROR'){
        $validator = Validator::make(
            $inputs,
            $rules
        );

        if ($validator->fails()) {
            $errorString = implode(" ",$validator->messages()->all());
            throw new GeneralException($errorString,$error_code);
        }

        return true;
    }
}
