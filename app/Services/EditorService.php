<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Exceptions\ValidFieldException;
use App\FitIn\EditorAuthID as AuthID;
use App\EditorPro\Models\User;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Carbon;

class EditorService
{
    /**
     * @var AuthID
     */
    private $authID;

    public function __construct(AuthID $authID,
                                UploadService $uploadService)
    {
        $this->authID = $authID;
        $this->uploadService = $uploadService;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getProfile($user_auth_id)
    {

        $user = User::where('auth_id', $user_auth_id)->first();
        return $user;
    }

    /**
     * @return mixed
     */
    public function getCurrentUser()
    {
        return $this->getGuard()->user();
    }

    public function getGuard()
    {
        return Auth()->guard('editor');
    }

    public function updateProfile($id, $data)
    {
        unset($data['status'], $data['active'], $data['email'], $data['phone']);

        $user = User::findOrFail($id);
        $user->update($data);

        return $user;
    }

    /**
     * @param $arrUser
     * @return mixed
     * @throws GeneralException
     */
    public function register($arrUser)
    {
        $authData = $this->authID->register($arrUser['email'] ?? null, $arrUser['password'], $arrUser['phone'] ?? null);
        if($authData['code'] == 0){
            $user = User::create([
                'auth_id' => $authData['data']['id'],
                'name' => $arrUser['name'],
                'email' => $authData['data']['email'],
                'active' => 0,
                'brand' => $arrUser['brand'] ?? NULL,
                'config' => [
                    'private_key' => gen_uuid()
                ]
            ]);  
            return response()->json(
                ['code' => 0, 'data' => [], 'message' => __('Register successfully! Please check your email to activate!')]
            );
        }
        else {
            return response()->json($authData);
        }
    }


    private function createOrUpdateUser($resultAuth)
    {
        
        $user = User::where('auth_id', $resultAuth['id'])->first();

        if (!$user) {
            $userInfo = $this->authID->getInfo($resultAuth['token']);
            $arrEmail = explode('@', $resultAuth['email']);
            $user = User::create([
                'name' => $arrEmail[0],
                'email' => $userInfo['email'],
                'phone' => !empty($userInfo['phone']) ? $userInfo['phone'] : null,
                'auth_id' => $userInfo['id'],
                'app_id' => $userInfo['appId'] ?? null,
                'active' => $userInfo['status'] > 0 ? 1 : 0,
                'config' => [
                    'private_key' => gen_uuid()
                ]
            ]);
        }
       
        return $user;
    }

    /**
     * @param $email
     * @return bool
     * @throws GeneralException
     * @throws ValidFieldException
     */
    public function forgotPassword($email)
    {
        $this->checkEmailPhone($email);

        return $this->authID->forgotPassword($email);
    }


    /**
     * @param $user
     * @param $authToken
     * @return array
     */
    private function loginByUser($user, $resultAuth, $expire)
    {
     
        $cache_time = $this->getCacheExpire($expire);

        $this->setCacheLogin($user, $resultAuth['token'], $cache_time);
        
        $arrToken = [
            'access_token' => $resultAuth['token'],
            'expires_in' => $resultAuth['expire'],
            'error' => 0
        ];

        return $arrToken;
    }

    public function setCacheLogin(User $user, $token, $expire_in_10min)
    {
        Cache::put('oauth:editor:' . md5($token), serialize($user), $expire_in_10min);
    }

    public function getCacheExpire($expire){
        //Tinh time cho expire truoc 10phut
        $expire_in = (int)($expire / 1000);
        $expire_in_10min = $expire_in - 600;
        //Tinh second het han memcached
        $cache_time = $expire_in_10min - time();
        return $cache_time;
    }

    public function removeCacheLogin(User $user, $token)
    {
        return Cache::pull('oauth:editor:' . md5($token));
    }

    public function getCacheLogin($token)
    {
        
        //Cache
        $tokenCache = Cache::get('oauth:editor:' . md5($token));
        if ($tokenCache) {
            $tokenCache = unserialize($tokenCache);
            return $tokenCache;
        }

        $oAuthID = AuthID::getInstance();
        $arrDataUser = $oAuthID->tokenVerify($token);
        
        if ($arrDataUser) {

            $cache_time = $this->getCacheExpire($arrDataUser['expire']);

            $oUser = $this->getProfile($arrDataUser['id']);
            $this->setCacheLogin($oUser, $token, $cache_time );
            return $oUser;
        }

        return false;
    }

    /**
     * @param $email
     * @param $password
     * @return array
     * @throws GeneralException
     * @throws ValidFieldException
     */
    public function login($email, $password)
    {
        //$this->checkEmailPhone($email);
       
        $resultAuth = $this->authID->login($email, $password);
        
        if (!$resultAuth) {
            return 0;
        }
        if((is_array($resultAuth) && !$resultAuth['status'])){
            return -1;
        }
        // $isConfirm = $this->authID->hasConfirmation($resultAuth['token']);

        // if (!$isConfirm) {
        //     return false;
        // }

        $user = $this->createOrUpdateUser($resultAuth);

        return $this->loginByUser($user, $resultAuth, $resultAuth['expire']);
    }

    /**
     * @param $social
     * @param $access_token
     * @return array
     * @throws GeneralException
     */
    public function loginBySocial($social, $access_token)
    {
        $resultAuth = $this->authID->socialLogin($social, $access_token);
        
        if (!$resultAuth) {
            return false;
        }

        $user = $this->createOrUpdateUser($resultAuth);

        return $this->loginByUser($user, $resultAuth, $resultAuth['expire']);
    }

     /**
     * Log Out.
     *
     * @return bool
     */
    public function logout()
    {
        $token = request()->bearerToken();

        $user = $this->getCacheLogin($token);
        $this->removeCacheLogin($user, $token);

        return $this->authID->invalidateToken($token);
          
    }

    /**
     * @param $email
     * @param $new_password
     * @param $re_password
     * @return bool
     * @throws \App\Exceptions\GeneralException
     */
    public function changePassword($email, $new_password, $re_password)
    {
        return $this->authID->changePassword($email, $new_password, $re_password);
    }

    public function changePasswordById($token, $old_password, $new_password)
    {
        
        return $this->authID->changePasswordById($token, $old_password, $new_password);
    }


    /**
     * @param $token
     * @param $email
     * @param $new_password
     * @param $retype_new_password
     * @return bool
     * @throws GeneralException
     * @throws ValidFieldException
     */
    public function resetNewPassword($token, $email, $new_password, $retype_new_password)
    {
        $this->checkEmailPhone($email);


        return $this->authID->resetPassword($token, $email, $new_password, $retype_new_password);
    }

    /**
     * @param $id
     * @param $file
     * @return bool
     */
    public function updateAvatar($id, $file)
    {

        return false;
    }


    /**
     * @param $email
     * @throws ValidFieldException
     */
    public function checkEmailPhone($email)
    {
        if ($this->authID->isValidEmail($email)) {
            if (!$this->authID->hasExistEmail($email)) {
                throw new ValidFieldException(__('Email không chính xác. Vui lòng thử lại.'), 'email');
            }
        } else {
            if (!$this->authID->hasExistPhone($email)) {
                throw new ValidFieldException(__('Số điện thoại không chính xác. Vui lòng thử lại.'), 'email');
            }
        }
    }

    /**
     * @param $arrData
     * @return bool
     * @throws GeneralException
     * @throws ValidFieldException
     */
    public function verifyAccount($email, $otp_code)
    {
        $this->checkEmailPhone($email);

        return $this->authID->verifyAccount($email, $otp_code);
    }

    /**
     * @param $email
     * @param $otp_code
     * @return bool
     * @throws GeneralException
     */
    public function verifyOtp($email, $otp_code){
        return $this->authID->checkOtpValid($email, $otp_code);
    }

    /**
     * @param $emailOrPhone
     * @return mixed
     */
    public function activeAccount($emailOrPhone){

        $user = User::where('email', $emailOrPhone)
            ->orWhere('phone', $emailOrPhone)
            ->first();
        if($user){
            $user->active = 1;
            $user->save();
        }
        

        return $user;
    }


    public function existEmail($email)
    {
        return $this->authID->hasUniqueEmail($email);
    }

    /**
     * @param $email
     * @throws GeneralException
     */
    public function removeAccount($id)
    {
        try {
            $id = (int) $id;
            $result = $this->authID->removeAccount($id);
            
            $user = User::where('auth_id', $id)->first();

            if($user){
                $user->delete();
            }


            return $user;
        } catch (\Exception $exception) {
            $arrError = [
                'api' => config('app.url'),
                'error' => class_basename($exception) . ' | ' .$exception->getMessage(),
                'stack' => mb_substr($exception->getTraceAsString(), 0, 150)  
            ];
            $channel = env('LOG_CHANNEL', 'telegram');
            Log::channel($channel)->debug($arrError);
            return false;
        }
    }

    public function resendOtp($email){
        return $this->authID->confirmationResend($email);
    }

    public function changeProfile($input){
        return $this->authID->changeProfile($input);
    }
}
