<?php
namespace App\Services;


use App\Models\Ver2\Attachment;
use App\Models\Ver2\Object3d;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Validator;
use App\Helpers\Utils;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Http\Request;

class UploadService
{


    public function uploadThumbGroup($instance, $base64, $pathinfo = [])
    {
        $driver = config('fitin.asset.filesystem_driver');
        
        $type = get_class($instance)::DIR_NAME;
        $subPath = \App\Helpers\Utils::locationPath($pathinfo);
        
        $data = substr($base64, strpos($base64, ',') + 1);
        
        $data = base64_decode($data);

        $file_name = new \MongoDB\BSON\ObjectId();
        $file_name = "$file_name.png";
        $path = "$type/$subPath/". get_class($instance)::THUMB_DIR . "/$file_name";
            
        $upload = Storage::disk($driver)->put($path, $data);
        
        uploadDriver($file_name, $driver, $path, Storage::disk($driver)->url($path), $instance);
        
        $data = [
            "file_name" => $file_name,
            "url" =>  Storage::disk($driver)->url($path),
            "file_path" =>  "$path"
        ]; 

        return $data;          
    }


    public function syncImageFromUrl($instance, $url , $pathinfo = [])
    {
        $driver = config('fitin.asset.filesystem_driver');
        
        $type = get_class($instance)::DIR_NAME;
        $subPath = \App\Helpers\Utils::locationPath($pathinfo);

        $file_name = $instance->getKey();
        $file_name = "$file_name.png";
        $path = "$type/$subPath/$file_name";
        $data = file_get_contents($url);   
        $upload = Storage::disk($driver)->put($path, $data);

        uploadDriver($file_name, $driver, $path, Storage::disk($driver)->url($path), $instance);

        $data = [
            "file_name" => $file_name,
            "url" =>  Storage::disk($driver)->url($path),
            "file_path" =>  "$path"
        ]; 

        return $data;          
    }


    

}
