<?php

namespace App\Services;

use App\Models\Ver2\Object3d;
use App\Models\Ver2\V360;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Validator;
use App\FitIn\RabbitMQ;


class AssetService
{

    public function generateMp4($object){
        if(!$object->v360s || count($object->v360s) < 60){
            return false;
        }
        $json = json_encode([
            'name' => $object->name,
            'type' => 'mp4'
        ]);
        $rabbitmq = new RabbitMQ();
        $rabbitmq->sendMessages($json);
        return true;
    }
}
