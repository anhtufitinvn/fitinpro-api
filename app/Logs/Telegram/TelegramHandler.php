<?php
namespace App\Logs\Telegram;

use Monolog\Logger;
use Monolog\Handler\AbstractProcessingHandler;

use Illuminate\Support\Facades\Auth;

use Telegram\Bot\Api;

class TelegramHandler extends AbstractProcessingHandler
{
    protected $bot;
    protected $chatId;
    public function __construct(array $config,int $level = Logger::DEBUG,bool $bubble = true) {
        $this->bot = new Api($config['botKey']);
        $this->chatId = $config['chatId'];
        parent::__construct($level, $bubble);
    }
    protected function write(array $record) {
        
        $messageArray = [
            'chat_id' => $this->chatId,
            'text' => mb_substr($record['message'], 0, 4095),
            'parse_mode' => 'html',
            'disable_web_page_preview' => true,
        ];

        
        $this->bot->sendMessage($messageArray);
    }
}

