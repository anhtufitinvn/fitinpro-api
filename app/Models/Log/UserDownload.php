<?php

namespace App\Models\Log;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Traits\CacheTrait;

class UserDownload extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'UserDownloadLogs';

    protected $fillable = [
        "userId" ,
        "key",
        "model",
        "driver",
        "path",
        "url",
        "address"
       
    ];

    public const CACHE_TAG = 'user-download';

}