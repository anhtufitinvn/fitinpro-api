<?php

namespace App\Models\Log;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Traits\CacheTrait;

class RenderLog extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'RenderLogs';

    protected $fillable = [
        "payloadId" ,
        "email",
        "userId",
        "time",
        "environment",
        "serverName",
        "versionBuild",
        "type",
        "content"
       
    ];

    public const CACHE_TAG = 'render-log';

}