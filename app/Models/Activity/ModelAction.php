<?php

namespace App\Models\Activity;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Traits\CacheTrait;

class ModelAction extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'ModelActions';

    protected $fillable = [
        "model",
        "type",
        "action",
        "param",
        "description"
       
    ];

    public const CACHE_TAG = 'user-model-actions';

}