<?php

namespace App\Models\Ver2\RefactorModel;

use App\Models\Ver2\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;
use App\Traits\CacheTrait;
use App\Traits\AttributeSchemaTrait;

class Objects extends Model {

    use AttributeSchemaTrait, CacheTrait;

    protected $collection = 'Objects';
    protected $primaryKey = 'name';

    public const CACHE_TAG = 'object3d';

    const PENDING = 'pending';
    const PUBLISHED = 'published';

    const MODEL_TYPE = "model";

    const DIR_NAME = 'objects';
    const ASSET_DIR = 'assets';
    const THUMB_DIR = 'thumb';
    const THUMB_MATERIAL_DIR = 'thumb/material';
    const FBX_DIR = 'fbx';
    const PREFAB_DIR = 'prefab';
    const IOS_DIR = 'ios';
    const ANDROID_DIR = 'android';
    const GLTF_DIR = 'gltf';
    const TEXTURE_DIR = 'textures';

    const UNITY_DRIVER = "unity";
    const UNITY_WEB_DIR = "webgl-assets";

    protected $fillable = [
        //"dimension",
        //"enable_dimension",
        '_id',
        "keywords",
        "vendor",
        "attributes",
        "ownerID",
        "category",
        "name",
        "groupName",
        "brand",
        "project",
        "layout",
        "thumb",
        "fbx",
        //"prefab",
        //"prefabType",
        //"colorRange" ,
        "ar",
        "desc" ,
        "status" ,
        //"editorProAsset",
        //"isPrimary" ,
        //"isFloat",
        //"isHole",
        //"dependency",
        //--"hdrp" ,
       
        "materials", 
        "gltf",
        //"anchor",
        //"renderable",
        //"isTTS",
        //'position',
        //'positionXYZ',
        //'rotation',
        //'rotationXYZ',
        //'scale',
        //'scaleXYZ',
        'unity',
        'editorPro'
    ];


    protected $sub_attributes = [
        'attributes' => [
            "dimension",
            "colorRange",
            "isPrimary" ,
            "isFloat",
            "isHole",
            "dependency",
            "enable_dimension",
            "anchor",
            "renderable",
            "isTTS",
            'position',
            'positionXYZ',
            'rotation',
            'rotationXYZ',
            'scale',
            'scaleXYZ',
        ],
        'unity' => [
            'prefab',
            'prefabType'
        ],
        'editorPro' => [
            'asset'
        ],
        'ar' => [
            'android',
            'ios'
        ]
    ];

    protected $appends = ['fbxUrl', 'thumbUrl', 'gltfUrl',  'thumbMaterialUrl'];

    protected $attributes_schema = [
       
        'name' => 'string',
        'brand' => 'string',
        'ar' => 'array',
        'attributes' => 'array',
        'materials' => 'array',
        'editorPro' => 'array',
        'unity' => 'array',
    ];

    protected $strtolower = [
        "brand", "vendor", "category", "name", "groupName"
    ];
    
    public function scopeWithoutTTS($query){
        return $query->whereNull('attributes.isTTS')->orWhere('attributes.isTTS', false);
    }

    public function scopeWithTTS($query){
        return $query->whereNotNull('attributes.isTTS')->where('attributes.isTTS', true);
    }

    public function textures(){
        return $this->hasMany(Texture::class, 'group', 'name');
    }

    public function bundles(){
        return $this->belongsToMany(Bundle::class, null , 'object3d_ids', 'bundle_ids');
    }

    public function _category(){
        return $this->belongsTo(Category::class, 'category', 'code');
    }

    public function _owner(){
        return $this->belongsTo(User::class, 'ownerID', 'auth_id');
    }

    public function getThumbUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->thumb ? Storage::disk($driver)->url($this->thumb) : config('fitin.default_asset_image');
    }

    public function getFbxUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->fbx ? Storage::disk($driver)->url($this->fbx) : null;
    }

    public function getUnityAttribute($value){
        $driver = self::UNITY_DRIVER;
        $unity = $value;
        if(!empty($unity) && is_array($unity)){    
            $unity['prefabUrl'] = !empty($unity['prefab']) ? Storage::disk($driver)->url($unity['prefab']) : null;
        }

        return $unity;
    }

    public function getEditorProAttribute($value){
        $driver = config('fitin.editor.filesystem_driver');
        $editorPro = $value;
        if(!empty($editorPro) && is_array($editorPro)){    
            $editorPro['assetUrl'] = !empty($editorPro['asset']) ? Storage::disk($driver)->url($editorPro['asset']) : null;
        }

        return $editorPro;
    }

    public function getArAttribute($value){
        $driver_ar = config('fitin.asset.filesystem_driver');
        $ar = $value;
        if(!empty($ar) && is_array($ar)){
            
            $ar['iosUrl'] = !empty($ar['ios']) ? Storage::disk($driver_ar)->url($ar['ios']) : null;
            $ar['androidUrl'] = !empty($ar['android']) ? Storage::disk($driver_ar)->url($ar['android']) : null;
        }
        return $ar;
    }

    // public function getPrefabUrlAttribute(){
    //     $driver = config('fitin.asset.filesystem_driver');
    //     return $this->prefab ? Storage::disk($driver)->url($this->prefab) : null;
    // }

    public function getGltfUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->gltf ? Storage::disk($driver)->url($this->gltf) : null;
    }

    public function getThumbMaterialUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->thumbMaterial ? Storage::disk($driver)->url($this->thumbMaterial) : null;
    }

    // public function getEditorProAssetUrlAttribute(){
    //     $driver = config('fitin.editor.filesystem_driver');
    //     return $this->editorProAsset ? Storage::disk($driver)->url($this->editorProAsset) : null;
    // }

    public function getImageAttribute(){
        return $this->thumbUrl;
    }

    public function getImageUrlAttribute()
    {
        return $this->thumbUrl;
    }

    public function getDownloadUrlAttribute(){
        return Utils::editorStoreRoute('editor.object.download', ['key' => $this->name, 'id' => $this->_id]);
    }

    public function getEditorProUploadVersionAttribute(){
        if(!$this->editorProAsset){
            return 0;
        }else{
            $pathArray = explode('/', $this->editorProAsset);
            if (count($pathArray) < 3){
                return 0;
            }
            return $pathArray[count($pathArray) - 3];
        }
    }

    public static function getEditorProCollection(){
        return Object3d::whereNotNull('editorProAsset')->get();
    }
}