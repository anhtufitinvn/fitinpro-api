<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FileDriver extends Eloquent {

    protected $collection = 'FileDrivers';

    protected $fillable = [
        "file" ,
        "driver",
        "path",
        "url" ,
        "key",
        "model"   
    ];
    
}