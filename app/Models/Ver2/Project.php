<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Traits\CacheTrait;
use Illuminate\Support\Facades\Storage;

class Project extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'Projects';

    protected $fillable = [

        "name" ,
        "status",
        "code",
        "image",
        'ecom_id'
    ];
    
    public const CACHE_TAG = 'project';
    const PUBLISHED = "published";
    const PENDING = "pending";
    const MODEL_TYPE = "project";
    const DIR_NAME = "projects";

    protected $appends = ['imageUrl'];

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.asset.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image'); 
    }

    public static function getEditorProCollection(){

        $projects = Project::where('status', self::PUBLISHED)->get();
        return $projects;
    }

    public function scopePublished($query)
    {
        return $query->where('status', self::PUBLISHED);
    }
}