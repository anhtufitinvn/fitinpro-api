<?php

namespace App\Models\Ver2;

use App\Models\Ver2\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Traits\CacheTrait;

use App\Helpers\Utils;
use App\Traits\BootTrait;

class BundleRender extends Model {

    use CacheTrait;
    
    protected $collection = 'BundleRenders';
    protected $primaryKey = 'key_name';

    protected $fillable = [
        'key_name',
        'dataRender'
    ];
    
    public const CACHE_TAG = 'bundle-render';

}