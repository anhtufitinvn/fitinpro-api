<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Traits\CacheTrait;
use Illuminate\Support\Facades\Storage;

class Style extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'Styles';

    protected $fillable = [
        
        "name",
        "status",
        "code",
        "image",
        'ecom_id'
    ];
   
  

    public const CACHE_TAG = 'style';
    const DIR_NAME = "styles";
    const PUBLISHED = "published";
    const PENDING = "pending";

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.asset.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image'); 
    }

  

    public function layouts()
    {
        return $this->hasMany(Layout::class);
    }

    public function scopePublished($query)
    {
        return $query->where('status', self::PUBLISHED);
    }
}