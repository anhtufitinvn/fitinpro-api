<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class LayoutName extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'layout-name';

    protected $primaryKey = 'slug';

    protected $fillable = [

        "name",
        "slug",
        "group",
        "code",
        "hex",
        "image",
        "usedCount"
    ];

    public const CACHE_TAG = 'color';
    const MODEL_TYPE = "color";
    
    const DIR_NAME = 'colors';
    public function getImageUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : null;
    }

    public function color_group(){
        return $this->belongsTo(ColorGroup::class, 'group', 'code');
    }
}