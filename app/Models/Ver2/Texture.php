<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use \App\Helpers\Utils;
use App\Traits\CacheTrait;
use App\Jobs\MakeThumbTexture;

class Texture extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'Textures';
    public const CACHE_TAG = 'texture';
    const DIR_THUMB_1024 = "1024";

    protected $fillable = [      
        'public',
        'group',
        'brand',
        'file',
        'imagePath'
    ];
    const THUMB_DEFAULT_SIZE = 512;
    const THUMB_DEFAULT_MIME = 'jpeg';
    const THUMB_SIZE = [
        // '256x256' => [
        //     'width' => 256,
        //     'height' => 256,
        // ],
        '512x512' => [
            'width' => 512,
            'height' => 512,
        ],
        '1024x1024' => [
            'width' => 1024,
            'height' => 1024,
        ],
        '2048x2048' => [
            'width' => 2048,
            'height' => 2048,
        ]
    ];

    protected $appends = ['url'];

    public function object(){
        return $this->belongsTo(Object3d::class);
    }

    public function getUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        $file_name_array = pathinfo($this->file);
        $file_name = $file_name_array['filename'] . "." . self::THUMB_DEFAULT_MIME;
        $path_to_file = Utils::locationPath(Object3d::DIR_NAME, $this->brand, $this->group, Object3d::TEXTURE_DIR, self::THUMB_DEFAULT_SIZE, $file_name);
        return Storage::disk($driver)->url($path_to_file);
    }

    public function getFullSizeUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        $path_to_file = $this->imagePath ?? Utils::locationPath(Object3d::DIR_NAME, $this->brand, $this->group, Object3d::TEXTURE_DIR, $this->file);
        return Storage::disk($driver)->url($path_to_file);
    }

    public function deleteFile(){
        $driver = config('fitin.asset.filesystem_driver');
        $path_to_file = $this->imagePath ?? Utils::locationPath(Object3d::DIR_NAME, $this->brand, $this->group, Object3d::TEXTURE_DIR, $this->file);
        Storage::disk($driver)->delete($path_to_file);

        $file_name_array = pathinfo($this->file);
        $file_name = $file_name_array['filename'] . "." . self::THUMB_DEFAULT_MIME;
        foreach (self::THUMB_SIZE as $_size){
            $size = $_size['width'];
            $path_to_file = Utils::locationPath(Object3d::DIR_NAME, $this->brand, $this->group, Object3d::TEXTURE_DIR, $size, $file_name);
            Storage::disk($driver)->delete($path_to_file);
        }
    }

    public function makeMultiThumb()
    {
        
        $sizes = self::THUMB_SIZE;
        $driver = config('fitin.asset.filesystem_driver');
        foreach ($sizes as $size => $arrAttr) {
            $image_path = Storage::disk($driver)->path(Utils::locationPath(Object3d::DIR_NAME, $this->brand, $this->group, Object3d::TEXTURE_DIR, $this->file));
            
            if(!file_exists($image_path)){
                continue;
            }
            $oImageManager = Image::make($image_path);
            $originWidth = $oImageManager->width();
            if($arrAttr['width'] >= $originWidth && $arrAttr['width'] > self::THUMB_DEFAULT_SIZE){
                continue;
            }
            $higher = ($arrAttr['width'] >= $arrAttr['height']) ? $arrAttr['width'] : $arrAttr['height'];
            $oImageManager->fit($higher, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            //resizeCanvas keep the origin shape of image and fill additional blank to get the wanted width-height 
           // $oImageManager->resizeCanvas($arrAttr['width'],$arrAttr['height'], 'center', false, 'FFF');
            $oImageManager->stream();
            
            $arrFileName = pathinfo($this->file);
            $folderThumb = $arrAttr['width'];

            $newFileName = $arrFileName['filename'] . "." . self::THUMB_DEFAULT_MIME;
            $pathUpload = Utils::locationPath(Object3d::DIR_NAME, $this->brand, $this->group, Object3d::TEXTURE_DIR, $folderThumb,  $newFileName);
            
            Storage::disk($driver)->put($pathUpload, $oImageManager);
            
            uploadDriver($newFileName, $driver, $pathUpload , Storage::disk($driver)->url($pathUpload), $this);

        }
        return $this;
    }

    public function makeMultiThumbByQueue()
    {
        MakeThumbTexture::dispatch($this)->onQueue('3dresource');
        return $this;
    }
}