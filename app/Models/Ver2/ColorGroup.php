<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Traits\CacheTrait;

class ColorGroup extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'ColorGroups';

    protected $primaryKey = 'code';
    
    protected $fillable = [
        "name",
        "code",
        "hex",
        "image",
        "status"
    ];
    const MODEL_TYPE = "color";

    const PENDING = 'pending';
    const PUBLISHED = 'published';

    public const CACHE_TAG = 'color-group';

    public function scopePublished($query){
        return $query->where('status', self::PUBLISHED);
    }

    public function colors(){
        return $this->hasMany(Color::class, 'group', 'code');
    }

    public static function getEditorProCollection(){

        //Jens Hasmany not working with custom foreign key

        $colorGroups = ColorGroup::published()->get();
        foreach ($colorGroups as $group){
            $group->colors = Color::where('group', $group->code)->get();
        }
        
        return $colorGroups;
    }
}