<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class MaterialType extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'MaterialTypes';
    public const CACHE_TAG = 'material-type';

   
    protected $fillable = [       
        'name', 
        'code'
    ];

    public function materials(){
        return $this->belongsToMany(Material::class, null, 'type_ids', 'material_ids');
    }

}