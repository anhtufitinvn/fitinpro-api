<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Traits\CacheTrait;

class TemplateBundle extends Eloquent {

    use CacheTrait, SoftDeletes;
    
    protected $collection = 'TemplateBundles';

    protected $fillable = [   
        'dataJson',
        'userId',
        'imagePath',
        "name"
    ];

    public const CACHE_TAG = 'template-bundle';
    const DIR_NAME = 'users';
    const THUMB_DIR = 'thumb';

    protected $appends = ['image'];


    public function getImageAttribute()
    {
        $driver = config('fitin.asset.filesystem_driver');
        return $this->imagePath ? Storage::disk($driver)->url($this->imagePath) : 'https://object-dev.fitin3d.com/images/cube.png';
    
    }

}