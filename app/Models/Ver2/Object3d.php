<?php

namespace App\Models\Ver2;

use App\Models\Ver2\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

use App\Traits\AttributeSchemaTrait;
use App\Traits\BootTrait;
class Object3d extends Model {

    use AttributeSchemaTrait,  BootTrait;

    protected $collection = 'Objects';
    protected $primaryKey = 'name';

    public const CACHE_TAG = 'object3d';

    const PENDING = 'pending';
    const PUBLISHED = 'published';

    const MODEL_TYPE = "model";

    const DIR_NAME = 'objects';
    const ASSET_DIR = 'assets';
    const THUMB_DIR = 'thumb';
    const THUMB_EDITORPRO_DIR = 'thumb';
    const THUMB_MATERIAL_DIR = 'thumb/material';
    const FBX_DIR = 'fbx';
    const PREFAB_DIR = 'prefab';
    const IOS_DIR = 'ios';
    const ANDROID_DIR = 'android';
    const WEB_AR_IOS_DIR = 'web-ar/ios';
    const WEB_AR_ANDROID_DIR = 'web-ar/android';
    const GLTF_DIR = 'gltf';
    const TEXTURE_DIR = 'textures';

    const UNITY_DRIVER = "unity";
    const UNITY_WEB_DIR = "webgl-assets";

    protected $fillable = [
        //"dimension",
        //"enable_dimension",
        "displayName",
        "keywords",
        "vendor",
        "attributes",
        "ownerID",
        "category",
        "name",
        "groupName",
        "brand",
        "project",
        "layout",
        "thumb",
        "thumbMaterial",
        "video",
        "fbx",
        //"prefab",
        //"prefabType",
        //"colorRange" ,
        "ar",
        "webAr",
        "desc" ,
        "status" ,
        //"editorProAsset",
        //"isPrimary" ,
        //"isFloat",
        //"isHole",
        //"dependency",
        //--"hdrp" ,
       
        "materials", 
        "gltf",
        //"anchor",
        //"renderable",
        //"isTTS",
        //'position',
        //'positionXYZ',
        //'rotation',
        //'rotationXYZ',
        //'scale',
        //'scaleXYZ',
        'unity',
        'editorPro'
    ];


    protected $sub_attributes = [
        'attributes' => [
            "dimension",
            "colorRange",
            "isPrimary" ,
            "isFloat",
            "isHole",
            "dependency",
            "enable_dimension",
            "anchor",
            "renderable",
            "isTTS",
            'position',
            'positionXYZ',
            'rotation',
            'rotationXYZ',
            'scale',
            'scaleXYZ',
            'unit',
            'type'
        ],
        'unity' => [
            'prefab',
            'prefabType'
        ],
        'editorPro' => [
            'asset',
            'thumb'
        ],
        'ar' => [
            'android',
            'ios'
        ],
        'webAr' => [
            'android',
            'ios'
        ],
    ];

    protected $appends = ['fbxUrl', 'thumbUrl', 'gltfUrl',  'thumbMaterialUrl', 'v360'];

    protected $attributes_schema = [
       
        'name' => 'string',
        'brand' => 'string',
        'ar' => 'array',
        'webAr' => 'array',
        'attributes' => 'array',
        'materials' => 'array',
        'editorPro' => 'array',
        'unity' => 'array',
    ];

    protected $strtolower = [
        "brand", "vendor", "category", "name", "groupName"
    ];
    
    
    public function scopeWithoutTTS($query){
        return $query->whereNull('attributes.isTTS')->orWhere('attributes.isTTS', false);
    }

    public function scopeWithTTS($query){
        return $query->whereNotNull('attributes.isTTS')->where('attributes.isTTS', true);
    }

    public function textures(){
        return $this->hasMany(Texture::class, 'group', 'name');
    }

    public function bundles(){
        return $this->belongsToMany(Bundle::class, null , 'object3d_ids', 'bundle_ids');
    }

    public function _category(){
        return $this->belongsTo(Category::class, 'category', 'code');
    }

    public function _owner(){
        return $this->belongsTo(User::class, 'ownerID', 'auth_id');
    }

    public function v360s(){
        return $this->hasMany(V360::class, 'sku', 'name');
    }

    public function getV360Attribute(){
        if($count = count($this->v360s)){
            $bound = (int) ($count / 6);

            $frames = [$bound * 1 , $bound * 2, $bound * 3, $bound * 4, $bound * 5, $bound * 6];
            $result = $this->v360s->whereIn('frame', $frames);
            return $result;
        }
        return [];
    }

    public function getThumbUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->thumb ? Storage::disk($driver)->url($this->thumb) : config('fitin.default_asset_image');
    }

    public function getVideoUrlAttribute(){
        $driver = config('fitin.video.filesystem_driver');
        return $this->video ? Storage::disk($driver)->url($this->video) : null;
    }

    public function getFbxUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->fbx ? Storage::disk($driver)->url($this->fbx) : null;
    }

    public function getUnityAttribute($value){
        $driver = self::UNITY_DRIVER;
        $unity = $value;
        if(!empty($unity) && is_array($unity)){    
            $unity['prefabUrl'] = !empty($unity['prefab']) ? Storage::disk($driver)->url($unity['prefab']) : null;
        }

        return $unity;
    }

    public function getMaterialsAttribute($materials){
        $driver = config('fitin.asset.filesystem_driver');
        if(!empty($materials) && is_array($materials)){
            foreach ($materials as &$material){
                if(!empty($material) && is_array($material)){    
                    $material['aoMap'] = !empty($material['aoMapPath']) ? Storage::disk($driver)->url($material['aoMapPath']) : (!empty($material['aoMap']) ? $material['aoMap'] : null);
                    $material['map'] = !empty($material['mapPath']) ? Storage::disk($driver)->url($material['mapPath']) : (!empty($material['map']) ? $material['map'] : null);
                    $material['normalMap'] = !empty($material['normalMapPath']) ? Storage::disk($driver)->url($material['normalMapPath']) : (!empty($material['normalMap']) ? $material['normalMap'] : null);
                }
            }
        }
        
        

        return $materials;
    }

    public function setMaterialsAttribute($materials){
        $driver = config('fitin.asset.filesystem_driver');
        $static_url = config('fitin.asset.static_url');
        if(!empty($materials) && is_array($materials))
        {
            foreach ($materials as &$material){   

                if(!empty($material['aoMap'])){
                    $material['aoMapPath'] = (!empty($material['aoMapPath'])) ? $material['aoMapPath'] : str_replace("$static_url", "", $material['aoMap']);
                }
                if(!empty($material['map'])){
                    $material['mapPath'] = (!empty($material['mapPath'])) ? $material['mapPath'] :  str_replace("$static_url", "", $material['map']);
                }
                if(!empty($material['normalMap'])){
                    $material['normalMapPath'] = (!empty($material['normalMapPath'])) ? $material['normalMapPath'] :  str_replace("$static_url", "", $material['normalMap']);
                }
            }
        }
        

        $this->attributes['materials'] = $materials;
    }

    public function getEditorProAttribute($value){
        $driver = config('fitin.editor.filesystem_driver');
        $editorPro = $value;
        if(!empty($editorPro) && is_array($editorPro)){    
            $editorPro['assetUrl'] = !empty($editorPro['asset']) ? Storage::disk($driver)->url($editorPro['asset']) : null;
        }

        return $editorPro;
    }

    public function getArAttribute($value){
        $driver_ar = config('fitin.asset.filesystem_driver');
        $ar = $value;
        if(!empty($ar) && is_array($ar)){
            
            $ar['iosUrl'] = !empty($ar['ios']) ? Storage::disk($driver_ar)->url($ar['ios']) : null;
            $ar['androidUrl'] = !empty($ar['android']) ? Storage::disk($driver_ar)->url($ar['android']) : null;
        }
        return $ar;
    }

    public function getWebArAttribute($value){
        $driver_ar = config('fitin.asset.filesystem_driver');
        $ar = $value;
        if(!empty($ar) && is_array($ar)){
            
            $ar['iosUrl'] = !empty($ar['ios']) ? Storage::disk($driver_ar)->url($ar['ios']) : null;
            $ar['androidUrl'] = !empty($ar['android']) ? Storage::disk($driver_ar)->url($ar['android']) : null;
        }
        return $ar;
    }

    public function scopePublished($q){
        $q->where('status',self::PUBLISHED);
    }
    
    /******************************************************************************
     * DEPRECATED ATTRIBUTE 
     */

    public function getPrefabAttribute(){
        $unity = $this->unity;
        if(!empty($unity) && is_array($unity)){    
            return $unity['prefab'] ?? null;
        }
        return null;
    }
 
    public function getPrefabUrlAttribute(){
        $driver = self::UNITY_DRIVER;
        return $this->prefab ? Storage::disk($driver)->url($this->prefab) : null;
    }

    public function getEditorProAssetAttribute(){
        $editorPro = $this->editorPro;
        if(!empty($editorPro) && is_array($editorPro)){    
            return $editorPro['asset'] ?? null;
        }
        return null;
    }

    public function getEditorProAssetUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        return $this->editorProAsset ? Storage::disk($driver)->url($this->editorProAsset) : null;
    }

    // END DEPRECATED =============================================================




    public function getGltfUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->gltf ? Storage::disk($driver)->url($this->gltf) : null;
    }

    public function getThumbMaterialUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->thumbMaterial ? Storage::disk($driver)->url($this->thumbMaterial) : null;
    }


    public function getImageAttribute(){
        return $this->thumbUrl;
    }

    public function getImageUrlAttribute()
    {
        return $this->thumbUrl;
    }

    public function getEditorProImageUrlAttribute()
    {
        $editorPro = $this->editorPro;
        if($editorPro && is_array($editorPro) && !empty($editorPro['thumb'])){
            $driver = config('fitin.editor.filesystem_driver');
            return Storage::disk($driver)->url($editorPro['thumb']);
        }
        return $this->thumbUrl;
    }

    public function getDownloadUrlAttribute(){
        return Utils::editorStoreRoute('editor.object.download', ['key' => $this->name, 'id' => $this->_id]);
    }

    public function getEditorProUploadVersionAttribute(){
        if(!$this->editorProAsset){
            return 0;
        }else{
            $pathArray = explode('/', $this->editorProAsset);
            if (count($pathArray) < 3){
                return 0;
            }
            return $pathArray[count($pathArray) - 3];
        }
    }

    public static function getEditorProCollection(){
        return Object3d::whereNotNull('editorPro.asset')->where('attributes.type', '3d')->get();
    }

    public function scopeEditorPro($query){
        return $query->whereNotNull('editorPro.asset')->where('attributes.type', '3d');
    }
}