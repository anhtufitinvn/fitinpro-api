<?php

namespace App\Models\Ver2;

use App\Models\Ver2\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Traits\CacheTrait;
use App\Traits\AttributeSchemaTrait;
use App\Helpers\Utils;
use App\Traits\BootTrait;

class Bundle extends Model {

    use AttributeSchemaTrait, BootTrait;
    
    protected $collection = 'Bundles';
    protected $primaryKey = 'key_name';

    protected $fillable = [
        'category',
        'parent',
        "displayName",
        //'isTemplate',
        'room',
        'brand',
        'vendor',
        'object3d_ids',
        'json',
        'revision',
        'name',
        'status',
        //'hidden',
      
        'data_json',
        'item_list',
        'key_name',
        //'isEditor',
        //'isAndroid',
        //'isIos',
        'imagePath',
        'ownerId',
        
        //'position',
        //'positionXYZ',
        //'rotation',
        //'rotationXYZ',
        //'scale',
        //'scaleXYZ',
        'gltf',
        'settings',
        'attrs',
        'editorPro',
        'homestylerReference'
    ];
    
    const PENDING = 'pending';
    const PUBLISHED = 'published';

    const MODEL_TYPE = "bundle";
    
    const DIR_NAME = 'bundles';
    const GLTF_DIR = 'gltf';
    const THUMB_DIR = 'thumb';
    const JSON_DIR = 'json';
    public const CACHE_TAG = 'bundle';

    protected $sub_attributes = [
        'attrs' => [
            "dimension",
            "isTemplate",
            "hidden",
            "isEditor",
            "isAndroid",
            "isIos",
            "position",
            "positionXYZ",
            "rotation",
            "rotationXYZ",
            "scale",
            "scaleXYZ"
        
        ],
        
        'editorPro' => [
            'json'
        ]
    ];

    protected $appends = ['image', 'gltfUrl'];

    protected $attributes_schema = [
        'item_list' => 'array',
        'name' => 'string',
        'key_name' => 'string',
        'data_json' => 'array',
        'editorPro' => 'array',
        'attrs' => 'array',
        'settings' => 'array'
    ];

    protected $strtolower = [
        "key_name"
    ];

   
    
    public function getImageAttribute()
    {
        $driver = config('fitin.asset.filesystem_driver');
        return $this->imagePath ? \Illuminate\Support\Facades\Storage::disk($driver)->url($this->imagePath) : config('fitin.default_asset_image');
        //return $this->attachment ? $this->attachment->url : '';
    }

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.asset.filesystem_driver');
        return $this->imagePath ? \Illuminate\Support\Facades\Storage::disk($driver)->url($this->imagePath) : config('fitin.default_asset_image');
    }


    public function getGltfUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->gltf ? \Illuminate\Support\Facades\Storage::disk($driver)->url($this->gltf) : null;
    }
    
    public function getEditorProAttribute($value){
        $driver = config('fitin.asset.filesystem_driver');
        $editorPro = $value;
        if(!empty($editorPro) && is_array($editorPro)){    
            $editorPro['jsonUrl'] = !empty($editorPro['json']) ? Storage::disk($driver)->url($editorPro['json']) : null;
        }

        return $editorPro;
    }
    
    public function _category()
    {
        return $this->belongsTo(Category::class, 'category', 'code');
    }

    public function _room()
    {
        return $this->belongsTo(Room::class, 'room', 'code');
    }

    public function _brand()
    {
        return $this->belongsTo(Brand::class, 'brand', 'code');
    }

    public function _vendor()
    {
        return $this->belongsTo(Vendor::class, 'vendor', 'code');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'ownerId', 'auth_id');
    }

    public function object3ds()
    {
        return $this->belongsToMany(Object3d::class, null, 'bundle_ids', 'object3d_ids');
    }


    public function scopePublished($q){
        $q->whereNotNull('status')->where('status',self::PUBLISHED);
    }
   

    public function getDownloadUrlAttribute(){
        return Utils::editorStoreRoute('editor.bundle.json.download', ['key' => $this->key_name, 'id' => $this->_id]);
    }

    public function getEditorProUploadVersionAttribute(){
        return $this->revision;
    }

    public static function getEditorProCollection(){
        return Bundle::whereNull('parent')->whereNotNull('editorPro')->get();
    }
}