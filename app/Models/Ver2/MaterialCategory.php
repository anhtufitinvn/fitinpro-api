<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class MaterialCategory extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'MaterialCategories';
    public const CACHE_TAG = 'material-category';

   
    protected $fillable = [       
        'name', 
        'code'
    ];

    public function materials(){
        return $this->belongsToMany(Material::class, null, 'category_ids', 'material_ids');
    }

}