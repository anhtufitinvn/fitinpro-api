<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Traits\CacheTrait;

class Attachment extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'Attachments';

    protected $fillable = [
        'id',
        'path',
        'size', 
        'mime_type', 
        'name',
        'filename',
        'origin_name',
        'type',

        "attachmentable_id",
        "attachmentable_type",
        "meta",
        "reference"
    ];
   
    public const CACHE_TAG = 'attachment';

    const AVATAR_SIZE = [
        'width' => 128,
        'height' => 128,
    ];

    const THUMB_SIZE = [
        '512x512' => [
            'width' => 512,
            'height' => 512,
        ],
        '256x256' => [
            'width' => 256,
            'height' => 256,
        ],
        '128x128' => [
            'width' => 128,
            'height' => 128,
        ]
    ];

    protected $appends = ['url'];

    const FOLDER_THUMB = 'thumbs';

    public function attachmentable()
    {
        return $this->morphTo();
    }

    public function getUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return  Storage::disk($driver)->url($this->path. '/'. $this->filename);
        //return ($this->type == 'image') ? Storage::url($this->path. '/'. $this->filename) : 'https://www.gravatar.com/avatar/00000000000000000000000000000000';
    }

    public function makeMultiThumb()
    {
        $sizes = Attachment::THUMB_SIZE;
        $driver = config('fitin.asset.filesystem_driver');
        foreach ($sizes as $size => $arrAttr) {
            $image_path = Storage::disk($driver)->path($this->path.'/'.$this->filename);
            if(!file_exists($image_path)){
                continue;
            }
            $oImageManager = Image::make($image_path);
            $oImageManager->resize($arrAttr['width'], null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $oImageManager->resizeCanvas($arrAttr['width'],$arrAttr['height'], 'center', false, 'FFF');
            $oImageManager->stream();
            
            $pathUpload = "images/". self::FOLDER_THUMB.'/' . $this->path . '/' . $size . '-' . $this->filename;

            Storage::disk($driver)->put($pathUpload, $oImageManager);
        }
        return true;
    }
}