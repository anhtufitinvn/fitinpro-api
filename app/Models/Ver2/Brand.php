<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class Brand extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'Brands';
    protected $primaryKey = 'code';
    protected $fillable = [
        "name" ,
        "code",
        //"slug",
        "status",
        "active",
        "image",
        "ecom_id"
       
    ];
    
    public const PENDING = "pending";
    public const PUBLISHED = "published";
    public const TRASH = "trash";

    public const CACHE_TAG = 'brand';

    //protected $appends = ['image'];

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.asset.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : null;
    }
}