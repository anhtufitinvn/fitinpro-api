<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class Category extends Eloquent {

    use CacheTrait;
    const MODEL_TYPE = "category";
    protected $collection = 'Categories';
    protected $primaryKey = 'code';
    protected $fillable = [
        "name" ,
        //"slug",
        "code",
        "parent_id",
        "active",
        "status",
        "sort" ,
        "is_menu",
        "image",
        "publishedEcom",
        'ecom_id'
       
    ];
    
    public const PENDING = "pending";
    public const PUBLISHED = "published";
    public const TRASH = "trash";

    public const CACHE_TAG = 'categories';

    public function scopePublished($q){
        $q->whereNotNull('status')->where('status',self::PUBLISHED);
    }
    
    public function getImageUrlAttribute()
    {
        $driver = config('fitin.asset.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image');;
    }

    public static function getEditorProCollection(){

        $categories = Category::where('status', self::PUBLISHED)->get();
        return $categories;
    }
}