<?php

namespace App\Models\Ver2;

use App\FitIn\LayoutJson;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Traits\CacheTrait;
use \App\Helpers\Utils;
use App\Traits\BootTrait;
use Illuminate\Support\Facades\Storage;

class Layout extends Eloquent {

    use BootTrait;
    
    protected $collection = 'Layouts';
    const PENDING = 'pending';
    const PUBLISHED = 'published';
    const TRASH = 'trash';

    public const CACHE_TAG = 'layout';
    const MODEL_TYPE = "layout";
    const DIR_NAME = "layouts";
    const THUMB_DIR = 'thumb';
    const FBX_DIR = "fbx";
    const GLTF_DIR = "gltf";
    const JSON_DIR = "json";
    const PREFAB_DIR = "prefab";
    protected $primaryKey = 'name';
    protected $fillable = [
        "displayName",
        "name" ,
        "ownerId",
        "project" ,
        "style",
        'fbx',
        'gltf',
        //'prefab',
        "tag",
        "status",
        'image',
        "published_at",
        "description",
        "isTemplate",
     //   "editor",
        //"json",
        "editorPro",
        "dataJson",
        //"revision"
    ];

    protected $attributes_schema = [
        'json' => 'string',
        'dataJson' => 'array',
        'isTemplate' => 'boolean'
    ];

    protected $tts_attributes = [
        'name', 
        'layout', 
        'fbxUrl', 
        'fbx', 
        'gltfUrl', 
        'gltf', 
        'attributes' => ['position', 'scale', 'rotation'], 
        'materials',
        'editorPro' => [
            "prefab",
            "json",
            "revision"
        ]
        
    ];


    protected $appends = ['imageUrl'];

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.asset.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : null;
    
    }

    public function getFbxUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->fbx ? Storage::disk($driver)->url($this->fbx) : null;
    }

    public function getGltfUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->gltf ? Storage::disk($driver)->url($this->gltf) : null;
    }

    public function getJsonUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        $editorPro = $this->getOriginal('editorPro');
        $url = !empty($editorPro['json']) ? Storage::disk($driver)->url($editorPro['json']) : null;
        return $url;
    }

    public function getPrefabUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        $editorPro = $this->getOriginal('editorPro');
        $url = !empty($editorPro['prefab']) ? Storage::disk($driver)->url($editorPro['prefab']) : null;
        return $url;
    }

    public function getEditorProAttribute($value){
        
        $editorPro = $value;
        $assetdriver = config('fitin.editor.filesystem_driver');
        if(!empty($editorPro) && is_array($editorPro)){    
            $editorPro['prefabUrl'] = !empty($editorPro['prefab']) ? Storage::disk($assetdriver)->url($editorPro['prefab']) : null;
            $editorPro['jsonUrl'] = !empty($editorPro['json']) ? Storage::disk($assetdriver)->url($editorPro['json']) : null;
        }

        return $editorPro;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function style()
    {
        return $this->belongsTo(Style::class);
    }

    public function parent()
    {
        return $this->belongsTo(Layout::class, 'parentId', '_id');
    }

    public function scopePublished($query)
    {
        return $query->where('status', self::PUBLISHED);
    }



    public function getDownloadUrlAttribute(){

        if(!$this->editorPro || (is_array($this->editorPro) && empty($this->editorPro['json']))){
            return null;
        }
        return Utils::editorStoreRoute('editor.layout.download', ['id' => $this->_id, 'name' => $this->name]);
    }

    public function getDownloadPrefabUrlAttribute(){
        if(!$this->editorPro || (is_array($this->editorPro) && empty($this->editorPro['prefab']))){
            return null;
        }
        return Utils::editorStoreRoute('editor.layout.prefab.download', ['id' => $this->_id, 'name' => $this->name]);
    }

    public function getEditorProUploadVersionAttribute(){
        $editorPro = $this->getOriginal('editorPro');
        $revision = $editorPro['revision'] ??  null;
        return $revision;
    }

    public static function getEditorProCollection(){
        return Layout::whereNull('parentId')->get();
    }


    public function buildDependencies($dpdc = NULL){
        if(is_array($dpdc) && count($dpdc)){
            $objects = Object3d::whereIn('name', $dpdc)->get();
            $array = [];
            foreach ($objects as $obj){
                $obj_data = [];
                foreach ($this->tts_attributes as $pr=>$att){
                    if(is_array($att)){
                        foreach ($att as $at){
                            $obj_data[$at] = ($obj->getAttribute($pr) && is_array($obj->getAttribute($pr)) && !empty($obj->getAttribute($pr)[$at])) ? $obj->getAttribute($pr)[$at] : null;
                        }
                    }
                    else{
                        $obj_data[$att] = $obj->$att;
                    }
                }
                $array[] = $obj_data;
            }
            $this->attributes['dependencies'] = $array;
            return true;
        }

        if(!$this->dataJson || !is_array($this->dataJson) || (empty($this->dataJson['tts']) && empty($this->dataJson['TTS']))){
            $this->attributes['dependencies'] = null;
            return true;
        }
        
        $currentDataJson = $this->dataJson;
        if(isset($this->dataJson['tts'])){
            $tts = $this->dataJson['tts'];
            $ttsArrayName = array_column($tts, "name");
        }else{
            $tts = $this->dataJson['TTS'];
            $ttsArrayName = array_column($tts, "Name");
        }
        
        $objects = Object3d::whereIn('name', $ttsArrayName)->get();
        $array = [];
        foreach ($objects as $obj){
            $obj_data = [];
            foreach ($this->tts_attributes as $pr=>$att){
                if(is_array($att)){
                    foreach ($att as $at){
                        $obj_data[$at] = ($obj->getAttribute($pr) && is_array($obj->getAttribute($pr)) && !empty($obj->getAttribute($pr)[$at])) ? $obj->getAttribute($pr)[$at] : null;
                    }
                }
                else{
                    $obj_data[$att] = $obj->$att;
                }
            }
            $array[] = $obj_data;
        }
        $this->attributes['dependencies'] = $array;
        return true;
        
    }
}