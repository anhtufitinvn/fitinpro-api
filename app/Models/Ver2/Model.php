<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Model extends Eloquent {
    
    function buildAttributeStructure( $input){
        $array_keys = array_keys($input);
        if(!count($array_keys)){
            return true;
        }
        $sub_attributes = $this->sub_attributes;
        if(!$sub_attributes){
            return true;
        }

        foreach($sub_attributes as $parent_attribute=>$sub_attribute_array){

            $parent_input = [];

            foreach($sub_attribute_array as $sub_attribute){

                //LEVEL 1 SUB ATTRIBUTE
                if(!is_array($sub_attribute)){
                    //IF INPUT ALREADY CORRECT FORMAT
                    if(!empty($input[$parent_attribute]) && is_array($input[$parent_attribute]) && !empty($input[$parent_attribute][$sub_attribute])){
                        $parent_input[$sub_attribute] = $input[$parent_attribute][$sub_attribute];
                    }
                    //IF ATTRIBUTE IS ROOT ATTRIBUTE
                    else if (in_array($sub_attribute, $array_keys)){
                        $parent_input[$sub_attribute] =  $input[$sub_attribute];
                    }
                }
            }
            if(count($parent_input)){
                $input[$parent_attribute] = $parent_input;
            }      
            
        }

        return $input;
        
    }
}