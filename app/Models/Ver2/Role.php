<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Role extends Eloquent
{
    protected $collection = 'Roles';
    //protected $primaryKey = 'name';
    public const CACHE_TAG = '3d-role';
    public const CACHE_KEY = '3d-role';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'user_ids'
    ];

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            forgetCacheRoles();
            forgetCacheUserRoles();
        });
     
        self::updated(function ($model) {
            forgetCacheRoles();
            forgetCacheUserRoles();
        });

        self::deleting(function ($model) {
            $model->users()->sync([]);
        });

        self::deleted(function ($model) {
            forgetCacheRoles();
            forgetCacheUserRoles();
        });
    }

    public function users(){
        return $this->belongsToMany( User::class, null ,'role_ids',
        'user_ids' );
    }
}
