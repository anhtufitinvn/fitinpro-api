<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;
use App\Traits\AttributeSchemaTrait;

class Room extends Eloquent {

    use AttributeSchemaTrait, CacheTrait;
    
    protected $collection = 'Rooms';

    protected $fillable = [
        "version",
        "parent_id",
        "owner_id",
        "name" ,
        "type",
        "code",
        "image",
        "geometry",
        "dataJson",
        "width",
        "length",
        "height",
        "wall_thickness",
        "shape_id",
        "shape_name",
        "wall_materials",
        "floor_materials",
        "doors",
        "windows",
        "gltf",
        "settings",
        "status"
    ];
    
    const PENDING = 'pending';
    const PUBLISHED = 'published';

    public const CACHE_TAG = 'room';

    const DIR_NAME = 'rooms';
    const GLTF_DIR = 'gltf';
    const THUMB_DIR = 'thumb';

    protected $appends = ['imageUrl', 'gltfUrl'];

    protected $attributes_schema = [
        'dataJson' => 'array',
        'wall_materials' => 'array',
        'floor_materials' => 'array',
        'doors' => 'array',
        'windows' => 'array',
        'code' => 'string',
        'name' => 'string',
        'gltf' => 'string',
        'image' => 'string',
        'geometry' => 'array',
        'settings' => 'array'
    ];

    protected $strtolower = [
    //    "code"
    ];

    // public function getImageAttribute()
    // {
    //     $driver = config('fitin.asset.filesystem_driver');
    //     return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image');
        
    // }

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.asset.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image');   
    }

    public function getGltfUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->gltf ? Storage::disk($driver)->url($this->gltf) : null;
    }
    
}