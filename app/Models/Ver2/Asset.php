<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

class Asset extends Eloquent {

    protected $collection = 'Assets';

    const PENDING = 0;
    
    const PUBLISHED = 1;
    protected $fillable = [
        "dimension" ,
        "desc",
        "keywords" ,
        "status" ,
      
        "attributes" ,
        "ownerID" ,

        "name" ,
        "brand" ,
        "fbx" ,   
        'vendor',  
        
        'prefab',
        "skuList" , 
        "prefab",
        "createdAt", 
        "updatedAt",
        "user_id",
        "category_id"
    ];
    protected $appends = ['thumb_image'];

    public function sku_lists(){
        return $this->hasMany(Sku::class);
    }

    public function textures(){
        return $this->hasMany(Texture::class);
    }

    public function getThumbImageAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        $path = $this->brand . '/' . $this->name . '/thumb/' . $this->thumb; 
        return Storage::disk($driver)->url($path);
    }

}