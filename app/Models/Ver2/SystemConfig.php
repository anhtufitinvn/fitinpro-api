<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Traits\AttributeSchemaTrait;

class SystemConfig extends Eloquent {

    use AttributeSchemaTrait;

    protected $collection = 'SystemConfig';

    protected $fillable = [     
        "type",
        "value",
        'environment'
    ];

    //public $primaryKey = 'type';
   
    public static function getConfig($env = 'cms'){
        $configs = SystemConfig::where('environment', $env)->get();
        $data = [];
        foreach ($configs as $c){
            $data[$c->type] = $c->value;
        }
        return $data;
    }
}