<?php

namespace App\Models\Ver2;


use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Traits\CacheTrait;

class File extends Eloquent
{

//   use CacheTrait;
    
    protected $fillable = [
        'path', 
        'attachmentType',
        'attachmentKey', 
        'name', 
        'size', 
        'fileType',
        'revision',
        'owner'
    ];

    protected $collection = "Files";
    
    public const CACHE_TAG = 'file';

    protected $appends = ['url'];

    public function getUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->path ? Storage::disk($driver)->url($this->imagePath) : '';
        
    }
}
