<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class V360 extends Eloquent {

    use CacheTrait;
    
    const DIR_360 = "view360";

    protected $collection = 'V360';

    protected $fillable = [
        "frame",
        "sku" ,
        "brand",
        "filename",
        "revision",
        "vendor"
       
    ];
    public const TYPE_NAME = 'v360';
    public const CACHE_TAG = 'v360';

    protected $appends = ['imageUrl'];

    public function getImageUrlAttribute(){
        $driver = config('fitin.v360.filesystem_driver');
        $path = \App\Helpers\Utils::locationPath($this->brand, $this->sku, $this->revision, $this->filename);
        return $this->filename ? Storage::disk($driver)->url($path) : null;
    }
}