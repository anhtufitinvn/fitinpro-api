<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class ObjectVideo extends Eloquent {

    use CacheTrait;

    protected $collection = 'ObjectVideos';

    protected $fillable = [
        "name",
        "path",
        "absolute_path"
    ];
    public const CACHE_TAG = 'object-videos';

    protected $appends = ['url'];

    public function getUrlAttribute(){
        $driver = config('fitin.video.filesystem_driver');
        return $this->path ? Storage::disk($driver)->url($this->path) : null;
    }
}