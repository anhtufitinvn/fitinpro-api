<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\Auth;

use App\Traits\HasRoles;
use App\FitIn\CacheManager;
use App\Traits\CacheTrait;

use Jenssegers\Mongodb\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject {
    use HasRoles;
    use Notifiable;

    use CacheTrait;
    public const CACHE_TAG = '3d-user';

    protected $collection = 'Users';


    protected $guarded = ['_id'];
    protected $guard = 'api';
    protected $guard_name = 'api';
    protected $primaryKey = 'auth_id';
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'active',
        'merchant_code',
        'date_of_birth',
        'gender',
        'app_id',
        'auth_id'

    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            self::flushCache();
            forgetCacheUserRoles();
        });

        self::updated(function ($model) {
            self::flushCache();
            forgetCacheUserRoles();
        });

        self::deleting(function ($model) {
            $model->roles()->sync([]);
        });

        self::deleted(function ($model) {
            self::flushCache();
            forgetCacheUserRoles();
        });
    }

    public function user_template_bundle()
    {
        return $this->hasMany(TemplateBundle::class, 'userId' ,'auth_id');
    }

    
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'auth_token' => $this->getAuthToken()
        ];
    }

    /**
     * Determine if the user has verified their email address.
     *
     * @return bool
     */
    public function hasVerifiedEmail()
    {
        if($this->active){
            return true;
        }

        //Check authID
        $oAuthID = AuthID::getInstance();
        $hasConfirmation = $oAuthID->hasConfirmation($oAuthID->getToken());

        //Store
        if($hasConfirmation){
            $this->update([
                'active' => 1
            ]);
        }

        return $hasConfirmation;
    }

    public function layouts()
    {
        return $this->hasMany(Layout::class);
    }   

    public function layout_versions()
    {
        return $this->hasMany(LayoutVersion::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class,'_id','to_user');
    }


    public function setAuthToken($token){
        $this->auth_token = $token;
    }

    public function getAuthToken(){
        return $this->auth_token;
    }

    public function roles(){
        return $this->belongsToMany( Role::class, null ,'user_ids',
        'role_ids' );
    }

    // public function permissions(){
    //     return $this->belongsToMany( Permission::class, null ,'user_ids',
    //     'permission_ids' );
    // }

}