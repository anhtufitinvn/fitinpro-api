<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use \App\Helpers\Utils;
use App\Traits\CacheTrait;

class Material extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'Materials';
    public const CACHE_TAG = 'material';
    const MODEL_TYPE = "material";
    public const PENDING = "pending";
    public const PUBLISHED = "published";
    public const TRASH = "trash";
    protected $primaryKey = 'code';

    protected $fillable = [       
        'name', 
        'code',
        'status',
        'hexColor',
        'map',
        'image',
        'normalMap',
        'attrs',
        'editorPro'
    ];

    // protected $sub_attributes = [
    //     'map' => [
    //         'path',
    //         'scale'
    //      ],
    //     'normalMap' => [
    //         'path',
    //         'scale'
    //      ],
    //     'attrs' => [
    //         "type",
    //         "emissiveIntensity",
    //         "emissive",
    //         "roughness",
    //         "metalness",
    //         "opacity"
        
    //     ],

    //     'editorPro' => [
    //         'asset'
    //     ]
    // ];
    
    protected $attributes_schema = [
       
        'name' => 'string',
        'code' => 'string',
        'hexColor' => 'string',
        'map' => 'array',
        'normalMap' => 'array',
        'attrs' => 'array',
        'editorPro' => 'array'
    ];
    const INDEXES = ['name'];
    const DIR_NAME = "materials";
    const TEXTURE_DIR = "textures";
    const PREFAB_DIR = "prefab";
    const THUMB_DEFAULT_SIZE = 512;
    const THUMB_DEFAULT_MIME = 'jpeg';
    const THUMB_SIZE = [
       
        '512x512' => [
            'width' => 512,
            'height' => 512,
        ],
        '1024x1024' => [
            'width' => 1024,
            'height' => 1024,
        ],
         '2048x2048' => [
             'width' => 2048,
             'height' => 2048,
         ]
    ];

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.asset.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image');;
    }

    public function getMapAttribute($value){
        $driver = config('fitin.asset.filesystem_driver');
        $map = $value;
        if(!empty($map) && is_array($map)){    
            $map['url'] = !empty($map['path']) ? Storage::disk($driver)->url($map['path']) : null;
        }

        return $map;
    }

    public function getNormalMapAttribute($value){
        $driver = config('fitin.asset.filesystem_driver');
        $map = $value;
        if(!empty($map) && is_array($map)){    
            $map['url'] = !empty($map['path']) ? Storage::disk($driver)->url($map['path']) : null;
        }

        return $map;
    }

    public function getEditorProAttribute($value){
        $driver = config('fitin.editor.filesystem_driver');
        $map = $value;
        if(!empty($map) && is_array($map)){    
            $map['assetUrl'] = !empty($map['asset']) ? Storage::disk($driver)->url($map['asset']) : null;
        }

        return $map;
    }

    public function makeMultiThumb($path, $filename)
    {
        $sizes = self::THUMB_SIZE;
        $driver = config('fitin.asset.filesystem_driver');
        $image_path = Storage::disk($driver)->path("$path/$filename");
        
        if(!file_exists($image_path)){
            return false;
        }
        $returnPath = null;
        foreach ($sizes as $size => $arrAttr) {
            $oImageManager = Image::make($image_path);
            $originWidth = $oImageManager->width();
            if($arrAttr['width'] >= $originWidth && $arrAttr['width'] > Material::THUMB_DEFAULT_SIZE){
                continue;
            }
            $higher = ($arrAttr['width'] >= $arrAttr['height']) ? $arrAttr['width'] : $arrAttr['height'];
            $oImageManager->fit($higher, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            //resizeCanvas keep the origin shape of image and fill additional blank to get the wanted width-height 
           // $oImageManager->resizeCanvas($arrAttr['width'],$arrAttr['height'], 'center', false, 'FFF');
            $oImageManager->stream();
            
            $folderThumb = $arrAttr['width'];
            
            $pathUpload = "$path/$folderThumb/$filename";
    
            Storage::disk($driver)->put($pathUpload, $oImageManager);
            uploadDriver($filename, $driver, $pathUpload , Storage::disk($driver)->url($pathUpload), $this);
            
            if($arrAttr['width'] ==  Material::THUMB_DEFAULT_SIZE){
                $returnPath = $pathUpload;
            }
        }
        return $returnPath;
    }

    public function getDownloadUrlAttribute(){
        if(!$this->editorPro || !is_array($this->editorPro) || empty($this->editorPro['asset'])){
            return null;
        }
        return Utils::editorStoreRoute('editor.material.download', ['id' => $this->_id, 'name' => $this->code ?? $this->_id]);
    }

    public function getEditorProUploadVersionAttribute(){
        $revision = ($this->editorPro && is_array($this->editorPro) && !empty($this->attrs['revision'])) ? $this->attrs['revision'] : null;
        return $revision;
    }

    public static function getEditorProCollection(){
        return Material::whereNotNull('editorPro')->whereNotNull('code')->get();
    }
}