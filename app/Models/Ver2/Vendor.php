<?php

namespace App\Models\Ver2;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\Traits\CacheTrait;

class Vendor extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'Vendors';
    protected $primaryKey = 'code';
    protected $fillable = [
        "name" ,
        "email",
        "code",
        "status",
        "password",
        "image"
    ];
    
    public const PENDING = "pending";
    public const PUBLISHED = "published";
    public const TRASH = "trash";

    public const CACHE_TAG = 'vendor';


    public function getImageUrlAttribute()
    {
        $driver = config('fitin.asset.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : null;
    }

   
}