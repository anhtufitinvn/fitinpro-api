<?php
namespace App\EditorPro\Services;

use Illuminate\Support\Facades\Storage;
use App\FitIn\AssetTuVan;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use App\EditorPro\Models\Project;
use App\EditorPro\Models\Style;
use App\EditorPro\Models\Layout;

use App\EditorPro\Repositories\ProjectRepository;
use App\EditorPro\Repositories\StyleRepository;
use App\EditorPro\Repositories\LayoutRepository;

class ResourceTuVanService
{
    protected $projectRepository, $styleRepository, $layoutRepository;

    public function __construct(ProjectRepository $projectRepository, StyleRepository $styleRepository, LayoutRepository $layoutRepository){
        $this->projectRepository = $projectRepository;
        $this->styleRepository = $styleRepository;
        $this->layoutRepository = $layoutRepository;
    }


    /**
        * @param $layout
        * @return object
        */

    public function syncFitinProjectAndLayouts(){
        
        $asset = new AssetTuVan();
        $is_sync = true;
        $page = 1;
        while($is_sync){
                
            echo "page : ". $page .PHP_EOL;
            $fitin_projects = $asset->getByType('projects', [ 'page' => $page]);
            $page++;
                
            if(!$fitin_projects || !count($fitin_projects)){
                $is_sync = false;
                break;
            }
                
            

            foreach ($fitin_projects as $fitin_project){
                $project = $this->syncProject($fitin_project);

                $fitin_project_by_id = $asset->getByType('projects', [ 'project_id' => $fitin_project['project_id']]);
                if(count($fitin_project_by_id)){
                    $fitin_project_detail = $fitin_project_by_id[0];
                }
                else{
                    continue;
                }
                if($fitin_project_detail && !empty($fitin_project_detail['layouts'])){
                    $fitin_layouts = $fitin_project_detail['layouts'];
                    
                    foreach($fitin_layouts as $fitin_layout){
                        $this->syncLayout($fitin_layout, $project);
                    }
                }
                
            }
        }
        
        return true;

    }

    protected function syncProject($fitin_project){
        $driver = config('fitin.editor.filesystem_driver');
        if(isset($fitin_project['slug'])){
            $code = $fitin_project['slug'];
        }else{
            $code = slug($fitin_project['project_name']);
        }
        $code = strtolower($code);
        $project = $this->projectRepository->find($code);
        if(!$project){
                $new_data_project = [
                    'code' => $code,
                    'name' => $fitin_project['project_name'],
                    'status' => Project::PUBLISHED,
                    'location' => $fitin_project['location'],
                    'fitin_id' => $fitin_project['project_id'],
                    'ownerId' => 'cms_1'
                ];
                $project = $this->projectRepository->create($new_data_project);

                if(isset($fitin_project['project_image_url'])){
            
                    $thumb_url = $fitin_project['project_image_url'];
                    $pathinfo = pathinfo($thumb_url);
                    $basename = $pathinfo['basename'];
                    if(file_valid($thumb_url)){
                        $file_ecom_content = file_get_contents($thumb_url);
                        $type = Project::DIR_NAME;
                        $thumb_dir = Project::THUMB_DIR;
                        $time = \Carbon\Carbon::now()->timestamp;
                        $path = "$type/{$project->code}/$thumb_dir/$time/$basename";
                        $download = Storage::disk($driver)->put($path, $file_ecom_content);
                        $url = Storage::disk($driver)->url($path);
                        $project->image = $path;
                        $project->save();
                        uploadDriver($basename, $driver, $path, $url , $project);
                        $project->makeMultiThumb();
                        clearCache([get_class($project)]);
                    }
                }
        }else{
            if(isset($fitin_project['project_name'])){
                    $project->name = $fitin_project['project_name'];
                    $project->location = $fitin_project['location'];
                    $project->fitin_id = $fitin_project['project_id'];
                    $project->save();
            }
        }

        echo "Done Project: {$project->fitin_id} : {$project->name}".PHP_EOL;
        return $project;
    }

    protected function syncLayout($fitin_layout, $project){
        $driver = config('fitin.editor.filesystem_driver');

        if(!isset($fitin_layout['object_key'])){
            return false;
            
        }
        $name = strtolower($fitin_layout['object_key']);
        $layout = $this->layoutRepository->find($name);
        if(!$layout){
            $new_data_layout = [
                'name' => $name,
                'displayName' => $fitin_layout['name'],
                'status' => Layout::PUBLISHED,
                'description' => $fitin_layout['desc'],
                'fitin_id' => $fitin_layout['layout_id'],
                'ownerId' => 'cms_1',
                'project' => $project->code,
                'meta' => $fitin_layout,
                'dataJson' => (isset($fitin_layout['layout_json'])) ? $fitin_layout['layout_json'] : []
            ];
            $layout = $this->layoutRepository->create($new_data_layout);

            if(isset($fitin_layout['image_url'])){
            
                $thumb_url = $fitin_layout['image_url'];
                $pathinfo = pathinfo($thumb_url);
                $basename = $pathinfo['basename'];
                if(file_valid($thumb_url)){
                    $file_ecom_content = file_get_contents($thumb_url);
                    $type = Layout::DIR_NAME;
                    $thumb_dir = Layout::THUMB_DIR;
                    $time = \Carbon\Carbon::now()->timestamp;
                    $path = "$type/{$layout->name}/$thumb_dir/$time/$basename";
                    $download = Storage::disk($driver)->put($path, $file_ecom_content);
                    $url = Storage::disk($driver)->url($path);
                    $layout->image = $path;
                    $layout->save();
                    uploadDriver($basename, $driver, $path, $url , $layout);
                    $layout->makeMultiThumb();
                    clearCache([get_class($layout)]);
                }
            }
        }
        
        else{
            if(isset($fitin_layout['name'])){
                $layout->displayName = $fitin_layout['name'];
                $layout->meta = $fitin_layout;
                $layout->fitin_id = $fitin_layout['layout_id'];
                $layout->description = $fitin_layout['desc'];
                $layout->project = $project->code;    

                $layout->save();
            }
        }
        echo "Done Layout : {$layout->fitin_id} : {$layout->displayName}".PHP_EOL;
        return true;
    }



    
}