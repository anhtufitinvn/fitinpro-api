<?php

namespace App\EditorPro\Services;

use App\EditorPro\Models\Permission;
use App\EditorPro\Models\Role;
use App\EditorPro\Models\CMSUser;
use Illuminate\Support\Facades\Cache;

class CacheService
{


    //Permission List
    public function getPermissions($params = []){

        $permissions = Cache::rememberForever(Permission::CACHE_TAG, function () {
            return Permission::all();
        });
        
        foreach ($params as $attr => $value) {
            $permissions = $permissions->where($attr, $value);
        }

        return $permissions;
    }

    //Role List
    public function getRoles($params = []){
        $roles = Cache::rememberForever(Role::CACHE_TAG, function () {
            return Role::all();
        });

        foreach ($params as $attr => $value) {
            $roles = $roles->where($attr, $value);
        }

        return $roles;
    }
    //Roles of User
    public function getUserRoles($user){
        $data = Cache::tags(Role::CACHE_TAG)->rememberForever($user->auth_id, function() use ($user) {
            
            return $user->roles;
        });
        return $data;
    }

    //Permission of User
    public static function getAllPermissions($user){
        $data = Cache::tags(Permission::CACHE_TAG)->rememberForever($user->auth_id, function() use ($user) {
            $permissions = $user->permissions;
        
            if ($user->roles) {
                $permissions = $permissions->merge($user->getPermissionsViaRoles());
            }
            
            return $permissions->sort()->values();
        });
        return $data;
    }

    public function forgetCachePermissions(){
        Cache::tags(Permission::CACHE_TAG)->flush();
    }

    public function forgetCacheRoles(){
        Cache::tags(Role::CACHE_TAG)->flush();
    }

    public function forgetCacheUsersRolesPermissions(){
        Cache::tags(Role::CACHE_TAG)->flush();
        Cache::tags(Permission::CACHE_TAG)->flush();
        Cache::tags(CMSUser::CACHE_TAG)->flush();
    }
}
