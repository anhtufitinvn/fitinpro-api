<?php
namespace App\EditorPro\Services;

use App\EditorPro\Models\Material;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\MaterialRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Excel;
use App\EditorPro\Excel\Imports\ExcelMaterial;

class ImportExcelService
{
    protected $materialRepository;

    public function __construct(MaterialRepository $materialRepository){
        $this->materialRepository = $materialRepository;
    }


    public function importExcelMaterial($file)
    {
        
        $driver = config('fitin.editor.filesystem_driver');

        $data = Excel::toCollection(new ExcelMaterial, $file);
        $sheet = $data[0];
        foreach ($sheet as $row){
            if(is_int($row[0])){
                
                $attrs = [
                    'isWall' => false,
                    'isFloor' => false,
                    'isCeiling' => false,
                    'price' => false
                ];
                $dimension = [
                    'l' => $row[7],
                    'w' => $row[8]
                ];
                $type = strtolower($row[6]);
                $array_type = explode(',', $type);
                if(in_array('wall', $array_type)){
                    $attrs['isWall'] = true;
                }
                if(in_array('floor', $array_type)){
                    $attrs['isFloor'] = true;
                }
                if(in_array('ceiling', $array_type)){
                    $attrs['isCeiling'] = true;
                }

                $material_data = [
                    'name' => $row[2],
                    'code' => $row[3],
                    'brand' => $row[4],
                    'category' => $row[5],
                    'status' => Material::PUBLISHED,
                    'dimension' => $dimension,
                    'attrs' => $attrs,
                    'description' => $row[10]
                ];

                $material = Material::firstOrCreate(['code' => $material_data['code']] ,$material_data);

                if($row[1]){
                    $thumb_url = $row[1];
                    $pathinfo = pathinfo($thumb_url);
                    $basename = $pathinfo['basename'];
                    
                    if(file_valid($thumb_url)){
                        $image_content = file_get_contents($thumb_url);
                        $type = Material::DIR_NAME;
                        $thumb_dir = Material::THUMB_DIR;
                        $time = \Carbon\Carbon::now()->timestamp;
                        $path = "$type/{$material->code}/$thumb_dir/$time/$basename";
                        $download = Storage::disk($driver)->put($path, $image_content);
                        $url = Storage::disk($driver)->url($path);
                        $material->image = $path;
                        $material->save();
                        uploadDriver($basename, $driver, $path, $url , $material);
                        $material->makeMultiThumb();
                        clearCache([get_class($material)]);
                    }
                }
            }
        }
        
        return true;
    }

   

}