<?php
namespace App\EditorPro\Services;

use Illuminate\Support\Facades\Storage;
use App\FitIn\Asset;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

use App\EditorPro\Models\Project;
use App\EditorPro\Models\Style;
use App\EditorPro\Models\Layout;

use App\EditorPro\Repositories\ProjectRepository;
use App\EditorPro\Repositories\StyleRepository;
use App\EditorPro\Repositories\LayoutRepository;

class ResourceService
{
    protected $projectRepository, $styleRepository, $layoutRepository;

    public function __construct(ProjectRepository $projectRepository, StyleRepository $styleRepository, LayoutRepository $layoutRepository){
        $this->projectRepository = $projectRepository;
        $this->styleRepository = $styleRepository;
        $this->layoutRepository = $layoutRepository;
    }


    /**
        * @param $layout
        * @return object
        */
    public function syncLayout($layout, $fitin_layout)
    {
        $driver = config('fitin.editor.filesystem_driver');
        if(isset($fitin_layout['name'])){
            $layout->displayName = $fitin_layout['name'];
        }

        if(isset($fitin_layout['description'])){
            $layout->description = $fitin_layout['description'];
        }

        if(isset($fitin_layout['layout_json'])){
            $layout->dataJson = $fitin_layout['layout_json'];
        }

        if(isset($fitin_layout['status']) && $fitin_layout['status'] == Layout::PENDING){
            $layout->status = $fitin_layout['status'];
        }
        $layout->ownerId = "cms_1";
        if(!$layout->image && isset($fitin_layout['image'])){
            
            $thumb_url = $fitin_layout['image'];
            $pathinfo = pathinfo($thumb_url);
            $basename = $pathinfo['basename'];
            if(file_valid($thumb_url)){
                $file_ecom_content = file_get_contents($thumb_url);
                $type = Layout::DIR_NAME;
                $thumb_dir = Layout::THUMB_DIR;
                $time = \Carbon\Carbon::now()->timestamp;
                $path = "$type/{$layout->name}/$thumb_dir/$time/$basename";
                $download = Storage::disk($driver)->put($path, $file_ecom_content);
                $url = Storage::disk($driver)->url($path);
                $layout->image = $path;
                $layout->save();
                uploadDriver($basename, $driver, $path, $url , $layout);
                $layout->makeMultiThumb();
                clearCache([get_class($layout)]);
            }
        }

        

        if(isset($fitin_layout['project'])){
            $fitin_project = $fitin_layout['project'];
            $code = null;
            if(isset($fitin_project['code'])){
                $code = $fitin_project['code'];
            }
            if(isset($fitin_project['slug'])){
                $code = $fitin_project['slug'];
            }
            if($code){
                $code = strtolower($code);
                $layout->project = $code;
                $project = $this->projectRepository->find($code);
                if(!$project){
                    $new_data_project = [
                        'code' => $code,
                        'name' => $fitin_project['name'],
                        'status' => Project::PUBLISHED,
                        'ownerId' => 'cms_1'
                    ];
                    $project = $this->projectRepository->create($new_data_project);

                    if(isset($fitin_project['image'])){
            
                        $thumb_url = $fitin_project['image'];
                        $pathinfo = pathinfo($thumb_url);
                        $basename = $pathinfo['basename'];
                        if(file_valid($thumb_url)){
                            $file_ecom_content = file_get_contents($thumb_url);
                            $type = Project::DIR_NAME;
                            $thumb_dir = Project::THUMB_DIR;
                            $time = \Carbon\Carbon::now()->timestamp;
                            $path = "$type/{$project->code}/$thumb_dir/$time/$basename";
                            $download = Storage::disk($driver)->put($path, $file_ecom_content);
                            $url = Storage::disk($driver)->url($path);
                            $project->image = $path;
                            $project->save();
                            uploadDriver($basename, $driver, $path, $url , $project);
                            $project->makeMultiThumb();
                            clearCache([get_class($project)]);
                        }
                    }
                }else{
                    if(isset($fitin_project['name'])){
                        $project->name = $fitin_project['name'];
                        $project->save();
                    }
                }
            }
        }
        

        if(isset($fitin_layout['style'])){
            $fitin_style = $fitin_layout['style'];
            $code = null;
            if(isset($fitin_style['code'])){
                $code = $fitin_style['code'];
            }
            if(isset($fitin_style['slug'])){
                $code = $fitin_style['slug'];
            }
            if($code){
                $code = strtolower($code);
                $layout->style = $code;
                $style = $this->styleRepository->find($code);
                if(!$style){
                    $new_data_style = [
                        'code' => $code,
                        'name' => $fitin_style['name'],
                        'status' => Project::PUBLISHED,
                        'ownerId' => 'cms_1'
                    ];
                    $style = $this->styleRepository->create($new_data_style);

                    if(isset($fitin_style['image'])){
            
                        $thumb_url = $fitin_style['image'];
                        $pathinfo = pathinfo($thumb_url);
                        $basename = $pathinfo['basename'];
                        if(file_valid($thumb_url)){
                            $file_ecom_content = file_get_contents($thumb_url);
                            $type = Style::DIR_NAME;
                            $thumb_dir = Style::THUMB_DIR;
                            $time = \Carbon\Carbon::now()->timestamp;
                            $path = "$type/{$style->code}/$thumb_dir/$time/$basename";
                            $download = Storage::disk($driver)->put($path, $file_ecom_content);
                            $url = Storage::disk($driver)->url($path);
                            $style->image = $path;
                            $style->save();
                            uploadDriver($basename, $driver, $path, $url , $style);
                        }
                    }
                }
                else{
                    if(isset($fitin_style['name'])){
                        $style->name = $fitin_style['name'];
                        $style->save();
                    }
                }
            }
        }


        $layout->save();

        return $layout;
    }

    public function syncFitinLayouts($limit = 50, $select_page = 0){
        
        $asset = new Asset();

        //Khong xac dinh cu the page & limit can sync => lay het
        if(!$select_page){
            $is_sync = true;
            $page = 1;
            while($is_sync){
                
                echo "page : ". $page .PHP_EOL;
                $fitin_layouts = $asset->getByType('layout', ['include_data_json' => 1, 'limit' => $limit, 'page' => $page]);
                $page++;
                
                if(!$fitin_layouts || !count($fitin_layouts)){
                    $is_sync = false;
                break;
                }
                
                $this->syncLayouts($fitin_layouts);
            }
        }
        //Chi sync trong page cu the
        else{
            $fitin_layouts = $asset->getByType('layouts', ['limit' => $limit, 'page' => $page]);
            $this->syncLayouts($fitin_layouts);
        }
        
        return true;

    }

    protected function syncLayouts($fitin_layouts){
        //$driver = config('fitin.editor.filesystem_driver');
        
        if(!$fitin_layouts || !count($fitin_layouts)){
            return false;
        }
        foreach ($fitin_layouts as $fitin_layout){
            
            if(!isset($fitin_layout['layout_json']) || !$fitin_layout['layout_json']){
                continue;
            }
            $dataJson = $fitin_layout['layout_json']; //json_decode($fitin_layout['layout_json'], true);
            $key = strtolower($dataJson['RoomId']);
            $layout = $this->layoutRepository->find($key);
            if(!$layout){
                $new_data = [
                    'dataJson' => $dataJson,
                    'name' => $key,
                    'isTemplate' => true,
                    'status' => Layout::PUBLISHED,
                    'ownerId' => 'cms_1',
                    'fitin_id' => (isset($fitin_layout['id'])) ? $fitin_layout['id'] : null
                ];
                $layout = $this->layoutRepository->create($new_data);
                
            }
            $this->syncLayout($layout, $fitin_layout);
            echo 'Done - '. $fitin_layout['name'] . PHP_EOL;
        }
    }
}