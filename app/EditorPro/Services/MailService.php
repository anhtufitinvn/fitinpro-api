<?php
namespace App\EditorPro\Services;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;


class MailService
{
    protected $api_endpoint;

    private $access_token;

    public $sender = 'FitIn <support@fitin.vn>';

    protected $api_url = [
        'basic' => 'api/v1/mail'
    ];

    
    public function __construct()
    {
        $this->access_token = config('fitin.mail.access_token');
        $this->api_endpoint = config('fitin.mail.api_endpoint');
    }


    /**
        * @param $action
        * @return string
        */
    public function getUrl($action)
    {
        return $this->api_endpoint . '/' . $this->api_url[$action];
    }

    /**
        * @param $url
        * @param $dataPost
        * @return bool|mixed
        */
    public function requestPOST($url, $dataPost)
    {
        try {
            $arrData = [
                'headers' => [
                'access-key' => $this->access_token
                ]
            ];
            $arrData['json'] = $dataPost;
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', $url, $arrData);
            
            if ($response->getStatusCode() == 200) {
                $jsonResult = $response->getBody()->getContents();
                $result = json_decode($jsonResult, 1);
                
                return $result;
            }
            return false;
        } catch (\Exception $e) {
            $arrError = [
                'api' => config('app.url'),
                'error' => class_basename($e) . ' | ' .$e->getMessage(),
                'stack' => mb_substr($e->getTraceAsString(), 0, 150)  
            ];
            $channel = env('LOG_CHANNEL', 'telegram');
            Log::channel($channel)->error($arrError);
            return false;
        }
    }


    public function sendBasic($email, $subject, $html)
    {
        $api = $this->getUrl('basic');
        
        $result = $this->requestPOST($api, [
            "email" => $email,
            "html" => $html,
            "subject" => $subject,
            "sender" => $this->sender
        ]);
        
        return $result;
    }


   

}