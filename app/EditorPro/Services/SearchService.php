<?php
namespace App\EditorPro\Services;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
use App\EditorPro\Models\Object3d;

class SearchService
{
    protected $api_endpoint;
    
    public function __construct()
    {
        
    }


    /**
        * @param $action
        * @return Collection
        */
    public function getRelatedObjectByKey($key, $params = [])
    {
        //Version 1 : lay theo category
        $limit =  (int) ($params['limit'] ?? 0);
        $object = Object3d::where('name', $key)->first();
        if(!$object){
            return collect([]);
        }
        $category = $object->category;
        if(!$category){
            return collect([]);
        }
        $query = new Object3d();
        $query = $query->where('category', $category)->where('name', '!=', $key);
        if($limit){
            return $query->latest('_id')->paginate($limit);
        }
        else {
            return $query->latest('_id')->get();
        }
    }



   

}