<?php

namespace App\EditorPro\Listeners;

use App\EditorPro\Models\Layout;
use App\EditorPro\Events\RejectLayout;
use App\EditorPro\Models\Notification;

use App\EditorPro\Services\MailService;

class RejectLayoutListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $mailService;

    public function __construct(MailService $mailService)
    {
       $this->mailService = $mailService;
    }

    /**
     * Handle the event.
     *
     * @param  ApproveLayout  $event
     * @return void
     */
    public function handle(RejectLayout $event)
    {
        $layout = $event->layout;
        $input['status'] = Layout::REJECTED;
        $layout->update($input);
        $data = [
            'receiverId' => $layout->ownerId,
            'type' => 'layout.rejected',
            'meta' => [
                'key' => $layout->name
            ]
            ];
        Notification::create($data);

        if($layout->owner && !empty($layout->owner->email)){
            $email = $layout->owner->email;
            $subject = 'Reject';
            $content = "You layout {$layout->name} has been rejected.";
    
            $this->mailService->sendBasic($email, $subject, $content);
        }
       

        return $layout;
    }


}
