<?php

namespace App\EditorPro\Listeners;

use App\EditorPro\Models\Bundle;
use App\EditorPro\Events\ApproveBundle;
use App\EditorPro\Models\Notification;
use App\EditorPro\Services\MailService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Repositories\BundleRepository;


use App\FitIn\ThreeJS;

class ApproveBundleListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $mailService;

    protected $bundleEditorRepository;

    public function __construct(
        MailService $mailService, 
        
        BundleRepository $bundleEditorRepository
       
    )
    {
       $this->mailService = $mailService;
       $this->bundleEditorRepository = $bundleEditorRepository;
    }

    /**
     * Handle the event.
     *
     * @param  ApproveBundle  $event
     * @return void
     */
    public function handle(ApproveBundle $event)
    {
        
        $bundle = $event->bundle;
        //$threeJS = new ThreeJS();
        //$bundle3JS = $threeJS->getByKey('bundle', $bundle->keyname);
        
        //$bundle3JS = $this->bundle3jsRepository->find($bundle->keyname);
        // if($bundle3JS){
        //     throw new \Exception('This bundle already exists on ThreeJS');
        // }

        $approved_bundle = $this->approveBundle($bundle); 
        if(!$approved_bundle){
            throw new \Exception('Approve proccess error, please try again');
        }

        $input['status'] = Bundle::PUBLISHED;
        $bundle->update($input);
        $data = [
            'receiverId' => $bundle->ownerId,
            'type' => 'bundle.approved',
            'meta' => [
                'key' => $bundle->keyname
            ]
            ];
        Notification::create($data);

        if($bundle->owner && !empty($bundle->owner->email)){
            $email = $bundle->owner->email;
        
            $subject = 'Approve';
            $content = "You bundle {$bundle->keyname} has been approved.";
            
            $this->mailService->sendBasic($email, $subject, $content);
            
            
        }

        return $bundle;
    }

    protected function approveBundle($bundle){
        if(!$bundle->dataJson || !is_array($bundle->dataJson) || empty($bundle->dataJson['List_Objects'])){
            return false;
        }
        
        // $threeJS = new ThreeJS();
        // $bundle3JS = $threeJS->createByType('bundle.store', $bundle->append('imageUrl')->toArray());
        // return $bundle3JS;
        return $bundle;
    }

}
