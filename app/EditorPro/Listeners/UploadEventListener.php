<?php

namespace App\EditorPro\Listeners;

use App\EditorPro\Events\UploadEvent;
use App\EditorPro\Models\FileDriver;

class UploadEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Handle the event.
     *
     * @param  UploadEvent  $event
     * @return void
     */
    public function handle(UploadEvent $event)
    {
        $rawData = $event->rawData;
        $file = $event->file;
        $driver = $event->driver;
        $path = $event->path;
        $url = $event->url;
        $instance = $event->instance;

        $data = [
            'size' => $rawData['size'] ?? 0,
            'type' =>  $rawData['fileType'] ?? null,
            'origin_name' => $rawData['originName'] ?? null,
            'revision' =>  $rawData['revision'] ?? 0,
            'file' => $file,
            'driver' => $driver,
            'path' => $path,
            'url' => $url,
        ];
        if($instance){
            $data['key'] = $instance->getKey();
            $data['model'] = get_class($instance);
        }
        $file = FileDriver::create($data);
        return $file;
        
    }
}
