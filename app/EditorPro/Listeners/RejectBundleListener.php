<?php

namespace App\EditorPro\Listeners;

use App\EditorPro\Models\Bundle;
use App\EditorPro\Events\RejectBundle;
use App\EditorPro\Models\Notification;

use App\EditorPro\Services\MailService;

class RejectBundleListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $mailService;

    public function __construct(MailService $mailService)
    {
       $this->mailService = $mailService;
    }

    /**
     * Handle the event.
     *
     * @param  ApproveBundle  $event
     * @return void
     */
    public function handle(RejectBundle $event)
    {
        $bundle = $event->bundle;
        $input['status'] = Bundle::REJECTED;
        $bundle->update($input);
        $data = [
            'receiverId' => $bundle->ownerId,
            'type' => 'bundle.rejected',
            'meta' => [
                'key' => $bundle->keyname
            ]
            ];
        Notification::create($data);

        if($bundle->owner && !empty($bundle->owner->email)){
            $email = $bundle->owner->email;
            $subject = 'Reject';
            $content = "You bundle {$bundle->keyname} has been rejected.";
    
            $this->mailService->sendBasic($email, $subject, $content);
        }
       

        return $bundle;
    }


}
