<?php

namespace App\EditorPro\Listeners;

use App\EditorPro\Models\Room;
use App\EditorPro\Events\ApproveRoom;
use App\EditorPro\Models\Notification;
use App\EditorPro\Services\MailService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Repositories\RoomRepository;


use App\FitIn\ThreeJS;

class ApproveRoomListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $mailService;

    protected $roomEditorRepository;

    public function __construct(
        MailService $mailService, 
        
        RoomRepository $roomEditorRepository
       
    )
    {
       $this->mailService = $mailService;
       $this->roomEditorRepository = $roomEditorRepository;
    }

    /**
     * Handle the event.
     *
     * @param  ApproveRoom  $event
     * @return void
     */
    public function handle(ApproveRoom $event)
    {
        
        $room = $event->room;
        
        //$room3JS = $threeJS->getByKey('room', $room->keyname);
        
        //$room3JS = $this->room3jsRepository->find($room->keyname);
        // if($room3JS){
        //     throw new \Exception('This room already exists on ThreeJS');
        // }

        $newroom = $this->approveRoom($room); 
        if(!$newroom){
            throw new \Exception('Approve proccess error, please try again');
        }

        $input['status'] = Room::PUBLISHED;
        $room->update($input);
        $data = [
            'receiverId' => $room->ownerId,
            'type' => 'room.approved',
            'meta' => [
                'key' => $room->keyname
            ]
            ];
        Notification::create($data);

        if($room->owner && !empty($room->owner->email)){
            $email = $room->owner->email;
        
            $subject = 'Approve';
            $content = "You room {$room->keyname} has been approved.";
            
            $this->mailService->sendBasic($email, $subject, $content);
            
            
        }

        return $room;
    }

    protected function approveRoom($room){
        if(!$room->dataJson || !is_array($room->dataJson) || empty($room->dataJson['TTS'])){
            return false;
        }
        
        $threeJS = new ThreeJS();
        $room3JS = $threeJS->createByType('room.store', $room->append('imageUrl')->toArray());
        return $room3JS;
    }

}
