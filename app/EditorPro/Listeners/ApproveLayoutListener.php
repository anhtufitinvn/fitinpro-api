<?php

namespace App\EditorPro\Listeners;

use App\EditorPro\Models\Layout;
use App\EditorPro\Events\ApproveLayout;
use App\EditorPro\Models\Notification;
use App\EditorPro\Services\MailService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Repositories\LayoutRepository;


use App\FitIn\ThreeJS;
use App\FitIn\Consultant;

use Illuminate\Contracts\Queue\ShouldQueue;

class ApproveLayoutListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $queue = 'consultant';

    public $connection = 'redis';

    protected $mailService;

    protected $layoutEditorRepository;

    public function __construct(
        MailService $mailService, 
        
        LayoutRepository $layoutEditorRepository
       
    )
    {
       $this->mailService = $mailService;
       $this->layoutEditorRepository = $layoutEditorRepository;
    }

    /**
     * Handle the event.
     *
     * @param  ApproveLayout  $event
     * @return void
     */
    public function handle(ApproveLayout $event)
    {
        
        $layout = $event->layout;
        $threeJS = new ThreeJS();
        //$layout3JS = $threeJS->getByKey('layout', $layout->keyname);
        
        //$layout3JS = $this->layout3jsRepository->find($layout->keyname);
        // if($layout3JS){
        //     throw new \Exception('This layout already exists on ThreeJS');
        // }

        $newlayout = $this->approveLayout($layout); 
        if(!$newlayout){
            throw new \Exception('Approve proccess error, please try again');
        }

        $input['status'] = Layout::PUBLISHED;
        $layout->update($input);
        $data = [
            'receiverId' => $layout->ownerId,
            'type' => 'layout.approved',
            'meta' => [
                'key' => $layout->name
            ]
            ];
        Notification::create($data);

        if($layout->owner && !empty($layout->owner->email)){
            $email = $layout->owner->email;
        
            $subject = 'Approve';
            $content = "You layout {$layout->name} has been approved.";
            
            $this->mailService->sendBasic($email, $subject, $content);
                    
        }

        return $layout;
    }

    protected function approveLayout($layout){
        if(!$layout->dataJson || !is_array($layout->dataJson) || empty($layout->dataJson['Items'])){
            return false;
        }
        
        //$threeJS = new ThreeJS();
        $consultant = new Consultant();
        $data = $layout->append('imageUrl')->toArray();
        $layoutCST = $consultant->createByType('layout.store', $data);
        return $layoutCST;
    }

}
