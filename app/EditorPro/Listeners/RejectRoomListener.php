<?php

namespace App\EditorPro\Listeners;

use App\EditorPro\Models\Room;
use App\EditorPro\Events\RejectRoom;
use App\EditorPro\Models\Notification;

use App\EditorPro\Services\MailService;

class RejectRoomListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    protected $mailService;

    public function __construct(MailService $mailService)
    {
       $this->mailService = $mailService;
    }

    /**
     * Handle the event.
     *
     * @param  ApproveRoom  $event
     * @return void
     */
    public function handle(RejectRoom $event)
    {
        $room = $event->room;
        $input['status'] = Room::REJECTED;
        $room->update($input);
        $data = [
            'receiverId' => $room->ownerId,
            'type' => 'room.rejected',
            'meta' => [
                'key' => $room->keyname
            ]
            ];
        Notification::create($data);

        if($room->owner && !empty($room->owner->email)){
            $email = $room->owner->email;
            $subject = 'Reject';
            $content = "You room {$room->keyname} has been rejected.";
    
            $this->mailService->sendBasic($email, $subject, $content);
        }
       

        return $room;
    }


}
