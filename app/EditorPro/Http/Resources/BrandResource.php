<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'name' => $this->name,
            'status' => $this->status,
            'code' => $this->code,
            'image' => $this->imageUrl
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
