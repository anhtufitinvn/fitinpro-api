<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ColorGroupResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'code' => $this->code,
            'hex' => $this->hex,
            'brand' => $this->brand
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
