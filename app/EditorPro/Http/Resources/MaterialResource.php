<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class MaterialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attrs = $this->attrs ?? [];
        if(!isset($attrs['price'])){
            $attrs['price'] = 0;
        }
        if(!isset($attrs['isWall'])){
            $attrs['isWall'] = false;
        }
        if(!isset($attrs['isFloor'])){
            $attrs['isFloor'] = false;
        }
        if(!isset($attrs['isCeiling'])){
            $attrs['isCeiling'] = false;
        }

        $metadata = $this->getOriginal('metadata') ?? [];
        $isOwner = false;
        if(!empty($metadata['user']) && !empty($metadata['user']['id']) && $metadata['user']['id'] == Auth::user()->auth_id){
            $isOwner = true;
        }

        return [
            'filename' => $this->_id,
            'display_name' => $this->name,
            "key" => $this->code,
            "marketCode" => $this->marketCode,
            'image' => $this->imageUrl,
            'downloadUrl' => $this->downloadUrl,
            'thumb128' => $this->thumb128Url,
            //'revision' => ($this->editorPro && is_array($this->editorPro) && !empty($this->attrs['revision'])) ? $this->attrs['revision'] : 0,
            'target' => null, //$this->type,
            'revision' => $this->getRevision(),
            'brand' => $this->brand,
            'description' => $this->description,
            'category' => $this->category,
            'dimension' => $this->dimension ?? ['l' => 0, 'w' => 0],
            'attrs' => $attrs,
            'extensions' => $this->extensions,
            'isOwner' => $this->isOwner
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
