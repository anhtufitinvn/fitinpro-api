<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;

class LayoutResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $meta = $this->meta;
        $data = [
        //    '_id' => $this->_id,
        //     'project' => $this->project,
        //     'style' => $this->style,
        //     'display_name' => $this->display_name,
        //     'name' => $this->name,
        //     'imageUrl' => $this->imageUrl,
        //     'fbx' => $this->fbx,
        //     'fbxUrl' => $this->fbxUrl,
        //     'json' => $this->json,
        //     'jsonUrl' => $this->jsonUrl,
        //     'prefab' => $this->prefab,
        //     'prefabUrl' => $this->prefabUrl,
            

            'filename' => $this->_id,
            'name' => $this->name,
            'key' => $this->name,
            //'category' => $this->category,
            'image' => $this->imageUrl,
            'downloadUrl' => $this->downloadUrl,
            //'version' => $this->editorProUploadVersion,
            "project" => $this->_project,

            "display_name" => $this->displayName,
            //"downloadPrefabUrl" => $this->downloadPrefabUrl,
            "revision" => $this->getRevision(),
            'thumb128' => $this->thumb128Url,
            'thumb256' => $this->thumb256Url,
            "dataJson" => $this->dataJson,

            'fitin_id' => $this->fitin_id,
            'location' => $this->location,
            'area' => ( is_array($meta) && isset($meta['area']) ) ? $meta['area'] : 0,
            'block' => ( is_array($meta) && isset($meta['block']) ) ? $meta['block'] : null,
            'style_name' => ( is_array($meta) && isset($meta['style_name']) ) ? $meta['style_name'] : null,
            'bed_room_num' => ( is_array($meta) && isset($meta['bed_room_num'])) ? $meta['bed_room_num'] : 0,
            //'dataJson'  => $this->dataJson,
            //'json' => $this->json,
            //'jsonUrl' => $this->jsonUrl,
            "area" => $this->area,
            "sort" => $this->sort,
            "description" => $this->description

        ];
        
        
        return $data;
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            
            'code' => 0,
            'message' => 'Success'
        ];
    }
}
