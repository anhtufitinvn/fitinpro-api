<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ObjectResourceCollection extends ResourceCollection
{
    public $collects = 'App\EditorPro\Http\Resources\ObjectResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'message' => 'Success',
            'code' => 0
        ];
    }
}
