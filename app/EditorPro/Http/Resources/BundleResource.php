<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;

class BundleResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
              
            'filename' => $this->_id,
            'key' => $this->keyname,
            'zip' => $this->metaZipUrl,
            'image' => $this->imageUrl,
            'brand' => $this->brand,
            "display_name" => $this->displayName,
            "status" => $this->status,
            "dataJson" => $this->dataJson,
            'thumb128' => $this->thumb128Url,
            "revision" => $this->zip_version

        ];
        
        
        return $data;
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            
            'code' => 0,
            'message' => 'Success'
        ];
    }
}
