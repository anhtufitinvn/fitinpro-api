<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LayoutCollection extends ResourceCollection
{
    public $collects = 'App\EditorPro\Http\Resources\LayoutResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,    
            'message' => 'Success',  
        ];
    }
}
