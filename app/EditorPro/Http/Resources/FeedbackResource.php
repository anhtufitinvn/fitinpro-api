<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FeedbackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'title' => $this->title,
            'content' => $this->content,
            'owner'  => ($this->owner) ? $this->owner->email : null
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
