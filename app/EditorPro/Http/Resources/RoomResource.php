<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;

class RoomResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $attrs = $this->attrs ?? ['dimension' => ['w' => 0, 'l' => 0]];
        $data = [

            'filename' => $this->_id,
            'name' => $this->keyname,
            'key' => $this->keyname,
            'image' => $this->imageUrl,
            'downloadUrl' => $this->downloadUrl,
            "display_name" => $this->displayName,
            "attrs" => $attrs,
            "revision" => $this->getRevision(),
            "dataJson" => $this->dataJson,
            'thumb128' => $this->thumb128Url,
            "category" => $this->category

        ];
        
        
        return $data;
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            
            'code' => 0,
            'message' => 'Success'
        ];
    }
}
