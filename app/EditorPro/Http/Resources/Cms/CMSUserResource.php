<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class CMSUserResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [     
          
            'name' => $this->name,
            'email' => $this->email,
            'auth_id' => $this->auth_id,
            'active' => $this->active,
            'roles' => $this->roles,
            'permissions' => $this->getAllPermissions(),
            'avatarUrl' => $this->avatarUrl
    
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
