<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class BuildConfigResourceMix extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'published' => null,
            'latest' => null
        ];
        $value = $this;
        
        if($value['latest']){
            $data['latest'] = [
                '_id' => $value['latest']->_id,
                'version' => $value['latest']->version,
                "brand" => $value['latest']->brand,
                'url' => $value['latest']->url,
                'status' => $value['latest']->status,
                'description' => $value['latest']->description,
                'force_update' => $value['latest']->force_update ? true : false,
                'updated_at' => $value['latest']->updated_at
            ];
        }

        if($value['published']){
            $data['published'] = [
                '_id' => $value['published']->_id,
                'version' => $value['published']->version,
                "brand" => $value['published']->brand,
                'url' => $value['published']->url,
                'status' => $value['published']->status,
                'description' => $value['published']->description,
                'force_update' => $value['published']->force_update ? true : false,
                'updated_at' => $value['published']->updated_at
            ];
        }
        return $data;
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
