<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;

class BundleResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            '_id' => $this->_id,
            'displayName'  => $this->displayName,
            'keyname'  => $this->keyname,
            'isTemplate'  => $this->isTemplate,   
            'name'  => $this->name,
            'status'  => $this->status,
            'dataJson'  => $this->dataJson,
            "published_at"  => $this->published_at,
            "description"  => $this->description,
            'imageUrl'  => $this->imageUrl,
            'ownerId'  => $this->ownerId,
            'owner' => $this->owner,
            'attrs'  => $this->attrs,
            'thumb128' => $this->thumb128Url,
            "zip_version" => $this->zip_version

        ];
        
        
        return $data;
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            
            'code' => 0,
            'message' => 'Success'
        ];
    }
}
