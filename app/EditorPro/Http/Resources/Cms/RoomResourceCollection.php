<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RoomResourceCollection extends ResourceCollection
{
    public $collects = 'App\EditorPro\Http\Resources\Cms\RoomResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,    
            'message' => 'Success',  
        ];
    }
}
