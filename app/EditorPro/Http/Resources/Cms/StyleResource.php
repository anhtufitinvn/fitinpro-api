<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class StyleResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [     
          
            'name' => $this->name,
            'code' => $this->code,
            'image' => $this->image,
            'imageUrl' => $this->imageUrl
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
