<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;

class RoomResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $attrs = $this->attrs ?? ['dimension' => ['w' => 0, 'l' => 0]];
        $data = [
            "_id" => $this->_id,
            "keyname" => $this->keyname,
            "displayName" => $this->displayName,
            'ownerId'  => $this->ownerId,
            'owner' => $this->owner,
            "settings" => $this->settings,
            "attrs" => $attrs,
            "parent" => $this->parent,
            "isTemplate" => $this->isTemplate,
            "status" => $this->status,
            'imageUrl' => $this->imageUrl,
            "published_at" => $this->published_at,
            "description" => $this->description,
            "dataJson" => $this->dataJson,
            "assetBundle" => $this->assetBundle,
            "assetBundleUrl" => $this->assetBundleUrl,
            'versionAssetBundle' => $this->versionAssetBundle,
            "revision" => $this->revision,
            'thumb128' => $this->thumb128Url,
            "category" => $this->category

        ];
        
        
        return $data;
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            
            'code' => 0,
            'message' => 'Success'
        ];
    }
}
