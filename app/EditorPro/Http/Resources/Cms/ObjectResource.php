<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Support\Facades\Storage;

class ObjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
   

    public function toArray($request)
    {   
        $attrs = $this->getOriginal('attributes');
        return [
    
            '_id' => $this->_id,
            'name' => $this->name,
            "displayName" => $this->displayName,
            'category' => $this->category,
            'vendor' => $this->vendor,
            'brand' => $this->brand,
            
            'thumbUrl' => $this->thumbUrl,
            'assetBundleUrl' => $this->assetBundleUrl,
            'assetBundle' => $this->assetBundle,
            'assetZipUrl' => $this->assetZipUrl,
            'versionAssetBundle' => $this->versionAssetBundle,
            'attributes' => $this->attributes ?? [],
            //'revision' => $this->revision,
            'status' => $this->status,
            'isTTS' => ( is_array($attrs) && isset($attrs['isTTS']) && $attrs['isTTS'] ) ? 1 : 0,
            'isCommon' => $this->isCommon,
            'thumb128Url' => $this->thumb128Url,
            'thumbList' => $this->thumbList,
            'revision' => $this->revision,
            'extensions' => $this->extensions
        ];



    }
    
    public function with($request)
    {
        return [
            'message' => 'Success',
            'code' => 0
        ];
    }
}
