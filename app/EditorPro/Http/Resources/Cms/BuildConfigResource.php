<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class BuildConfigResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'version' => $this->version,
            "brand" => $this->brand,
            'url' => $this->url,
            'status' => $this->status,
            'description' => $this->description,
            'force_update' => $this->force_update ? true : false,
            'updated_at' => $this->updated_at
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
