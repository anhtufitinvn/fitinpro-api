<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class FeedbackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        \Carbon\Carbon::setLocale('en');
        return [
            '_id' => $this->_id,
            'title' => $this->title,
            'content' => $this->content,
            'owner'  => $this->owner->append('avatarUrl'),
            'created_at' => $this->created_at,
            'created_at_format' => $this->created_at->diffForHumans()
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
