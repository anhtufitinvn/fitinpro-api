<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;

class LayoutResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $metadata = $this->metadata;
        if(empty($metadata['creator'])){
            $metadata['creator'] = [
                "email" => null
            ];
        }
        if(empty($metadata['rooms'])){
            $metadata['rooms'] = [];
        }
        if(empty($metadata['price'])){
            $metadata['price'] = [
                "estimated" => 0,
                "max" => 0,
                "min" => 0
            ];
        }
        if(empty($metadata['address'])){
            $metadata['address'] = [
                "block" => null,
                "apartment_code" => null
            ];
        }
        if(empty($metadata['style'])){
            $metadata['style'] = [
                "name" => null,
                "key" => null
            ];
        }

        $data = [

            '_id' => $this->_id,
            'name' => $this->name, 
            'imageUrl' => $this->imageUrl,
            "ownerId" => $this->ownerId,
            "project" => $this->project,
            "style" => $this->style,
            "displayName" => $this->displayName,
            "published_at" => $this->published_at,
            "description" => $this->description,
            "parent" => $this->isTemplate,
            "isTemplate" => $this->isTemplate,
            "status" => $this->status,
            "revision" => $this->revision,
            "dataJson" => $this->dataJson,
            'thumb128' => $this->thumb128Url,
            'thumb256' => $this->thumb256Url,
            "assetBundleUrl" => $this->assetBundleUrl,
            "assetBundle" => $this->assetBundle,
            'versionAssetBundle' => $this->versionAssetBundle,
            'fitin_id' => $this->fitin_id,

            'gallery' => $this->gallery ?? [],
            'files' => $this->files ?? [],
            'imageFloorPlanUrl' => $this->imageFloorPlanUrl,
            'metadata' => $metadata,
            "area" => $this->area,
            "sort" => $this->sort
        ];
        
        
        return $data;
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            
            'code' => 0,
            'message' => 'Success'
        ];
    }
}
