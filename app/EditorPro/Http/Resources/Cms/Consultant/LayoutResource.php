<?php

namespace App\EditorPro\Http\Resources\Cms\Consultant;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;

class LayoutResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
 
        $project = $this->_project;
        $style = $this->_style;
        $status = ($this->status == "published") ? "published" : "unpublished";

        $metadata = $this->metadata;
        if(empty($metadata['creator'])){
            $metadata['creator'] = [
                "email" => null
            ];
        }
        if(empty($metadata['rooms'])){
            $metadata['rooms'] = [];
        }
        if(empty($metadata['price'])){
            $metadata['price'] = [
                "estimated" => 0,
                "max" => 0,
                "min" => 0
            ];
        }
        if(empty($metadata['address'])){
            $metadata['address'] = [
                "block" => null,
                "apartment_code" => null
            ];
        }
        if(empty($metadata['style'])){
            $metadata['style'] = [
                "name" => $style ? $style->name : null,
                "key" => $style ? $style->code : null
            ];
        }

        $data = [
            'layout_id' => $this->_id,
            'layout_key_name' => $this->name,
            'layout_name' => $this->displayName,
            'status' => $status,
            'sort'  => $this->sort,
            'owner' => $this->owner,
            'area' => $this->area,
            'style_name' => $style ? $style->name : null,
            'style_key' => $style ? $style->code : null,
            'project_id' => $project ? $project->_id : null,
            'project_slug' => $project ? $project->code : null,
            'project_name' => $project ? $project->name : null,
            'desc' => $this->description,
            'is_template' => true,
            'image_path' => $this->image,
            'image_url' => $this->imageUrl,
            'image_floor_plan' => $this->image_floor_plan,
            'image_floor_plan_url' => $this->imageFloorPlanUrl,
            'attributes' => $this->attrs ?? [],
            'metadata' => $metadata,
            'gallery' => $this->gallery ?? [],
            //'extensions' => $this->extensions,
            'panorama_zip_url' => $this->files['panorama_zip_url']

        ];
        
        
        return $data;
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            
            'code' => 0,
            'message' => 'Success'
        ];
    }
}
