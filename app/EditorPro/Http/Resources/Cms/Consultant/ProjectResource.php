<?php

namespace App\EditorPro\Http\Resources\Cms\Consultant;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $owner = $this->_owner;
        $metadata = ($owner) ? ['creator' => ['email' => $owner->email]] : [];

        $status = ($this->status == "published") ? "published" : "unpublished";
        return [
            'project_id' => $this->_id,
            'project_name' => $this->name,
            "project_slug" => $this->code,
            'status' => $status,
            'sort'  => $this->sort,
            'landing_url' => $this->landing_url,
            'image_path' => $this->image,
            'image_url' => $this->imageUrl,
            'location' => $this->location,
            'desc' => $this->desc,
            'metadata' => $metadata,
            'attributes' => $this->attrs ?? [],
            'is_renovation' => false
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
