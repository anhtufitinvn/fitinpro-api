<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class ColorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $attrs = $this->attrs ?? [];
        if(!isset($attrs['price'])){
            $attrs['price'] = 0;
        }
        if(!isset($attrs['surface'])){
            $attrs['surface'] = 0;
        }

        return [
            'name' => $this->name,
            //'slug' => $this->slug,
            'hex'  => $this->hex,
            'group' => $this->group,
            'code' => $this->code,
            'imageUrl' => $this->imageUrl,
            'attrs' => $attrs,
            'meta' => $this->meta,
            'usedCount' => $this->usedCount ?? 0
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
