<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class MaterialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attrs = $this->attrs ?? [];
        if(!isset($attrs['price'])){
            $attrs['price'] = 0;
        }
        if(!isset($attrs['isWall'])){
            $attrs['isWall'] = false;
        }
        if(!isset($attrs['isFloor'])){
            $attrs['isFloor'] = false;
        }
        if(!isset($attrs['isCeiling'])){
            $attrs['isCeiling'] = false;
        }

        return [
            '_id' => $this->_id,
            'name' => $this->name,
            "code" => $this->code,
            "marketCode" => $this->marketCode,
            'imageUrl' => $this->imageUrl,
            'assetBundle' => $this->assetBundle,
            'assetBundleUrl' => $this->assetBundleUrl,
            'dimension' => $this->dimension ?? ['l' => 0, 'w' => 0],
            'type' => $this->type,
            'thumb128' => $this->thumb128Url,
            'category' => $this->category,
            'brand' => $this->brand,
            'description' => $this->description,
            'status' => $this->status,
            'attrs' => $attrs,
            'revision' => $this->revision,
            'versionAssetBundle' => $this->versionAssetBundle,
            'extensions' => $this->extensions
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
