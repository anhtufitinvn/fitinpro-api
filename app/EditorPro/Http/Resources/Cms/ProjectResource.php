<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'name' => $this->name,
            "code" => $this->code,
            'status' => $this->status,
            'thumb128' => $this->thumb128Url,
            'thumb256' => $this->thumb256Url,
            'imageUrl' => $this->imageUrl,
            'fitin_id' => $this->fitin_id,
            'location' => $this->location,
            'isTemplate' => $this->isTemplate,
            'attrs' => $this->attrs,
            'desc' => $this->desc,
            'sort' => $this->sort,
            'landing_url' => $this->landing_url
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
