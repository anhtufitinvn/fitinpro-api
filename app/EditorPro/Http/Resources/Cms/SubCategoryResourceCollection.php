<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SubCategoryResourceCollection extends ResourceCollection
{
    public $collects = 'App\EditorPro\Http\Resources\Cms\CategoryResource';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection;
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
