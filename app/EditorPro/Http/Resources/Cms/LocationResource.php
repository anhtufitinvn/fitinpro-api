<?php

namespace App\EditorPro\Http\Resources\Cms;

use Illuminate\Http\Resources\Json\JsonResource;

class LocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "lat" => $this->lat, 
            "long" => $this->long, 
            "city_id" => $this->city_id, 
            "city_name" => $this->city_name, 
            "districts" => $this->districts
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
