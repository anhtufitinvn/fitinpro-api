<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ObjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
   

    public function toArray($request)
    {   
        $attrs = $this->getOriginal('attributes');
        $metadata = $this->getOriginal('metadata') ?? [];
        $isOwner = false;
        if(!empty($metadata['user']) && !empty($metadata['user']['id']) && $metadata['user']['id'] == Auth::user()->auth_id){
            $isOwner = true;
        }

        return [
    
            'filename' => $this->_id,
            'name' => $this->name,
            "display_name" => $this->displayName,
            'key' => $this->getKey(),
            'brand' => $this->brand,
            'category' => $this->category,
            'originCategory' => $this->originCategory,
            'image' => $this->thumbUrl,
            'downloadUrl' => $this->downloadUrl,
            'revision' => $this->getRevision(),
            'isTTS' => ( is_array($attrs) && isset($attrs['isTTS']) && $attrs['isTTS'] ) ? 1 : 0,
            //'attributes' => $attrs,
            'isDecor' => ( is_array($attrs) && isset($attrs['isDecor']) && $attrs['isDecor'] ) ? 1 : 0,
            'isCarpet' => ( is_array($attrs) && isset($attrs['isCarpet']) && $attrs['isCarpet'] ) ? 1 : 0,
            'hangable' => ( is_array($attrs) && isset($attrs['hangable']) && $attrs['hangable'] ) ? 1 : 0,
            'putable' => ( is_array($attrs) && isset($attrs['putable']) && $attrs['putable'] ) ? 1 : 0,
            'hookable' => ( is_array($attrs) && isset($attrs['hookable']) && $attrs['hookable'] ) ? 1 : 0,
            'anchor' => ( is_array($attrs) && isset($attrs['anchor']) && $attrs['anchor'] ) ? $attrs['anchor'] : null,
            'isLight' => ( is_array($attrs) && isset($attrs['isLight']) && $attrs['isLight'] ) ? 1 : 0,
            'lightType' => ( is_array($attrs) && isset($attrs['lightType']) && $attrs['lightType'] ) ? $attrs['lightType'] : null,
            'canScale' => ( is_array($attrs) && isset($attrs['canScale']) && $attrs['canScale'] ) ? 1 : 0,
            'canArray' => ( is_array($attrs) && isset($attrs['canArray']) && $attrs['canArray'] ) ? 1 : 0,
            'canCook' => ( is_array($attrs) && isset($attrs['canCook']) && $attrs['canCook'] ) ? 1 : 0,
            'canDelete' => ( is_array($attrs) && isset($attrs['canDelete']) && $attrs['canDelete'] ) ? 1 : 0,
            'downloadZipUrl' => $this->downloadZipUrl,
            'status' => $this->status,
            'thumb128' => $this->thumb128Url,
            'zipRevision' => $this->zipRevision,
            'extensions' => $this->extensions,
            'isOwner' => $isOwner
        ];



    }
    
    public function with($request)
    {
        return [
            'message' => 'Success',
            'code' => 0
        ];
    }
}
