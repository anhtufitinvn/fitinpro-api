<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_id' => $this->_id,
            'name' => $this->name,
            "code" => $this->code,
            'status' => $this->status,
            'image' => $this->imageUrl,
            'thumb128' => $this->thumb128Url,
            'thumb256' => $this->thumb256Url,
            'layoutCount' => $this->layouts->count(),
            'fitin_id' => $this->fitin_id,
            'location' => $this->location,
            'sort' => $this->sort,
            'attrs' => $this->attrs,
            'desc' => $this->desc
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
