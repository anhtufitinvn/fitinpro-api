<?php

namespace App\EditorPro\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FitinMaterialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $attrs = $this->attrs ?? [];
        if(!isset($attrs['price'])){
            $attrs['price'] = 0;
        }
        if(!isset($attrs['isWall'])){
            $attrs['isWall'] = false;
        }
        if(!isset($attrs['isFloor'])){
            $attrs['isFloor'] = false;
        }
        if(!isset($attrs['isCeiling'])){
            $attrs['isCeiling'] = false;
        }

        return [
            'display_name' => $this->name,
            "key" => $this->code,
            "market_code" => $this->marketCode,
            'image' => $this->imageUrl,
     
            'brand' => $this->material_brand ? $this->material_brand->makeHidden(['_id', 'updated_at', 'created_at']) : null,
            'status' => $this->status,
            'dimension' => $this->dimension ?? ['l' => 0, 'w' => 0],
            'price' => $attrs['price']
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
