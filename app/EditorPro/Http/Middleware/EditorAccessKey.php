<?php

namespace App\EditorPro\Http\Middleware;

use Closure;

class EditorAccessKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(request()->header('access-key') &&  request()->header('access-key')  == config('fitin.editor.private_token')){         
           
            return $next($request);
        }else{
            return response()->json([
                'code' => -1,
                'message' => 'Unauthorized! Please add the correct access key',
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        }
        
    }
}
