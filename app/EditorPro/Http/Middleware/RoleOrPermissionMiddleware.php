<?php

namespace App\EditorPro\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Exceptions\UnauthorizedException;

class RoleOrPermissionMiddleware
{
    public function handle($request, Closure $next, $roleOrPermission)
    {
        if (Auth::guest()) {
            throw UnauthorizedException::notLoggedIn();
        }
    
        $rolesOrPermissions = is_array($roleOrPermission)
            ? $roleOrPermission
            : explode('|', $roleOrPermission);

        $user = auth()->user();   
        
        if ($user->hasRole($rolesOrPermissions) || $user->hasPermission($rolesOrPermissions)) {
            return $next($request);
        }

        return response()->json([
            'code' => -1,        
            'message' =>  'Bạn không có quyền truy cập thao tác này!',
            'data' => [
                'error_code' => 'UNAUTHORIZED_REQUEST'
            ],
            "errors" => [
                [
                    "error" => 1,
                    "message" => "",
                    "stack" => []
                ]
            ]
        ]);
        
    }
}
