<?php

namespace App\EditorPro\Http\Middleware;

use Closure;
use App\Exceptions\UnauthorizedException;

class RoleMiddleware
{
    public function handle($request, Closure $next, $role)
    {

        $roles = is_array($role)
            ? $role
            : explode('|', $role);
        
        $user = auth()->user();   
        $user_roles = $user->roles()->pluck('name')->toArray();
        
        foreach ($roles as $role) { 
            
            if (in_array($role, $user_roles)) {
                return $next($request);
            }
        }
        
        return response()->json([
            'code' => -1,        
            'message' =>  'Bạn không có quyền truy cập thao tác này!',
            'data' => [
                'error_code' => 'UNAUTHORIZED_REQUEST'
            ],
            "errors" => [
                [
                    "error" => 1,
                    "message" => "",
                    "stack" => []
                ]
            ]
        ]);
        //throw UnauthorizedException::forRoles($roles);
    }
}
