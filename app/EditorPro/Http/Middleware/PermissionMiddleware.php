<?php

namespace App\EditorPro\Http\Middleware;

use Closure;
use App\Exceptions\UnauthorizedException;

class PermissionMiddleware
{
    public function handle($request, Closure $next, $permission)
    {
        if (app('auth')->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);
        
        $user = app('auth')->user();    
        foreach ($permissions as $permission) {
            
            $user_permissions = $user->getAllPermissions()->pluck('name')->toArray();
            
            if (in_array($permission, $user_permissions)) {
                return $next($request);
            }
        }

        return response()->json([
            'code' => -1,        
            'message' =>  'Bạn không có quyền truy cập thao tác này!',
            'data' => [
                'error_code' => 'UNAUTHORIZED_REQUEST'
            ],
            "errors" => [
                [
                    "error" => 1,
                    "message" => "",
                    "stack" => []
                ]
            ]
        ]);
    }
}
