<?php

namespace App\EditorPro\Http\Controllers\Cms;

use App\EditorPro\Http\Resources\Cms\CMSUserResource;
use App\EditorPro\Http\Resources\Cms\CMSUserResourceCollection;
use App\EditorPro\Models\CMSUser;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\EditorPro\Repositories\CMSUserRepository;

class CMSUserController extends ApiController
{
    protected $userRepository;

    public function __construct(CMSUserRepository $userRepository){
        $this->userRepository = $userRepository;
    }

    public function me()
    {
        $me = Auth::user();
        return new CMSUserResource($me);
    }

    

    /**
     * Display a listing of the resource.
     *
     * @return UserCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $users = $this->userRepository->getAll($params);
        return new CMSUserResourceCollection($users);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'email'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $user = $this->userRepository->find($input['email']);
        if($user) {
            return $this->responseError('Account already exists', 'DATA_EXISTS');
        }

        $user = $this->userRepository->create($input);
        if($request->get('roles')){
            $user->syncRoles($request->get('roles'));
        }   
        return new CMSUserResource($user);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return UserResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $user = $this->userRepository->find($id);
        if(!$user){
            return $this->responseError('User not found' );
        }
        
        return new CMSUserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return UserResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->only(['name', 'active']);
        $user = $this->userRepository->update($key, $input);
        if(!$user){
            return $this->responseError('Account not found' );
        }
        if($request->get('roles')){
            $user->syncRoles($request->get('roles'));
        }
        return new CMSUserResource($user);
    }

    public function changePassword(Request $request, $key)
    {
        $input = $request->only(['password', 'new_password']);
        $validator = Validator::make($input, [
            'password'   => 'required',
            'new_password'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $credentials = [
            'email' => Auth::user()->email,
            'password' => $input['password']
        ];
        if(Auth::guard('editor-cms')->attempt($credentials)){
            $params = ['password' => (string) $input['new_password']];
            
            $user = $this->userRepository->update($key, $params);
            if(!$user){
                return $this->responseError('Account not found' );
            }
            
            return new CMSUserResource($user);
        }
        else{
            return $this->responseError('You password is not exact. Please try again' );
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $user = $this->userRepository->delete($key);
        if(!$user){
            return $this->responseError('Account not found' );
        }
        return $this->respondSuccess();
    }

}
