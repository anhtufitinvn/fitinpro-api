<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\UserResourceCollection;
use App\EditorPro\Http\Resources\Cms\UserResource;
use App\EditorPro\Http\Resources\Cms\FeedbackResourceCollection;

use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\Object3dRepository;
use App\EditorPro\Repositories\BundleRepository;
use App\EditorPro\Repositories\LayoutRepository;
use App\EditorPro\Repositories\ProjectRepository;

use App\EditorPro\Models\Object3d;
use App\EditorPro\Models\Bundle;
use App\EditorPro\Models\Layout;
use App\EditorPro\Models\Project;

use App\EditorPro\Repositories\UserRepository;

use App\EditorPro\Repositories\FeedbackRepository;

use Illuminate\Support\Facades\Validator;

class DashboardController extends ApiController
{
    protected $object3dRepository, $bundleRepository, $projectRepository, $layoutRepository, $userRepository, $feedbackRepository;

    public function __construct(
        Object3dRepository $object3dRepository,
        BundleRepository $bundleRepository,
        LayoutRepository $layoutRepository,
        ProjectRepository $projectRepository,
        UserRepository $userRepository,
        FeedbackRepository $feedbackRepository
        ){
        $this->object3dRepository = $object3dRepository;
        $this->bundleRepository = $bundleRepository;
        $this->projectRepository = $projectRepository;
        $this->layoutRepository = $layoutRepository;
        $this->userRepository = $userRepository;
        $this->feedbackRepository = $feedbackRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Collection
     */
    public function getOverall(Request $request)
    {
        $paramObjects = [
            'type' => 'item'
        ];
        $objects = $this->object3dRepository->getAll($paramObjects);
        $published_objects = $objects->where('status',Object3d::PUBLISHED);

        $bundles = $this->bundleRepository->getAll();
        $published_bundles = $bundles->where('status',Bundle::PUBLISHED);
        
        $projects = $this->projectRepository->getAll();
        $published_projects = $projects->where('status',Project::PUBLISHED);

        $paramLayouts = [
            'without_json' => 1
        ];
        $layouts =  $this->layoutRepository->getAll($paramLayouts);
        $published_layouts = $layouts->where('status',Layout::PUBLISHED);

        $result = [
            'objects_count' => $objects->count(),
            'published_objects_count' => $published_objects->count(),
            'bundles_count' => $bundles->count(),
            'published_bundles_count' => $published_bundles->count(),
            'projects_count' => $projects->count(),
            'published_projects_count' => $published_projects->count(),
            'layouts_count' => $layouts->count(),
            'published_layouts_count' => $published_layouts->count()
        ];

        return $this->responseObjectData($result);
    }

    public function getNewestUsers(Request $request)
    {
        $params = $request->all();
        if(empty($params['limit'])){
            $params['limit'] = 4;
        }

        $users = $this->userRepository->getAll($params);
     
        return new UserResourceCollection($users);
    }

    public function getNewestFeedbacks(Request $request)
    {
        $params = $request->all();
        if(empty($params['limit'])){
            $params['limit'] = 5;
        }

        $feedbacks = $this->feedbackRepository->getAll($params);
     
        return new FeedbackResourceCollection($feedbacks);
    }
   

    
    
}
