<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\LocationResourceCollection;
use App\EditorPro\Http\Resources\Cms\LocationResource;

use App\EditorPro\Models\Location;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\LocationRepository;
use Illuminate\Support\Facades\Validator;

class LocationController extends ApiController
{
    protected $locationRepository;

    public function __construct(LocationRepository $locationRepository){
        $this->locationRepository = $locationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return PageCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $pages = $this->locationRepository->getAll($params);
        return new LocationResourceCollection($pages);
    }

    
    
}
