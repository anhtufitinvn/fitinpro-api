<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\ColorResourceCollection;
use App\EditorPro\Http\Resources\Cms\ColorResource;

use App\EditorPro\Models\Color;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\ColorRepository;
use Illuminate\Support\Facades\Validator;
use App\EditorPro\Services\ImportExcelService;

class ColorController extends ApiController
{
    protected $colorRepository, $importService;

    public function __construct(ColorRepository $colorRepository, ImportExcelService $importService){
        $this->colorRepository = $colorRepository;
        $this->importService = $importService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return ColorCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $color = $this->colorRepository->getAll($params);
        return new ColorResourceCollection($color);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateColorRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['code'] = space_replace(strtolower($input['code']));
        $color = $this->colorRepository->find($input['code']);
        if($color) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }
        
        $color = $this->colorRepository->create($input);
           
        return new ColorResource($color);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return ColorResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $color = $this->colorRepository->find($id);
        if(!$color){
            return $this->responseError('Color not found' );
        }
        return new ColorResource($color);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return ColorResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code']);
        $color = $this->colorRepository->update($key, $input);
        if(!$color){
            return $this->responseError('Color not found' );
        }
        
        return new ColorResource($color);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $color = $this->colorRepository->delete($key);
        if(!$color){
            return $this->responseError('Color not found' );
        }
        return $this->respondSuccess();
    }

    /**
     *
     * @param  Request
     * @return JsonResponse|Response
     */
    public function createByExcel(Request $request)
    {
        $file = $request->file('file');
        
        $validator = Validator::make(
            [
                'file' => $file,
                'extension' => strtolower($file->getClientOriginalExtension())
            ], 
            [
                'file' =>  'required',
                'extension'      => 'required|in:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $import = $this->importService->importExcelColor($file);
        if($import){
            return $this->respondSuccess();
        }else{
            return $this->respondError('Import Failed, Please check again');  
        }
    }
}
