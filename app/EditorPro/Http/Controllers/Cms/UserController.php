<?php

namespace App\EditorPro\Http\Controllers\Cms;

use App\EditorPro\Http\Resources\Cms\UserResource;
use App\EditorPro\Http\Resources\Cms\UserResourceCollection;

use App\EditorPro\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Services\EditorService;

use App\EditorPro\Repositories\UserRepository;

class UserController extends ApiController
{
    protected $service, $userRepository;

    public function __construct(EditorService $service, UserRepository $userRepository){
        $this->service = $service;
        $this->userRepository = $userRepository;
    }

    public function me()
    {
        $me = Auth::user();
        return new UserResource($me);
    }

    public function changeProfile(Request $request)
    {
        $token = $request->bearerToken();
        $input = $request->only(['address', 'avatar', 'full_name', 'phone']);
        $result = $this->service->changeProfile($input);
        if($result){
            $data = [
                'phone' => $result['phone'],
                'avatar' => $result['avatar'],
                'full_name' => $result['full_name'],
            ];
            $me = Auth::user();
            $me->update($data);
            return new UserResource($me);
        }
        else{           
            return $this->responseError('Update Failed', 'VERIFICATION_FAILED');
        }
        
    }


    public function removeAccount(Request $request){
        $user = Auth::user();
        
        $id = $request->get('id');
        $result = $this->service->removeAccount($id);
        if($result){
            return $this->respondSuccess('Delete Successfully!');
        }else{
            return $this->responseError('Delete Account Failed', 'INVALID');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return UserCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $users = $this->userRepository->getAll($params);
        return new UserResourceCollection($users);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    // public function store(Request $request)
    // {
    //     $input = $request->all();

    //     $validator = Validator::make($input, [
    //         'code'   => 'required'
    //     ]);
        
    //     if($validator->fails()){
    //         $errorString = implode(",", $validator->messages()->all());
    //         return $this->responseError($errorString, "INVALID");  
    //     }
    //     $input['code'] = strtolower($input['code']);
    //     $user = $this->userRepository->find($input['code']);
    //     if($user) {
    //         return $this->responseError('Code duplicated', 'DATA_EXISTS');
    //     }

    //     $user = $this->userRepository->create($input);
           
    //     return new UserResource($user);
    // }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return UserResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $user = $this->userRepository->find($id);
        if(!$user){
            return $this->responseError('User not found' );
        }
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return UserResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->only(['address', 'phone', 'full_name', 'brand']);
        $user = $this->userRepository->update($key, $input);
        if(!$user){
            return $this->responseError('User not found' );
        }
        
        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $result = $this->service->removeAccount($key);
        if($result){
            return $this->respondSuccess('Delete Successfully!');
        }else{
            return $this->responseError('Delete Account Failed', 'INVALID');
        }
    }

}
