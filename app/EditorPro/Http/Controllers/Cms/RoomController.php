<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\RoomResourceCollection;
use App\EditorPro\Http\Resources\Cms\RoomResource;
use App\EditorPro\Events\ApproveRoom;
use App\EditorPro\Events\RejectRoom;
use App\EditorPro\Models\Room;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\RoomRepository;
use App\EditorPro\Repositories\Object3dRepository;
use Illuminate\Support\Facades\Validator;

class RoomController extends ApiController
{
    protected $roomRepository, $objectRepository;

    public function __construct(RoomRepository $roomRepository, Object3dRepository $objectRepository){
        $this->roomRepository = $roomRepository;
        $this->objectRepository = $objectRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return RoomCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $rooms = $this->roomRepository->getAll($params);
        return new RoomResourceCollection($rooms);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRoomRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        $input = $request->all();

        //$input['ownerId'] = $user->auth_id;
        $input['isTemplate'] = true;
        $validator = Validator::make($input, [
            'keyname'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['keyname'] = space_replace(strtolower($input['keyname']), '_');
        $room = $this->roomRepository->find($input['keyname']);
        if($room) {
            return $this->responseError('Key name duplicated', 'DATA_EXISTS');
        }
        $input['status'] = Room::PUBLISHED;
        $dataJson = $input['dataJson'] ?? [];
        $dataJson['RoomId'] = $input['keyname'];
        $input['dataJson'] = $dataJson;
        $room = $this->roomRepository->create($input);
           
        return new RoomResource($room);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return RoomResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $room = $this->roomRepository->find($id);
        if(!$room){
            return $this->responseError('Room not found' );
        }
        return new RoomResource($room);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return RoomResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['keyname', 'thumbList']);
        $room = $this->roomRepository->update($key, $input);
        if(!$room){
            return $this->responseError('Room not found' );
        }
        
        return new RoomResource($room);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $room = $this->roomRepository->delete($key);
        if(!$room){
            return $this->responseError('Room not found' );
        }
        return $this->respondSuccess();
    }

    /**
     * .
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function approved(Request $request,$key)
    {
        $room = $this->roomRepository->find($key);
        if(!$room){
            return $this->responseError('Room not found' );
        }
        
        if($room->status != Room::WAITING){
            return $this->responseError('Room is not in wating status. Cannot approve this room' );
        }

        
        $event = event(new ApproveRoom($room));
        return new RoomResource($event[0]);
    }

    public function rejected(Request $request,$key)
    {
        $room = $this->roomRepository->find($key);
        if(!$room){
            return $this->responseError('Room not found' );
        }
       
        if($room->status != Room::WAITING){
            return $this->responseError('Room is not in wating status. Cannot approve this room' );
        }

        //$input['status'] = Room::REJECTED;
        //$room = $this->roomRepository->update($key, $input);
        $event = event(new RejectRoom($room));
        return new RoomResource($event[0]);
    }
    
    public function getListItems(Request $request, $key){
        $room = $this->roomRepository->find($key);
        if(!$room){
            return $this->responseError('Room not found' );
        }
        $dataJson = $room->dataJson;
        if(!is_array($dataJson) || empty($dataJson['TTS']) || !is_array($dataJson['TTS'])){
            return $this->responseError('Room JSON wrong format' );
        }

        $listObjects = $dataJson['TTS'];
        $listObjectsKey = array_column($listObjects, 'Name');

        $object3dItems = $this->objectRepository->getByKeys($listObjectsKey);
        
        $array_values = array_count_values($listObjectsKey);
        array_walk($array_values, function (&$value, $key) use ($object3dItems){
            $arr = [
                'count' => $value,
                'object' => $object3dItems->find($key)
            ];
            $value = $arr;
        });
        
        return $this->responseObjectData($array_values);
    }
}
