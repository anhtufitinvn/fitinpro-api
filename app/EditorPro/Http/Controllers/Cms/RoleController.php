<?php

namespace App\EditorPro\Http\Controllers\Cms;

use App\EditorPro\Http\Resources\Cms\RoleResource;
use App\EditorPro\Http\Resources\Cms\RoleResourceCollection;
use App\EditorPro\Models\Role;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\EditorPro\Repositories\RoleRepository;

class RoleController extends ApiController
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository){
        $this->roleRepository = $roleRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return RoleCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $roles = $this->roleRepository->getAll($params);
        return new RoleResourceCollection($roles);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'title'   => 'required',
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['name'] = space_replace(strtolower($input['name']));
        $role = $this->roleRepository->find($input['name']);
        if($role) {
            return $this->responseError('Name duplicated', 'DATA_EXISTS');
        }
        if(!empty($input['permissions'])){
            $role->syncPermissions($input['permissions']);
        }
        $role = $this->roleRepository->create($input);
           
        return new RoleResource($role);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return RoleResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $role = $this->roleRepository->find($id);
        if(!$role){
            return $this->responseError('Role not found' );
        }
        return new RoleResource($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return RoleResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->only(['title', 'permissions']);
        $role = $this->roleRepository->update($key, $input);
        
        if(!$role){
            return $this->responseError('Role not found' );
        }
        if(!empty($input['permissions'])){
            $role->syncPermissions($input['permissions']);
        }
        return new RoleResource($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $role = $this->roleRepository->delete($key);
        if(!$role){
            return $this->responseError('Account not found' );
        }
        return $this->respondSuccess();
    }

}
