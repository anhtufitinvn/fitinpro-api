<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\MaterialCategoryResourceCollection;
use App\EditorPro\Http\Resources\Cms\MaterialCategoryResource;

use App\EditorPro\Models\MaterialCategory;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\MaterialCategoryRepository;
use Illuminate\Support\Facades\Validator;

class MaterialCategoryController extends ApiController
{
    protected $materialCategoryRepository;

    public function __construct(MaterialCategoryRepository $materialCategoryRepository){
        $this->materialCategoryRepository = $materialCategoryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return MaterialCategoryCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $materialCategories = $this->materialCategoryRepository->getAll($params);
        return new MaterialCategoryResourceCollection($materialCategories);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateMaterialCategoryRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['code'] = strtolower($input['code']);
        $materialCategory = $this->materialCategoryRepository->find($input['code']);
        if($materialCategory) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }
        
        $materialCategory = $this->materialCategoryRepository->create($input);
           
        return new MaterialCategoryResource($materialCategory);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return MaterialCategoryResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $materialCategory = $this->materialCategoryRepository->find($id);
        if(!$materialCategory){
            return $this->responseError('MaterialCategory not found' );
        }
        return new MaterialCategoryResource($materialCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return MaterialCategoryResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code']);
        $materialCategory = $this->materialCategoryRepository->update($key, $input);
        if(!$materialCategory){
            return $this->responseError('MaterialCategory not found' );
        }
        
        return new MaterialCategoryResource($materialCategory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $materialCategory = $this->materialCategoryRepository->delete($key);
        if(!$materialCategory){
            return $this->responseError('MaterialCategory not found' );
        }
        return $this->respondSuccess();
    }

    
}
