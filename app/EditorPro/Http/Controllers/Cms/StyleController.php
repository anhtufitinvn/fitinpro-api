<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\StyleResourceCollection;
use App\EditorPro\Http\Resources\Cms\StyleResource;

use App\EditorPro\Models\Style;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\StyleRepository;
use App\EditorPro\Repositories\Object3dRepository;
use App\EditorPro\Repositories\LocationRepository;
use Illuminate\Support\Facades\Validator;

class StyleController extends ApiController
{
    protected $styleRepository, $objectRepository, $locationRepository;

    public function __construct(StyleRepository $styleRepository, Object3dRepository $objectRepository, LocationRepository $locationRepository){
        $this->styleRepository = $styleRepository;
        $this->objectRepository = $objectRepository;
        $this->locationRepository = $locationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return StyleCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $styles = $this->styleRepository->getAll($params);
        return new StyleResourceCollection($styles);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['code'] = space_replace(strtolower($input['code']));
        $style = $this->styleRepository->find($input['code']);
        if($style) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }
        if(!empty($input['location'])){
            $location = $input['location'];
            $city = $this->locationRepository->getCity($location['city_id'] ?? 0);
            if($city){
                $input['location']['city_name'] = $city->city_name;
            }

            $district = $this->locationRepository->getDistrict($location['district_id'] ?? 0);
            
            if($district){
                $input['location']['district_name'] = $district->districts[0]['district_name'];
            }
        }
        $input['ownerId'] = Auth::user()->auth_id;
        $style = $this->styleRepository->create($input);
           
        return new StyleResource($style);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return StyleResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $style = $this->styleRepository->find($id);
        if(!$style){
            return $this->responseError('Style not found' );
        }
        return new StyleResource($style);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return StyleResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code', 'thumbList']);
        if(!empty($input['location'])){
            $location = $input['location'];
            $city = $this->locationRepository->getCity($location['city_id'] ?? 0);
            if($city){
                $input['location']['city_name'] = $city->city_name;
            }

            $district = $this->locationRepository->getDistrict($location['district_id'] ?? 0);
            
            if($district){
                $input['location']['district_name'] = $district->districts[0]['district_name'];
            }
        }
        $style = $this->styleRepository->update($key, $input);
        if(!$style){
            return $this->responseError('Style not found' );
        }
        
        return new StyleResource($style);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $style = $this->styleRepository->delete($key);
        if(!$style){
            return $this->responseError('Style not found' );
        }
        return $this->respondSuccess();
    }


    public function approved(Request $request,$key)
    {
        $style = $this->styleRepository->find($key);
        if(!$style){
            return $this->responseError('Style not found' );
        }
        
        if($style->status != Style::WAITING){
            return $this->responseError('Style is not in wating status. Cannot approve this style' );
        }

        
        $event = event(new ApproveStyle($style));
        return new StyleResource($event[0]);
    }

    public function rejected(Request $request,$key)
    {
        $style = $this->styleRepository->find($key);
        if(!$style){
            return $this->responseError('Style not found' );
        }
       
        if($style->status != Style::WAITING){
            return $this->responseError('Style is not in wating status. Cannot approve this style' );
        }

        //$input['status'] = Style::REJECTED;
        //$style = $this->styleRepository->update($key, $input);
        $event = event(new RejectStyle($style));
        return new StyleResource($event[0]);
    }
    
    public function getListItems(Request $request, $key){
        $style = $this->styleRepository->find($key);
        if(!$style){
            return $this->responseError('Style not found' );
        }
        $dataJson = $style->dataJson;
        if(!is_array($dataJson) || empty($dataJson['TTS']) || !is_array($dataJson['TTS'])){
            return $this->responseError('Style JSON wrong format' );
        }

        $listObjects = $dataJson['TTS'];
        $listObjectsKey = array_column($listObjects, 'Name');

        $object3dItems = $this->objectRepository->getByKeys($listObjectsKey);
        
        $array_values = array_count_values($listObjectsKey);
        array_walk($array_values, function (&$value, $key) use ($object3dItems){
            $arr = [
                'count' => $value,
                'object' => $object3dItems->find($key)
            ];
            $value = $arr;
        });
        
        return $this->responseObjectData($array_values);
    }
    
}
