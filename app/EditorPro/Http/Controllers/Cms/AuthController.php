<?php

namespace App\EditorPro\Http\Controllers\Cms;

use App\EditorPro\Models\CMSUser;
use App\EditorPro\Http\Resources\Cms\CMSUserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Storage;

/**
 * @group 1. Authentication
 */
class AuthController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    

    public function __construct()
    {
        
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $credentials = $request->only('email', 'password');
        $token = null;
        try { 
           if (Auth::guard('editor-cms')->attempt($credentials)) {              
                $token = JWTAuth::fromUser(Auth::guard('editor-cms')->user());
                
                return response()->json([
                    'code' => 0,
                    'data' => [
                        'access_token' => $token,
                        'auth' => Auth::guard('editor-cms')->user()->load('roles')->append(['avatarUrl', 'all_permissions'])
                    ],
                    'message' => "Success"
                ]);
           }else{
           
            return $this->responseError('Unauthorized!', 'UnauthorizedException', -401);
           }

        } catch (\Exception $e) {
            return $this->respondError($e->getMessage());
        }
    }


    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $result = Auth::guard('editor-cms')->logout();
        return $this->respondSuccess(__('Successfully logged out'));
    }



    

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('editor-cms');
    }

    public function updateProfile(Request $request){
        $input = $request->only(['name']);
        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $user = $this->guard()->user();
        $params = ['name' => $input['name']];     
        $user->update($params);     
        return new CMSUserResource($user);
    }


    public function updatePassword(Request $request)
    {
        $input = $request->only(['password', 'new_password', 'confirm_new_password']);
        $validator = Validator::make($input, [
            'password'   => 'required',
            'new_password'   => 'required|same:confirm_new_password',
            'confirm_new_password' => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $user = $this->guard()->user();
        $credentials = [
            'email' => $user->email,
            'password' => $input['password']
        ];
        if( $this->guard()->attempt($credentials)){
            $params = ['password' => (string) $input['new_password']];     
            $user->update($params);     
            return new CMSUserResource($user);
        }
        else{
            return $this->responseError('You password is not exact. Please try again' );
        }
        
    }
    
    public function uploadAvatar(Request $request){
        $driver = config('fitin.editor.filesystem_driver'); 
        $file = $request->file('image');
        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
        ]);

        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $type = CMSUser::WORKSPACE_DIR;

        $thumb_dir = CMSUser::THUMB_DIR;
        $user = $this->guard()->user();
        
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = strtolower($arrFileName['basename']);
            
            $revision = \Carbon\Carbon::now()->timestamp;
            $path = "$type/{$user->auth_id}/$thumb_dir/$revision";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);

            uploadDriver($file_base_name, $driver, "$path/$file_base_name", Storage::disk($driver)->url("$path/$file_base_name"), $user);
        

            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name"
            ];       
            

            $user->avatar = $upload;
            $user->save();
        
            return new CMSUserResource($user);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

}
