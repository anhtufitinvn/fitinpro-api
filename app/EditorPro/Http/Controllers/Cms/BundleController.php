<?php

namespace App\EditorPro\Http\Controllers\Cms;

use App\EditorPro\Http\Resources\Cms\BundleResourceCollection;
use App\EditorPro\Http\Resources\Cms\BundleResource;
use App\EditorPro\Events\ApproveBundle;
use App\EditorPro\Events\RejectBundle;
use App\EditorPro\Models\Bundle;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

use App\EditorPro\Repositories\BundleRepository;
use App\EditorPro\Repositories\Object3dRepository;

class BundleController extends ApiController
{
    protected $bundleRepository, $objectRepository;

    public function __construct(BundleRepository $bundleRepository, Object3dRepository $objectRepository){
        $this->bundleRepository = $bundleRepository;
        $this->objectRepository = $objectRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return BundleCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $bundles = $this->bundleRepository->getAll($params);
        return new BundleResourceCollection($bundles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        $input = $request->all();
        //$input['ownerId'] = $user->auth_id;
        $input['isTemplate'] = true;
        
        $validator = Validator::make($input, [
            'keyname'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['keyname'] = space_replace(strtolower($input['keyname']), '_');
        $bundle = $this->bundleRepository->find($input['keyname']);
        if($bundle) {
            return $this->responseError('Key name duplicated', 'DATA_EXISTS');
        }
        $input['status'] = Bundle::PUBLISHED;
        $dataJson = $input['dataJson'] ?? [];
        $dataJson['Key_name'] = $input['keyname'];
        $input['dataJson'] = $dataJson;
        $bundle = $this->bundleRepository->create($input);
        $event = event(new ApproveBundle($bundle));
        return new BundleResource($event[0]);
        //return new BundleResource($bundle);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $id
     * @return BundleResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $key)
    {
        $bundle = $this->bundleRepository->find($key);
        if(!$bundle){
            return $this->responseError('Bundle not found' );
        }
        return new BundleResource($bundle);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $id
     * @return BundleResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['keyname', 'thumbList']);
        $bundle = $this->bundleRepository->update($key, $input);
        if(!$bundle){
            return $this->responseError('Bundle not found' );
        }
        
        return new BundleResource($bundle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $id
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $bundle = $this->bundleRepository->delete($key);
        if(!$bundle){
            return $this->responseError('Bundle not found' );
        }
        return $this->respondSuccess();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function approved(Request $request,$key)
    {
        $bundle = $this->bundleRepository->find($key);
        if(!$bundle){
            return $this->responseError('Bundle not found' );
        }
         if(!$bundle->metaZip){
            return $this->responseError('Zip meta does not uploaded. Cannot approve this bundle' );
        }

        if($bundle->status != Bundle::WAITING){
            return $this->responseError('Bundle is not in wating status. Cannot approve this bundle' );
        }

        
        $event = event(new ApproveBundle($bundle));
        return new BundleResource($event[0]);
    }

    public function rejected(Request $request,$key)
    {
        $bundle = $this->bundleRepository->find($key);
        if(!$bundle){
            return $this->responseError('Bundle not found' );
        }
        if(!$bundle->metaZip){
            return $this->responseError('Zip meta does not uploaded. Cannot approve this bundle' );
        }

        if($bundle->status != Bundle::WAITING){
            return $this->responseError('Bundle is not in wating status. Cannot approve this bundle' );
        }

        //$input['status'] = Bundle::REJECTED;
        //$bundle = $this->bundleRepository->update($key, $input);
        $event = event(new RejectBundle($bundle));
        return new BundleResource($event[0]);
    }
    
    public function getListItems(Request $request, $key){
        $bundle = $this->bundleRepository->find($key);
        if(!$bundle){
            return $this->responseError('Bundle not found' );
        }
        $dataJson = $bundle->dataJson;
        if(!is_array($dataJson) || empty($dataJson['List_Objects']) || !is_array($dataJson['List_Objects'])){
            return $this->responseError('Bundle JSON wrong format' );
        }

        $listObjects = $dataJson['List_Objects'];
        $listObjectsKey = array_column($listObjects, 'Key_name');

        $object3dItems = $this->objectRepository->getByKeys($listObjectsKey);
        
        $array_values = array_count_values($listObjectsKey);
        array_walk($array_values, function (&$value, $key) use ($object3dItems){
            $arr = [
                'count' => $value,
                'object' => $object3dItems->find($key)
            ];
            $value = $arr;
        });
        
        return $this->responseObjectData($array_values);
    }
}
