<?php

namespace App\EditorPro\Http\Controllers\Cms;

use App\EditorPro\Http\Resources\Cms\LayoutResourceCollection;
use App\EditorPro\Http\Resources\Cms\LayoutResource;
use App\EditorPro\Http\Resources\Cms\LayoutResourceWithoutJsonCollection;
use App\EditorPro\Http\Resources\Cms\LayoutResourceWithoutJson;
use App\EditorPro\Events\ApproveLayout;
use App\EditorPro\Events\RejectLayout;
use App\FitIn\Asset;
use App\EditorPro\Models\Layout;
use App\EditorPro\Models\LayoutRoom;
use App\EditorPro\Models\Object3d;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\EditorPro\Repositories\LayoutRepository;
use App\EditorPro\Repositories\Object3dRepository;
use App\EditorPro\Repositories\StyleRepository;
use App\EditorPro\Repositories\ProjectRepository;

use App\EditorPro\Services\ResourceService;

class LayoutController extends ApiController
{
    protected $layoutRepository, $objectRepository, $resourceService;

    public function __construct(
        LayoutRepository $layoutRepository, 
        Object3dRepository $objectRepository, 
        StyleRepository $styleRepository, 
        ProjectRepository $projectRepository,
        ResourceService $resourceService
    ){
        $this->layoutRepository = $layoutRepository;
        $this->objectRepository = $objectRepository;
        $this->projectRepository = $projectRepository;
        $this->styleRepository = $styleRepository;
        $this->resourceService = $resourceService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return LayoutCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $layouts = $this->layoutRepository->getAll($params);
        if($request->get('without_json')){
            return new LayoutResourceWithoutJsonCollection($layouts);
        }
        else return new LayoutResourceCollection($layouts);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateLayoutRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        //$user = Auth::user();
        $input = $request->except(['gallery']);
        //$input['ownerId'] = $user->auth_id;
        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['isTemplate'] = true;
        $input['name'] = space_replace(strtolower($input['name']), '_');
        $layout = $this->layoutRepository->find($input['name']);
        if($layout) {
            return $this->responseError('Key name duplicated', 'DATA_EXISTS');
        }
        if(empty($input['status'])){
            $input['status'] = Layout::PENDING;
        }

        $input['metadata'] = $this->buildMetadata($input);

        $layout = $this->layoutRepository->create($input);
           
        return new LayoutResource($layout);
        
       
    }

    protected function buildMetadata($input, $key = NULL){
        $metadata = $input['metadata'] ?? [];
        if(!empty($input['style'])){
            $style = $this->styleRepository->find($input['style']);
            if($style){
                $metadata['style'] = [
                    'name' => $style->name,
                    'key' => $input['style']
                ];
            }
        }

        if(!$key){
            $metadata['creator'] = [
                'email' => Auth::user()->email
            ];
            
        }
        
        $input_rooms = $metadata['rooms'] ?? [];
        $rooms = [];
        foreach ($input_rooms as $room){
            if(!empty($room['_id'])){
                $rooms[] = $room;
            }else{
                $new_room = LayoutRoom::create($room);
                $rooms[] = $new_room->toArray();
            }
            
        }
        $metadata['rooms'] = $rooms;

        return $metadata;
    }


    public function storeByUploadJson(Request $request){
        $file = $request->file('file');
        
        $validator = Validator::make($request->all(), [
            'file' =>  'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        if ($file && $file->isValid()) {
            $json = file_get_contents($file->getRealPath());
            $json = json_decode($json, true);
            $key = strtolower($json['RoomId']);
            
            $layout = $this->layoutRepository->find($key);
            if($layout) {
                return $this->responseError('Key name duplicated', 'DATA_EXISTS');
            }
            $new_data = [
                'dataJson' => $json,
                'name' => $key,
                'status' => Layout::PUBLISHED,
                'isTemplate' => true
            ];
            $layout = $this->layoutRepository->create($new_data);

            $asset = new Asset();
            $fitin_layout = $asset->getByKey('layout-by-key', $key);

            

            if($fitin_layout){
                $newLayout = $this->resourceService->syncLayout($layout, $fitin_layout);
            }

            return new LayoutResource($layout);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return LayoutResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $key)
    {
        $layout = $this->layoutRepository->find($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        return new LayoutResource($layout);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return LayoutResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['name', 'thumbList', 'gallery']);
        $input['metadata'] = $this->buildMetadata($input, $key);
        $layout = $this->layoutRepository->update($key, $input);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        
        return new LayoutResource($layout);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $layout = $this->layoutRepository->delete($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }

        return $this->respondSuccess();
    }

    public function destroyGallery(Request $request, $key, $id)
    {
        

        $layout = $this->layoutRepository->deleteGallery($key, $id);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }

        return $this->respondSuccess('Success', ['gallery_id' => $id]);
    }


    public function approved(Request $request,$key)
    {
        $layout = $this->layoutRepository->find($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        
        if($layout->status != Layout::WAITING){
            return $this->responseError('Layout is not in wating status. Cannot approve this layout' );
        }

        
        $event = event(new ApproveLayout($layout));
        return new LayoutResource($event[0]);
    }

    public function rejected(Request $request,$key)
    {
        $layout = $this->layoutRepository->find($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
       
        if($layout->status != Layout::WAITING){
            return $this->responseError('Layout is not in wating status. Cannot approve this layout' );
        }

        //$input['status'] = Layout::REJECTED;
        //$layout = $this->layoutRepository->update($key, $input);
        $event = event(new RejectLayout($layout));
        return new LayoutResource($event[0]);
    }
    
    public function getListItems(Request $request, $key){
        $layout = $this->layoutRepository->find($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        $dataJson = $layout->dataJson;
        if(!is_array($dataJson) || empty($dataJson['Items']) || !is_array($dataJson['Items'])){
            return $this->responseError('Layout JSON wrong format' );
        }

        $listObjects = $dataJson['Items'];
        $listObjectsKey = array_column($listObjects, 'Name');

        $object3dItems = $this->objectRepository->getByKeys($listObjectsKey);
        
        $array_values = array_count_values($listObjectsKey);
        array_walk($array_values, function (&$value, $key) use ($object3dItems){
            $arr = [
                'count' => $value,
                'object' => $object3dItems->find($key)
            ];
            $value = $arr;
        });
        
        return $this->responseObjectData($array_values);
    }

    public function getDataJson(Request $request, $key){
        $layout = $this->layoutRepository->find($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        $dataJson = $layout->dataJson;
        
        return $this->responseObjectData($dataJson);
    }

    public function syncDataFromEcom(Request $request){
        $sync = $this->resourceService->syncFitinLayouts();
        if($sync){
            return $this->respondSuccess();
        }else{
            return $this->respondError('Sync failed, Please check again');  
        }
    }
}
