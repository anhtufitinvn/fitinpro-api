<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\ColorBrandResourceCollection;
use App\EditorPro\Http\Resources\Cms\ColorBrandResource;

use App\EditorPro\Models\ColorBrand;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\ColorBrandRepository;
use Illuminate\Support\Facades\Validator;

class ColorBrandController extends ApiController
{
    protected $colorBrandRepository;

    public function __construct(ColorBrandRepository $colorBrandRepository){
        $this->colorBrandRepository = $colorBrandRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return ColorBrandCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $colorBrands = $this->colorBrandRepository->getAll($params);
        return new ColorBrandResourceCollection($colorBrands);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateColorBrandRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['code'] = strtolower($input['code']);
        $colorBrand = $this->colorBrandRepository->find($input['code']);
        if($colorBrand) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }
        
        $colorBrand = $this->colorBrandRepository->create($input);
           
        return new ColorBrandResource($colorBrand);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return ColorBrandResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $colorBrand = $this->colorBrandRepository->find($id);
        if(!$colorBrand){
            return $this->responseError('ColorBrand not found' );
        }
        return new ColorBrandResource($colorBrand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return ColorBrandResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code']);
        $colorBrand = $this->colorBrandRepository->update($key, $input);
        if(!$colorBrand){
            return $this->responseError('ColorBrand not found' );
        }
        
        return new ColorBrandResource($colorBrand);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $colorBrand = $this->colorBrandRepository->delete($key);
        if(!$colorBrand){
            return $this->responseError('ColorBrand not found' );
        }
        return $this->respondSuccess();
    }

    
}
