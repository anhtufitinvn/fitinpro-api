<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\RoomCategoryResourceCollection;
use App\EditorPro\Http\Resources\Cms\RoomCategoryResource;

use App\EditorPro\Models\RoomCategory;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\RoomCategoryRepository;
use Illuminate\Support\Facades\Validator;

class RoomCategoryController extends ApiController
{
    protected $roomCategoryRepository;

    public function __construct(RoomCategoryRepository $roomCategoryRepository){
        $this->roomCategoryRepository = $roomCategoryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return RoomCategoryCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $roomCategories = $this->roomCategoryRepository->getAll($params);
        return new RoomCategoryResourceCollection($roomCategories);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRoomCategoryRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['code'] = strtolower($input['code']);
        $roomCategory = $this->roomCategoryRepository->find($input['code']);
        if($roomCategory) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }

        $roomCategory = $this->roomCategoryRepository->create($input);
           
        return new RoomCategoryResource($roomCategory);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return RoomCategoryResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $roomCategory = $this->roomCategoryRepository->find($id);
        if(!$roomCategory){
            return $this->responseError('RoomCategory not found' );
        }
        return new RoomCategoryResource($roomCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return RoomCategoryResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code']);
        $roomCategory = $this->roomCategoryRepository->update($key, $input);
        if(!$roomCategory){
            return $this->responseError('RoomCategory not found' );
        }
        
        return new RoomCategoryResource($roomCategory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $roomCategory = $this->roomCategoryRepository->delete($key);
        if(!$roomCategory){
            return $this->responseError('RoomCategory not found' );
        }
        return $this->respondSuccess();
    }

    
}
