<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\BrandResourceCollection;
use App\EditorPro\Http\Resources\Cms\BrandResource;

use App\EditorPro\Models\Brand;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\BrandRepository;
use Illuminate\Support\Facades\Validator;

class BrandController extends ApiController
{
    protected $brandRepository;

    public function __construct(BrandRepository $brandRepository){
        $this->brandRepository = $brandRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return BrandCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $brands = $this->brandRepository->getAll($params);
        return new BrandResourceCollection($brands);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['code'] = strtolower($input['code']);
        $brand = $this->brandRepository->find($input['code']);
        if($brand) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }

        $brand = $this->brandRepository->create($input);
           
        return new BrandResource($brand);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return BrandResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $brand = $this->brandRepository->find($id);
        if(!$brand){
            return $this->responseError('Brand not found' );
        }
        return new BrandResource($brand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return BrandResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code']);
        $brand = $this->brandRepository->update($key, $input);
        if(!$brand){
            return $this->responseError('Brand not found' );
        }
        
        return new BrandResource($brand);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $brand = $this->brandRepository->delete($key);
        if(!$brand){
            return $this->responseError('Brand not found' );
        }
        return $this->respondSuccess();
    }

    
}
