<?php

namespace App\EditorPro\Http\Controllers\Cms;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\Object3dRepository;
use App\EditorPro\Http\Resources\Cms\ObjectResourceCollection;
use App\EditorPro\Http\Resources\Cms\ObjectResource;

class ObjectController extends ApiController
{
    
    protected  $object3dRepository;
    public function __construct(Object3dRepository $object3dRepository){
        $this->object3dRepository = $object3dRepository;
    }



    public function index(Request $request)
    {
        $params = $request->all();

        $objects = $this->object3dRepository->getAll($params);
        return new ObjectResourceCollection($objects);
    }


    public function get3JSObjectNotExist(Request $request){
        $params = $request->all();
        $objects = $this->object3dRepository->getAll($params);
        $objects_array = $objects->pluck('name')->toArray();
        $objects_3js = \DB::collection('Objects')->whereNotIn('name', $objects_array)
        ->where(function ($q) {
            $q->whereNull('attributes.isTTS')->orWhere('attributes.TTS', false)->orWhere('attributes.TTS', 0);
        })
        ->get();
        return response()->json([
            'code' => 0,
            "message" => "success",
            'data' => $objects_3js->pluck('name')->toArray()
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return ObjectResource
     */
    public function show(Request $request, $key)
    {
        $object = $this->object3dRepository->find($key);
        if(!$object){
            return $this->responseError('Object not found' );
        }
        return new ObjectResource($object);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        
        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['name'] = space_replace(strtolower($input['name']), '_');
        $object = $this->object3dRepository->find($input['name']);
        if ($object) {
            return $this->responseError('Key name duplicated', 'DATA_EXISTS');
        }
        $object = $this->object3dRepository->create($input);

        return new ObjectResource($object);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return ObjectResource|JsonResponse
     */
    public function update(Request $request, $key)
    {
       
        $input = $request->except(['name', 'thumbList']);
        $object = $this->object3dRepository->update($key, $input);
        if(!$object){
            return $this->responseError('Object not found' );
        }
        
        return new ObjectResource($object);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     */
    public function destroy(Request $request,$key)
    {
        $object = $this->object3dRepository->delete($key);
        if(!$object){
            return $this->responseError('Object not found' );
        }
        return $this->respondSuccess();
    }
}
