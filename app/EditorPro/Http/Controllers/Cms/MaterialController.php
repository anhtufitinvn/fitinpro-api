<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Models\Material;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\MaterialRepository;

use App\EditorPro\Http\Resources\Cms\MaterialResource;
use App\EditorPro\Http\Resources\Cms\MaterialResourceCollection;
use Excel;
use App\EditorPro\Excel\Imports\ExcelMaterial;
use App\EditorPro\Services\ImportExcelService;

class MaterialController extends ApiController
{
    protected $materialRepository, $importService;

    public function __construct(MaterialRepository $materialRepository, ImportExcelService $importService){
        $this->materialRepository = $materialRepository;
        $this->importService = $importService;
    }


    public function index(Request $request)
    {
        $params = $request->all();
    
        $materials = $this->materialRepository->getAll($params);
        return new MaterialResourceCollection($materials);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return MaterialResource
     */
    public function show(Request $request, $key)
    {
        $material = $this->materialRepository->find($key);
        if(!$material){
            return $this->responseError('Material not found' );
        }
        return new MaterialResource($material);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = request()->all();
        
        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $input['code'] = space_replace(strtolower($input['code']), '_');
        $material = $this->materialRepository->find($input['code']);
        if ($material) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }
        if(empty($input['status'])){
            $input['status'] = Material::PUBLISHED;
        }
        $material = $this->materialRepository->create($input);

        return new MaterialResource($material);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return MaterialResource|JsonResponse
     */
    public function update(Request $request, $key)
    {
       
        $input = $request->except(['code', 'thumbList']);
        $material = $this->materialRepository->update($key, $input);
        if(!$material){
            return $this->responseError('Material not found' );
        }
        
        return new MaterialResource($material);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     */
    public function destroy(Request $request,$key)
    {
        $material = $this->materialRepository->delete($key);
        if(!$material){
            return $this->responseError('Material not found' );
        }
        return $this->respondSuccess();
    }

    /**
     *
     * @param  Request
     * @return JsonResponse|Response
     */
    public function createByExcel(Request $request)
    {
        $file = $request->file('file');
        
        $validator = Validator::make(
            [
                'file' => $file,
                'extension' => strtolower($file->getClientOriginalExtension())
            ], 
            [
                'file' =>  'required',
                'extension'      => 'required|in:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $import = $this->importService->importExcelMaterial($file);
        if($import){
            return $this->respondSuccess();
        }else{
            return $this->respondError('Import Failed, Please check again');  
        }
    }
}
