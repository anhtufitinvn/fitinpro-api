<?php

namespace App\EditorPro\Http\Controllers\Cms;

use App\EditorPro\Http\Resources\Cms\FeedbackResourceCollection;
use App\EditorPro\Http\Resources\Cms\FeedbackResource;
use App\EditorPro\Models\Feedback;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\FeedbackRepository;

use Illuminate\Support\Facades\Validator;

class FeedbackController extends ApiController
{
    protected $feedbackRepository;

    public function __construct(FeedbackRepository $feedbackRepository){
        $this->feedbackRepository = $feedbackRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return FeedbackCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $feedbacks = $this->feedbackRepository->getAll($params);
        return new FeedbackResourceCollection($feedbacks);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateFeedbackRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = request()->all();
        $validator = Validator::make($input, [
            'content'   => 'required',
            'title'  => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $input['ownerId'] = Auth::user()->auth_id;
        $feedback = $this->repository->create($input);

        return new FeedbackResource($feedback);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return FeedbackResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $feedback = $this->feedbackRepository->find($id);
        if(!$feedback){
            return $this->responseError('Feedback not found' );
        }
        return new FeedbackResource($feedback);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return FeedbackResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->all();
        $feedback = $this->feedbackRepository->update($key, $input);
        if(!$feedback){
            return $this->responseError('Feedback not found' );
        }
        
        return new FeedbackResource($feedback);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $feedback = $this->feedbackRepository->delete($key);
        if(!$feedback){
            return $this->responseError('Feedback not found' );
        }
        return $this->respondSuccess();
    }

}
