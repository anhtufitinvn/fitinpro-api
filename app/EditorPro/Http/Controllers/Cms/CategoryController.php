<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\CategoryResourceCollection;
use App\EditorPro\Http\Resources\Cms\CategoryResource;

use App\EditorPro\Models\Category;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\CategoryRepository;
use Illuminate\Support\Facades\Validator;

class CategoryController extends ApiController
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository){
        $this->categoryRepository = $categoryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return CategoryCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $categories = $this->categoryRepository->getAll($params);
        return new CategoryResourceCollection($categories);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCategoryRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        if(isset($input['priority'])){
            $input['priority'] = (int) $input['priority'];
        }
        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['code'] = strtolower($input['code']);
        $category = $this->categoryRepository->find($input['code']);
        if($category) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }

        $category = $this->categoryRepository->create($input);
           
        return new CategoryResource($category);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return CategoryResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $category = $this->categoryRepository->find($id);
        if(!$category){
            return $this->responseError('Category not found' );
        }
        return new CategoryResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return CategoryResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code']);
        if(isset($input['priority'])){
            $input['priority'] = (int) $input['priority'];
        }
        $category = $this->categoryRepository->update($key, $input);
        if(!$category){
            return $this->responseError('Category not found' );
        }
        
        return new CategoryResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $category = $this->categoryRepository->delete($key);
        if(!$category){
            return $this->responseError('Category not found' );
        }
        return $this->respondSuccess();
    }

    
}
