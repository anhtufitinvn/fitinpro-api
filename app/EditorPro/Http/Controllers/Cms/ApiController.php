<?php

namespace App\EditorPro\Http\Controllers\Cms;

use App\Traits\JsonRespondController;
use App\Http\Controllers\Controller;
use App\Helpers\Utils;
use Illuminate\Support\Facades\Cache;
use \Illuminate\Support\Facades\Storage;
use App\Events\UploadEvent;

class ApiController extends Controller
{
    use JsonRespondController;


    /**
     * Return generic json response with the given data.
     *
     * @param $data
     * @param int $statusCode
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respond($data, $statusCode = 200, $headers = [])
    {
        return response()->json($data, $statusCode, $headers);
    }

    /**
     * Respond with success.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondSuccess($message = 'Success', $data = [])
    {
        return $this->respond([
            'code' => 0,
            'data' => $data,
            'message' => $message
        ]);
    }

    /**
     * Respond with created.
     *
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondCreated($data)
    {
        return $this->respond($data, 200);
    }

    /**
     * Respond with no content.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondNoContent()
    {
        return $this->respond(null, 204);
    }

    /**
     * Respond with error.
     *
     * @param $message
     * @param $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondError($message, $statusCode = -1)
    {
        return response()->json([
            'code' => -1,
            'message' => $message,
            'data' => [
                
            ],
            'errors' => [
                ['error' => $statusCode, 'message' => $message]
            ]
        ]);
    }

    /**
     * Respond with unauthorized.
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondUnauthorized($message = 'Unauthorized')
    {
        return $this->respondError($message, -401);
    }

    /**
     * Respond with forbidden.
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondForbidden($message = 'Forbidden')
    {
        return $this->respondError($message, -1);
    }

    /**
     * Respond with not found.
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondNotFound($message = 'Not Found')
    {
        return $this->respondError($message, -1);
    }

    /**
     * Respond with failed login.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondFailedLogin()
    {
        return $this->respond([
            'errors' => [
                'email or password' => 'is invalid',
            ]
        ], -401);
    }

    /**
     * Respond with internal error.
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondInternalError($message = 'Internal Error')
    {
        return $this->respondError($message, 500);
    }
 
    /**
     * Sends a response that the object has been deleted, and also indicates
     * the id of the object that has been deleted.
     *
     * @param  int $id
     * @return JsonResponse
     */
    public function respondObjectDeleted($id)
    {
        return response()->json([
            'code' => 0,
            'message' => 'success',
            'data' => [
                'id' => $id,
                'deleted' => true
            ]
        ]);
    }



    public function responseData($data, $code = 0){
        
        return response()->json([
            'code' => $code,
            'message' => 'success',
            'data' => [
                'list' => $data
            ]
        ]);
    }

    public function responseObjectData($data, $code = 0){
        
        return response()->json([
            'code' => $code,
            'message' => 'success',
            'data' => $data
        ]);
    }


    public function responseError($message, $error_code = 'NOT_FOUND', $code = -1){
        return response()->json([        
            'code' => $code,
            'message' => $message,
            'data' => [
                'error_code' => $error_code
            ],
            'errors' => []
        ]);
    }

    public function clearCache($class){
        Cache::tags($class::CACHE_TAG)->flush();
    }

}
