<?php

namespace App\EditorPro\Http\Controllers\Cms;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\EditorPro\Models\User;
use App\EditorPro\Models\Layout;
use App\EditorPro\Models\Object3d;
use App\EditorPro\Models\Library;
use App\EditorPro\Models\Bundle;
use App\EditorPro\Models\Room;
use App\EditorPro\Models\Material;
use App\EditorPro\Models\Category;
use App\EditorPro\Models\RoomCategory;
use App\EditorPro\Models\Brand;
use App\EditorPro\Models\Project;
use App\EditorPro\Models\MaterialCategory;
use App\EditorPro\Models\MaterialBrand;
use App\EditorPro\Models\ColorBrand;
use App\EditorPro\Models\Gallery;

use App\EditorPro\Http\Resources\Cms\LayoutResource;
use App\EditorPro\Http\Resources\Cms\ObjectResource;
use App\EditorPro\Http\Resources\Cms\RoomResource;

class UploadController extends ApiController
{
    /** *****************************************************************************
     * Upload Image.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadImage(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = "images";
        $file = $request->file('image');

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");
        }


        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            
            $date = Carbon::now()->format('m/d');
            $path = "$type/$date";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];

            saveFileHistory($dataFile, $driver);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

   
    /** *****************************************************************************
     * Upload  Layout asset bundle.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadLayoutAsset(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Layout::DIR_NAME;
        
        $validator = Validator::make($request->all(), [
            'file' =>  "required|file",
            'name' => 'required'
        ]);
            
        if( $validator->fails()){
            return $this->responseError('File Missing or Invalid', "INVALID");   
        }
        $file = $request->file('file');
        $name = $request->get('name');
        $layout = Layout::where('name', $name)->first();
        
        if(!$layout){
            return $this->responseError('Layout not found');
        }
        // if($layout->isTemplate){
        //     return $this->responseError('This is Template Layout. You cannot modify this!');
        // }
        
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = strtolower($arrFileName['basename']);
            $revision = \Carbon\Carbon::now()->timestamp;

            $header_version = $request->header('Version');
            if($header_version){
                $path = "$type/$name/".Layout::ASSET_DIR."/$header_version/$revision";
               
            }else{
                $path = "$type/$name/".Layout::ASSET_DIR."/$revision";
            }
        
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);

            $data = [
                 "file_name" => $file_base_name,
                 "path" => "$path/$file_base_name",
                 "url" =>  Storage::disk($driver)->url($upload)
            ];       
            
            

            if($header_version){
                $versionAssetBundle = $layout->getOriginal('versionAssetBundle');
                $versionAssetBundle[$header_version]['assetBundle'] = $upload;
                $versionAssetBundle[$header_version]['revision'] = $revision;
                $layout->versionAssetBundle = $versionAssetBundle;
            }else{
                $layout->assetBundle = "$path/$file_base_name";
                $layout->revision = $revision;
            }

            $layout->save();
            
            //$layout = Layout::find($id);
            
            // $array = [
            //     "filename" => 
            //     'name' => $layout->name,
            //     'key' => $layout->getKey(),
            // //    'category' => $layout->category,
            //     'image' => $layout->imageUrl,
            //     'downloadUrl' => $layout->downloadUrl,
            //     'version' => $layout->editorProUploadVersion,
            //     'downloadPrefabUrl' => $layout->downloadPrefabUrl
            // ];
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($layout),
                'attachmentKey' => $layout->name,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $revision
            ];
            //$this->uploadDriver($file_base_name, $driver, "$path/$file_base_name", Storage::disk($driver)->url("$path/$file_base_name"), $layout);
            saveFileHistory($dataFile, $driver, $layout);    
            clearCache([get_class($layout), Library::class]);
            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


    /** *****************************************************************************
     * Upload Layout Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadLayoutThumb(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Layout::DIR_NAME;
        $file = $request->file('image');
        $layout_id =  strtolower($request->get('name'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
            'name'   => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('File Or Name Invalid', "INVALID");   
        }

        $obj = Layout::where('_id', $layout_id)->orWhere('name', $layout_id)->first();
        if(!$obj){
            return $this->responseError('Layout not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$layout_id/" . Layout::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $obj->image = "$path/$file_base_name";
            $obj->save();
            $obj->makeMultiThumb();
            clearCache([get_class($obj)]);
            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $obj);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Bundle Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadBundleThumb(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Bundle::DIR_NAME;
        $file = $request->file('image');
        $key =  strtolower($request->get('keyname'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
            'keyname'   => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('File Or Name Invalid', "INVALID");   
        }

        $instance = Bundle::where('keyname', $key)->first();
        if(!$instance){
            return $this->responseError('Bundle not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$key/" . Bundle::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $instance->image = "$path/$file_base_name";
            $instance->save();
            $instance->makeMultiThumb(); 
            clearCache([get_class($instance)]);
            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($instance),
                'attachmentKey' => $instance->keyname,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $instance);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


/** *****************************************************************************
     * Upload Object Editor Pro Asset bundle.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadObjectAsset(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Object3d::DIR_NAME;
        $name = strtolower($request->get('name'));
        $file = $request->file('file');
        $brand =  strtolower($request->get('brand'));

        $validator = Validator::make($request->all(), [
            'file' =>  'required|file',
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $obj = Object3d::where('name', $name)->first();
        if(!$obj){
            return $this->responseError('Object3d not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            if(!$brand){
                $brand = explode('_', $file_name)[0];
            }
            if(!$name){
                $name = $file_name;
            }
            $time = Carbon::now()->timestamp;
            $new_file_base_name = strtolower($arrFileName['basename']);
            //$new_file_base_name = md5_file($file);
            $header_version = $request->header('Version');
            if($header_version){
                $path = "$type/$brand/".Object3d::ASSET_DIR."/$name/$header_version/$time" ;
            }
            else{
                $path = "$type/$brand/".Object3d::ASSET_DIR."/$name/$time" ;
            }
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $new_file_base_name);
            $data = [
                "file_name" => $new_file_base_name,
                "path" => "$path/$new_file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       
            if($header_version){
                $versionAssetBundle = $obj->getOriginal('versionAssetBundle');
                $versionAssetBundle[$header_version]['assetBundle'] = $upload;
                $versionAssetBundle[$header_version]['revision'] = $time;
                $obj->versionAssetBundle = $versionAssetBundle;
            }else{
                $obj->assetBundle = $upload;
                $obj->revision = $time;
            }
            
            $obj->save();


            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->name,
                'path' => $upload,
                'name' => $new_file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $obj);
            clearCache([get_class($obj), Library::class]);
            return new ObjectResource($obj);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Object Editor Pro FBX.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadObjectZip(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Object3d::DIR_NAME;
        $name = strtolower($request->get('name'));
        $file = $request->file('file');
        $brand =  strtolower($request->get('brand'));

        $validator = Validator::make($request->all(), [
            'file' =>  'required|file|mimetypes:application/zip',
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $obj = Object3d::where('name', $name)->first();
        if(!$obj){
            return $this->responseError('Object3d not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            if(!$brand){
                $brand = explode('_', $file_name)[0];
            }
            if(!$name){
                $name = $file_name;
            }
            $time = Carbon::now()->timestamp;
            $new_file_base_name = urlencode(strtolower($arrFileName['basename']));
            //$new_file_base_name = strtolower($arrFileName['basename']);
            //$new_file_base_name = md5_file($file);
            $path = "$type/$brand/".Object3d::ZIP_DIR."/$name/$time" ;
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $new_file_base_name);
            $data = [
                "file_name" => $new_file_base_name,
                "path" => "$path/$new_file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $obj->assetZip = $upload;
            $obj->save();


            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->name,
                'path' => $upload,
                'name' => $new_file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $obj);
            clearCache([get_class($obj), Library::class]);
            return new ObjectResource($obj);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Object Editor Pro Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadObjectThumb(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Object3d::DIR_NAME;
        $name = strtolower($request->get('name'));
        $file = $request->file('image');
        $brand =  strtolower($request->get('brand'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $obj = Object3d::where('name', $name)->first();
        if(!$obj){
            return $this->responseError('Object3d not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            if(!$brand){
                $brand = explode('_', $file_name)[0];
            }
            if(!$name){
                $name = $file_name;
            }
            $time = Carbon::now()->timestamp;
            //$new_file_base_name = urlencode(strtolower($arrFileName['basename']));
            $new_file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$type/$brand/".Object3d::THUMB_DIR."/$name/$time" ;
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $new_file_base_name);
            $data = [
                "file_name" => $new_file_base_name,
                "path" => "$path/$new_file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $obj->thumb = $upload;
            $obj->save();
            $obj->makeMultiThumb();    

            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->name,
                'path' => $upload,
                'name' => $new_file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $obj);
            clearCache([get_class($obj), Library::class]);
            return new ObjectResource($obj);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
    

    /** *****************************************************************************
     * Upload  Room asset bundle.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadRoomAsset(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Room::DIR_NAME;
        
        $validator = Validator::make($request->all(), [
            'file' =>  "required|file",
            'keyname' => 'required'
        ]);
            
        if( $validator->fails()){
            return $this->responseError('File Missing or Invalid', "INVALID");   
        }
        $file = $request->file('file');
        $keyname = $request->get('keyname');
        $room = Room::where('keyname', $keyname)->first();
        
        if(!$room){
            return $this->responseError('Room not found');
        }
        // if($room->isTemplate){
        //     return $this->responseError('This is Template Room. You cannot modify this!');
        // }
        
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = strtolower($arrFileName['basename']);
            $revision = \Carbon\Carbon::now()->timestamp;

            $header_version = $request->header('Version');
            if($header_version){
                
                $path = "$type/$keyname/". Room::ASSET_DIR ."/$header_version/$revision";
            }else{
                $path = "$type/$keyname/". Room::ASSET_DIR ."/$revision";
            }
            
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);

            $data = [
                 "file_name" => $file_base_name,
                 "path" => "$path/$file_base_name"
            ];       
            
                        
            if($header_version){
                $versionAssetBundle = $room->getOriginal('versionAssetBundle');
                $versionAssetBundle[$header_version]['assetBundle'] = $upload;
                $versionAssetBundle[$header_version]['revision'] = $revision;
                $room->versionAssetBundle = $versionAssetBundle;
            }else{
                $room->assetBundle = "$upload";
                $room->revision = $revision;
            }
            $room->save();
            
            
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($room),
                'attachmentKey' => $room->keyname,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $revision
            ];
            saveFileHistory($dataFile, $driver, $room);    
            clearCache([get_class($room), Library::class]);
            return new RoomResource($room);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


    /** *****************************************************************************
     * Upload Room Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadRoomThumb(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Room::DIR_NAME;
        $file = $request->file('image');
        $key =  strtolower($request->get('keyname'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
            'keyname'   => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('File Or Name Invalid', "INVALID");   
        }

        $room = Room::where('keyname', $key)->first();
        if(!$room){
            return $this->responseError('Room not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$key/" . Room::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $room->image = "$path/$file_base_name";
            $room->save();
            $room->makeMultiThumb();
            clearCache([get_class($room)]);
            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($room),
                'attachmentKey' => $room->keyname,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $room);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }



    /** *****************************************************************************
     * Upload Material Asset Bundle.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadMaterialAsset(Request $request){
        $driver = config('fitin.editor.filesystem_driver');

        $type = Material::DIR_NAME;
        $file = $request->file('file');
        $code =  strtolower($request->get('code'));

        $validator = Validator::make($request->all(), [
            'file' =>  'required|file',
            'code'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $material = Material::where('code', $code)->first();
        if(!$material){
            return $this->responseError('Material not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = strtolower($arrFileName['basename']);
            $time = Carbon::now()->timestamp;
            
            $header_version = $request->header('Version');
            if($header_version){
                $path = "$type/$code/" . Material::PREFAB_DIR ."/$header_version/$time";
            
            }else{
                $path = "$type/$code/" . Material::PREFAB_DIR ."/$time";
            }
            
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            

            if($header_version){
                $versionAssetBundle = $material->getOriginal('versionAssetBundle');
                $versionAssetBundle[$header_version]['assetBundle'] = $upload;
                $versionAssetBundle[$header_version]['revision'] = $time;
                $material->versionAssetBundle = $versionAssetBundle;
            }else{
                $material->assetBundle = $upload;
                $material->revision = $time;
            }

            $material->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($material),
                'attachmentKey' => $material->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $material);

            //uploadDriver($file_base_name, $driver, $upload , Storage::disk($driver)->url($upload), $material);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Material Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadMaterialThumb(Request $request){
        $driver = config('fitin.editor.filesystem_driver');

        $type = Material::DIR_NAME;
        $file = $request->file('image');
        $code =  strtolower($request->get('code'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'code'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $material = Material::where('code', $code)->first();
        if(!$material){
            return $this->responseError('Material not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$code/" . Material::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $material->image = "$path/$file_base_name";
            $material->save();
            $material->makeMultiThumb();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($material),
                'attachmentKey' => $material->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $material);
            //uploadDriver($file_base_name, $driver, $upload , Storage::disk($driver)->url($upload), $material);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


    /** *****************************************************************************
     * Upload Category Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadCategoryThumb(Request $request){
        $driver = config('fitin.editor.filesystem_driver');

        $type = Category::DIR_NAME;
        $file = $request->file('image');
        $code =  strtolower($request->get('code'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'code'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $category = Category::where('code', $code)->first();
        if(!$category){
            return $this->responseError('Category not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$code/" . Category::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $category->image = "$path/$file_base_name";
            $category->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($category),
                'attachmentKey' => $category->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $category);

            //uploadDriver($file_base_name, $driver, $upload , Storage::disk($driver)->url($upload), $category);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


    /** *****************************************************************************
     * Upload Room Category Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadRoomCategoryThumb(Request $request){
        $driver = config('fitin.editor.filesystem_driver');

        $type = RoomCategory::DIR_NAME;
        $file = $request->file('image');
        $code =  strtolower($request->get('code'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'code'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $category = RoomCategory::where('code', $code)->first();
        if(!$category){
            return $this->responseError('Room Category not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$code/" . RoomCategory::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $category->image = "$path/$file_base_name";
            $category->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($category),
                'attachmentKey' => $category->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $category);

            //uploadDriver($file_base_name, $driver, $upload , Storage::disk($driver)->url($upload), $category);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }



    /** *****************************************************************************
     * Upload Material Category Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadMaterialCategoryThumb(Request $request){
        $driver = config('fitin.editor.filesystem_driver');

        $type = MaterialCategory::DIR_NAME;
        $file = $request->file('image');
        $code =  strtolower($request->get('code'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'code'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $category = MaterialCategory::where('code', $code)->first();
        if(!$category){
            return $this->responseError('Material Category not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$code/" . MaterialCategory::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $category->image = "$path/$file_base_name";
            $category->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($category),
                'attachmentKey' => $category->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $category);

            //uploadDriver($file_base_name, $driver, $upload , Storage::disk($driver)->url($upload), $category);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }




    /** *****************************************************************************
     * Upload Object Brand Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadBrandThumb(Request $request){
        $driver = config('fitin.editor.filesystem_driver');

        $type = Brand::DIR_NAME;
        $file = $request->file('image');
        $code =  strtolower($request->get('code'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'code'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $brand = Brand::where('code', $code)->first();
        if(!$brand){
            return $this->responseError('Brand not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$code/" . Brand::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $brand->image = "$path/$file_base_name";
            $brand->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($brand),
                'attachmentKey' => $brand->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $brand);

            //uploadDriver($file_base_name, $driver, $upload , Storage::disk($driver)->url($upload), $brand);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Material Brand Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadMaterialBrandThumb(Request $request){
        $driver = config('fitin.editor.filesystem_driver');

        $type = MaterialBrand::DIR_NAME;
        $file = $request->file('image');
        $code =  strtolower($request->get('code'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'code'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $category = MaterialBrand::where('code', $code)->first();
        if(!$category){
            return $this->responseError('Material Brand not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$code/" . MaterialBrand::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $category->image = "$path/$file_base_name";
            $category->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($category),
                'attachmentKey' => $category->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $category);

            //uploadDriver($file_base_name, $driver, $upload , Storage::disk($driver)->url($upload), $category);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Project Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadProjectThumb(Request $request){
        $driver = config('fitin.editor.filesystem_driver');

        $type = Project::DIR_NAME;
        $file = $request->file('image');
        $code =  strtolower($request->get('code'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'code'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $project = Project::where('code', $code)->first();
        if(!$project){
            return $this->responseError('Project not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$code/" . Project::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $project->image = "$path/$file_base_name";
            $project->save();
            $project->makeMultiThumb();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($project),
                'attachmentKey' => $project->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $project);

            //uploadDriver($file_base_name, $driver, $upload , Storage::disk($driver)->url($upload), $project);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


    /** *****************************************************************************
     * Upload Color Brand Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadColorBrandThumb(Request $request){
        $driver = config('fitin.editor.filesystem_driver');

        $type = ColorBrand::DIR_NAME;
        $file = $request->file('image');
        $code =  strtolower($request->get('code'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'code'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $brand = ColorBrand::where('code', $code)->first();
        if(!$brand){
            return $this->responseError('Material Brand not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$code/" . ColorBrand::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $brand->image = "$path/$file_base_name";
            $brand->save();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($brand),
                'attachmentKey' => $brand->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $brand);

            //uploadDriver($file_base_name, $driver, $upload , Storage::disk($driver)->url($upload), $brand);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


    /** *****************************************************************************
     * Upload Layout Paronama Zip.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadLayoutPanoramaZip(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Layout::DIR_NAME;
        
        $validator = Validator::make($request->all(), [
            'file' =>  "required|file",
            'name' => 'required'
        ]);
            
        if( $validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");    
        }
        $file = $request->file('file');
        $name = $request->get('name');
        $layout = Layout::where('name', $name)->first();
        
        if(!$layout){
            return $this->responseError('Layout not found');
        }
        // if($layout->isTemplate){
        //     return $this->responseError('This is Template Layout. You cannot modify this!');
        // }
        
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = strtolower($arrFileName['basename']);
            $revision = \Carbon\Carbon::now()->timestamp;

            $path = "$type/$name/".Layout::ZIP_DIR."/$revision";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);

            $data = [
                 "file_name" => $file_base_name,
                 "path" => "$path/$file_base_name",
                 "url" =>  Storage::disk($driver)->url($upload)
            ];       
            
            
            $filedata = $layout->getOriginal('files') ?? [];
            $filedata['panorama_zip'] = $upload;
            $layout->files = $filedata;

            $layout->save();
            
            
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($layout),
                'attachmentKey' => $layout->name,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $revision
            ];
            
            saveFileHistory($dataFile, $driver, $layout);    
            clearCache([get_class($layout), Library::class]);
            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Layout Floor Plan.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadLayoutFloorPlan(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Layout::DIR_NAME;
        $file = $request->file('image');
        $layout_id =  strtolower($request->get('name'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
            'name'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");
        }

        $obj = Layout::where('_id', $layout_id)->orWhere('name', $layout_id)->first();
        if(!$obj){
            return $this->responseError('Layout not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$layout_id/" . Layout::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $obj->image_floor_plan = "$path/$file_base_name";
            $obj->save();
            clearCache([get_class($obj)]);
            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $obj);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


    /** *****************************************************************************
     * Upload Layout Gallery.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadLayoutGallery(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Gallery::DIR;
        $file = $request->file('image');
        $layout_id =  strtolower($request->get('name'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
            'name'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID"); 
        }

        $obj = Layout::where('_id', $layout_id)->orWhere('name', $layout_id)->first();
        if(!$obj){
            return $this->responseError('Layout not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];

            $date = Carbon::now()->format('m/d');
            $path = "$type/layouts/$date";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            // $data = [
            //     "file_name" => $file_base_name,
            //     "path" => $upload,
            //     "url" =>  Storage::disk($driver)->url($upload)
            // ];       

            $data_gallery = [
                "image_path" => $upload,
                "thumb_path" => $upload,
                "image_url" =>  Storage::disk($driver)->url($upload),
                "thumb_url" => Storage::disk($driver)->url($upload),
                "index" => 0,
                "hidden" => 0
            ];


            $gallery = $obj->gallery()->create($data_gallery);
            
            clearCache([get_class($obj)]);
            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $obj);

            return response()->json([
                'code' => 0,
                'data' => $gallery,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
}
