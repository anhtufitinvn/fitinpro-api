<?php

namespace App\EditorPro\Http\Controllers\Cms;

use App\EditorPro\Http\Resources\Cms\PermissionResource;
use App\EditorPro\Http\Resources\Cms\PermissionResourceCollection;
use App\EditorPro\Models\Permission;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\EditorPro\Repositories\PermissionRepository;

class PermissionController extends ApiController
{
    protected $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository){
        $this->permissionRepository = $permissionRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return RoleCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $permissions = $this->permissionRepository->getAll($params);
        return new PermissionResourceCollection($permissions);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'title'   => 'required',
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['name'] = space_replace(strtolower($input['name']));
        $permission = $this->permissionRepository->find($input['name']);
        if($permission) {
            return $this->responseError('Name duplicated', 'DATA_EXISTS');
        }

        $permission = $this->permissionRepository->create($input);
           
        return new PermissionResource($permission);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return RoleResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $permission = $this->permissionRepository->find($id);
        if(!$permission){
            return $this->responseError('Permission not found' );
        }
        return new PermissionResource($permission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return RoleResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->only(['title']);
        $permission = $this->permissionRepository->update($key, $input);
        if(!$permission){
            return $this->responseError('Permission not found' );
        }
        
        return new PermissionResource($permission);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $permission = $this->permissionRepository->delete($key);
        if(!$permission){
            return $this->responseError('Account not found' );
        }
        return $this->respondSuccess();
    }

}
