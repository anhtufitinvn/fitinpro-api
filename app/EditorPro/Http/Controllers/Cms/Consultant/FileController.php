<?php

namespace App\EditorPro\Http\Controllers\Cms\Consultant;

use App\EditorPro\Http\Resources\Cms\Consultant\FileResourceCollection;
use App\EditorPro\Http\Resources\Cms\Consultant\FileResource;

use App\EditorPro\Models\FileDriver;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

use App\EditorPro\Http\Controllers\Cms\ApiController;

class FileController extends ApiController
{

    public function __construct(
       
    ){
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return LayoutCollection
     */
    public function listImages(Request $request)
    {
        $params = $request->all();
        if(!empty($params['page']) && empty($params['limit'])){
            $params['limit'] = 500;
        };

        if(!empty($params['limit'])){
            $params['limit'] = (int) $params['limit'];
        }
        $query = new FileDriver();
        $query = $query->where('type', 'image');
        if(!empty($params['t'])){
            $timestamp = \Carbon\Carbon::createFromTimestamp($params['t']);
            $query = $query->where('created_at', '>=', $timestamp);
        }
        if(!empty($params['limit'])){
            $images = $query->paginate($params['limit']);
        }
        else {
            $images = $query->get();
        }

        return new FileResourceCollection($images);
    }


    
   
}
