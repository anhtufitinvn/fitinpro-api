<?php

namespace App\EditorPro\Http\Controllers\Cms\Consultant;


use App\EditorPro\Http\Resources\Cms\Consultant\ProjectResourceCollection;
use App\EditorPro\Http\Resources\Cms\Consultant\ProjectResource;

use App\EditorPro\Models\Project;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\ProjectRepository;
use App\EditorPro\Repositories\Object3dRepository;
use Illuminate\Support\Facades\Validator;
use App\EditorPro\Http\Controllers\Cms\ApiController;

class ProjectController extends ApiController
{
    protected $projectRepository, $objectRepository;

    public function __construct(ProjectRepository $projectRepository, Object3dRepository $objectRepository){
        $this->projectRepository = $projectRepository;
        $this->objectRepository = $objectRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return ProjectCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();

        if(!empty($params['page']) && empty($params['limit'])){
            $params['limit'] = 200;
        }
        $params['isTemplate'] = 1;
        $projects = $this->projectRepository->getAll($params);
        return new ProjectResourceCollection($projects);
    }

    
   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return ProjectResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $project = $this->projectRepository->find($id);
        if(!$project){
            return $this->responseError('Project not found' );
        }
        return new ProjectResource($project);
    }
    
}
