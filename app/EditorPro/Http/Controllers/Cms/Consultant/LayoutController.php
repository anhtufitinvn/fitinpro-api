<?php

namespace App\EditorPro\Http\Controllers\Cms\Consultant;

use App\EditorPro\Http\Resources\Cms\Consultant\LayoutResourceCollection;
use App\EditorPro\Http\Resources\Cms\Consultant\LayoutResource;
use App\EditorPro\Http\Resources\Cms\Consultant\LayoutResourceWithJson;

use App\EditorPro\Models\Layout;
use App\EditorPro\Models\Object3d;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\EditorPro\Repositories\LayoutRepository;
use App\EditorPro\Repositories\Object3dRepository;
use App\EditorPro\Repositories\StyleRepository;
use App\EditorPro\Repositories\ProjectRepository;

use App\EditorPro\Services\ResourceService;
use App\EditorPro\Http\Controllers\Cms\ApiController;

class LayoutController extends ApiController
{
    protected $layoutRepository, $objectRepository, $resourceService;

    public function __construct(
        LayoutRepository $layoutRepository, 
        Object3dRepository $objectRepository, 
        StyleRepository $styleRepository, 
        ProjectRepository $projectRepository,
        ResourceService $resourceService
    ){
        $this->layoutRepository = $layoutRepository;
        $this->objectRepository = $objectRepository;
        $this->projectRepository = $projectRepository;
        $this->styleRepository = $styleRepository;
        $this->resourceService = $resourceService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return LayoutCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        if(!empty($params['page']) && empty($params['limit'])){
            $params['limit'] = 200;
        }
        
        $params['without_json'] = 1;
        $params['is-template'] = true;
        $layouts = $this->layoutRepository->getAll($params);

        return new LayoutResourceCollection($layouts);
    }


    
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return LayoutResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $key)
    {
        $layout = $this->layoutRepository->find($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        return new LayoutResourceWithJson($layout);
    }

    
    public function getListItems(Request $request, $key){
        $layout = $this->layoutRepository->find($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        $dataJson = $layout->dataJson;
        if(!is_array($dataJson) || empty($dataJson['Items']) || !is_array($dataJson['Items'])){
            return $this->responseError('Layout JSON wrong format' );
        }

        $listObjects = $dataJson['Items'];
        $listObjectsKey = array_column($listObjects, 'Name');

        $object3dItems = $this->objectRepository->getByKeys($listObjectsKey);
        
        $array_values = array_count_values($listObjectsKey);
        array_walk($array_values, function (&$value, $key) use ($object3dItems){
            $arr = [
                'count' => $value,
                'object' => $object3dItems->find($key)
            ];
            $value = $arr;
        });
        
        return $this->responseObjectData($array_values);
    }

    public function getDataJson(Request $request, $key){
        $layout = $this->layoutRepository->find($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        $dataJson = $layout->dataJson;
        
        return $this->responseObjectData($dataJson);
    }

    public function syncDataFromEcom(Request $request){
        $sync = $this->resourceService->syncFitinLayouts();
        if($sync){
            return $this->respondSuccess();
        }else{
            return $this->respondError('Sync failed, Please check again');  
        }
    }
}
