<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\ColorGroupResourceCollection;
use App\EditorPro\Http\Resources\Cms\ColorGroupResource;

use App\EditorPro\Models\ColorGroup;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\ColorGroupRepository;
use Illuminate\Support\Facades\Validator;

class ColorGroupController extends ApiController
{
    protected $colorGroupRepository;

    public function __construct(ColorGroupRepository $colorGroupRepository){
        $this->colorGroupRepository = $colorGroupRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return ColorGroupCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $colorGroups = $this->colorGroupRepository->getAll($params);
        return new ColorGroupResourceCollection($colorGroups);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateColorGroupRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['code'] = strtolower($input['code']);
        $colorGroup = $this->colorGroupRepository->find($input['code']);
        if($colorGroup) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }
        
        $colorGroup = $this->colorGroupRepository->create($input);
           
        return new ColorGroupResource($colorGroup);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return ColorGroupResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $colorGroup = $this->colorGroupRepository->find($id);
        if(!$colorGroup){
            return $this->responseError('ColorGroup not found' );
        }
        return new ColorGroupResource($colorGroup);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return ColorGroupResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code']);
        $colorGroup = $this->colorGroupRepository->update($key, $input);
        if(!$colorGroup){
            return $this->responseError('ColorGroup not found' );
        }
        
        return new ColorGroupResource($colorGroup);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $colorGroup = $this->colorGroupRepository->delete($key);
        if(!$colorGroup){
            return $this->responseError('ColorGroup not found' );
        }
        return $this->respondSuccess();
    }

    
}
