<?php

namespace App\EditorPro\Http\Controllers\Cms;

use App\EditorPro\Models\BuildConfig;
use App\EditorPro\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Http\Resources\Cms\BuildConfigResourceCollection;
use App\EditorPro\Http\Resources\Cms\BuildConfigResource;
use App\EditorPro\Http\Resources\Cms\BuildConfigResourceMix;

use App\EditorPro\Repositories\BrandRepository;

class ConfigController extends ApiController
{
    protected $brandRepository;

    public function __construct(BrandRepository $brandRepository){
        $this->brandRepository = $brandRepository;
    }

    public function getBuildConfig(Request $request)
    {
        $key = $request->get('brand');
        if($key){
            $brand = Brand::where('code', $key)->first();
            if(!$brand){
                return $this->responseError('Brand not found' );
            }
        }
       

        $builds = BuildConfig::getVersion($key);
        
        return new BuildConfigResourceCollection($builds);
    }

    public function index(Request $request)
    {
        $key = $request->get('brand');
        

        $builds = BuildConfig::getAllVersion($key);
        
        return new BuildConfigResourceCollection($builds);
    }

    public function getBuildConfigLatest(Request $request)
    {
        $key = $request->get('brand');
        if($key){
            $brand = Brand::where('code', $key)->first();
            if(!$brand){
                return $this->responseError('Brand not found' );
            }
        }
        
        
        $buildPublished = BuildConfig::getLatestPublishedVersion($key);
        $buildLatest = BuildConfig::getLatestVersion($key);
        
        $result = [
            'published' => $buildPublished,
            'latest' => $buildLatest
        ];

        return new BuildConfigResourceMix($result);
        //return new BuildConfigResource($build);
    }

    
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return LayoutResource|JsonResponse
     */
    public function show(Request $request, $id)
    {
        
    }


    /** *****************************************************************************
     * Upload Bundle Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadBuildConfig(Request $request)
    {   
        $driver = config('fitin.editor.filesystem_driver');

        $dir = BuildConfig::BUILD_DIR;
        $file = $request->file('file');
        

        $validator = Validator::make($request->all(), [
            'file' =>  'required',
            //'brand'   => 'required',
            'version' => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $version = strtolower($request->get('version'));

        //$pattern = "/^[0-9]{4}.[0-9]{2}.[0-9]{2}[0-9a-zA-Z._]+$/";
        $pattern = "/^[0-9]+.[0-9]+.[0-9]+$/";

        if(!preg_match($pattern, $version)){
            return $this->responseError('Version format not match. Version must be "x.y.z" format' );
        }
        $brand = strtolower($request->get('brand'));
        if($brand){
            $brand_instance = $this->brandRepository->find($brand);
            if(!$brand_instance){
                return $this->responseError('Brand not found' );
            }
        }
        
        $version_check = BuildConfig::checkVersion($version, $brand);
        if(!$version_check){
            return $this->responseError('Version already exists or not valid. Please input new version', 'ERROR' );
        }
        
        $create_data = [
            'version' => $version,
            'force_update' => $request->get('force_update'),
            'description' => $request->get('description')
        ];
        if($brand){
            $create_data['brand'] = $brand;
        }
        $new_version_instance = BuildConfig::create($create_data);

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            if(isset($arrFileName['extension'])){
                $file_base_name = $version. "." . $arrFileName['extension'];
            }else{
                $file_base_name = $version;
            }
            if(!$brand){
                $brand = "common";
            }
            $time = Carbon::now()->timestamp;
            $version_dir_name = slug($version);
            $path = "$dir/$brand/$version_dir_name/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $new_version_instance->path = "$path/$file_base_name";
            $new_version_instance->save();
            clearCache([get_class($new_version_instance)]);
            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'build',
                'attachmentType' => get_class($new_version_instance),
                'attachmentKey' => $new_version_instance->_id,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $new_version_instance);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    public function update(Request $request, $id){
        $input = $request->only(['status', 'force_update', 'description']);

        $validator = Validator::make($input, [
            'status'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $build = BuildConfig::find($id);
        if(!$build){
            return $this->responseError('Không tìm thấy bản build', 'NOT_FOUND');  
        }
        $build->update($input);
        return new BuildConfigResource($build);
    }
}
