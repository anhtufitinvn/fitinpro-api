<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\PageResourceCollection;
use App\EditorPro\Http\Resources\Cms\PageResource;

use App\EditorPro\Models\Page;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\PageRepository;
use Illuminate\Support\Facades\Validator;

class PageController extends ApiController
{
    protected $pageRepository;

    public function __construct(PageRepository $pageRepository){
        $this->pageRepository = $pageRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return PageCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $pages = $this->pageRepository->getAll($params);
        return new PageResourceCollection($pages);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['code'] = space_replace(strtolower($input['code']));
        $page = $this->pageRepository->find($input['code']);
        if($page) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }

        $page = $this->pageRepository->create($input);
           
        return new PageResource($page);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return PageResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $page = $this->pageRepository->find($id);
        if(!$page){
            return $this->responseError('Page not found' );
        }
        return new PageResource($page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return PageResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code']);
        $page = $this->pageRepository->update($key, $input);
        if(!$page){
            return $this->responseError('Page not found' );
        }
        
        return new PageResource($page);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $page = $this->pageRepository->delete($key);
        if(!$page){
            return $this->responseError('Page not found' );
        }
        return $this->respondSuccess();
    }

    
}
