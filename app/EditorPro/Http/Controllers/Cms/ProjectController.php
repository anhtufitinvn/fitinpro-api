<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\ProjectResourceCollection;
use App\EditorPro\Http\Resources\Cms\ProjectResource;

use App\EditorPro\Models\Project;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\ProjectRepository;
use App\EditorPro\Repositories\Object3dRepository;
use App\EditorPro\Repositories\LocationRepository;
use Illuminate\Support\Facades\Validator;

class ProjectController extends ApiController
{
    protected $projectRepository, $objectRepository, $locationRepository;

    public function __construct(ProjectRepository $projectRepository, Object3dRepository $objectRepository, LocationRepository $locationRepository){
        $this->projectRepository = $projectRepository;
        $this->objectRepository = $objectRepository;
        $this->locationRepository = $locationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return ProjectCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $projects = $this->projectRepository->getAll($params);
        return new ProjectResourceCollection($projects);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['code'] = space_replace(strtolower($input['code']));
        $project = $this->projectRepository->find($input['code']);
        if($project) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }

        if(empty($input['location'])){
            $input['location'] = [
                'city_id' => 79,
                'district_id' => 760,
                'address' => "",
                'lat' => "",
                'long' => ""
            ];
        };

        $location = $input['location'];
        $city = $this->locationRepository->getCity($location['city_id'] ?? 0);
        if($city){
            $input['location']['city_name'] = $city->city_name;
        }

        $district = $this->locationRepository->getDistrict($location['district_id'] ?? 0);
            
        if($district){
            $input['location']['district_name'] = $district->districts[0]['district_name'];
        }
        
        $input['ownerId'] = Auth::user()->auth_id;
        $project = $this->projectRepository->create($input);
           
        return new ProjectResource($project);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return ProjectResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $project = $this->projectRepository->find($id);
        if(!$project){
            return $this->responseError('Project not found' );
        }
        return new ProjectResource($project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return ProjectResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code', 'thumbList']);
        if(!empty($input['location'])){
            $location = $input['location'];
            $city = $this->locationRepository->getCity($location['city_id'] ?? 0);
            if($city){
                $input['location']['city_name'] = $city->city_name;
            }

            $district = $this->locationRepository->getDistrict($location['district_id'] ?? 0);
            
            if($district){
                $input['location']['district_name'] = $district->districts[0]['district_name'];
            }
        }
        $project = $this->projectRepository->update($key, $input);
        if(!$project){
            return $this->responseError('Project not found' );
        }
        
        return new ProjectResource($project);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $project = $this->projectRepository->delete($key);
        if(!$project){
            return $this->responseError('Project not found' );
        }
        return $this->respondSuccess();
    }


    public function approved(Request $request,$key)
    {
        $project = $this->projectRepository->find($key);
        if(!$project){
            return $this->responseError('Project not found' );
        }
        
        if($project->status != Project::WAITING){
            return $this->responseError('Project is not in wating status. Cannot approve this project' );
        }

        
        $event = event(new ApproveProject($project));
        return new ProjectResource($event[0]);
    }

    public function rejected(Request $request,$key)
    {
        $project = $this->projectRepository->find($key);
        if(!$project){
            return $this->responseError('Project not found' );
        }
       
        if($project->status != Project::WAITING){
            return $this->responseError('Project is not in wating status. Cannot approve this project' );
        }

        //$input['status'] = Project::REJECTED;
        //$project = $this->projectRepository->update($key, $input);
        $event = event(new RejectProject($project));
        return new ProjectResource($event[0]);
    }
    
    public function getListItems(Request $request, $key){
        $project = $this->projectRepository->find($key);
        if(!$project){
            return $this->responseError('Project not found' );
        }
        $dataJson = $project->dataJson;
        if(!is_array($dataJson) || empty($dataJson['TTS']) || !is_array($dataJson['TTS'])){
            return $this->responseError('Project JSON wrong format' );
        }

        $listObjects = $dataJson['TTS'];
        $listObjectsKey = array_column($listObjects, 'Name');

        $object3dItems = $this->objectRepository->getByKeys($listObjectsKey);
        
        $array_values = array_count_values($listObjectsKey);
        array_walk($array_values, function (&$value, $key) use ($object3dItems){
            $arr = [
                'count' => $value,
                'object' => $object3dItems->find($key)
            ];
            $value = $arr;
        });
        
        return $this->responseObjectData($array_values);
    }
    
}
