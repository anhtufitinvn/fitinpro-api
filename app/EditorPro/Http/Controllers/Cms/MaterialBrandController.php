<?php

namespace App\EditorPro\Http\Controllers\Cms;


use App\EditorPro\Http\Resources\Cms\MaterialBrandResourceCollection;
use App\EditorPro\Http\Resources\Cms\MaterialBrandResource;

use App\EditorPro\Models\MaterialBrand;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\MaterialBrandRepository;
use Illuminate\Support\Facades\Validator;

class MaterialBrandController extends ApiController
{
    protected $materialBrandRepository;

    public function __construct(MaterialBrandRepository $materialBrandRepository){
        $this->materialBrandRepository = $materialBrandRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return MaterialBrandCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $materialBrands = $this->materialBrandRepository->getAll($params);
        return new MaterialBrandResourceCollection($materialBrands);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateMaterialBrandRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['code'] = strtolower($input['code']);
        $materialBrand = $this->materialBrandRepository->find($input['code']);
        if($materialBrand) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }
        
        $materialBrand = $this->materialBrandRepository->create($input);
           
        return new MaterialBrandResource($materialBrand);
    }

   
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return MaterialBrandResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $materialBrand = $this->materialBrandRepository->find($id);
        if(!$materialBrand){
            return $this->responseError('MaterialBrand not found' );
        }
        return new MaterialBrandResource($materialBrand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return MaterialBrandResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['code']);
        $materialBrand = $this->materialBrandRepository->update($key, $input);
        if(!$materialBrand){
            return $this->responseError('MaterialBrand not found' );
        }
        
        return new MaterialBrandResource($materialBrand);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $key
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $materialBrand = $this->materialBrandRepository->delete($key);
        if(!$materialBrand){
            return $this->responseError('MaterialBrand not found' );
        }
        return $this->respondSuccess();
    }

    
}
