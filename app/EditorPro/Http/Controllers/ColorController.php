<?php

namespace App\EditorPro\Http\Controllers;

use App\EditorPro\Models\Color;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\ColorRepository;

use App\EditorPro\Http\Resources\ColorResource;
use App\EditorPro\Http\Resources\ColorResourceCollection;

class ColorController extends ApiController
{
    protected $repository;

    public function __construct(ColorRepository $repository){
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
        $colors = $this->repository->getAll($params);

        return new ColorResourceCollection($colors);
    }

    public function getMostUsed(Request $request)
    {
        $categories = $this->repository->getMostUsedList();
        return new ColorResourceCollection($categories);
    }

}
