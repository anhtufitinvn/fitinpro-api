<?php

namespace App\EditorPro\Http\Controllers;

use App\FitIn\EditorAuthID as AuthID;

use App\EditorPro\Http\Resources\UserResource;
use App\EditorPro\Http\Resources\UserResourceCollection;
use App\EditorPro\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Services\EditorService;

class UserController extends ApiController
{
    protected $service;

    public function __construct(EditorService $service){
        $this->service = $service;
    }

    public function me()
    {
        $me = Auth::user();
        return new UserResource($me);
    }

    public function changeProfile(Request $request)
    {
        $token = $request->bearerToken();
        $input = $request->only(['address', 'avatar', 'full_name', 'phone']);
        $result = $this->service->changeProfile($input);
        if($result){
            $data = [
                'phone' => $result['phone'],
                'avatar' => $result['avatar'],
                'full_name' => $result['full_name'],
            ];
            $me = Auth::user();
            $me->update($data);
            return new UserResource($me);
        }
        else{           
            return $this->responseError('Update Failed', 'VERIFICATION_FAILED');
        }
        
    }


    public function removeAccount(Request $request){
        $user = Auth::user();
        if(!$user->isAdmin){
            return $this->responseError('Only Admin Can Delete User', 'INVALID');
        }
        $id = $request->get('id');
        $result = $this->service->removeAccount($id);
        if($result){
            return $this->respondSuccess('Delete Successfully!');
        }else{
            return $this->responseError('Delete Account Failed', 'INVALID');
        }
    }

}
