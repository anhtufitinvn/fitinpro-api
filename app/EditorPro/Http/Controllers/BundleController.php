<?php

namespace App\EditorPro\Http\Controllers;

use App\EditorPro\Http\Resources\BundleResourceCollection;
use App\EditorPro\Http\Resources\BundleResource;

use App\EditorPro\Models\Bundle;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\BundleRepository;
use App\EditorPro\Events\UserDuplicateAsset;

class BundleController extends ApiController
{
    protected $bundleRepository;

    public function __construct(BundleRepository $bundleRepository){
        $this->bundleRepository = $bundleRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return BundleCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $bundles = $this->bundleRepository->getAllUser($params);
        return new BundleResourceCollection($bundles);
    }

    public function indexTemplate(Request $request)
    {
        $params = $request->all();
        $bundles = $this->bundleRepository->getAllTemplate($params);
        return new BundleResourceCollection($bundles);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        $input = $request->all();
        $input['ownerId'] = $user->auth_id;
        $input['isTemplate'] = true;
        $input['status'] = Bundle::PRIVATE;
        $name = $input['displayName'] ?? NULL;

        if(is_array($input['dataJson']) && isset($input['dataJson']['Key_name'])){
            $name = $input['dataJson']['Key_name'];
        }
        if(!$name){
            return $this->responseError('name not found'); 
        }
        $name =  $input['keyname'] = Bundle::PREFIX ."_".slug($name, '_');
        $bundle = $this->bundleRepository->find($name);
        if($bundle) {
            $name = $name.'_'.\Carbon\Carbon::now()->timestamp;
            $input['keyname'] = $name;
        }
        $dataJson = $input['dataJson'] ?? [];
        $dataJson['Key_name'] = $name;
        $input['dataJson'] = $dataJson;
        // if(empty($input['brand']) && $user->brand){
        //     $input['brand'] = $user->brand;
        // }
        $bundle = $this->bundleRepository->create($input);
           
        return new BundleResource($bundle);
    }


    public function duplicate(Request $request, $key)
    {
        $bundle = $this->bundleRepository->duplicate($key);
        if(!$bundle){
            return $this->responseError('Bundle not found'); 
        }
        event(new UserDuplicateAsset(Auth::user(), $bundle, $bundle->keyname));
        return new BundleResource($bundle);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return BundleResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $key)
    {
        $bundle = $this->bundleRepository->find($key);
        if(!$bundle){
            return $this->responseError('Bundle not found' );
        }
        return new BundleResource($bundle);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return BundleResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['keyname', 'status']);
        $bundle = $this->bundleRepository->update($key, $input);
        if(!$bundle){
            return $this->responseError('Bundle not found' );
        }
        
        return new BundleResource($bundle);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $bundle = $this->bundleRepository->delete($key);
        if(!$bundle){
            return $this->responseError('Bundle not found' );
        }
        return $this->respondSuccess();
    }


    public function requestApproved(Request $request,$key){
        $bundle = $this->bundleRepository->find($key);
        if(!$bundle){
            return $this->responseError('Bundle not found' );
        }
        if(!$bundle->metaZip){
            return $this->responseError('Zip meta does not uploaded. Cannot request approve this bundle' );
        }

        if(!$bundle->dataJson || (!is_array($bundle->dataJson)) || (empty($bundle->dataJson['List_Objects']))){
            return $this->responseError('Bundle dataJson is invalid' );
        }

        if($bundle->status != Bundle::PRIVATE){
            return $this->responseError('Bundle is not private any more. Cannot request approve this bundle' );
        }

        //$input['status'] = Bundle::WAITING;
        $bundle = $this->bundleRepository->requestApprove($key);
        
        return new BundleResource($bundle);
    }
    
}
