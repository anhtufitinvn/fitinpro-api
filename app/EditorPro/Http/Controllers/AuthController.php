<?php

namespace App\EditorPro\Http\Controllers;

use App\FitIn\EditorAuthID as AuthID;
use App\EditorPro\Models\User;
use App\EditorPro\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Exceptions\GeneralException;
use App\Services\EditorService;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Http\Resources\UserResource;

/**
 * @group 1. Authentication
 */
class AuthController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    protected $userService;

    public function __construct(EditorService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'email'   => 'required',
            'password' => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $arResult = $this->userService->login($inputs['email'], $inputs['password']);

        if ($arResult === -1){
            return response()->json([
                'code' => -1,
                'message' => 'Tài khoản chưa kích hoạt',
                "errors" => [
                    [
                        "error" => config('fitin.errors.auth.inactive'),
                        "message" => "Tài khoản chưa kích hoạt",
                        "stack" => []
                    ]
                ]
            ]);
        }

        if ($arResult === 0){
            return response()->json([
                'code' => -1,
                'message' => 'Thông tin đăng nhập ko hợp lệ. Vui lòng thử lại hoặc đổi mật khẩu',
                "errors" => [
                    [
                        "error" => config('fitin.errors.auth.invalid'),
                        "message" => "Thông tin đăng nhập ko hợp lệ. Vui lòng thử lại hoặc đổi mật khẩu",
                        "stack" => []
                    ]
                ]
            ]);
        }

        return response()->json([
            'code' => 0,
            "message" => "success",
            'data' => $arResult
        ]);
    }

    public function loginByKey(Request $request)
    {

        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'key'   => 'required',
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $arResult = $this->userService->login($inputs['email'], $inputs['password']);

        if ($arResult === -1){
            return response()->json([
                'code' => -1,
                'message' => 'Tài khoản chưa kích hoạt',
                "errors" => [
                    [
                        "error" => config('fitin.errors.auth.inactive'),
                        "message" => "Tài khoản chưa kích hoạt",
                        "stack" => []
                    ]
                ]
            ]);
        }

        if ($arResult === 0){
            return response()->json([
                'code' => -1,
                'message' => 'Thông tin đăng nhập ko hợp lệ. Vui lòng thử lại hoặc đổi mật khẩu',
                "errors" => [
                    [
                        "error" => config('fitin.errors.auth.invalid'),
                        "message" => "Thông tin đăng nhập ko hợp lệ. Vui lòng thử lại hoặc đổi mật khẩu",
                        "stack" => []
                    ]
                ]
            ]);
        }

        return response()->json([
            'code' => 0,
            "message" => "success",
            'data' => $arResult
        ]);
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $result = $this->userService->logout();
        return $this->respondSuccess(__('Successfully logged out'));
    }



    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($arrToken)
    {
        return response()->json([
            'code' => 0,
            'data' => [
                'access_token' => $arrToken['access_token'],
                'token_type' => 'bearer',
                'expires_in' => $this->guard()->factory()->getTTL() * 60,
                'ecommerce_token' => $arrToken['ecommerce_token'] ?? NULL,
                'auth_token' => $arrToken['id_token'] ?? NULL,
            ],
            
            'message' => 'Login success!'
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('editor');
    }

    public function social($social)
    {
        $access_token = request()->input('access_token', false);      
        if (!$access_token) {
            return response()->json([
                'data' => [
                    'error_code' => 'TOKEN_MISSING',
                ],
                
                'code' => -401,
                'message' => __('access_token is required!')
            ]);
        }

        $arrResult = $this->userService->loginBySocial($social, $access_token);
        
        if (!$arrResult){
            return response()->json([
                'code' => -1,
                'message' => 'Token not valid',
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        }

        return response()->json([
            'code' => 0,
            'message' => 'Success',
            'data' => $arrResult
        ]);

    }

    public function register(Request $request)
    {
        $arrUser = $request->all();
        $validator = Validator::make(
            $arrUser,
            [
                'name' => 'required|max:255',
                'password' => 'required|min:6',
                'email' => 'required_without:phone|email|unique:users',
                'phone' => 'required_without:email'
            ]
        );

        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());

            return response()->json([
                         
                'code' => -1,
                'message' => $errorString,
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        }
        if(isset($arrUser['brand'])){
            $brand = Brand::where('code', $arrUser['brand'])->first();
            if(!$brand){
                return response()->json([
                         
                    'code' => -1,
                    'message' => 'Brand not exists. Please register with another brand!',
                    "errors" => [
                        [
                            "error" => 1,
                            "message" => "",
                            "stack" => []
                        ]
                    ]
                ]);
            }
        }

        if(isset($arrUser['email'])) {
            return $this->registerByEmail($arrUser);
        }else{
            return $this->registerByPhone($arrUser);
        }
        
    }

    protected function registerByEmail($input){
        $oAuthID = new AuthID();
        
        $existEmail = $oAuthID->hasUniqueEmail($input['email']);

        if ($existEmail) {
            return response()->json([     
                'code' => -1,
                'message' => __('That email has already been taken'),
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        }
        
        return $this->userService->register($input);
        
    }

    protected function registerByPhone($input){
        $oAuthID = new AuthID();
        
        $existEmail = $oAuthID->hasUniquePhone($input['phone']);

        if ($existEmail) {
            return response()->json([     
                'code' => -1,
                'message' => __('That phone number has already been taken'),
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        }

        return $this->userService->register($input);
        
    }

    public function resendConfirmation(Request $request){
        $input = $request->get('email');
        $oAuthID = new AuthID();
        $resend = $oAuthID->confirmationResend($input);
        return response()->json(
            ['code' => 0, 'data' => [], 'message' => __('Confirmation email has been sent successfully! Please activate email!')]
        );
    }

    public function verifyAccount()
    {

        $inputs = request()->input();

        $validate = Validator::make($inputs, [
            'email' => 'required',
            'otp_code' => 'required',
        ]);

        if($validate->fails()){
            return $this->responseError('Thông tin không đầy đủ. Vui lòng thử lại'); 
        }
        
        $isVerify = $this->userService->verifyAccount($inputs['email'], $inputs['otp_code']);
        
        if (!$isVerify) {

            return $this->responseError('Mã xác thực không đúng. Vui lòng thử lại', 'OTP_CODE_WRONG' ); 
        }

        $this->userService->activeAccount($inputs['email']);
        
        return response()->json(
            ['code' => 0, 'data' => true, 'message' => __('Xác thực tài khoản thành công')]
        );
    }

    public function forgotPassword(Request $request){
        //
        $email = $request->get('email');
        $result = $this->userService->forgotPassword($email);

        return response()->json(
            $result
        );
    }

    public function resetPassword(Request $request){
        //
        $email = $request->get('email');
        $token = $request->get('token');
        $new_password = $request->get('new_password');
        $retype_new_password = $request->get('retype_new_password');
        $result = $this->userService->resetNewPassword($token, $email, $new_password, $retype_new_password);
        
        if($result)
        return response()->json(
            $result
        );
    }

    public function changePassword(Request $request){
        //
        $token = $request->bearerToken();
        $old_password = $request->get('old_password');
        $new_password = $request->get('new_password');
        
        $result = $this->userService->changePasswordByID($token,  $old_password, $new_password);
        
        if($result)
        return response()->json(
            $result
        );
    }

    public function verifyOtpCode(Request $request){
        $otp = $request->get('otp_code');
        $email = $request->get('email');
        $result = $this->userService->verifyOtp($email, $otp);
        
        return response()->json(
            $result
        );
    }

    public function uploadAvatar(Request $request){
        $driver = config('fitin.editor.filesystem_driver');
        
        $file = $request->file('image');
        
        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
        ]);

        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        
        $type = User::WORKSPACE_DIR;

        $thumb_dir = User::THUMB_DIR;
        $user = Auth::user();
        
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = strtolower($arrFileName['basename']);
            
            $revision = \Carbon\Carbon::now()->timestamp;
            $path = "$type/{$user->auth_id}/$thumb_dir/$revision";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);

            uploadDriver($file_base_name, $driver, "$path/$file_base_name", Storage::disk($driver)->url("$path/$file_base_name"), $user);
        

            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name"
            ];       
            

            $user->avatar = $upload;
            $user->save();
        
            return new UserResource($user);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
}
