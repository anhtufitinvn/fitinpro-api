<?php

namespace App\EditorPro\Http\Controllers;


use App\EditorPro\Http\Resources\ColorGroupResource;
use App\EditorPro\Http\Resources\ColorGroupResourceCollection;
use Illuminate\Support\Facades\Validator;
use App\EditorPro\Models\ColorGroup;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use App\EditorPro\Repositories\ColorGroupRepository;

class ColorGroupController extends ApiController
{
    protected $colorGroupRepository;

    public function __construct(ColorGroupRepository $colorGroupRepository){
        $this->colorGroupRepository = $colorGroupRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $colors = $this->colorGroupRepository->getAll($params);

        return new ColorGroupResourceCollection($colors);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
       
        
    }
  

    /**
     * Display the specified resource.
     *
     * @param Brand $category
     * @return BrandResource
     */
    public function show($key)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        
    }
}
