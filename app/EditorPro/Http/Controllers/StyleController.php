<?php

namespace App\EditorPro\Http\Controllers;

use App\FitIn\EditorAuthID as AuthID;

use App\EditorPro\Models\Style;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\StyleRepository;

use App\EditorPro\Http\Resources\StyleResource;
use App\EditorPro\Http\Resources\StyleResourceCollection;

class StyleController extends ApiController
{
    protected $repository;

    public function __construct(StyleRepository $repository){
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $styles = $this->repository->getAll($params);
        return new StyleResourceCollection($styles);
    }

}
