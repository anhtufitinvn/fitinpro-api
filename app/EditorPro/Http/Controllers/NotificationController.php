<?php

namespace App\EditorPro\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\NotificationRepository;

use App\EditorPro\Http\Resources\NotificationResource;
use App\EditorPro\Http\Resources\NotificationResourceCollection;

class NotificationController extends ApiController
{
    protected $repository;

    public function __construct(NotificationRepository $repository){
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
        $notifications = $this->repository->getAll($params);
        return new NotificationResourceCollection($notifications);
    }

    public function indexUnread(Request $request)
    {

        $notifications = $this->repository->getUnread();
        
        return new NotificationResourceCollection($notifications);
    }

    public function read(Request $request, $id)
    {

        $notification = $this->repository->read($id);
        if(!$notification){
            return $this->responseError('Notification not found'); 
        }
        return new NotificationResource($notification);
    }

    public function indexUnreadCount(Request $request)
    {
        $notifications = $this->repository->getUnreadCount();
        return response()->json([
            'code' => 0,
            "message" => "success",
            'data' => [
                'count' => $notifications
            ]
        ]);
    }
}
