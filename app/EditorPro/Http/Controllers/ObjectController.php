<?php

namespace App\EditorPro\Http\Controllers;

use App\FitIn\Asset;

use App\EditorPro\Models\Object3d;
use App\EditorPro\Models\Library;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\Object3dRepository;
use App\EditorPro\Http\Resources\ObjectResourceCollection;
use App\EditorPro\Http\Resources\ObjectResource;

class ObjectController extends ApiController
{
    
    protected  $object3dRepository;
    public function __construct(Object3dRepository $object3dRepository){
        $this->object3dRepository = $object3dRepository;
    }


    public function filter(Request $request)
    {
        $keys = $request->object_key;
        if(!$keys){
            return $this->responseError('Need object_key');
        }
        $count = count(explode(',', $keys));
        $asset = new Asset();
        $data = $asset->getByType('product-filter', ['object_key' => $keys, 'limit' => $count + 1]);

        return $this->responseData($data);
    }

    public function filterV2(Request $request)
    {
        $keys = $request->get('keys') ?? [];
        if(!is_array($keys) || empty($keys)){
            return $this->responseError('Need keys as array');
        }
        
        $count = count($keys);
        $key_string = implode(',', $keys);
        $asset = new Asset();
        $data = $asset->getByType('product-filter', ['object_key' => $key_string, 'limit' => $count + 1]);

        return $this->responseData($data);
    }

    public function getRelatedObjects(Request $request, $key){
        $params = $request->all();
        $objects = $this->object3dRepository->getRelatedByKey($key, $params);
        return new ObjectResourceCollection($objects);
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $params['type'] = $request->get('type');
        //$params['published'] = true;
        //$params['filter-by'] = 'brand';
        $params['filter-by'] = 'designer';
        $objects = $this->object3dRepository->getAll($params);
        return new ObjectResourceCollection($objects);
    }

    public function indexByUser(Request $request)
    {
        $params = $request->all();
    
        $objects = $this->object3dRepository->getAllByUser($params);
        return new ObjectResourceCollection($objects);
    }


    public function store(Request $request)
    {
        $input = request()->except(['metadata', 'extensions']);
        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $name = strtolower($request->get('name'));
        $object = $this->object3dRepository->find($name);
        if ($object) {
            return $this->responseError('Key name duplicated', 'DATA_EXISTS');
        }

        $metadata = [
            "user" => [
                "email" => Auth::user()->email,
                "id" => Auth::user()->auth_id
            ]
        ];
        $input['metadata'] = $metadata;
        $input['status'] = Object3d::PRIVATE;
        
        $object = $this->object3dRepository->create($input);

        return new ObjectResource($object);
   }

    // public function indexUser(Request $request)
    // {
    //     $params = $request->all();
    //     $params['type'] = $request->get('type');
    //     //$params['published'] = true;
    //     $objects = $this->object3dRepository->getUserLibrary($params);
    //     return new ObjectResourceCollection($objects);
    // }


    public function indexByKeys(Request $request)
    {
        $keys = $request->object_key;
        if(!$keys){
            return $this->responseError('Need object_key');
        }
        $keys = explode(',', $keys );
        $objects = $this->object3dRepository->getByKeys($keys);
        return new ObjectResourceCollection($objects);
    }

    public function indexByKeysV2(Request $request)
    {
        $keys = $request->get('keys') ?? [];
        if(!is_array($keys) || empty($keys)){
            return $this->responseError('Need keys as array');
        }
        //$keys = explode(',', $keys );
        $objects = $this->object3dRepository->getByKeys($keys);
        return new ObjectResourceCollection($objects);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return ObjectResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['name', 'metadata', 'extensions']);
        $objects = $this->object3dRepository->update($key, $input);
        if(!$objects){
            return $this->responseError('Object not found' );
        }
        
        return new ObjectResource($objects);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $objects = $this->object3dRepository->delete($key);
        if(!$objects){
            return $this->responseError('Objects not found' );
        }
        return $this->respondSuccess();
    }

    public function getNotExistObject(){
        $asset = new Asset();
        $ecom_objects = $asset->getEcomItems();
        $editorpro_objects = $this->object3dRepository->getAll()->pluck('name')->toArray();

        $results = array_diff($ecom_objects, $editorpro_objects);
        return $this->responseObjectData($results);
    }

    /**
     * USER UPLOAD ZIP FILE & CREATE OBJECT 
     */
    public function createByUpload(){
        
    }
}
