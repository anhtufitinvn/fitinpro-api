<?php

namespace App\EditorPro\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\FeedbackRepository;

use App\EditorPro\Http\Resources\FeedbackResource;
use App\EditorPro\Http\Resources\FeedbackResourceCollection;

class FeedbackController extends ApiController
{
    protected $repository;

    public function __construct(FeedbackRepository $repository){
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
        $data = $this->repository->getAll($params);

        return new FeedbackResourceCollection($data);
    }

    public function store(Request $request)
    {
        $input = request()->all();
        $validator = Validator::make($input, [
            'content'   => 'required',
            'title'  => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $input['ownerId'] = Auth::user()->auth_id;
        $feedback = $this->repository->create($input);

        return new FeedbackResource($feedback);
    }

}
