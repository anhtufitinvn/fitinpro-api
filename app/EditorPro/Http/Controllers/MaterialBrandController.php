<?php

namespace App\EditorPro\Http\Controllers;


use App\EditorPro\Http\Resources\MaterialBrandResource;
use App\EditorPro\Http\Resources\MaterialBrandResourceCollection;
use Illuminate\Support\Facades\Validator;
use App\EditorPro\Models\MaterialBrand;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use App\EditorPro\Repositories\MaterialBrandRepository;

class MaterialBrandController extends ApiController
{
    protected $materialBrandRepository;

    public function __construct(MaterialBrandRepository $materialBrandRepository){
        $this->materialBrandRepository = $materialBrandRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $materials = $this->materialBrandRepository->getAll($params);

        return new MaterialBrandResourceCollection($materials);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
       
        
    }
  

    /**
     * Display the specified resource.
     *
     * @param Brand $category
     * @return BrandResource
     */
    public function show($key)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        
    }
}
