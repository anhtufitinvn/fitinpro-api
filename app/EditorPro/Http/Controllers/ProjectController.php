<?php

namespace App\EditorPro\Http\Controllers;

use App\FitIn\EditorAuthID as AuthID;

use App\EditorPro\Models\Project;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\ProjectRepository;

use App\EditorPro\Http\Resources\ProjectResource;
use App\EditorPro\Http\Resources\ProjectResourceCollection;

class ProjectController extends ApiController
{
    protected $repository;

    public function __construct(ProjectRepository $repository){
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $projects = $this->repository->getAll($params);
        return new ProjectResourceCollection($projects);
    }

}
