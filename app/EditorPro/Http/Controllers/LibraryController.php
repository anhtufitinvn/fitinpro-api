<?php

namespace App\EditorPro\Http\Controllers;

use App\FitIn\EditorAuthID as AuthID;

use App\EditorPro\Models\User;
use App\EditorPro\Models\Library;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\LibraryRepository;

use App\EditorPro\Http\Resources\LibraryResource;
use App\EditorPro\Http\Resources\LibraryResourceCollection;
use App\EditorPro\Http\Resources\ObjectResourceCollection;
use App\EditorPro\Http\Resources\ObjectResource;

class LibraryController extends ApiController
{
    protected $repository;

    public function __construct(LibraryRepository $repository){
        $this->repository = $repository;
    }


    public function confirmDownload(Request $request)
    {
        $id = $request->get('id');
        $this->repository->confirm($id);
        return $this->respondSuccess();
        
    }

    public function all(Request $request)
    {
        $lib = $this->repository->getAll();
        return \App\Helpers\Utils::LibraryCollection($lib);
        
    }

    public function getByUser(Request $request)
    {
        $user = Auth::user();
        $lib = $this->repository->getByUser($user);
        return \App\Helpers\Utils::LibraryCollection($lib);
        
    }

    public function getObjects(Request $request){
        $params = $request->all();
        $user = Auth::user();
        $lib = $this->repository->getObjects($user, $params);
        return new ObjectResourceCollection($lib);
    }

    public function deleteUserLibrary(Request $request)
    {
        $user = Auth::user();
        $lib = $this->repository->deleteByUser($user);
        return $this->respondSuccess('Your library has been cleared!');
    }
}
