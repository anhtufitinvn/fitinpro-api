<?php

namespace App\EditorPro\Http\Controllers;


use App\EditorPro\Http\Resources\RoomResourceCollection;
use App\EditorPro\Http\Resources\RoomResource;

use App\EditorPro\Models\Room;
use App\EditorPro\Models\Object3d;
use Illuminate\Foundation\Auth\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\RoomRepository;
use App\EditorPro\Events\UserDuplicateAsset;
use Illuminate\Support\Facades\Validator;

class RoomController extends ApiController
{
    protected $roomRepository;

    public function __construct(RoomRepository $roomRepository){
        $this->roomRepository = $roomRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return RoomCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $rooms = $this->roomRepository->getAllUser($params);
        return new RoomResourceCollection($rooms);
    }

    public function indexTemplate(Request $request)
    {
        $params = $request->all();
        $rooms = $this->roomRepository->getAllTemplate($params);
        return new RoomResourceCollection($rooms);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRoomRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        $input = $request->all();

        $input['ownerId'] = $user->auth_id;
        $input['isTemplate'] = true;
        $input['status'] = Room::PRIVATE;
        $name = $input['keyname'] ?? NULL;
       
        if(is_array($input['dataJson']) && isset($input['dataJson']['RoomId'])){
            $name = $input['dataJson']['RoomId'];
        }
        if(!$name){
            return $this->responseError('Room keyname not found'); 
        }
        $name = strtolower($name);
        $name =  $input['keyname'] = Room::PREFIX ."_".slug($name, '_');
        $room = $this->roomRepository->find($name);
        if($room) {
            
            $input['keyname'] = $name.'_'.\Carbon\Carbon::now()->timestamp;
        }
       
        $dataJson = $input['dataJson'] ?? [];
        $dataJson['RoomId'] = $name;
        $input['dataJson'] = $dataJson;

        $room = $this->roomRepository->create($input);
           
        return new RoomResource($room);
    }


    public function duplicate(Request $request, $key)
    {
        $room = $this->roomRepository->duplicate($key);
        if(!$room){
            return $this->responseError('Room not found'); 
        }
        event(new UserDuplicateAsset(Auth::user(), $room, $room->_id));
        return new RoomResource($room);
    }

    // public function duplicateRoomUser(Request $request, $key){
    //     $room = $this->roomRepository->duplicateRoomUser($key);
    //     if(!$room){
    //         return $this->responseError('Room not found'); 
    //     }
    //     event(new UserDuplicateAsset(Auth::user(), $room, $room->_id));
    //     return new RoomResource($room);
    // }
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return RoomResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $room = $this->roomRepository->find($id);
        if(!$room){
            return $this->responseError('Room not found' );
        }
        return new RoomResource($room);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RoomResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->except(['keyname', 'status']);
        $room = $this->roomRepository->update($key, $input);
        if(!$room){
            return $this->responseError('Room not found' );
        }
        
        return new RoomResource($room);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $room = $this->roomRepository->delete($key);
        if(!$room){
            return $this->responseError('Room not found' );
        }
        return $this->respondSuccess();
    }

    public function requestApproved(Request $request,$key){
        $room = $this->roomRepository->find($key);
        if(!$room){
            return $this->responseError('Room not found' );
        }
        

        if(!$room->dataJson || (!is_array($room->dataJson)) || (empty($room->dataJson['TTS']))){
            return $this->responseError('Room dataJson is invalid' );
        }

        if($room->status != Room::PRIVATE){
            return $this->responseError('Room is not private any more. Cannot request approve this Room' );
        }

        //$input['status'] = Room::WAITING;
        $room = $this->roomRepository->requestApprove($key);
        
        return new RoomResource($room);
    }
    
}
