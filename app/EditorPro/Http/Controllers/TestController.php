<?php

namespace App\EditorPro\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\EditorPro\Models\User;
//use App\Models\Ver2\Bundle;
use App\Repositories\Bundle\BundleRepository;
use App\EditorPro\Repositories\Object3dRepository;
use App\EditorPro\Repositories\MaterialRepository;
use App\Repositories\Layout\LayoutRepository;
use App\EditorPro\Repositories\LayoutRepository as EditorProLayoutRepository;
use App\EditorPro\Repositories\BundleRepository as EditorProBundleRepository;
use App\EditorPro\Events\UserStoreLibrary;
use App\EditorPro\Repositories\RoomRepository;

class TestController extends ApiController
{
   

    /** *****************************************************************************
     * Download Layout PREFAB
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function testEncrypt(Request $request)
    {
        $name = "result.mp4";
        $path = "test.mp4";
        //$filepath = Storage::disk('public')->path($path);
        
        $file = Storage::disk('editor')->get($path);

        $file = $this->encryptDes($file);
        //$file = openssl_encrypt($file, 'DES-EDE3', '123456', OPENSSL_RAW_DATA);
        
        //  return response()->download($file, $name, [
        //      'Content-Type'  => Storage::mimeType($path),
        //  ]);

    //     return response()->stream(function () use ($filepath, $path){
    //         //Can add logic to chunk the file here if you want but the point is to stream data
    //         readfile($filepath);
    //    },200);
        return response($file)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($file))
            ->header('Content-Disposition', 'attachment; filename=' . $name)
            ->header('Content-Transfer-Encoding', 'binary');
    }

    public function encryptDes($file){
        // Generate key based on current time
        $secret_key = utf8_encode('123456');
        // Hash the key with SHA256
        $key = hash('SHA256', $secret_key, true);
        // Use AES 128 w/ CBC as encryption method
        $method  = "aes-256-cbc";
        // Get cipher length based on encryption method
        $cypher_length = openssl_cipher_iv_length($method);
        // Generate IV -- possible issue
        //$iv = random_bytes($cypher_length);
        $iv = hex2bin('31ba091e42a58ea391a3a421d4e6f961');
        $output = openssl_encrypt($file, $method, $key, OPENSSL_RAW_DATA, $iv);

        return $output;       
    }
}
