<?php

namespace App\EditorPro\Http\Controllers;

use App\FitIn\EditorAuthID as AuthID;

use App\EditorPro\Models\RoomCategory;
use App\EditorPro\Models\Library;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\RoomCategoryRepository;

use App\EditorPro\Http\Resources\RoomCategoryResource;
use App\EditorPro\Http\Resources\RoomCategoryResourceCollection;

class RoomCategoryController extends ApiController
{
    protected $repository;

    public function __construct(RoomCategoryRepository $repository){
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $roomRoomCategories = $this->repository->getAll($params);
        return new RoomCategoryResourceCollection($roomRoomCategories);
    }

}
