<?php

namespace App\EditorPro\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\EditorPro\Models\User;
use App\EditorPro\Models\Layout;
use App\EditorPro\Models\Object3d;
use App\EditorPro\Models\Library;
use App\EditorPro\Models\Bundle;
use App\EditorPro\Models\Room;
use App\EditorPro\Models\Material;

use App\EditorPro\Http\Resources\LayoutResource;
use App\EditorPro\Http\Resources\ObjectResource;
use App\EditorPro\Http\Resources\RoomResource;
use App\EditorPro\Http\Resources\MaterialResource;

class UploadController extends ApiController
{
    public function __construct(){
       
    }



    /** *****************************************************************************
     * Upload File Config.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadConfig(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');
        
        $file = $request->file('file');
        
        $validator = Validator::make($request->all(), [
            'file' =>  'required|file'
        ]);

        if( $validator->fails()){
            return $this->responseError('File Missing or Invalid', "INVALID");   
        }
        
        
        $type = User::WORKSPACE_DIR;

        $config_dir = User::CONFIG_DIR;
        $user = Auth::user();
        if($user->config){
            $current_config_path = $user->config['path'];
            $hash_file = md5_file(Storage::disk($driver)->path($current_config_path));

            if($hash_file == md5_file($file)){
                return $this->responseError("You upload config has no change.", "INVALID");
            }
        }
        
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = strtolower($arrFileName['basename']);
            
            $revision = \Carbon\Carbon::now()->timestamp;
            $path = "$type/{$user->auth_id}/$config_dir/$revision";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);

            $this->uploadDriver($file_base_name, $driver, "$path/$file_base_name", Storage::disk($driver)->url("$path/$file_base_name"), $user);
        

            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name"
            ];       
            $current_config = $user->config ?? [];
            $config = array_merge($current_config , [
                'revision' => $revision,
                'path' => "$path/$file_base_name",
                'headers' => collect($request->header())->transform(function ($item) {
                    return $item[0];
                })->toArray()
            ]);

            $user->config = $config;
            $user->save();
        
            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    
    /** *****************************************************************************
     * Upload Database Model.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadModel(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');
        
        $file = $request->file('file');
        
        $validator = Validator::make($request->all(), [
            'file' =>  'required|file'
        ]);

        if( $validator->fails()){
            return $this->responseError('File Missing or Invalid', "INVALID");   
        }
        
        
        $type = User::WORKSPACE_DIR;

        $model_dir = User::MODEL_DIR;
        $user = Auth::user();
        if($user->model){
            $model_path = $user->model['path'];
            $hash_file = md5_file(Storage::disk($driver)->path($model_path));

            if($hash_file == md5_file($file)){
                return $this->responseError("You upload model has no change.", "INVALID");
            }
        }
        
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = strtolower($arrFileName['basename']);
            
            $revision = \Carbon\Carbon::now()->timestamp;
            $path = "$type/{$user->auth_id}/$model_dir/$revision";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);

            $this->uploadDriver($file_base_name, $driver, "$path/$file_base_name", Storage::disk($driver)->url("$path/$file_base_name"), $user);
        

            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name"
            ];       

            $model = [
                'revision' => $revision,
                'path' => "$path/$file_base_name",
            ];

            $user->model = $model;
            $user->save();
        
            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    
    /** *****************************************************************************
     * Upload JSON BUndle.
     *
     * @param Request $request
     * 
     * @return Array
     */

    // public function uploadBundleJson(Request $request, $id)
    // {
    //     $driver = config('fitin.editor.filesystem_driver');
        
    //     $file = $request->file('file');

    //     $validator = Validator::make($request->all(), [
    //         'file' =>  "required|file"
    //     ]);

    //     if( $validator->fails()){
    //         return $this->responseError('File Missing or Invalid', "INVALID");   
    //     }
    //     $bundle = Bundle::find($id);
    //     if(!$bundle){
    //         return $this->responseError('Bundle not found');
    //     }

    //     if($bundle->isTemplate){
    //         return $this->responseError('This is Template Bundle. You cannot modify this!');
    //     }

        
    //     if ($file && $file->isValid()) {
    //         $arrFileName = pathinfo($file->getClientOriginalName());
            
    //         $file_name = strtolower($arrFileName['filename']);
    //         $file_base_name = strtolower($arrFileName['basename']);
    //         $revision = \Carbon\Carbon::now()->timestamp;
            
    //         $type = User::WORKSPACE_DIR;
    //         $bundle_dir = User::BUNDLE_DIR;
    //         $user = Auth::user();
    //         $path = "$type/{$user->auth_id}/$bundle_dir/{$bundle->parent}/$id/$revision";

    //         $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
    //         $data = [
    //             "file_name" => $file_base_name,
    //             "path" => "$path/$file_base_name"
    //         ];       

    //         $bundle->update([
    //             'revision' =>  $revision,
    //             'json' =>  "$path/$file_base_name"
    //         ]);

    //         $this->uploadDriver($file_base_name, $driver, "$path/$file_base_name", Storage::disk($driver)->url("$path/$file_base_name"), $bundle);
        
    //         $this->clearCache(Library::class);
            
    //         return response()->json([
    //             'code' => 0,
    //             'data' => $data,
    //             'message' => 'Upload success'
    //         ]);
            
    //     } else {
    //         return $this->responseError('Lỗi quá trình upload');   
                       
    //     }
    // }

    /** *****************************************************************************
     * Upload Object Editor Pro FBX.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadBundleZip(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Bundle::DIR_NAME;
        $file = $request->file('file');
        $key =  strtolower($request->get('keyname'));

        $validator = Validator::make($request->all(), [
            'file' =>  'required|file|mimetypes:application/zip',
            'keyname'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $instance = Bundle::where('keyname', $key)->first();
        if(!$instance){
            return $this->responseError('Bundle not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = $key. "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$key/" . Bundle::ZIP_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);

           
            $zip = new \ZipArchive();
            if($zip->open(Storage::disk($driver)->path($upload)) === TRUE )
            {
                if ($zip->locateName('manifest.json') !== false)
                {
                    $string = file_get_contents('zip://'.Storage::disk($driver)->path($upload).'#manifest.json');
                    if(isJSON($string)){
                        $json = json_decode($string);
                        if(isset($json->version)){
                            $version = $json->version;
                            if($version >$instance->zip_version){
                                $instance->zip_version = $version;
                            }
                            else{
                                Storage::disk($driver)->delete($upload);
                                Storage::disk($driver)->deleteDirectory($path);
                                return $this->responseError('Version is less than current version!');
                            }
                        }else{
                            Storage::disk($driver)->delete($upload);
                            Storage::disk($driver)->deleteDirectory($path);
                            return $this->responseError('Manifest must have version');
                        }
                    }else{
                        Storage::disk($driver)->delete($upload);
                        Storage::disk($driver)->deleteDirectory($path);
                        return $this->responseError('Manifest must be json format. Please check again');
                    }
                }else{
                    Storage::disk($driver)->delete($upload);
                    Storage::disk($driver)->deleteDirectory($path);
                    return $this->responseError('Zip does not contains manifest.json . Please check again');
                }
            }
            else {
                Storage::disk($driver)->delete($upload);
                Storage::disk($driver)->deleteDirectory($path);
                return $this->responseError('Cannot find Zip archive. Please check the upload process');
            }
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $instance->metaZip = "$path/$file_base_name";
            
            $instance->save();
            clearCache([get_class($instance)]);
            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($instance),
                'attachmentKey' => $instance->keyname,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $instance);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }

    }

    /** *****************************************************************************
     * Upload  Layout asset bundle.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadLayoutAsset(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Layout::DIR_NAME;
        
        $validator = Validator::make($request->all(), [
            'file' =>  "required|file",
            'name' => 'required'
        ]);
            
        if( $validator->fails()){
            return $this->responseError('File Missing or Invalid', "INVALID");   
        }
        $file = $request->file('file');
        $name = $request->get('name');
        $layout = Layout::where('name', $name)->first();
        
        if(!$layout){
            return $this->responseError('Layout not found');
        }
        
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = strtolower($arrFileName['basename']);
            $revision = \Carbon\Carbon::now()->timestamp;

            $header_version = $request->header('Version');
            if($header_version){
                $path = "$type/$name/".Layout::ASSET_DIR."/$header_version/$revision";
               
            }else{
                $path = "$type/$name/".Layout::ASSET_DIR."/$revision";
            }
        
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);

            $data = [
                 "file_name" => $file_base_name,
                 "path" => "$path/$file_base_name",
                 "url" =>  Storage::disk($driver)->url($upload)
            ];       
            
            

            if($header_version){
                $versionAssetBundle = $layout->getOriginal('versionAssetBundle');
                $versionAssetBundle[$header_version]['assetBundle'] = $upload;
                $versionAssetBundle[$header_version]['revision'] = $revision;
                $layout->versionAssetBundle = $versionAssetBundle;
            }else{
                $layout->assetBundle = "$path/$file_base_name";
                $layout->revision = $revision;
            }

            $layout->save();
            
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($layout),
                'attachmentKey' => $layout->name,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $revision
            ];
            
            saveFileHistory($dataFile, $driver, $layout);    
            clearCache([get_class($layout), Library::class]);
            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


    /** *****************************************************************************
     * Upload Layout Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadLayoutThumb(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Layout::DIR_NAME;
        $file = $request->file('image');
        $layout_id =  strtolower($request->get('name'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
            'name'   => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('File Or Name Invalid', "INVALID");   
        }

        $obj = Layout::where('_id', $layout_id)->orWhere('name', $layout_id)->first();
        if(!$obj){
            return $this->responseError('Layout not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$layout_id/" . Layout::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $obj->image = "$path/$file_base_name";
            $obj->save();
            $obj->makeMultiThumb();
            clearCache([get_class($obj)]);
            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $obj);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Bundle Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadBundleThumb(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Bundle::DIR_NAME;
        $file = $request->file('image');
        $key =  strtolower($request->get('keyname'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
            'keyname'   => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('File Or Name Invalid', "INVALID");   
        }

        $instance = Bundle::where('keyname', $key)->first();
        if(!$instance){
            return $this->responseError('Bundle not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$key/" . Bundle::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $instance->image = "$path/$file_base_name";
            $instance->save();
            $instance->makeMultiThumb(); 
            clearCache([get_class($instance)]);
            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($instance),
                'attachmentKey' => $instance->keyname,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $instance);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


/** *****************************************************************************
     * Upload Object Editor Pro Asset bundle.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadObjectAsset(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Object3d::DIR_NAME;
        $name = strtolower($request->get('name'));
        $file = $request->file('file');
        $brand =  strtolower($request->get('brand'));

        $validator = Validator::make($request->all(), [
            'file' =>  'required|file',
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            return $this->responseError('File Or Name Invalid', "INVALID");   
        }

        $obj = Object3d::where('name', $name)->first();
        if(!$obj){
            return $this->responseError('Object3d not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            if(!$brand){
                $brand = explode('_', $file_name)[0];
            }
            if(!$name){
                $name = $file_name;
            }
            $time = Carbon::now()->timestamp;
            $new_file_base_name = strtolower($arrFileName['basename']);
            //$new_file_base_name = md5_file($file);
            $path = "$type/$brand/".Object3d::ASSET_DIR."/$name/$time" ;
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $new_file_base_name);
            $data = [
                "file_name" => $new_file_base_name,
                "path" => "$path/$new_file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $obj->assetBundle = $upload;
            $obj->revision = $time ;
            $obj->save();


            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->name,
                'path' => $upload,
                'name' => $new_file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $obj);
            clearCache([get_class($obj), Library::class]);
            return new ObjectResource($obj);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Object Editor Pro FBX.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadObjectZip(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Object3d::DIR_NAME;
        $name = strtolower($request->get('name'));
        $file = $request->file('file');
        $brand =  strtolower($request->get('brand'));

        $validator = Validator::make($request->all(), [
            'file' =>  'required|file|mimetypes:application/zip',
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $obj = Object3d::where('name', $name)->first();
        if(!$obj){
            return $this->responseError('Object3d not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            if(!$brand){
                $brand = explode('_', $file_name)[0];
            }
            if(!$name){
                $name = $file_name;
            }
            $time = Carbon::now()->timestamp;
            $new_file_base_name = urlencode(strtolower($arrFileName['basename']));
            //$new_file_base_name = md5_file($file);
            $path = "$type/$brand/".Object3d::ZIP_DIR."/$name/$time" ;
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $new_file_base_name);
            $data = [
                "file_name" => $new_file_base_name,
                "path" => "$path/$new_file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $obj->assetZip = $upload;
            $obj->save();

            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->name,
                'path' => $upload,
                'name' => $new_file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $obj);
            clearCache([get_class($obj), Library::class]);
            return new ObjectResource($obj);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


    /** *****************************************************************************
     * Upload Object Editor Pro ZIP : contains FBX, material, texture, config.json.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadObjectExtensionZip(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Object3d::DIR_NAME;
        $name = strtolower($request->get('name'));
        $file = $request->file('file');
       

        $validator = Validator::make($request->all(), [
            'file' =>  'required|file|mimetypes:application/zip',
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $obj = Object3d::where('name', $name)->first();
 
        if(!$obj){
            return $this->responseError('Object3d not found');
        }
        checkEditorOwner($obj);
        $brand =  $obj->brand ?? "fitin";
        $brand =  strtolower($brand);

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            
            if(!$name){
                $name = $file_name;
            }
            $time = Carbon::now()->timestamp;
            $new_file_base_name = urlencode(strtolower($arrFileName['basename']));
            $path = "$type/$brand/".Object3d::ZIP_DIR."/$name/$time" ;
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $new_file_base_name);
            $data = [
                "file_name" => $new_file_base_name,
                "path" => "$path/$new_file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $extensions = $obj->getOriginal('extensions') ?? [];
            $extensions['zip'] = [
                'path' => $upload,
                'revision' => $time
            ];
            $obj->extensions = $extensions;
            $obj->save();

            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->name,
                'path' => $upload,
                'name' => $new_file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $obj);
            clearCache([get_class($obj), Library::class]);

            return new ObjectResource($obj);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
    
    /** *****************************************************************************
     * Upload Object Editor Pro thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadObjectThumb(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Object3d::DIR_NAME;
        $name = strtolower($request->get('name'));
        $file = $request->file('image');
        $brand =  strtolower($request->get('brand'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $obj = Object3d::where('name', $name)->first();
        if(!$obj){
            return $this->responseError('Object3d not found');
        }
        checkEditorOwner($obj);
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            if(!$brand){
                $brand = explode('_', $file_name)[0];
            }
            if(!$name){
                $name = $file_name;
            }
            $time = Carbon::now()->timestamp;
            //$new_file_base_name = urlencode(strtolower($arrFileName['basename']));
            $new_file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $path = "$type/$brand/".Object3d::THUMB_DIR."/$name/$time" ;
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $new_file_base_name);
            $data = [
                "file_name" => $new_file_base_name,
                "path" => "$path/$new_file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $obj->thumb = $upload;
            $obj->save();
            $obj->makeMultiThumb(); 

            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->name,
                'path' => $upload,
                'name' => $new_file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $obj);
            clearCache([get_class($obj), Library::class]);
            return new ObjectResource($obj);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload  Room asset bundle.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadRoomAsset(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Room::DIR_NAME;
        
        $validator = Validator::make($request->all(), [
            'file' =>  "required|file",
            'keyname' => 'required'
        ]);
            
        if( $validator->fails()){
            return $this->responseError('File Missing or Invalid', "INVALID");   
        }
        $file = $request->file('file');
        $keyname = $request->get('keyname');
        $room = Room::where('keyname', $keyname)->first();
        
        if(!$room){
            return $this->responseError('Room not found');
        }
        // if($room->isTemplate){
        //     return $this->responseError('This is Template Room. You cannot modify this!');
        // }
        
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            $file_base_name = strtolower($arrFileName['basename']);
            $revision = \Carbon\Carbon::now()->timestamp;

            $path = "$type/$keyname/".Room::ASSET_DIR."/$revision";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);

            $data = [
                 "file_name" => $file_base_name,
                 "path" => "$path/$file_base_name"
            ];       
            
            
            $room->assetBundle = "$path/$file_base_name";
            $room->revision = $revision;
            $room->save();
            
            
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($room),
                'attachmentKey' => $room->keyname,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $revision
            ];
            saveFileHistory($dataFile, $driver, $room);    
            clearCache([get_class($room), Library::class]);
            return new RoomResource($room);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }


    /** *****************************************************************************
     * Upload Room Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function uploadRoomThumb(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Room::DIR_NAME;
        $file = $request->file('image');
        $key =  strtolower($request->get('keyname'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,jpeg,png',
            'keyname'   => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('File Or Name Invalid', "INVALID");   
        }

        $room = Room::where('keyname', $key)->first();
        if(!$room){
            return $this->responseError('Room not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $ext = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $ext;
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$key/" . Room::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $room->image = "$path/$file_base_name";
            $room->save();
            $room->makeMultiThumb();
            clearCache([get_class($room)]);
            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($room),
                'attachmentKey' => $room->keyname,
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $room);

            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Material Thumb.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadMaterialThumb(Request $request){
        $driver = config('fitin.editor.filesystem_driver');

        $type = Material::DIR_NAME;
        $file = $request->file('image');
        $code =  strtolower($request->get('code'));

        $validator = Validator::make($request->all(), [
            'image' =>  'required|mimes:jpg,png,jpeg',
            'code'   => 'required'
        ]);
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $material = Material::where('code', $code)->first();
        if(!$material){
            return $this->responseError('Material not found');
        }
        checkEditorOwner($material);
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            $file_name = strtolower($arrFileName['filename']);
            //$file_base_name = strtolower($arrFileName['basename']);
            $file_base_name = md5_file($file). "." . $arrFileName['extension'];
            $time = Carbon::now()->timestamp;
            
            $path = "$type/$code/" . Material::THUMB_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            $data = [
                "file_name" => $file_base_name,
                "path" => "$path/$file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $material->image = "$path/$file_base_name";
            $material->save();
            $material->makeMultiThumb();

            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'image',
                'attachmentType' => get_class($material),
                'attachmentKey' => $material->getKey(),
                'path' => $upload,
                'name' => $file_base_name,
                'originName' => $file_name
            ];
            saveFileHistory($dataFile, $driver, $material);

            
            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

    /** *****************************************************************************
     * Upload Material Editor Pro ZIP : contains FBX, material, texture, config.json.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadMaterialExtensionZip(Request $request)
    {
        $driver = config('fitin.editor.filesystem_driver');

        $type = Material::DIR_NAME;
        $file = $request->file('file');
        $code =  strtolower($request->get('code'));

        $validator = Validator::make($request->all(), [
            'file' =>  'required|file|mimetypes:application/zip',
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }

        $material = Material::where('code', $code)->first();
        if(!$material){
            return $this->responseError('Material not found');
        }
        checkEditorOwner($material);
        $brand = $material->brand ?? 'fitin';
        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            
            if(!$code){
                $code = $file_name;
            }
            $time = Carbon::now()->timestamp;
            $new_file_base_name = urlencode(strtolower($arrFileName['basename']));
            $path = "$type/$brand/$code/".Material::ZIP_DIR."/$time" ;
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $new_file_base_name);
            $data = [
                "file_name" => $new_file_base_name,
                "path" => "$path/$new_file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            $extensions = $material->getOriginal('extensions') ?? [];
            $extensions['zip'] = [
                'path' => $upload,
                'revision' => $time
            ];
            $material->extensions = $extensions;
            $material->save();

            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'file',
                'attachmentType' => get_class($material),
                'attachmentKey' => $material->getKey(),
                'path' => $upload,
                'name' => $new_file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            saveFileHistory($dataFile, $driver, $material);
            clearCache([get_class($material), Library::class]);

            return new MaterialResource($material);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }
}
