<?php

namespace App\EditorPro\Http\Controllers;

use App\FitIn\EditorAuthID as AuthID;

use App\EditorPro\Models\Category;
use App\EditorPro\Models\Library;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\CategoryRepository;

use App\EditorPro\Http\Resources\CategoryResource;
use App\EditorPro\Http\Resources\CategoryResourceCollection;

class CategoryController extends ApiController
{
    protected $repository;

    public function __construct(CategoryRepository $repository){
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $categories = $this->repository->getAll($params);
        return new CategoryResourceCollection($categories);
    }

}
