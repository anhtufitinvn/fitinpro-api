<?php

namespace App\EditorPro\Http\Controllers;


use App\EditorPro\Http\Resources\LayoutCollection;
use App\EditorPro\Http\Resources\LayoutResource;
use Illuminate\Support\Facades\Validator;

use App\EditorPro\Models\Layout;
use App\EditorPro\Models\LayoutRoom;
use App\EditorPro\Models\Object3d;
use Illuminate\Foundation\Auth\User;
use App\EditorPro\Events\SendConsultantLayout;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\EditorPro\Repositories\LayoutRepository;
use App\EditorPro\Repositories\StyleRepository;

use App\EditorPro\Events\UserDuplicateAsset;

class LayoutController extends ApiController
{
    protected $layoutRepository;

    public function __construct(
        LayoutRepository $layoutRepository,
        StyleRepository $styleRepository
    ){
        $this->layoutRepository = $layoutRepository;
        $this->styleRepository = $styleRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return LayoutCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $layouts = $this->layoutRepository->getAllUser($params);
        return new LayoutCollection($layouts);
    }

    public function listKeys(Request $request)
    {
        $data = Layout::select(['displayName', 'name'])->orderBy('name', 'ASC')->get();
        return $this->responseData($data->toArray());
    }

    public function indexTemplate(Request $request)
    {
        $params = $request->all();
        $layouts = $this->layoutRepository->getAllTemplate($params);
        return new LayoutCollection($layouts);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateLayoutRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
    
        $input = $request->except(['gallery']);
        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        $input['isTemplate'] = false;
        $input['name'] = space_replace(strtolower($input['name']), '_');
        $layout = $this->layoutRepository->find($input['name']);
        if($layout) {
            return $this->responseError('Key name duplicated', 'DATA_EXISTS');
        }
        
        $input['status'] = Layout::PRIVATE;

        $input['metadata'] = $this->buildMetadata($input);

        $layout = $this->layoutRepository->create($input);
           
        return new LayoutResource($layout);
        
       
    }

    protected function buildMetadata($input, $key = NULL){
        $metadata = $input['metadata'] ?? [];
        if(!empty($input['style'])){
            $style = $this->styleRepository->find($input['style']);
            if($style){
                $metadata['style'] = [
                    'name' => $style->name,
                    'key' => $input['style']
                ];
            }
        }

        if(!$key){
            $metadata['creator'] = [
                'id' => Auth::user()->auth_id,
                'email' => Auth::user()->email
            ];
            
        }
        
        $input_rooms = $metadata['rooms'] ?? [];
        $rooms = [];
        foreach ($input_rooms as $room){
            if(!empty($room['_id'])){
                $rooms[] = $room;
            }else{
                $new_room = LayoutRoom::create($room);
                $rooms[] = $new_room->toArray();
            }
            
        }
        $metadata['rooms'] = $rooms;

        return $metadata;
    }



    public function duplicate(Request $request, $key)
    {
        $layout = $this->layoutRepository->duplicate($key);
        if(!$layout){
            return $this->responseError('Layout not found'); 
        }
        event(new UserDuplicateAsset(Auth::user(), $layout, $layout->_id));
        return new LayoutResource($layout);
    }

    // public function duplicateLayoutUser(Request $request, $key){
    //     $layout = $this->layoutRepository->duplicateLayoutUser($key);
    //     if(!$layout){
    //         return $this->responseError('Layout not found'); 
    //     }
    //     event(new UserDuplicateAsset(Auth::user(), $layout, $layout->_id));
    //     return new LayoutResource($layout);
    // }
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return LayoutResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $layout = $this->layoutRepository->find($id);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        return new LayoutResource($layout);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return LayoutResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $id)
    {
        $input = $request->except(['name']);
        $layout = $this->layoutRepository->update($id, $input);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        
        return new LayoutResource($layout);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$id)
    {
        $layout = $this->layoutRepository->delete($id);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        return $this->respondSuccess();
    }

    public function requestApproved(Request $request,$key){
        $layout = $this->layoutRepository->find($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        

        if(!$layout->dataJson || (!is_array($layout->dataJson)) || (empty($layout->dataJson['Items']))){
            return $this->responseError('Layout dataJson is invalid' );
        }

        if($layout->status != Layout::PRIVATE){
            return $this->responseError('Layout is not private any more. Cannot request approve this layout' );
        }

        //$input['status'] = Room::WAITING;
        $layout = $this->layoutRepository->requestApprove($key);
        
        return new LayoutResource($layout);
    }

    public function postConsultant(Request $request, $key)
    {
        $layout = $this->layoutRepository->find($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        
        if($layout->status != Layout::WAITING){
            return $this->responseError('Layout is not in wating status. Cannot approve this layout' );
        }

        
        $event = event(new SendConsultantLayout($layout));
        return new LayoutResource($event[0]);
    }
}
