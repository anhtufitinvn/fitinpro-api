<?php

namespace App\EditorPro\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\EditorPro\Models\User;
//use App\Models\Ver2\Bundle;
use App\Repositories\Bundle\BundleRepository;
use App\EditorPro\Repositories\Object3dRepository;
use App\EditorPro\Repositories\MaterialRepository;
use App\Repositories\Layout\LayoutRepository;
use App\EditorPro\Repositories\LayoutRepository as EditorProLayoutRepository;
use App\EditorPro\Repositories\BundleRepository as EditorProBundleRepository;
use App\EditorPro\Events\UserStoreLibrary;
use App\EditorPro\Repositories\RoomRepository;

class DownloadController extends ApiController
{
    protected   $bundleRepository, 
                $objectRepository, 
                $materialRepository, 
                $layoutRepository, 
                $roomRepository, 
                $editorProLayoutRepository, 
                $editorProBundleRepository;

    public function __construct(
        BundleRepository $bundleRepository, 
        Object3dRepository $objectRepository, 
        MaterialRepository $materialRepository,
        LayoutRepository $layoutRepository,
        EditorProLayoutRepository $editorProLayoutRepository,
        EditorProBundleRepository $editorProBundleRepository,
        RoomRepository $roomRepository
    )
    {
       $this->bundleRepository = $bundleRepository;
       $this->objectRepository = $objectRepository;
       $this->materialRepository = $materialRepository;
       $this->layoutRepository = $layoutRepository;
       $this->editorProLayoutRepository = $editorProLayoutRepository;
       $this->editorProBundleRepository = $editorProBundleRepository;
       $this->roomRepository = $roomRepository;
    }

    /** *****************************************************************************
     * Download File Config.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function downloadConfig(Request $request, $tmp)
    {
        
        $user = Auth::user();

        if($user->_id != $tmp){
            return $this->responseError("Data not match", "INVALID");
        }

        if(!$user->config){
            return $this->responseError("No config file has been uploaded", "INVALID");
        }
        $config = $user->config;
        $path = $config['path'];
        $driver = config('fitin.editor.filesystem_driver');
        $file = Storage::disk($driver)->path($path);
        
        $array_path = explode('/', $path);
        $name = $array_path[count($array_path) - 1];

        saveUserDownload($name, $driver, $path, Storage::disk($driver)->url($path) , $user);

        $gzipable = config('fitin.enable_gzip');
        if($gzipable){
            $time = \Carbon\Carbon::now()->timestamp;
            $gzfile =  "$time-$name".".gz";
            $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
            $file = $this->encryptDes(file_get_contents($file));
            gzwrite($fp, $file);
            gzclose($fp);
            return response()->download($gzfile, $gzfile)->deleteFileAfterSend(true);
        }else{

            $enc_file = Storage::disk($driver)->get($path);
            $enc_file = $this->encryptDes($enc_file);
            $workspace = $user->getWorkspaceDir();
            Storage::disk($driver)->put("$workspace/$path", $enc_file);
            return response($enc_file)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($enc_file))
            ->header('Content-Disposition', 'attachment; filename=' . $name)
            ->header('Content-Transfer-Encoding', 'binary');

            //return response()->download($file, $name);
        }
        
    }

    /** *****************************************************************************
     * Download File Model.
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function downloadModel(Request $request, $tmp)
    {
        
        $user = Auth::user();
        
        if($user->_id != $tmp){
            return $this->responseError("Data not match", "INVALID");
        }

        if(!$user->model){
            return responseError("No model has been uploaded", "INVALID");
        }
        $config = $user->model;
        $path = $config['path'];
        $driver = config('fitin.editor.filesystem_driver');
        $array_path = explode('/', $path);
        $name = $array_path[count($array_path) - 1];
        $file = Storage::disk($driver)->path($path);
        
        saveUserDownload($name, $driver, $path, Storage::disk($driver)->url($path) , $user);

        $gzipable = config('fitin.enable_gzip');
        if($gzipable){
            $time = \Carbon\Carbon::now()->timestamp;
            $gzfile =  "$time-$name".".gz";
            $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
            $file = $this->encryptDes(file_get_contents($file));
            gzwrite($fp, $file);
            gzclose($fp);
            return response()->download($gzfile, $gzfile)->deleteFileAfterSend(true);
        }else{

            $enc_file = Storage::disk($driver)->get($path);
            $enc_file = $this->encryptDes($enc_file);
            $workspace = $user->getWorkspaceDir();
            //Storage::disk($driver)->put("$workspace/$path", $enc_file);
            return response($enc_file)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($enc_file))
            ->header('Content-Disposition', 'attachment; filename=' . $name)
            ->header('Content-Transfer-Encoding', 'binary');

            //return response()->download($file, $name);
        }
        
    }


    /** *****************************************************************************
     * Download Object Asset Bundle
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function downloadObject(Request $request, $id, $key)
    {
        $user = Auth::user();
       
        $object3d = $this->objectRepository->find($key);
 
        if(!$object3d){
            return $this->responseError('Object3D not found'); 
        }

        if($object3d->_id != $id){
            return $this->responseError('Data not match'); 
        }

        $driver = config('fitin.editor.filesystem_driver');
        $path = $object3d->getAssetBundle();

        if(!$path){
            return $this->responseError('This Object3D has no file uploaded yet'); 
        }
        $array_path = explode('/', $path);
        $name = $array_path[count($array_path) - 1];
        $file = Storage::disk($driver)->path($path);

        $event = event(new UserStoreLibrary($user, $object3d, $key));

        saveUserDownload($name, $driver, $path, Storage::disk($driver)->url($path) , $object3d);

        $gzipable = config('fitin.enable_gzip');
        if($gzipable){
            $time = \Carbon\Carbon::now()->timestamp;
            $gzfile =  "$time-$name".".gz";
            $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
            $file = $this->encryptDes(file_get_contents($file));
            gzwrite($fp, $file);
            gzclose($fp);
            return response()->download($gzfile, $gzfile, ['ID' => $event[0]->_id])->deleteFileAfterSend(true);
        }else{

            $enc_file = Storage::disk($driver)->get($path);
            $enc_file = $this->encryptDes($enc_file);
            $workspace = $user->getWorkspaceDir();
            if(config('fitin.enabled_encrypt_file')){
                Storage::disk($driver)->put("$workspace/$path", $enc_file);
            }
            return response($enc_file)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($enc_file))
            ->header('Content-Disposition', 'attachment; filename=' . $name)
            ->header('Content-Transfer-Encoding', 'binary')
            ->header('ID', $event[0]->_id);

            //return response()->download($file, $name);
        }
        

    }

    /** *****************************************************************************
     * Download Object FBX
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function downloadObjectZip(Request $request, $id, $key)
    {
        $user = Auth::user();
        
        $object3d = $this->objectRepository->find($key);
 
        if(!$object3d){
            return $this->responseError('Object3D not found'); 
        }

        if($object3d->_id != $id){
            return $this->responseError('Data not match'); 
        }

        $driver = config('fitin.editor.filesystem_driver');
        
        $path = $object3d->assetZip;
        if(!$path){
            return $this->responseError('This Object3D has no file uploaded yet'); 
        }
        $array_path = explode('/', $path);
        $name = $array_path[count($array_path) - 1];
        $file = Storage::disk($driver)->path($path);

        //$event = event(new UserStoreLibrary($user, $object3d, $key));

        saveUserDownload($name, $driver, $path, Storage::disk($driver)->url($path) , $object3d);

        $gzipable = config('fitin.enable_gzip');
        if($gzipable){
            $time = \Carbon\Carbon::now()->timestamp;
            $gzfile =  "$time-$name".".gz";
            $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
            $file = $this->encryptDes(file_get_contents($file));
            gzwrite($fp, $file);
            gzclose($fp);
            return response()->download($gzfile, $gzfile)->deleteFileAfterSend(true);
        }else{

            $enc_file = Storage::disk($driver)->get($path);
            $enc_file = $this->encryptDes($enc_file);
            $workspace = $user->getWorkspaceDir();
            //Storage::disk($driver)->put("$workspace/$path", $enc_file);
            return response($enc_file)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($enc_file))
            ->header('Content-Disposition', 'attachment; filename=' . $name)
            ->header('Content-Transfer-Encoding', 'binary');

            //return response()->download($file, $name);
        }
        

    }


    /** *****************************************************************************
     * Download Bundle
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function downloadBundleJson(Request $request, $id, $key)
    {
        $user = Auth::user();
        
        $bundle = $this->bundleRepository->find($key);

        if(!$bundle){
            return $this->responseError('Bundle not found'); 
        }

        if($bundle->_id != $id){
            return $this->responseError('Data not match'); 
        }


        if(!$bundle->editorPro || (is_array($bundle->editorPro) && empty($bundle->editorPro['json']))){
            return $this->responseError('Bundle has no json uploaded');
        }
        $path = $bundle->editorPro['json'];
        $array_path = explode('/', $path);
        $name = $array_path[count($array_path) - 1];
        $driver = config('fitin.asset.filesystem_driver');
        $file = Storage::disk($driver)->path($path);

        $event = event(new UserStoreLibrary($user, $bundle, $key));

        saveUserDownload($name, $driver, $path, Storage::disk($driver)->url($path) , $bundle);

        $gzipable = config('fitin.enable_gzip');
        if($gzipable){
            $time = \Carbon\Carbon::now()->timestamp;
            $gzfile =  "$time-$name".".gz";
            $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
            $file = $this->encryptDes(file_get_contents($file));
            gzwrite($fp, $file);
            gzclose($fp);
            return response()->download($gzfile, $gzfile, ['ID' => $event[0]->_id])->deleteFileAfterSend(true);
        }else{

            $enc_file = Storage::disk($driver)->get($path);
            $enc_file = $this->encryptDes($enc_file);
            $workspace = $user->getWorkspaceDir();
            //Storage::disk($driver)->put("$workspace/$path", $enc_file);
            return response($enc_file)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($enc_file))
            ->header('Content-Disposition', 'attachment; filename=' . $name)
            ->header('Content-Transfer-Encoding', 'binary')
            ->header('ID', $event[0]->_id);

            //return response()->download($file, $name);
        }
    }

    /** *****************************************************************************
     * Download Bundle
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function downloadMaterial(Request $request, $name, $id)
    {
        $user = Auth::user();
        
        $material = $this->materialRepository->find($name);

        if(!$material){
            return $this->responseError('Material not found'); 
        }

        if($material->_id != $id){
            return $this->responseError('Data not match'); 
        }

        if(!$material->assetBundle){
            return $this->responseError('Asset bundle not uploaded'); 
        }

        $path = $material->getAssetBundle();
        $arr = explode('/', $path);
        $file_name = $arr[count($arr) - 1];
        $driver = config('fitin.editor.filesystem_driver');
        $file = Storage::disk($driver)->path($path);

        $event = event(new UserStoreLibrary($user, $material, $name));

        saveUserDownload($file_name, $driver, $path, Storage::disk($driver)->url($path) , $material);
        
        $gzipable = config('fitin.enable_gzip');
        
        if($gzipable){
            $time = \Carbon\Carbon::now()->timestamp;
            $gzfile =  "$time-$file_name".".gz";
            $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
            $file = $this->encryptDes(file_get_contents($file));
            gzwrite($fp, $file);
            gzclose($fp);
            return response()->download($gzfile, $gzfile, ['ID' => $event[0]->_id])->deleteFileAfterSend(true);
        }else{

            $enc_file = Storage::disk($driver)->get($path);
            $enc_file = $this->encryptDes($enc_file);
            $workspace = $user->getWorkspaceDir();
            if(config('fitin.enabled_encrypt_file')){
                Storage::disk($driver)->put("$workspace/$path", $enc_file);
            }
            return response($enc_file)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($enc_file))
            ->header('Content-Disposition', 'attachment; filename=' . $file_name)
            ->header('Content-Transfer-Encoding', 'binary')
            ->header('ID', $event[0]->_id);

            //return response()->download($file, $name);
        }
    }

    /** *****************************************************************************
     * Download Layout PREFAB
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function downloadLayoutPrefab(Request $request, $name, $id)
    {
        $user = Auth::user();
        
        $layout = $this->editorProLayoutRepository->find($name);
        
        if(!$layout){
            return $this->responseError('Layout not found'); 
        }

        if($layout->_id != $id){
            return $this->responseError('Data not match'); 
        }

        $path = $layout->getAssetBundle();
        if(!$path){
            return $this->responseError('Env not found'); 
        }
        $array_path = explode('/', $path);
        $name = $array_path[count($array_path) - 1];
        $driver = config('fitin.editor.filesystem_driver');
        $file = Storage::disk($driver)->path($path);

        saveUserDownload($name, $driver, $path, Storage::disk($driver)->url($path) , $layout);

        $gzipable = config('fitin.enable_gzip');
        if($gzipable){
            $time = \Carbon\Carbon::now()->timestamp;
            $gzfile =  "$time-$name".".gz";
            $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
            $file = $this->encryptDes(file_get_contents($file));
            gzwrite($fp, $file);
            gzclose($fp);
            return response()->download($gzfile, $gzfile)->deleteFileAfterSend(true);
        }else{

            $enc_file = Storage::disk($driver)->get($path);
            $enc_file = $this->encryptDes($enc_file);
            $workspace = $user->getWorkspaceDir();
            if(config('fitin.enabled_encrypt_file')){
                Storage::disk($driver)->put("$workspace/$path", $enc_file);
            }
            return response($enc_file)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($enc_file))
            ->header('Content-Disposition', 'attachment; filename=' . $name)
            ->header('Content-Transfer-Encoding', 'binary');

            //return response()->download($file, $name);
        }
    }

    /** *****************************************************************************
     * Download Layout JSON
     *
     * @param Request $request
     * 
     * @return Array
     */

    // public function downloadLayoutJson(Request $request, $name, $id)
    // {
    //     $user = Auth::user();
        
    //     $layout = $this->layoutRepository->find($name);
        
    //     if(!$layout){
    //         return $this->responseError('Layout not found'); 
    //     }

    //     if($layout->_id != $id){
    //         return $this->responseError('Data not match'); 
    //     }

    //     $path = $layout->editorPro['json'] ?? NULL;
    //     if(!$path){
    //         return $this->responseError('json not found'); 
    //     }
    //     $array_path = explode('/', $path);
    //     $name = $array_path[count($array_path) - 1];
    //     $driver = config('fitin.editor.filesystem_driver');
    //     $file = Storage::disk($driver)->path($path);

    //     saveUserDownload($name, $driver, $path, Storage::disk($driver)->url($path) , $layout);
        
    //     $gzipable = config('fitin.enable_gzip');
    //     if($gzipable){
    //         $gzfile =  $name.".gz";
    //         $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
    //         gzwrite($fp, file_get_contents($file));
    //         gzclose($fp);
    //         return response()->download($gzfile, $gzfile)->deleteFileAfterSend(true);
    //     }else{
    //         return response()->download($file, $name);
    //     }
    // }

    public function downloadBundleEditorJson(Request $request, $rev, $id)
    {
        $user = Auth::user();
        
        $bundle = $this->editorProBundleRepository->find($id);

        if(!$bundle){
            return $this->responseError('Bundle not found'); 
        }

        if($bundle->revision != $rev){
            return $this->responseError('Data not match'); 
        }

        $path = $layout->json;
        if(!$path){
            return $this->responseError('json not found'); 
        }
        $array_path = explode('/', $path);
        $name = $array_path[count($array_path) - 1];
        $driver = config('fitin.editor.filesystem_driver');
        $file = Storage::disk($driver)->path($path);

        saveUserDownload($name, $driver, $path, Storage::disk($driver)->url($path) , $bundle);
        
        $gzipable = config('fitin.enable_gzip');
        if($gzipable){
            $time = \Carbon\Carbon::now()->timestamp;
            $gzfile =  "$time-$name".".gz";
            $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
            $file = $this->encryptDes(file_get_contents($file));
            gzwrite($fp, $file);
            gzclose($fp);
            return response()->download($gzfile, $gzfile)->deleteFileAfterSend(true);
        }else{

            $enc_file = Storage::disk($driver)->get($path);
            $enc_file = $this->encryptDes($enc_file);
            $workspace = $user->getWorkspaceDir();
            //Storage::disk($driver)->put("$workspace/$path", $enc_file);
            return response($enc_file)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($enc_file))
            ->header('Content-Disposition', 'attachment; filename=' . $name)
            ->header('Content-Transfer-Encoding', 'binary');

            //return response()->download($file, $name);
        }
    }

    // public function downloadLayoutEditorJson(Request $request, $rev, $id)
    // {
    //     $user = Auth::user();
        
    //     $layout = $this->editorProLayoutRepository->find($id);

    //     if(!$layout){
    //         return $this->responseError('Layout not found'); 
    //     }

    //     if(!$layout->editorPro || empty( $layout->editorPro['revision']) || $layout->editorPro['revision'] != $rev){
    //         return $this->responseError('Data not match'); 
    //     }

    //     $path = $layout->editorPro['json'] ?? NULL;
    //     if(!$path){
    //         return $this->responseError('json not found'); 
    //     }
    //     $array_path = explode('/', $path);
    //     $name = $array_path[count($array_path) - 1];
    //     $driver = config('fitin.editor.filesystem_driver');
    //     $file = Storage::disk($driver)->path($path);

    //     saveUserDownload($name, $driver, $path, Storage::disk($driver)->url($path) , $layout);
        
    //     $gzipable = config('fitin.enable_gzip');
    //     if($gzipable){
    //         $gzfile =  $name.".gz";
    //         $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
    //         gzwrite($fp, file_get_contents($file));
    //         gzclose($fp);
    //         return response()->download($gzfile, $gzfile)->deleteFileAfterSend(true);
    //     }else{
    //         return response()->download($file, $name);
    //     }
    // }

    // public function downloadLayoutEditorPrefab(Request $request, $rev, $id)
    // {
    //     $user = Auth::user();
        
    //     $layout = $this->editorProLayoutRepository->find($id);

    //     if(!$layout){
    //         return $this->responseError('Layout not found'); 
    //     }

    //     if( $layout->revision!= $rev){
    //         return $this->responseError('Data not match'); 
    //     }

    //     $path = $layout->assetBundle ?? NULL;
    //     if(!$path){
    //         return $this->responseError('env not found'); 
    //     }
    //     $array_path = explode('/', $path);
    //     $name = $array_path[count($array_path) - 1];
    //     $driver = config('fitin.editor.filesystem_driver');
    //     $file = Storage::disk($driver)->path($path);

    //     saveUserDownload($name, $driver, $path, Storage::disk($driver)->url($path) , $layout);
        
    //     $gzipable = config('fitin.enable_gzip');
    //     if($gzipable){
    //         $gzfile =  $name.".gz";
    //         $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
    //         gzwrite($fp, file_get_contents($file));
    //         gzclose($fp);
    //         return response()->download($gzfile, $gzfile)->deleteFileAfterSend(true);
    //     }else{
    //         return response()->download($file, $name);
    //     }
    // }


    /** *****************************************************************************
     * Download Layout PREFAB
     *
     * @param Request $request
     * 
     * @return Array
     */

    public function downloadRoomPrefab(Request $request, $name, $id)
    {
        $user = Auth::user();
        
        $room = $this->roomRepository->find($name);
        
        if(!$room){
            return $this->responseError('Room not found'); 
        }

        if($room->_id != $id){
            return $this->responseError('Data not match'); 
        }

        $path = $room->getAssetBundle();
        if(!$path){
            return $this->responseError('Env not found'); 
        }
        $array_path = explode('/', $path);
        $name = $array_path[count($array_path) - 1];
        $driver = config('fitin.editor.filesystem_driver');
        $file = Storage::disk($driver)->path($path);

        saveUserDownload($name, $driver, $path, Storage::disk($driver)->url($path) , $room);

        $gzipable = config('fitin.enable_gzip');
        if($gzipable){
            $time = \Carbon\Carbon::now()->timestamp;
            $gzfile =  "$time-$name".".gz";
            $fp = gzopen($gzfile, 'w9'); // w == write, 9 == highest compression
            $file = $this->encryptDes(file_get_contents($file));
            gzwrite($fp, $file);
            gzclose($fp);
            return response()->download($gzfile, $gzfile)->deleteFileAfterSend(true);
        }else{

            $enc_file = Storage::disk($driver)->get($path);
            $enc_file = $this->encryptDes($enc_file);
            $workspace = $user->getWorkspaceDir();

            if(config('fitin.enabled_encrypt_file')){
                Storage::disk($driver)->put("$workspace/$path", $enc_file);
            }
            
            return response($enc_file)
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($enc_file))
            ->header('Content-Disposition', 'attachment; filename=' . $name)
            ->header('Content-Transfer-Encoding', 'binary');

            //return response()->download($file, $name);
        }
    }

    public function encryptDes($file){
        if(!config('fitin.enabled_encrypt_file')){
            return $file;
        }
        
        $user = Auth::user();
        $private_key = $user->getPrivateKey();
        // Generate key based on current time
        $secret_key = utf8_encode($private_key);
        // Hash the key with SHA256
        $key = hash('SHA256', $secret_key, true);
        // Use AES 128 w/ CBC as encryption method
        $method  = "aes-256-cbc";
        // Get cipher length based on encryption method
        $cypher_length = openssl_cipher_iv_length($method);
        // Generate IV -- possible issue
        //$iv = random_bytes($cypher_length);
        $iv = hex2bin('31ba091e42a58ea391a3a421d4e6f961');
        $output = openssl_encrypt($file, $method, $key, OPENSSL_RAW_DATA, $iv);

        return $output;       
    }
}
