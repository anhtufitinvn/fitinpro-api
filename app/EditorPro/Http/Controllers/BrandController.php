<?php

namespace App\EditorPro\Http\Controllers;


use App\EditorPro\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\BrandRepository;

use App\EditorPro\Http\Resources\BrandResource;
use App\EditorPro\Http\Resources\BrandResourceCollection;

class BrandController extends ApiController
{
    protected $repository;

    public function __construct(BrandRepository $repository){
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $brands = $this->repository->getAll($params);

        return new BrandResourceCollection($brands);

    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return BrandResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $id)
    {
        $brand = $this->repository->find($id);
        if(!$brand){
            return $this->responseError('Brand not found' );
        }
        return new BrandResource($brand);
    }

}
