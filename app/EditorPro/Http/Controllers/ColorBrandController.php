<?php

namespace App\EditorPro\Http\Controllers;


use App\EditorPro\Http\Resources\ColorBrandResource;
use App\EditorPro\Http\Resources\ColorBrandResourceCollection;
use Illuminate\Support\Facades\Validator;
use App\EditorPro\Models\ColorBrand;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use App\EditorPro\Repositories\ColorBrandRepository;

class ColorBrandController extends ApiController
{
    protected $colorBrandRepository;

    public function __construct(ColorBrandRepository $colorBrandRepository){
        $this->colorBrandRepository = $colorBrandRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $colors = $this->colorBrandRepository->getAll($params);

        return new ColorBrandResourceCollection($colors);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
       
        
    }
  

    /**
     * Display the specified resource.
     *
     * @param Brand $category
     * @return BrandResource
     */
    public function show($key)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        
    }
}
