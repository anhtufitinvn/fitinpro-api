<?php

namespace App\EditorPro\Http\Controllers;

use App\EditorPro\Models\MaterialCategory;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\MaterialCategoryRepository;

use App\EditorPro\Http\Resources\MaterialCategoryResource;
use App\EditorPro\Http\Resources\MaterialCategoryResourceCollection;

class MaterialCategoryController extends ApiController
{
    protected $repository;

    public function __construct(MaterialCategoryRepository $repository){
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
        //$params['published'] = true;
        $materialMaterialCategories = $this->repository->getAll($params);
        return new MaterialCategoryResourceCollection($materialMaterialCategories);
    }

}
