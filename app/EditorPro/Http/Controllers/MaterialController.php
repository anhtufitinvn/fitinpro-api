<?php

namespace App\EditorPro\Http\Controllers;


use App\Models\Ver2\Material;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\MaterialRepository;

use App\EditorPro\Http\Resources\MaterialResource;
use App\EditorPro\Http\Resources\MaterialResourceCollection;
use App\EditorPro\Http\Resources\FitinMaterialResourceCollection;
class MaterialController extends ApiController
{
    protected $repository;

    public function __construct(MaterialRepository $repository){
        $this->repository = $repository;
    }


    public function index(Request $request)
    {
        $params = $request->all();
        //$params['published'] = true;
        $params['filter-by'] = 'designer';
        $materials = $this->repository->getAll($params);
        return new MaterialResourceCollection($materials);
    }

    public function specificIndex(Request $request)
    {
        $params = $request->all();
    
        $materials = $this->repository->getAll($params);
        return new FitinMaterialResourceCollection($materials);
    }
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  string $key
     * @return MaterialResource
     */
    public function show(Request $request, $key)
    {
        $material = $this->repository->find($key);
        if(!$material){
            return $this->responseError('Material not found' );
        }
        return new MaterialResource($material);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $input = request()->all();
        
        $validator = Validator::make($input, [
            'code'   => 'required'
        ]);
        
        if($validator->fails()){
            $errorString = implode(",", $validator->messages()->all());
            return $this->responseError($errorString, "INVALID");  
        }
        
        $input['code'] = space_replace(strtolower($input['code']), '_');
        $material = $this->repository->find($input['code']);
        if ($material) {
            return $this->responseError('Code duplicated', 'DATA_EXISTS');
        }
        $input['status'] = Material::PRIVATE;
        $metadata = [
            "user" => [
                "email" => Auth::user()->email,
                "id" => Auth::user()->auth_id
            ]
        ];
        $input['metadata'] = $metadata;

        $material = $this->repository->create($input);

        return new MaterialResource($material);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param string $key
     * @return MaterialResource|JsonResponse
     */
    public function update(Request $request, $key)
    {
       
        $input = $request->except(['code', 'thumbList']);
        $material = $this->repository->update($key, $input);
        if(!$material){
            return $this->responseError('Material not found' );
        }
        
        return new MaterialResource($material);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     */
    public function destroy(Request $request,$key)
    {
        $material = $this->repository->delete($key);
        if(!$material){
            return $this->responseError('Material not found' );
        }
        return $this->respondSuccess();
    }

}
