<?php

namespace App\EditorPro\Http\Controllers\Web;

use App\EditorPro\Models\Bundle;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\Controller;
use App\EditorPro\Repositories\BundleRepository;

class BundleController extends BaseController
{
    protected $bundleRepository;

    public function __construct(BundleRepository $bundleRepository){
        $this->bundleRepository = $bundleRepository;
    }
    
    public function getForm(Request $request, $key)
    {
        $token = $request->get('access_token');
        $bundle = $this->bundleRepository->find($key);
        if(!$bundle){
            return $this->respondNotFound();
        }
        $bundle->getEcomProductList();
        
        $api = route('editor.bundles.update', $key);
        return view('editorPro.web.bundle.update', compact('bundle', 'api', 'token'))->render();
    }
   
}
