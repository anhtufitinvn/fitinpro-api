<?php

namespace App\EditorPro\Http\Controllers\Web;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class HomeController extends BaseController
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return response()->json([
            'message' => 'Welcome to Fitin Pro Api',
            'version' => '1.0.1',
            'date' => '2020-05-25'
        ]);
    }
}
