<?php

namespace App\EditorPro\Http\Controllers\Web;

use App\FitIn\Asset;

use App\Models\Ver2\Color;
use App\EditorPro\Models\Library;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\Object3dRepository;
use App\EditorPro\Http\Resources\ObjectResourceCollection;
use App\EditorPro\Http\Resources\ObjectResource;

class ObjectController extends BaseController
{
    
    protected  $object3dRepository;
    public function __construct(Object3dRepository $object3dRepository){
        $this->object3dRepository = $object3dRepository;
    }


    public function filter(Request $request)
    {
        $keys = $request->object_key;
        if(!$keys){
            return $this->responseError('Need object_key');
        }
        $count = count(explode(',', $keys));
        $asset = new Asset();
        $data = $asset->getByType('product-filter', ['object_key' => $keys, 'limit' => $count + 1]);

        return $this->responseData($data);
    }

    public function getDetail(Request $request, $key)
    {
        $asset = new Asset();
        $data = $asset->getByKey('product-by-key', $key);
        
        $object = $this->object3dRepository->find($key);
        $url = env('ECOM_DOMAIN', 'https://fitin.vn') .  $data['url'];
        return view('editorPro.web.object.show', compact('data', 'object', 'url'))->render();
    }

}
