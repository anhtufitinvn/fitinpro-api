<?php

namespace App\EditorPro\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Helpers\Utils;
use Illuminate\Support\Facades\Cache;
use \Illuminate\Support\Facades\Storage;
use App\Events\UploadEvent;

class BaseController extends Controller
{
 
    public function respondNotFound()
    {
        return view('editorPro.errors.404');
    }

    
}
