<?php

namespace App\EditorPro\Http\Controllers\Web;

use App\FitIn\Asset;

use App\EditorPro\Models\Library;

use App\EditorPro\Models\BuildConfig;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\EditorPro\Repositories\PageRepository;


class PageController extends BaseController
{
    
    protected  $pageRepository;
    public function __construct(PageRepository $pageRepository){
        $this->pageRepository = $pageRepository;
    }


    public function show(Request $request, $key)
    {
        $page = $this->pageRepository->find($key);
        if(!$page){
            return $this->respondNotFound();
        }
        return view('editorPro.web.page.show', compact('page'));
    }

    public function tutorial(Request $request){
        $config = BuildConfig::getLatestVersion();
        $configStable = BuildConfig::getLatestPublishedVersion();

        $downloadUrl = "";
        $downloadVersion = "";
        $downloadStableUrl= "";
        $downloadStableVersion = "";
        if($config){
            $downloadUrl = $config->url;
            $downloadVersion = $config->version;
            if($configStable && $configStable->_id != $config->_id){
                $downloadStableUrl = $configStable->url;
                $downloadStableVersion = $configStable->version;
            }
        }   
        
        $youtubeUrl = "https://www.youtube.com/embed/myjl4qNtj_0";
        return view('editorPro.web.tutorial.index', compact('downloadUrl', 'youtubeUrl', 'downloadStableUrl', 'downloadVersion', 'downloadStableVersion'));
    }
}
