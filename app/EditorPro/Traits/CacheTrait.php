<?php

namespace App\EditorPro\Traits;

use Illuminate\Support\Facades\Cache;

trait CacheTrait
{
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            
        });

        self::created(function ($model) {
            self::flushCache();
        });

        self::updating(function ($model) {

        });

        self::updated(function ($model) {
            self::flushCache();
        });

        self::deleting(function ($model) {

        });

        self::deleted(function ($model) {
            self::flushCache();
        });
    }

    public static function flushCache()
    {   
        $cacheModel = [
            \App\EditorPro\Models\Library::class,
            \App\EditorPro\Models\Object3d::class,
            \App\EditorPro\Models\Bundle::class,
            \App\EditorPro\Models\Room::class,
            \App\EditorPro\Models\Project::class,
            \App\EditorPro\Models\Style::class,
            \App\EditorPro\Models\Layout::class,
            \App\EditorPro\Models\Material::class,
            \App\EditorPro\Models\User::class
        ];

         
        if(in_array(self::class, $cacheModel)){
            foreach($cacheModel as $model){           
                if(defined("$model::CACHE_TAG")){   
                    Cache::tags($model::CACHE_TAG)->flush();
                }   
            } 
        }else{
            if(defined("self::CACHE_TAG")){   
                Cache::tags(self::CACHE_TAG)->flush();
            }  

            if(defined("self::ADDITIONAL_CACHE_TAG")){   
                Cache::tags(self::ADDITIONAL_CACHE_TAG)->flush();
            }  
        }
        
    }
}
