<?php

namespace App\EditorPro\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use App\EditorPro\Models\Permission;
use App\EditorPro\Services\CacheService;

trait HasPermissions
{
    private $permissionClass;

    public function getPermissionClass(){
        $this->permissionClass = Permission::class;   
        return $this->permissionClass;
    }

    public static function bootHasPermissions()
    {
        static::deleting(function ($model) {
            if (method_exists($model, 'isForceDeleting') && ! $model->isForceDeleting()) {
                return;
            }

            $model->permissions()->sync([]);
        });
    }


    /**
     * A model may have multiple direct permissions.
     */
    public function permissions(): MorphToMany
    {
        return $this->morphToMany(
            Permission::class,
            'model',
             null,
            'permission_ids',
            'user_ids'
        );
    }


    /**
     * Return all the permissions the model has via roles.
     */
    public function getPermissionsViaRoles(): Collection
    {
        return $this->load('roles', 'roles.permissions')
            ->roles->flatMap(function ($role) {
                return $role->permissions;
            })->sort()->values();
    }

    /**
     * Return all the permissions the model has, both directly and via roles.
     *
     * @throws \Exception
     */
    public function getAllPermissions(): Collection
    {   
        $cacheService = new CacheService;
        $data = $cacheService->getAllPermissions($this);     
        return $data;
    }

    /**
     * Grant the given permission(s) to a role.
     *
     * @param string|array|\Spatie\Permission\Contracts\Permission|\Illuminate\Support\Collection $permissions
     *
     * @return $this
     */
    public function givePermissionTo(...$permissions)
    {
        $permissions = collect($permissions)
            ->flatten()
            ->map(function ($permission) {
                if (empty($permission)) {
                    return false;
                }

                return $this->getStoredPermission($permission);
            })
            ->filter(function ($permission) {
                return $permission instanceof Permission;
            })           
            ->map->id
            ->all();

        $model = $this->getModel();
            
        if ($model->exists) {
            $this->permissions()->sync($permissions, false);
            $model->load('permissions');
        }
        
        return $this;
    }

    /**
     * Remove all current permissions and set the given ones.
     *
     * @param string|array|\Spatie\Permission\Contracts\Permission|\Illuminate\Support\Collection $permissions
     *
     * @return $this
     */
    public function syncPermissions(...$permissions)
    {
        $this->permissions()->sync([]);

        return $this->givePermissionTo($permissions);
    }

    /**
     * Revoke the given permission.
     *
     * @param \Spatie\Permission\Contracts\Permission|\Spatie\Permission\Contracts\Permission[]|string|string[] $permission
     *
     * @return $this
     */
    public function revokePermissionTo($permission)
    {
        $this->permissions()->detach($this->getStoredPermission($permission));

        $this->load('permissions');

        return $this;
    }

    public function getPermissionNames(): Collection
    {
        return $this->permissions->pluck('name');
    }

    /**
     * @param string|array|\Spatie\Permission\Contracts\Permission|\Illuminate\Support\Collection $permissions
     *
     * @return \Spatie\Permission\Contracts\Permission|\Spatie\Permission\Contracts\Permission[]|\Illuminate\Support\Collection
     */
    protected function getStoredPermission($permissions)
    {
        $permissionClass = $this->getPermissionClass();
        if (is_numeric($permissions)) {
            return $permissionClass::where('_id', $permissions)->first();
        }

        if (is_string($permissions)) {
            return $permissionClass::where('name',$permissions)->first();
        }

        if (is_array($permissions)) {
            return $permissionClass::whereIn('name', $permissions)
                ->get();
        }

        return $permissions;
    }


    /**
     * Determine if the model has any of the given permissions.
     *
     * @param array ...$permissions
     *
     * @return bool
     * @throws \Exception
     */
    public function hasPermission(...$permissions): bool
    {
        if (is_array($permissions[0])) {
            $permissions = $permissions[0];
        }

        foreach ($permissions as $permission) {
            if ($this->checkPermissionTo($permission)) {
                return true;
            }
        }

        return false;
    }

    /**
     * An alias to hasPermissionTo(), but avoids throwing an exception.
     *
     * @param string|int|\Spatie\Permission\Contracts\Permission $permission
     * @param string|null $guardName
     *
     * @return bool
     */
    public function checkPermissionTo($permission, $guardName = null): bool
    {
        try {
            return $this->hasPermissionTo($permission, $guardName);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Determine if the model may perform the given permission.
     *
     * @param string|int|\Spatie\Permission\Contracts\Permission $permission
     * @param string|null $guardName
     *
     * @return bool
     * @throws PermissionDoesNotExist
     */
    public function hasPermissionTo($permission, $guardName = null): bool
    {
        return $this->hasPermissionViaRole($permission) || $this->hasDirectPermission($permission);
    }

    /**
     * Determine if the model has the given permission.
     *
     * @param string|int|\Spatie\Permission\Contracts\Permission $permission
     *
     * @return bool
     * @throws PermissionDoesNotExist
     */
    public function hasDirectPermission($permission): bool
    {
        //$permissionClass = $this->getPermissionClass();
        $service = new CacheService();
        $permissions = $service->getPermissions();
        if (is_string($permission)) {
            $permission = $permissions->where('name', $permission)->first();
            if (!$permission) {
                return false;
            }
        }

        //MONGO DB NOT USE INT ID
        // if (is_int($permission)) {
        //     $permission = $permissionClass->findById($permission);
        //     if (! $permission) {
        //         return false;
        //     }
        // }

        // if (! $permission instanceof Permission) {
        //     return false;
        // }
        
        return $this->permissions->contains('_id', $permission->_id) || $this->permissions->contains('name', $permission->name);
    }

    /**
     * Determine if the model has, via roles, the given permission.
     *
     * @param \Spatie\Permission\Contracts\Permission $permission
     *
     * @return bool
     */
    protected function hasPermissionViaRole($permission): bool
    {
        $service = new CacheService();
        $permissions = $service->getPermissions();
        
        if (is_string($permission)) {
            
            $permission = $permissions->where('name', $permission)->first();
            
            if (!$permission) {
                return false;
            }
        }
        
        return $this->hasRole($permission->roles);
    }

    public function forgetCachedPermissions()
    {
        $service = new CacheService();
        $service->forgetCacheUsersRolesPermissions();
        //app(PermissionRegistrar::class)->forgetCachedPermissions();
    }

}

