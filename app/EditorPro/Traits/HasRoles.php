<?php

namespace App\EditorPro\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use App\EditorPro\Models\Role;
use App\EditorPro\Services\CacheService;

trait HasRoles
{
    use HasPermissions;

    private $roleClass;

    public static function bootHasRoles()
    {
        static::deleting(function ($model) {
            if (method_exists($model, 'isForceDeleting') && ! $model->isForceDeleting()) {
                return;
            }

            $model->roles()->sync([]);
        });
    }

    public function getRoleClass()
    {   
        $this->roleClass = Role::class;
    
        return $this->roleClass;
    }

    /**
     * A model may have multiple roles.
     */
    public function roles()
    {
        return $this->morphToMany(
            Role::class,
            'model',    
            'role_id'
        );
    }


    /**
     * Assign the given role to the model.
     *
     * @param array|string|\Spatie\Permission\Contracts\Role ...$roles
     *
     * @return $this
     */
    public function assignRole(...$roles)
    {
        $roles = collect($roles)
            ->flatten()
            ->map(function ($role) {
                if (empty($role)) {
                    return false;
                }

                return $this->getStoredRole($role);
            })
            ->filter(function ($role) {
                return $role instanceof Role;
            })
            ->map->id
            ->all();

        $model = $this->getModel();

        if ($model->exists) {
            $this->roles()->sync($roles, false);
            $model->load('roles');
        } 

        return $this;
    }

    /**
     * Revoke the given role from the model.
     *
     * @param string|\Spatie\Permission\Contracts\Role $role
     */
    public function removeRole($role)
    {
        $this->roles()->detach($this->getStoredRole($role));

        $this->load('roles');

        return $this;
    }

    /**
     * Remove all current roles and set the given ones.
     *
     * @param array|\Spatie\Permission\Contracts\Role|string ...$roles
     *
     * @return $this
     */
    public function syncRoles(...$roles)
    {
        //$this->roles()->detach();
        $this->roles()->sync([]);
        return $this->assignRole($roles);
    }

    /**
     * Determine if the model has (one of) the given role(s).
     *
     * @param string|int|array|\Spatie\Permission\Contracts\Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasRole($roles)
    {
        $user_roles = (new CacheService)->getUserRoles($this);
        if (is_string($roles) && false !== strpos($roles, '|')) {
            $roles = $this->convertPipeToArray($roles);
        }
        
        if (is_string($roles)) {
            //return $this->roles->contains('name', $roles);
            
            return $user_roles->contains(function ($val, $key) use ($roles) {
                return $val->name == $roles || $val->_id == $roles;
            });
        }

        if (is_int($roles)) {
            return $user_roles->contains('_id', $roles);
        }

        if ($roles instanceof Role) {
            return $user_roles->contains('_id', $roles->_id) || $user_roles->contains('name', $roles->name);
        }

        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }

            return false;
        }

        return $roles->intersect($user_roles)->isNotEmpty();
    }

    /**
     * Determine if the model has any of the given role(s).
     *
     * @param string|array|\Spatie\Permission\Contracts\Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasAnyRole($roles)
    {
        return $this->hasRole($roles);
    }

    public function getRoleNames()
    {
        return $this->roles->pluck('name');
    }

    protected function getStoredRole($role)
    {
        $roleClass = $this->getRoleClass();

        if (is_numeric($role)) {
            return $roleClass::where('_id', $role)->first();
        }

        if (is_string($role)) {
            return $roleClass::where('name', $role)->orWhere('_id', $role)->first();
        }
        

        return $role;
    }

    protected function convertPipeToArray(string $pipeString)
    {
        $pipeString = trim($pipeString);

        if (strlen($pipeString) <= 2) {
            return $pipeString;
        }

        $quoteCharacter = substr($pipeString, 0, 1);
        $endCharacter = substr($quoteCharacter, -1, 1);

        if ($quoteCharacter !== $endCharacter) {
            return explode('|', $pipeString);
        }

        if (! in_array($quoteCharacter, ["'", '"'])) {
            return explode('|', $pipeString);
        }

        return explode('|', trim($pipeString, $quoteCharacter));
    }

}
