<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Project;
use App\EditorPro\Models\Layout;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class ProjectRepository extends EloquentRepository implements ProjectRepositoryInterface {
    
    const CACHE_TIME = 60 * 60 * 24;

    protected $upload_service;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Project::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $published = $params['published'] ?? 0;
        if(!empty($params['isTemplate']) && ($params['isTemplate'] || $params['isTemplate'] === false)){
            $isTemplate = $params['isTemplate'];
        }
        else{
            $isTemplate = NULL;
        }
        $withLayout = $params['with-layout'] ?? false;
        $time = $params['t'] ?? 0;
        $concat = concat(":",  $name, $withLayout, $published, $isTemplate, $time, $limit, $page);
        
        $key = "editor-index:$concat";
        $collections = Cache::tags(Project::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $withLayout, $isTemplate, $published, $time, $limit, $page){
            $query = $this->model;
            if($name){
               
                $query = $query->where(function ($q) use ($name) {
                    $q->where('code', 'like', '%'. $name . '%')
                          ->orWhere('name', 'like', '%'. $name . '%');
                });
            }

            if($published){
                $query = $query->published();
            }

            if($isTemplate){
                $query = $query->where('isTemplate', true);
            }else if ($isTemplate === false){
                $query = $query->where('isTemplate', false);
            }
            
            if($time){
                $timestamp = \Carbon\Carbon::createFromTimestamp($time);
                
                $query = $query->where('updated_at', '>=', $timestamp);
            }

            if($withLayout){
                //USE this as Jens/mongodb cannot use whereHas if constraint column is different from _id
                $layouts = \DB::collection('EditorLayouts')->select('project')->distinct()->get()->toArray();
                $query = $query->whereIn('code', $layouts);
            }
            
            if($limit){
                return $query->orderBy('sort', 'DESC')->latest()->paginate($limit);
            }
            else {
                return $query->orderBy('sort', 'DESC')->latest()->get();
            }
           
        });
        return $collections;
    }

    
	public function create($input)
	{
        if(empty( $input['status'])){
            $input['status'] = Project::PENDING;
        }
        $input['sort'] = (int) ($input['sort'] ?? 0);
		$project = $this->model->create($input);
        
        return $project;
    }
    
    public function find($key){
        $cache_key = "editor-show:$key";
        $project = Cache::tags(Project::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('code', $key)->first();
        });
        
        return $project;
    }


    public function update($key, $input){
        $project = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$project){
            return false;
        }
        $input['sort'] = (int) ($input['sort'] ?? 0);
        $project->update($input);
        return $project;
    }

    public function delete($key){
        $project = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$project){
            return false;
        }

        $project->delete();
        return $project;
    }
}