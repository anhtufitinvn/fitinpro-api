<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Feedback;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class FeedbackRepository extends EloquentRepository implements FeedbackRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;


    public function __construct()
    {
        $this->model = app($this->model());

    }

	public function model()
	{
		return Feedback::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $orderBy = $params['orderBy'] ?? null;  
        $ascending =  $params['ascending'] ?? null;
        $concat = concat(":",$orderBy, $ascending, $limit, $page);
        
        $key = "editor-index:$concat";
        $collections = Cache::tags(Feedback::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($orderBy, $ascending, $limit, $page){
            $query = $this->model;
            if($orderBy){
                $query = $query->orderBy($orderBy, $ascending ? "ASC" : "DESC");
            }
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
           
        });
        return $collections;
    }

    
	public function create($input)
	{
		$feedback = $this->model->create($input);
        
        return $feedback;
    }
    
    public function find($key){
        $cache_key = "editor-show:$key";
        $feedback = Cache::tags(Feedback::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->find($key);
        });
        
        return $feedback;
    }


    public function update($key, $input){
        $feedback = $this->model->find($key);
        if(!$feedback){
            return false;
        }
        $feedback->update($input);
        return $feedback;
    }

    public function delete($key){
        $feedback = $this->model->find($key);
        
        if(!$feedback){
            return false;
        }
        $feedback->delete();
        return $feedback;
    }
}