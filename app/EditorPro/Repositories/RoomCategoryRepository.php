<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\RoomCategory;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class RoomCategoryRepository extends EloquentRepository implements RoomCategoryRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return RoomCategory::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
    
        $concat = concat(":",  $name, $limit, $page);

        $key = "editor-index:$concat";
        $collections = Cache::tags(RoomCategory::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $limit, $page){
            $query = $this->model;
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }
            
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
           
        });
        return $collections;
    }

    
	public function create($input)
	{
       
		$roomRoomCategory = $this->model->create($input);
        
        return $roomRoomCategory;
    }
    
    public function find($key){
        $cache_key = "editor-show:$key";
        $roomRoomCategory = Cache::tags(RoomCategory::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('code', $key)->first();
        });
        
        return $roomRoomCategory;
    }


    public function update($key, $input){
        $roomRoomCategory = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$roomRoomCategory){
            return false;
        }
        $roomRoomCategory->update($input);
        return $roomRoomCategory;
    }

    public function delete($key){
        $roomRoomCategory = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$roomRoomCategory){
            return false;
        }

        $roomRoomCategory->delete();
        return $roomRoomCategory;
    }
}