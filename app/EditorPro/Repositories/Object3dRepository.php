<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Object3d;
use App\EditorPro\Models\Category;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;
use App\FitIn\Asset;
use App\EditorPro\Services\SearchService;

class Object3dRepository extends EloquentRepository implements Object3dRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    protected $upload_service, $asset, $categoryRepository, $searchService;

    public function __construct(UploadService $upload_service, Asset $asset, CategoryRepository $categoryRepository, SearchService $searchService)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
        $this->asset = $asset;
        $this->categoryRepository = $categoryRepository;
        $this->searchService = $searchService;
    }

	public function model()
	{
		return Object3d::class;
	}
    
    public function getAll($params = [])
    {           
        
        $type = $params['type'] ?? null;
        $vendor = $params['vendor'] ?? null;
        $brand = $params['brand'] ?? null;
        $category = strtolower($params['category'] ?? null);
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ;     
        $name = strtolower($params['name'] ?? null);
        $start = strtolower($params['start'] ?? null);
        $build = $params['build'] ?? null;
        $filter_by = $params['filter-by'] ?? null;
        $published = $params['published'] ?? 0;
        $status = $params['status'] ?? null;
        $user = $params['user'] ?? Auth::user()->auth_id;
        $concat = concat(":", $filter_by, $vendor, $brand, $build, $start, $name, $category, $limit, $page, $status, $published, $user);

        if($type == 'item'){
            $key = "editor-indexItem:";
        }elseif($type == 'tts'){
            $key = "editor-indexTTS:";
        }else{
            $key = "editor-index:";
        }
        $key = $key . $concat;
        
        
        $collections = Cache::tags(Object3d::CACHE_TAG)->rememberForever($key, function () use($vendor, $filter_by, $name, $brand, $build, $start, $category, $limit, $page, $type, $status, $published, $user) {
            $query = $this->model;
            if($brand){
                $array_brand = explode(',', strtolower($brand));
                $query = $query->whereIn('brand', $array_brand);
            }
            if($category || $category == -1){
                if($category == -1){
                    $query = $query->where(function ($q) {
                        $q->whereNull('category')
                              ->orWhere('category', '');
                    });
                }else{
                    $categoryList = [];
                    $categoryInstance = $this->categoryRepository->find($category);
                    
                    if($categoryInstance){
                        $cat = $categoryInstance->load('sub_categories');
                        $categoryList = $cat->sub_categories->pluck('code')->toArray();                                        
                    }
                    array_push($categoryList, $category);
                    
                    $query = $query->whereIn('category', $categoryList);
                }
                
            }
            if($vendor){
                $query = $query->where('vendor', $vendor);
            }
            if($name){
                //query = $query->where('name', 'like', '%'. $name . '%');
                $query = $query->where(function ($q) use ($name) {
                    $q->where('displayName', 'like', '%'. $name . '%')
                          ->orWhere('name', 'like', '%'. $name . '%');
                });
            }
            if($start){
                if($type == 'tts'){
                    $query = $query->where('name', 'regexp', "/^[a-z0-9]+_[a-z0-9]+_$start./");
                }
                else {
                    $query = $query->where('name', 'regexp', "/^[a-z0-9]+_$start./");
                }
            }


            if($type == 'item'){
               $query = $query->withoutTTS();
            }
            elseif($type == 'tts'){
                $query = $query->withTTS();
            }
            

            if($published){
                $query = $query->published();
            }

            if($build !== null){
                if($build){
                    $query = $query->whereNotNull('assetBundle');
                }
                else{
                    $query = $query->whereNull('assetBundle')->whereNotNull('assetZip');
                }
            }

            if($filter_by == 'brand'){
                $query = $query->filterByBrand();
            }else if ($filter_by == 'designer'){
                $query = $query->filterByDesigner();
            }

            if($limit){
                return $query->latest()->orderBy('_id', 'DESC')->paginate($limit);
            }
            else {
                return $query->latest()->orderBy('_id', 'DESC')->get();
            }

        });
        return $collections;
    }


    public function getAllByUser($params = [])
    {           
        
        $type = $params['type'] ?? null;
        $vendor = $params['vendor'] ?? null;
        $brand = $params['brand'] ?? null;
        $category = strtolower($params['category'] ?? null);
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ;     
        $name = strtolower($params['name'] ?? null);
        
        $published = $params['published'] ?? 0;
        $status = $params['status'] ?? null;
        $user =  Auth::user()->auth_id;
        $concat = concat(":",  $vendor, $brand, $name, $category, $limit, $page, $status, $published, "&user=$user");

        if($type == 'item'){
            $key = "editor-user-indexItem:";
        }elseif($type == 'tts'){
            $key = "editor-user-indexTTS:";
        }else{
            $key = "editor-user-index:";
        }
        $key = $key . $concat;
        
        
        $collections = Cache::tags(Object3d::CACHE_TAG)->rememberForever($key, function () use($vendor, $name, $brand, $category, $limit, $page, $type, $status, $published, $user) {
            $query = $this->model;
            if($brand){
                $array_brand = explode(',', strtolower($brand));
                $query = $query->whereIn('brand', $array_brand);
            }
            if($category || $category == -1){
                if($category == -1){
                    $query = $query->where(function ($q) {
                        $q->whereNull('category')
                              ->orWhere('category', '');
                    });
                }else{
                    $categoryList = [];
                    $categoryInstance = $this->categoryRepository->find($category);
                    
                    if($categoryInstance){
                        $cat = $categoryInstance->load('sub_categories');
                        $categoryList = $cat->sub_categories->pluck('code')->toArray();                                        
                    }
                    array_push($categoryList, $category);
                    
                    $query = $query->whereIn('category', $categoryList);
                }
                
            }
            if($vendor){
                $query = $query->where('vendor', $vendor);
            }
            if($name){
                //query = $query->where('name', 'like', '%'. $name . '%');
                $query = $query->where(function ($q) use ($name) {
                    $q->where('displayName', 'like', '%'. $name . '%')
                          ->orWhere('name', 'like', '%'. $name . '%');
                });
            }
            

            if($type == 'item'){
               $query = $query->withoutTTS();
            }
            elseif($type == 'tts'){
                $query = $query->withTTS();
            }
            

            if($published){
                $query = $query->published();
            }

            
            $query = $query->where('metadata.user.id', $user);

            if($limit){
                return $query->latest()->orderBy('_id', 'DESC')->paginate($limit);
            }
            else {
                return $query->latest()->orderBy('_id', 'DESC')->get();
            }

        });
        return $collections;
    }


    public function getRelatedByKey($object_key, $params = []){

        $limit =  (int) ($params['limit'] ?? 0);
        $cache_key = concat(":", 'editor-object-related', $object_key, $limit);
        $objects = Cache::tags(Object3d::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($object_key, $limit, $params) {
            return $this->searchService->getRelatedObjectByKey($object_key, $params);
        });
        return $objects;
    }

    public function getByKeys($keys = []){
        $key = implode(':', $keys);
        $cache_key = "editor-list:$key";
        $objects = Cache::tags(Object3d::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key, $keys) {
            return $this->model->whereIn('name', $keys)->get();
        });
        return $objects;
    }

    public function find($key){
        $cache_key = "editor-show:$key";
        $object = Cache::tags(Object3d::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('name', $key)->first();
        });
        return $object;
    }

    public function create($input)
	{
        $input['ownerId'] = Auth::user()->auth_id;
        if(empty( $input['status'])){
            $input['status'] = Object3d::PENDING;
        }
		$object = $this->model->create($input);
        
        return $object;
    }

    public function update($key, $input){
        $object = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        if(!$object){
            return false;
        }

        checkEditorOwner($object);

        if(isset($input['attributes']) && is_array($input['attributes'])){
            $current_attributes = $object->getOriginal('attributes') ?? [];
            $new_attributes = array_merge($current_attributes, $input['attributes']);
            $input['attributes'] = $new_attributes;
        }
        $object->update($input);
        return $object;
    }

    public function delete($key){
        $object = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        if(!$object){
            return false;
        }
        checkEditorOwner($object);
        $object->delete();
        return $object;
    }
}