<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\ColorBrand;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class ColorBrandRepository extends EloquentRepository implements ColorBrandRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());

    }

	public function model()
	{
		return ColorBrand::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $published = $params['published'] ?? 0;
        $concat = concat(":",  $name, $published, $limit, $page);

        $key = "editor-index:".$concat;
        $collections = Cache::tags(ColorBrand::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $published, $limit, $page) {
            $query = $this->model;
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }

            if($published){
                $query = $query->published();
            }

            if($limit){
                return $query->latest()->orderBy('_id', 'DESC')->paginate($limit);
            }
            else {
                return $query->latest()->orderBy('_id', 'DESC')->get();
            }
            
        });
        return $collections;
    }
    
	

    public function find($key){
        $cache_key = "editor-show:$key";
        $color = Cache::tags(ColorBrand::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('code', $key)->first();
        });
        
        return $color;   
    }

    public function create($input)
	{
		$colorBrand = $this->model->create($input);
        
        return $colorBrand;
    }

    public function update($key, $input){
        $colorBrand = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$colorBrand){
            return false;
        }
        $colorBrand->update($input);
        return $colorBrand;
    }

    public function delete($key){
        $colorBrand = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        
        if(!$colorBrand){
            return false;
        }
        $colorBrand->delete();
        return $colorBrand;
    }
}