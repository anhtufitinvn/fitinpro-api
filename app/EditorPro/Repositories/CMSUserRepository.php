<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\CMSUser;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class CMSUserRepository extends EloquentRepository implements UserRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return CMSUser::class;
    }
    
    public function getAll($params = [])
    {
        $cache_key = "index:";
        $page = (int) (request()->get('page'));
        $limit = (int) (request()->get('limit', 0));
        $concat = concat(":", $page, $limit);
        $cache_key = $cache_key . $concat;
        $collections = Cache::tags(CMSUser::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($page, $limit)  {
            $query = $this->model;
            
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
        });
        return $collections;
    }

    public function create($input)
	{
        $input['auth_id'] = 'cms';
		$user = $this->model->create($input);
        return $user;
    }

    public function find($key){
        $cache_key = "show:$key";
        $user = Cache::tags(CMSUser::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('auth_id', $key)->orWhere('email', $key)->first();
        });
        
        return $user;   
    }


    public function update($key, $input){
        $user = $this->model->where('auth_id', $key)->orWhere('email', $key)->first();
        if(!$user){
            return false;
        }
        $user->update($input);
        return $user;
    }

    
    

    public function delete($key){
        $user = $this->model->where('auth_id', $key)->orWhere('email', $key)->first();
        
        if(!$user){
            return false;
        }
        $user->delete();
        return $user;
    }

}