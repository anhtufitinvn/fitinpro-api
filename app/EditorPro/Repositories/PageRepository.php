<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Page;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class PageRepository extends EloquentRepository implements PageRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Page::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
      
        $concat = concat(":",  $name, $limit, $page);

        $key = "editor-index:$concat";
        $collections = Cache::tags(Page::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $limit, $page){
            $query = $this->model;
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }

            
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
           
        });
        return $collections;
    }

    
	public function create($input)
	{
       
		$page = $this->model->create($input);
        
        return $page;
    }
    
    public function find($key){
        $cache_key = "editor-show:$key";
        $page = Cache::tags(Page::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('code', $key)->first();
        });
        
        return $page;
    }


    public function update($key, $input){
        $page = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$page){
            return false;
        }
        $page->update($input);
        return $page;
    }

    public function delete($key){
        $page = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$page){
            return false;
        }

        $page->delete();
        return $page;
    }
}