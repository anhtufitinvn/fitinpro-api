<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Style;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class StyleRepository extends EloquentRepository implements StyleRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    protected $upload_service;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Style::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 40);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $published = $params['published'] ?? 0;
        $concat = concat(":",  $name, $published, $limit, $page);

        $key = "editor-index:$concat";
        $collections = Cache::tags(Style::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $published, $limit, $page){
            $query = $this->model;
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }

            if($published){
                $query = $query->published();
            }
            return $query->paginate($limit);
           
        });
        return $collections;
    }

    
	public function create($input)
	{
        
    }
    
    public function find($key){
        $cache_key = "editor-show:$key";
        $project = Cache::tags(Style::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('code', $key)->first();
        });
        
        return $project;
    }


    public function update($key, $input){
        
    }

    public function delete($key){
        
    }
}