<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Library;
use App\EditorPro\Models\Object3d;
use App\EditorPro\Models\Layout;
use Illuminate\Support\Facades\Cache;

class LibraryRepository extends EloquentRepository implements LibraryRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Library::class;
	}
    
    public function confirm($id)
    {
        $lib = $this->model->find($id);
        if($lib){
            $lib->update(['downloadStatus' => Library::SUCCESS]);
            Cache::tags(Library::CACHE_TAG)->flush();
        }
        return true;
    }

    public function getAll($params = [])
    {
        $key = "index";
        $collections = Cache::tags(Library::CACHE_TAG)->remember($key, self::CACHE_TIME, function () {
            return $this->model->getList();
        });
        return $collections;
    }
    
    public function getByUser($user)
    {
        $userId = $user->auth_id;
        $key = "show:$userId";
        
        $collections = Cache::tags(Library::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use ($userId) {
            return $this->model->getListByUser($userId);
        });
        return $collections;
    }
    
    public function getObjects($user, $params = [])
    {
        $type = $params['type'] ?? null;
        $vendor = $params['vendor'] ?? null;
        $brand = $params['brand'] ?? null;
        $category = $params['category'] ?? null;
        $limit =  (int) $params['limit'] ?? 40;;
        $page =  (int) $params['page'] ?? 1;;    
        $name = $params['name'] ?? null;;
        $userId = $user->auth_id;
        $concat = concat(":", $userId, $type, $vendor, $brand, $name, $category, $limit, $page);

        $key = "editor-indexObject:";

        $key = $key . $concat;
        

        $collections = Cache::tags(Library::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($userId, $vendor, $name, $brand, $category, $limit, $page, $type) {
            $library_array = $this->model->where('userId', $userId)->where('downloadStatus', Library::SUCCESS)->where('model', Object3d::class)->get()->pluck('key')->toArray();
           
            $query = new Object3d();
            $query = $query->whereIn('name', $library_array);
            if($brand){
                $query = $query->where('brand', $brand);
            }
            if($category){
                $query = $query->where('category', $category);
            }
            if($vendor){
                $query = $query->where('vendor', $vendor);
            }
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }
           
            if($type == 'item'){
               $query = $query->withoutTTS();
            }
            elseif($type == 'tts'){
                $query = $query->withTTS();
            }

            return $query->paginate($limit);
        });
        return $collections;
    }
    
	public function deleteByUser($user)
    {
        $this->model->where('userId', $user->auth_id)->delete();
        Cache::tags(Library::CACHE_TAG)->flush();
        return true;
    }
}