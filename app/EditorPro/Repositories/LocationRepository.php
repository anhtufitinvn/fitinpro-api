<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Location;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class LocationRepository extends EloquentRepository implements LocationRepositoryInterface {
    
    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Location::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $concat = concat(":", $limit, $page);
        $key = "editor-index:$concat";
        $collections = Cache::tags(Location::CACHE_TAG)->rememberForever($key, function () use($limit, $page){
            $query = $this->model;
           
            if($limit){
                return $query->paginate($limit);
            }
            else {
                return $query->get();
            }
           
        });
        return $collections;
    }

    public function getCity($id){
        $cache_key = "editor-show-city:$id";
        $instance = Cache::tags(Location::CACHE_TAG)->rememberForever($cache_key, function () use($id) {
            return $this->model->where('city_id', (int) $id)->first();
        });
        
        return $instance;
    }

    public function getDistrict($id){
        $cache_key = "editor-show-distrtict:$id";
        $instance = Cache::tags(Location::CACHE_TAG)->rememberForever($cache_key, function () use($id) {
            $projectArray = [

                'districts' => [
                    '$elemMatch' => ['district_id' => (int) $id]
                ],
            ];
            
            $city = $this->model->where('districts.district_id', (int) $id)->first();
            if($city){
                return $this->model->where('city_id', $city->city_id)->project($projectArray)->first();
            }else{
                return null;
            }
            
        });
        
        return $instance;
    }

    // public function getWard($id){
    //     $cache_key = "editor-show-ward:$id";
    //     $instance = Cache::tags(Location::CACHE_TAG)->rememberForever($cache_key, function () use($id) {
    //         return $this->model->where('districts.wards.ward_id', (int) $id)->first();
    //     });
        
    //     return $instance;
    // }
}