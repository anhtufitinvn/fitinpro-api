<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Role;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class RoleRepository extends EloquentRepository implements UserRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Role::class;
    }
    
    public function getAll($params = [])
    {
        $cache_key = "index:";
        $page = (int) (request()->get('page'));
        $limit = (int) (request()->get('limit', 0));
        $concat = concat(":", $page, $limit);
        $cache_key = $cache_key . $concat;
        $collections = Cache::tags(Role::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($page, $limit)  {
            $query = $this->model;
            
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
        });
        return $collections;
    }

    

    public function find($key){
        $cache_key = "show:$key";
        $key = (int) $key;
        $role = Cache::tags(Role::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('name', $key)->orWhere('_id', $key)->first();
        });
        
        return $role;   
    }


    public function update($key, $input){
        $role = $this->model->where('name', $key)->orWhere('_id', $key)->first();
        if(!$role){
            return false;
        }
        $role->update($input);
        return $role;
    }

    
    

    public function delete($key){
        $role = $this->model->where('name', $key)->orWhere('_id', $key)->first();
        
        if(!$role){
            return false;
        }
        $role->delete();
        return $role;
    }

}