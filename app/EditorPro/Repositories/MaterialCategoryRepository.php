<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\MaterialCategory;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class MaterialCategoryRepository extends EloquentRepository implements MaterialCategoryRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return MaterialCategory::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
    
        $concat = concat(":",  $name, $limit, $page);

        $key = "editor-index:$concat";
        $collections = Cache::tags(MaterialCategory::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $limit, $page){
            $query = $this->model;
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }
            
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
           
        });
        return $collections;
    }

    
	public function create($input)
	{
       
		$materialCategory = $this->model->create($input);
        
        return $materialCategory;
    }
    
    public function find($key){
        $cache_key = "editor-show:$key";
        $materialCategory = Cache::tags(MaterialCategory::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('code', $key)->first();
        });
        
        return $materialCategory;
    }


    public function update($key, $input){
        $materialCategory = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$materialCategory){
            return false;
        }
        $materialCategory->update($input);
        return $materialCategory;
    }

    public function delete($key){
        $materialCategory = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$materialCategory){
            return false;
        }

        $materialCategory->delete();
        return $materialCategory;
    }
}