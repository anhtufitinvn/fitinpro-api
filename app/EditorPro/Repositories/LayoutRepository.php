<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;

use App\EditorPro\Models\Layout;
use Illuminate\Support\Facades\Cache;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;

class LayoutRepository extends EloquentRepository implements LayoutRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    protected $upload_service;

    public function __construct(UploadService $upload_service)
    {
        $this->model = app($this->model());
        $this->upload_service = $upload_service;
    }

	public function model()
	{
		return Layout::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $project = $params['project'] ?? NULL;
        $without_json = $params['without_json'] ?? NULL;
        $orderBy = $params['orderBy'] ?? null;  
        $ascending =  $params['ascending'] ?? null;
        $time = $params['t'] ?? 0;
        if(!empty($params['is-template']) && ($params['is-template'] || $params['is-template'] === false)){
            $isTemplate = $params['is-template'];
        }
        else{
            $isTemplate = NULL;
        }
        $room = $params['room'] ?? 0;
        $concat = concat(":",  $name, $project, $room, $without_json, $isTemplate, $time, $limit, $page, $orderBy, $ascending);

        $key = "editor-index:$concat";
        $collections = Cache::tags(Layout::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $time, $project, $room, $without_json, $isTemplate, $limit, $page, $orderBy, $ascending) {
            $query = $this->model;
            if($name){
                //$query = $query->where('name', 'like', '%'. $name . '%');
                $query = $query->where(function ($q) use ($name) {
                    $q->where('displayName', 'like', '%'. $name . '%')
                          ->orWhere('name', 'like', '%'. $name . '%');
                });
            }

            if($project){
                $query = $query->where('project', strtolower($project));
            }
            
            if($isTemplate){
                $query = $query->where('isTemplate', true);
            }else if ($isTemplate === false){
                $query = $query->where('isTemplate', false);
            }
            
            if($without_json){
                $query = $query->exclude(['dataJson']);
            }

            if($room){
                $query = $query->where('meta.bed_room_num', (int) $room);
            }

            if($time){
                $timestamp = \Carbon\Carbon::createFromTimestamp($time);
                
                $query = $query->where('updated_at', '>=', $timestamp);
            }

            if($orderBy){
                $query = $query->orderBy($orderBy, $ascending ? "ASC" : "DESC");
            }
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
        });
        
        return $collections;
    }

    public function getAllUser($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $project = $params['project'] ?? NULL;
        $room = $params['room'] ?? 0;
        $user = Auth::user();
        $concat = concat(":",  $name, $project,  $room, $user->auth_id, $limit, $page);
        
        $key = "editor-index-user:$concat";
        $collections = Cache::tags(Layout::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $user,  $room, $project, $limit, $page) {
            $query = $this->model;
            if($name){
                //$query = $query->where('name', 'like', '%'. $name . '%');
                $query = $query->where(function ($q) use ($name) {
                    $q->where('displayName', 'like', '%'. $name . '%')
                          ->orWhere('name', 'like', '%'. $name . '%');
                });
            }

            if($project){
                $query = $query->where('project', strtolower($project));
            }
            
            $query = $query->where('ownerId', $user->auth_id);

            if($room){
                $query = $query->where('meta.bed_room_num', (int) $room);
            }

            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
            
        });
        
        return $collections;
    }

    public function getAllTemplate($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $project = $params['project'] ?? NULL;
        $room = $params['room'] ?? 0;
        $concat = concat(":",  $name, $project,  $room, $limit, $page);

        $key = "editor-index-template:$concat";
        $collections = Cache::tags(Layout::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $project, $room, $limit, $page) {
            $query = $this->model;
            if($name){
                //$query = $query->where('name', 'like', '%'. $name . '%');
                $query = $query->where(function ($q) use ($name) {
                    $q->where('displayName', 'like', '%'. $name . '%')
                          ->orWhere('name', 'like', '%'. $name . '%');
                });
            }
            if($project){
                $query = $query->where('project', strtolower($project));
            }
            $query = $query->where('isTemplate', true);

            if($room){
                $query = $query->where('meta.bed_room_num', (int) $room);
            }
            
            $query = $query->published();
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
        });
        return $collections;
    }
    
	public function create($input)
	{
		$layout = $this->model->create($input);
        
        if(!empty($input['gallery']) && is_array($input['gallery'])){
            foreach ($input['gallery'] as $img){
                $layout->gallery()->create($img);
            }
            
        }
        return $layout;
    }

    public function duplicate($key)
	{
        $user = Auth::user();
        $parent = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        if(!$parent){
            return false;
        }
        if(!$parent->dataJson || empty($parent->dataJson['RoomId'])){
            return false;
        }
        $parentArray = $parent->toArray();
        $time = \Carbon\Carbon::now()->timestamp;

        $parentArray['name'] = $parentArray['dataJson']['RoomId'] . "_" . $time;
        unset($parentArray['_id']);
        
        $newLayout = new Layout();
        $newLayout->fill($parentArray);
        // $editorPro = $parentArray['editorPro'] ?? NULL;
        // if($editorPro && empty($editorPro['revision'])){
        //     $editorPro['revision'] = \Carbon\Carbon::now()->timestamp;
        // }
        // $newLayout->editorPro = $editorPro;
        $newLayout->parent = $parent->name;
        $newLayout->save();
        
        $newLayout->update([
            'ownerId' => $user->auth_id,
            'isTemplate' => false,
            'status' => Layout::PRIVATE
        ]);

        return $newLayout;
    }
    
    public function duplicateLayoutUser($key){
        $layout = $this->find($key);
        if(!$layout){
            return false;
        }
        $newLayout = $layout->replicate();
        $newLayout->save();
        return $newLayout;
    }

    public function find($key){
        $cache_key = "show:$key";
        $layout = Cache::tags(Layout::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('name', $key)->first();
        });
        
        return $layout;   
    }


    public function update($key, $input){
        $layout = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        if(!$layout){
            return false;
        }

        checkEditorOwner($layout);
        $layout->update($input);
        return $layout;
    }

    public function delete($key){
        $layout = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        
        if(!$layout){
            return false;
        }
        checkEditorOwner($layout);
        $layout->delete();
        return $layout;
    }

    public function deleteGallery($key, $gallery_id){
        $layout = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        
        if(!$layout){
            return false;
        }
        $layout->gallery()->destroy($gallery_id);
        return $layout;
    }

    public function requestApprove($key){
        $layout = $this->model->where('_id', $key)->orWhere('name', $key)->first();
        if(!$layout){
            return false;
        }

        $input['status'] = Layout::WAITING;

        $layout->update($input);
        return $layout;
    }
}