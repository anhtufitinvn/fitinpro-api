<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Color;
use App\EditorPro\Models\ColorGroup;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use App\Services\UploadService;

class ColorRepository extends EloquentRepository implements ColorRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Color::class;
	}
    
    public function getAll($params = [])
    {
        
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $group = $params['group'] ?? NULL;
        $brand = $params['brand'] ?? NULL;
        $orderBy = $params['orderBy'] ?? null;  
        $ascending =  $params['ascending'] ?? null;
        $concat = concat(":",  $name, $group, $brand, $limit, $page, $orderBy, $ascending);

        $key = "editor-index:".$concat;
        
        $collections = Cache::tags(Color::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use ($name, $group, $brand, $limit, $page,  $orderBy, $ascending) {
            //Jensseger MongoDB has, whereHas not working as expected. So use 2 queries below.
            $query = $this->model;
            
            if($group){
                $arrayGroup = explode(",", $group);
                $query = $query->whereIn('group', $arrayGroup);
            }
            if($brand){
                
                $array_brand = explode(',', strtolower($brand));
                $colorGroups = ColorGroup::whereIn('brand', $array_brand)->get()->pluck('code');
                $query = $query->whereIn('group', $colorGroups);
            }

            if($name){
                $query = $query->where('name', 'like', '%'. $name. '%');
            }
            
            $query = $query->orderBy('usedCount', 'DESC');

            if($orderBy){
                $query = $query->orderBy($orderBy, $ascending ? "ASC" : "DESC");
            }

            if($limit){
                return $query->latest()->orderBy('_id', 'DESC')->paginate($limit);
            }
            else {
                return $query->latest()->orderBy('_id', 'DESC')->get();
            }
            
        });

        return $collections;
    }

    
    public function find($key){
        $cache_key = "show:$key";
        $color = Cache::tags(Color::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('code', $key)->first();
        });
        
        return $color;   
    }

    
    public function updateCount($key){
        $color = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$color){
            return false;
        }
        $count = $color->usedCount ?? 0;
        $color->usedCount = $count + 1;
        $color->save();
        return $color;
    }


    public function getMostUsedList(){
        $limit = (int) request()->get('limit', 10);
        $colors = $this->model->orderBy('usedCount', 'DESC')->take($limit)->get();
        return $colors;
    }

    public function create($input)
	{
		$group = $this->model->create($input);
        
        return $group;
    }

    public function update($key, $input){
        $group = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$group){
            return false;
        }
        $group->update($input);
       
        return $group;
    }

    public function delete($key){
        $color = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        
        if(!$color){
            return false;
        }
        $color->delete();
        return $color;
    }
}