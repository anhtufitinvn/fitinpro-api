<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Bundle;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class BundleRepository extends EloquentRepository implements BundleRepositoryInterface {
    
    const CACHE_TIME = 3660 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Bundle::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $orderBy = $params['orderBy'] ?? null;  
        $ascending =  $params['ascending'] ?? null;
        $concat = concat(":",  $name, $limit, $page, $orderBy, $ascending);

        $key = "editor-index:$concat";
        $collections = Cache::tags(Bundle::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $limit, $page, $orderBy, $ascending) {
            $query = $this->model;
            if($name){
                //$query = $query->where('displayName', 'like', '%'. $name . '%');
                $query = $query->where(function ($q) use ($name) {
                    $q->where('displayName', 'like', '%'. $name . '%')
                          ->orWhere('keyname', 'like', '%'. $name . '%');
                });
            }
            
            if($orderBy){
                $query = $query->orderBy($orderBy, $ascending ? "ASC" : "DESC");
            }
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }

            
        });
        return $collections;
    }
    
    public function getAllTemplate($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $orderBy = $params['orderBy'] ?? null;  
        $ascending =  $params['ascending'] ?? null;
        $concat = concat(":",  $name, $limit, $page, $orderBy, $ascending);

        $key = "editor-index-template:$concat";
        $collections = Cache::tags(Bundle::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $limit, $page, $orderBy, $ascending) {
            $query = $this->model;
            if($name){
                
                $query = $query->where(function ($q) use ($name) {
                    $q->where('displayName', 'like', '%'. $name . '%')
                          ->orWhere('keyname', 'like', '%'. $name . '%');
                });
            }
            $query = $query->published();
            $query = $query->where('isTemplate', true);

            if($orderBy){
                $query = $query->orderBy($orderBy, $ascending ? "ASC" : "DESC");
            }
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
        });
        return $collections;
    }

    public function getAllUser($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 40);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $orderBy = $params['orderBy'] ?? null;  
        $ascending =  $params['ascending'] ?? null;
        $user = Auth::user();
        $concat = concat(":",  $name, $user->auth_id, $limit, $page, $orderBy, $ascending);

        $key = "editor-index-user:$concat";
        $collections = Cache::tags(Bundle::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $user, $limit, $page, $orderBy, $ascending) {
            $query = $this->model;
            if($name){
                $query = $query->where(function ($q) use ($name) {
                    $q->where('displayName', 'like', '%'. $name . '%')
                          ->orWhere('keyname', 'like', '%'. $name . '%');
                });
            }

            
            $query = $query->where('ownerId', $user->auth_id);
            if($orderBy){
                $query = $query->orderBy($orderBy, $ascending ? "ASC" : "DESC");
            }
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
        });
        
        return $collections;
    }

	public function create($input)
	{
        
        if(empty( $input['status'])){
            $input['status'] = Bundle::PRIVATE;
        }
		$bundle = $this->model->create($input);
        
        return $bundle;
    }
    
    public function duplicate($key)
	{
        $user = Auth::user();
        $parent = Bundle::where('_id', $key)->orWhere('keyname', $key)->first();
        if(!$parent){
            return false;
        }
        if(!$parent->dataJson){
            return false;
        }
        $parentArray = $parent->toArray();
        $time = \Carbon\Carbon::now()->timestamp;

        $parentArray['keyname'] =  slug($parentArray['displayName'], '_'). '_' . $time;
        unset($parentArray['_id']);
        $newBundle = new Bundle();
        $newBundle->fill($parentArray);
        
        $newBundle->save();
        
        $newBundle->update([
            'parent' => $parent->keyname,
            'ownerId' => $user->auth_id,
            'isTemplate' => false,
            'status' => Bundle::PRIVATE
        ]);

        return $newBundle;
    }

    public function find($key){
        $cache_key = "editor-show:$key";
        $bundle = Cache::tags(Bundle::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('keyname', $key)->first();
        });
        
        return $bundle;
    }


    public function update($key, $input){
        $bundle = $this->model->where('_id', $key)->orWhere('keyname', $key)->first();
        if(!$bundle){
            return false;
        }

        if(Auth::guard('editor')->check()){
            $input['status'] = Bundle::PRIVATE;
        }

        $bundle->update($input);
        return $bundle;
    }

    public function requestApprove($key){
        $bundle = $this->model->where('_id', $key)->orWhere('keyname', $key)->first();
        if(!$bundle){
            return false;
        }

        $input['status'] = Bundle::WAITING;

        $bundle->update($input);
        return $bundle;
    }

    public function delete($key){
        $bundle = $this->model->where('_id', $key)->orWhere('keyname', $key)->first();
        if(!$bundle){
            return false;
        }

        $bundle->delete();
        return $bundle;
    }
}