<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Brand;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class BrandRepository extends EloquentRepository implements BrandRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Brand::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $published = $params['published'] ?? 0;
        $concat = concat(":",  $name, $published, $limit, $page);

        $key = "editor-index:$concat";
        $collections = Cache::tags(Brand::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $published, $limit, $page){
            $query = $this->model;
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }

            if($published){
                $query = $query->published();
            }
            
            if($limit){
                return $query->paginate($limit);
            }
            else {
                return $query->get();
            }
           
        });
        return $collections;
    }

    
	public function create($input)
	{
        if(empty( $input['status'])){
            $input['status'] = Brand::PENDING;
        }
		$brand = $this->model->create($input);
        
        return $brand;
    }
    
    public function find($key){
        $cache_key = "editor-show:$key";
        $brand = Cache::tags(Brand::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('code', $key)->first();
        });
        
        return $brand;
    }


    public function update($key, $input){
        $brand = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$brand){
            return false;
        }
        $brand->update($input);
        return $brand;
    }

    public function delete($key){
        $brand = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$brand){
            return false;
        }

        $brand->delete();
        return $brand;
    }
}