<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Permission;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class PermissionRepository extends EloquentRepository implements UserRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Permission::class;
    }
    
    public function getAll($params = [])
    {
        $cache_key = "index:";
        $page = (int) (request()->get('page'));
        $limit = (int) (request()->get('limit', 0));
        $concat = concat(":", $page, $limit);
        $cache_key = $cache_key . $concat;
        $collections = Cache::tags(Permission::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($page, $limit)  {
            $query = $this->model;
            
            if($limit){
                return $query->orderBy('group', 'ASC')->paginate($limit);
            }
            else {
                return $query->orderBy('group', 'ASC')->get();
            }
        });
        return $collections;
    }

    

    public function find($key){
        $cache_key = "show:$key";
        $key = (int) $key;
        $permission = Cache::tags(Permission::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('name', $key)->orWhere('_id', $key)->first();
        });
        
        return $permission;   
    }


    public function update($key, $input){
        $permission = $this->model->where('name', $key)->orWhere('_id', $key)->first();
        if(!$permission){
            return false;
        }
        $permission->update($input);
        return $permission;
    }

    
    

    public function delete($key){
        $permission = $this->model->where('name', $key)->orWhere('_id', $key)->first();
        
        if(!$permission){
            return false;
        }
        $permission->delete();
        return $permission;
    }

}