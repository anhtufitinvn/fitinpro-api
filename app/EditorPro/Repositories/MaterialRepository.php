<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Material;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class MaterialRepository extends EloquentRepository implements MaterialRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;


    public function __construct()
    {
        $this->model = app($this->model());

    }

	public function model()
	{
		return Material::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $brand = $params['brand'] ?? NULL;
        $category = $params['category'] ?? NULL;
        $target = $params['target'] ?? NULL;
        $published = $params['published'] ?? NULL;
        $filter_by = $params['filter-by'] ?? null;
        $keys = $params['keys'] ?? NULL;
        $concat = concat(":",  $name, $brand, $category, $target, $published, md5($keys), $filter_by, $limit, $page);

        $key = "editor-index:$concat";
        $collections = Cache::tags(Material::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name,  $brand, $category, $target, $published, $keys, $filter_by, $limit, $page){
            $query = $this->model;
            if($name){
               // $query = $query->where('name', 'like', '%'. $name . '%');
                $query = $query->where(function ($q) use ($name) {
                    $q->where('code', 'like', '%'. $name . '%')
                          ->orWhere('name', 'like', '%'. $name . '%');
                });
            }
            if($brand){
                $array_brand = explode(',', strtolower($brand));
                $query = $query->whereIn('brand', $array_brand);
            }
            if($category){
                $query = $query->where('category', strtolower($category));
            }
            if($target){
                $attr = ucfirst(strtolower($target));
                $query = $query->where("attrs.is$attr", true);
            }

            if($keys){
                if (strpos($keys, ',') !== false) {
                    $array_keys = explode(',', strtolower($keys));
                }else if(strpos($keys, ' ') !== false) {
                    $keys = preg_replace('!\s+!', ' ', $keys);
                    $array_keys = explode(' ', strtolower($keys));
                }else{
                    $array_keys = [$keys];
                }
                
                $query = $query->whereIn('code', $array_keys);
            }
            
            if($published){
                $query = $query->published();
            }

            if ($filter_by == 'designer'){
                $query = $query->filterByDesigner();
            }

            if($limit){
                return $query->latest()->orderBy('_id', 'DESC')->paginate($limit);
            }
            else {
                return $query->latest()->orderBy('_id', 'DESC')->get();
            }
            
           
        });
        return $collections;
    }

    
	public function create($input)
	{
        if(!empty($input['category'])){
            $input['category'] = strtolower($input['category']);
        }
        if(!empty($input['brand'])){
            $input['brand'] = strtolower($input['brand']);
        }
		$material = $this->model->create($input);
        
        return $material;
    }
    
    public function find($key){
        $cache_key = "editor-show:$key";
        $material = Cache::tags(Material::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('code', $key)->first();
        });
        
        return $material;
    }


    public function update($key, $input){
        if(!empty($input['category'])){
            $input['category'] = strtolower($input['category']);
        }
        if(!empty($input['brand'])){
            $input['brand'] = strtolower($input['brand']);
        }
        $material = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$material){
            return false;
        }
        checkEditorOwner($material);
        $material->update($input);
        return $material;
    }

    public function delete($key){
        $material = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        
        if(!$material){
            return false;
        }
        checkEditorOwner($material);
        $material->delete();
        return $material;
    }
}