<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Category;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class CategoryRepository extends EloquentRepository implements CategoryRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Category::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $published = $params['published'] ?? 0;
        $concat = concat(":",  $name, $published, $limit, $page);

        $key = "editor-index:$concat";
        $collections = Cache::tags(Category::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $published, $limit, $page){
            $query = $this->model;
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }

            if($published){
                $query = $query->published();
            }
            $query = $query->isParent();
            $query = $query->sort();
            if($limit){
                return $query->paginate($limit);
            }
            else {
                return $query->get();
            }
           
        });
        return $collections;
    }

    
	public function create($input)
	{
        if(empty( $input['status'])){
            $input['status'] = Category::PUBLISHED;
        }
		$category = $this->model->create($input);
        
        return $category;
    }
    
    public function find($key){
        $cache_key = "editor-show:$key";
        $category = Cache::tags(Category::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('code', $key)->first();
        });
        
        return $category;
    }


    public function update($key, $input){
        $category = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$category){
            return false;
        }
        $category->update($input);
        return $category;
    }

    public function delete($key){
        $category = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$category){
            return false;
        }

        $category->delete();
        return $category;
    }
}