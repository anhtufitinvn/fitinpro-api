<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\Notification;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class NotificationRepository extends EloquentRepository implements NotificationRepositoryInterface {
    
    //const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Notification::class;
	}
    
    public function getAll($params = [])
    {
        $user = Auth::user();
        $limit =  (int) ($params['limit'] ?? 40);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $query = $this->model->where('receiverId', $user->auth_id);
        return $query->latest()->paginate($limit);
    }

    public function getUnread()
    {
        $user = Auth::user();
        $data = $this->model->where('receiverId', $user->auth_id)->whereNull('read')->latest()->get();
       // $this->model->where('receiverId', $user->auth_id)->whereNull('read')->update(['read' => true]);
        return $data;
    }
    public function getUnreadCount()
    {
        $user = Auth::user();
        return $this->model->where('receiverId', $user->auth_id)->whereNull('read')->count();
    }
    
	public function create($input)
	{
        
    }
    
    public function find($key){
        return $this->model->find($key);
      
    }

    public function read($key){
        $noti = $this->model->find($key);
        if($noti){
            $noti->update(['read' => true]);
            
        }
        return $noti;
    }

    public function update($key, $input){
        
    }

    public function delete($key){
       
    }
    
}