<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\ColorGroup;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class ColorGroupRepository extends EloquentRepository implements ColorGroupRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());

    }

	public function model()
	{
		return ColorGroup::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $published = $params['published'] ?? 0;
        $brand = $params['brand'] ?? NULL;
        $concat = concat(":",  $name, $brand, $published, $limit, $page);

        $key = "editor-index:".$concat;
        $collections = Cache::tags(ColorGroup::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $brand, $published, $limit, $page) {
            $query = $this->model;
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }

            if($brand){
                $array_brand = explode(',', strtolower($brand));
                $query = $query->whereIn('brand', $array_brand);
            }

            if($published){
                $query = $query->published();
            }

            if($limit){
                return $query->latest()->orderBy('_id', 'DESC')->paginate($limit);
            }
            else {
                return $query->latest()->orderBy('_id', 'DESC')->get();
            }
            
        });
        return $collections;
    }
    
	

    public function find($key){
        $cache_key = "editor-show:$key";
        $color = Cache::tags(ColorGroup::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('_id', $key)->orWhere('code', $key)->first();
        });
        
        return $color;   
    }


    public function create($input)
	{
		$group = $this->model->create($input);
        
        return $group;
    }

    public function update($key, $input){
        $group = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$group){
            return false;
        }
        $group->update($input);
       
        return $group;
    }

    public function delete($key){
        $group = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        
        if(!$group){
            return false;
        }
        $group->delete();
        return $group;
    }

}