<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\User;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class UserRepository extends EloquentRepository implements UserRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return User::class;
    }
    
    public function getAll($params = [])
    {
        $cache_key = "index:";
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $orderBy = $params['orderBy'] ?? null;  
        $ascending =  $params['ascending'] ?? null;
        $concat = concat(":", $page, $limit, $name, $orderBy, $ascending);
        $cache_key = $cache_key . $concat;
        
        $collections = Cache::tags(User::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($name, $page, $limit, $orderBy, $ascending)  {
            $query = $this->model;

            if($name){
                $query = $query->where(function ($q) use ($name) {
                    $q->where('full_name', 'like', '%'. $name . '%')
                          ->orWhere('name', 'like', '%'. $name . '%')
                          ->orWhere('email', 'like', '%'. $name . '%');
                });
            }

            if($orderBy){
                $query = $query->orderBy($orderBy, $ascending ? "ASC" : "DESC");
            }
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
        });
        return $collections;
    }

    

    public function find($key){
        $cache_key = "show:$key";
        $key = (int) $key;
        $user = Cache::tags(User::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('auth_id', $key)->first();
        });
        
        return $user;   
    }


    public function update($key, $input){
        $key = (int) $key;
        $user = $this->model->where('auth_id', $key)->first();
        if(!$user){
            return false;
        }
        $user->update($input);
        return $user;
    }

    
    

    public function delete($key){
        $user = $this->model->where('auth_id', $key)->first();
        
        if(!$user){
            return false;
        }
        $user->delete();
        return $user;
    }

}