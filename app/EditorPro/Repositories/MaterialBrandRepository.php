<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;
use App\EditorPro\Models\MaterialBrand;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;

class MaterialBrandRepository extends EloquentRepository implements MaterialBrandRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return MaterialBrand::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
    
        $concat = concat(":",  $name, $limit, $page);

        $key = "editor-index:$concat";
        $collections = Cache::tags(MaterialBrand::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $limit, $page){
            $query = $this->model;
            if($name){
                $query = $query->where('name', 'like', '%'. $name . '%');
            }
            
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
           
        });
        return $collections;
    }

    
	public function create($input)
	{
       
		$materialBrand = $this->model->create($input);
        
        return $materialBrand;
    }
    
    public function find($key){
        $cache_key = "editor-show:$key";
        $materialBrand = Cache::tags(MaterialBrand::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('code', $key)->first();
        });
        
        return $materialBrand;
    }


    public function update($key, $input){
        $materialBrand = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$materialBrand){
            return false;
        }
        $materialBrand->update($input);
        return $materialBrand;
    }

    public function delete($key){
        $materialBrand = $this->model->where('_id', $key)->orWhere('code', $key)->first();
        if(!$materialBrand){
            return false;
        }

        $materialBrand->delete();
        return $materialBrand;
    }
}