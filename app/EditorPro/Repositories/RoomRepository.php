<?php
namespace App\EditorPro\Repositories;

use App\Repositories\EloquentRepository;

use App\EditorPro\Models\Room;
use Illuminate\Support\Facades\Cache;

use Illuminate\Support\Facades\Auth;

class RoomRepository extends EloquentRepository implements RoomRepositoryInterface {
    
    const CACHE_TIME = 3600 * 24;

    protected $upload_service;

    public function __construct()
    {
        $this->model = app($this->model());
    }

	public function model()
	{
		return Room::class;
	}
    
    public function getAll($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 0);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $category = $params['category'] ?? NULL;
        $orderBy = $params['orderBy'] ?? null;  
        $ascending =  $params['ascending'] ?? null;
        $concat = concat(":",  $name, $category, $limit, $page, $orderBy, $ascending);

        $key = "editor-index:$concat";
        $collections = Cache::tags(Room::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $category, $limit, $page, $orderBy, $ascending) {
            $query = $this->model;
            if($name){
                $query = $query->where(function ($q) use ($name) {
                    $q->where('displayName', 'like', '%'. $name . '%')
                          ->orWhere('keyname', 'like', '%'. $name . '%');
                });
            }

            if($category){
                $query = $query->where('category', strtolower($category));
            }
            
            if($orderBy){
                $query = $query->orderBy($orderBy, $ascending ? "ASC" : "DESC");
            }
            
            if($limit){
                return $query->latest()->paginate($limit);
            }
            else {
                return $query->latest()->get();
            }
        });
        
        return $collections;
    }

    public function getAllUser($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 40);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
        $category = $params['category'] ?? NULL;
        $concat = concat(":",  $name, $category, $limit, $page);

        $key = "editor-index-user:$concat";
        $collections = Cache::tags(Room::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $category, $limit, $page) {
            $query = $this->model;
            if($name){
                $query = $query->where(function ($q) use ($name) {
                    $q->where('displayName', 'like', '%'. $name . '%')
                          ->orWhere('keyname', 'like', '%'. $name . '%');
                });
            }

           
            $user = Auth::user();
            $query = $query->where('ownerId', $user->auth_id);
            if($category){
                $query = $query->where('category', strtolower($category));
            }
            return $query->latest()->paginate($limit);
        });
        
        return $collections;
    }

    public function getAllTemplate($params = [])
    {
        $limit =  (int) ($params['limit'] ?? 40);
        $page =  (int) ($params['page'] ?? 0)  ; 
        $name = $params['name'] ?? NULL;
      
        $category = $params['category'] ?? NULL;
        $concat = concat(":",  $name, $category, $limit, $page);

        $key = "editor-index-template:$concat";
        $collections = Cache::tags(Room::CACHE_TAG)->remember($key, self::CACHE_TIME, function () use($name, $category, $limit, $page) {
            $query = $this->model;
            if($name){
                $query = $query->where(function ($q) use ($name) {
                    $q->where('displayName', 'like', '%'. $name . '%')
                          ->orWhere('keyname', 'like', '%'. $name . '%');
                });
            }
            
            $query = $query->where('isTemplate', true);
            if($category){
                $query = $query->where('category', strtolower($category));
            }
            return $query->latest()->paginate($limit);
        });
        return $collections;
    }
    
	public function create($input)
	{
        if(empty( $input['status'])){
            $input['status'] = Room::PENDING;
        }
		$room = $this->model->create($input);
        
        return $room;
    }

    public function duplicate($key)
	{
        $user = Auth::user();
        $parent = $this->model->where('_id', $key)->orWhere('keyname', $key)->first();
        if(!$parent){
            return false;
        }
        if(!$parent->dataJson || empty($parent->dataJson['RoomId'])){
            return false;
        }
        $parentArray = $parent->toArray();
        $time = \Carbon\Carbon::now()->timestamp;

        $parentArray['keyname'] = $parentArray['dataJson']['RoomId'] . "_" . $time;
        unset($parentArray['_id']);
        
        $newRoom = new Room();
        $newRoom->fill($parentArray);
        $newRoom->parent = $parent->keyname;
        $newRoom->save();
        
        $newRoom->update([
            'ownerId' => $user->auth_id,
            'isTemplate' => false,
            'status' => Room::PRIVATE
        ]);

        return $newRoom;
    }
    
    public function duplicateRoomUser($key){
        $room = $this->find($key);
        if(!$room){
            return false;
        }
        $newRoom = $room->replicate();
        $newRoom->save();
        return $newRoom;
    }

    public function find($key){
        $cache_key = "show:$key";
        $room = Cache::tags(Room::CACHE_TAG)->remember($cache_key, self::CACHE_TIME, function () use($key) {
            return $this->model->where('keyname', $key)->first();
        });
        
        return $room;   
    }


    public function update($key, $input){
        $room = $this->model->where('keyname', $key)->first();
        if(!$room){
            return false;
        }
        $room->update($input);
        return $room;
    }

    public function requestApprove($key){
        $room = $this->model->where('keyname', $key)->first();
        if(!$room){
            return false;
        }

        $input['status'] = Room::WAITING;

        $room->update($input);
        return $room;
    }

    public function delete($key){
        $room = $this->model->where('keyname', $key)->first();
        
        if(!$room){
            return false;
        }
        $room->delete();
        return $room;
    }
}