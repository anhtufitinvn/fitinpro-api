<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\EditorPro\Traits\CacheTrait;

class ColorGroup extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'EditorColorGroups';

    protected $primaryKey = 'code';
    
    protected $fillable = [
        "name",
        "code",
        "brand",
        "hex",
        "image",
        "description",
        "status"
    ];

    const PENDING = 'pending';
    const PUBLISHED = 'published';

    public const CACHE_TAG = 'editor-color-group';

    public function scopePublished($query){
        return $query->where('status', self::PUBLISHED);
    }

    public function colors(){
        return $this->hasMany(Color::class, 'group', 'code');
    }

    public function _brand(){
        return $this->belongsTo(ColorBrand::class, 'brand', 'code');
    }

    public static function getEditorProCollection(){

        //Jens Hasmany not working with custom foreign key

        $colorGroups = ColorGroup::published()->get();
        foreach ($colorGroups as $group){
            $group->colors = Color::where('group', $group->code)->get();
        }
        
        return $colorGroups;
    }
}