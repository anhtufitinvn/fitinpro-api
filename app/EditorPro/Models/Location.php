<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Traits\CacheTrait;

class Location extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'Editor_RSLocations';
    protected $fillable = [
        "lat", 
        "long", 
        "city_id", 
        "city_name", 
        "districts"
    ];
    
   
    public const CACHE_TAG = 'editor-location';
    
}