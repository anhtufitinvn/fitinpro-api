<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use App\EditorPro\Traits\CacheTrait;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Helpers\Utils;
use Illuminate\Support\Facades\Auth;

class Object3d extends Model {

    use CacheTrait;

    protected $collection = 'EditorObjects';
    protected $primaryKey = 'name';

    public const CACHE_TAG = 'editor-objects';

    const PENDING = 'pending';
    const PUBLISHED = 'published';
    const PRIVATE = 'private';

    const MODEL_TYPE = "model";

    const DIR_NAME = 'objects';
    const ASSET_DIR = 'assets';
    const THUMB_DIR = 'thumb';
    const THUMB_EDITORPRO_DIR = 'thumb';
    const THUMB_MATERIAL_DIR = 'thumb/material';
    const FBX_DIR = 'fbx';
    const PREFAB_DIR = 'prefab';
    const IOS_DIR = 'ios';
    const ZIP_DIR = 'zip';
    const ANDROID_DIR = 'android';
    const GLTF_DIR = 'gltf';
    const TEXTURE_DIR = 'textures';

    const UNITY_DRIVER = "unity";
    const UNITY_WEB_DIR = "webgl-assets";
    const THUMB_DEFAULT_MIME = "jpg";
    const THUMB_SIZE = [
        '128x128' => [
            'width' => 128,
            'height' => 128,
        ],
        '256x256' => [
             'width' => 256,
             'height' => 256,
        ]
    ];
    const INDEXES = ['category', 'name', 'displayName'];
    protected $fillable = [
        //"dimension",
        //"enable_dimension",
        "displayName",
        "vendor",
        "attributes",
        "ownerId",
        "category",
        "name",
        "brand",
        "thumb",
        "status" ,
        'unity',
        'assetBundle',
        'assetZip',
        'isCommon',
        'thumbList',
        'revision',

        //USE FOR ASSET BUNDLE VERSION
        'versionAssetBundle',

        'extensions',
        'metadata'
    ];
    
    // [
    //     'versionAssetBundle' => [
    //         'v2018' => [
    //             'assetBundle' => 'path',
    //             'revision' => '12345678'
    //         ],
    //         'v2019' => [
    //             'assetBundle' => 'path',
    //             'revision' => '12345678'
    //         ]
    //     ]
    // ]

    protected $sub_attributes = [
        'attributes' => [
            "dimension",
            //"colorRange",
            //"isPrimary" ,
            //"isFloat",
            //"isHole",
            "dependency",
            "enable_dimension",
           
            //"renderable",
            "isTTS",
            //'position',
            //'positionXYZ',
            //'rotation',
            //'rotationXYZ',
            //'scale',
            //'scaleXYZ',
            'unit',
            //'type',
            "putable",
            "hangable",

            "hookable",
            "anchor",

            "isDecor",
            "isCarpet",
            "isLight", // 0/1
            "lightType" ,//   "line"/"spot"/"point"
            "canArray",
            "canScale",
            "canCook",
            "canDelete"
        ],
        
    ];


    protected $attributes_schema = [  
        'name' => 'string',
        'brand' => 'string',
        'attributes' => 'array'
    ];

    protected $appends = ['thumbUrl'];
    
    public function scopeWithoutTTS($query){
        return $query->where(function ($q)  {
            $q->whereNull('attributes')
            ->orWhereNull('attributes.isTTS')
            ->orWhere('attributes.isTTS', false)
            ->orWhere('attributes.isTTS', 0);
        });
       
    }

    public function scopeWithTTS($query){
        return $query->where(function ($q)  {
            $q->where('attributes.isTTS', 1)
            ->orWhere('attributes.isTTS', true);
        });
    }

   
    public function _owner(){
        return $this->belongsTo(User::class, 'ownerId', 'auth_id');
    }


    public function getThumbUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        return $this->thumb ? Storage::disk($driver)->url($this->thumb) : config('fitin.default_asset_image');
    }

    public function getThumb128UrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        $thumbList = $this->thumbList;
        if(is_array($thumbList) && !empty($thumbList["128"])){
            return Storage::disk($driver)->url($thumbList["128"]) ;
        }else{
            return $this->thumbUrl;
        }
    }

    public function getAssetZipUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        return $this->assetZip ? Storage::disk($driver)->url($this->assetZip) : null;
    }

    public function _category(){
        return $this->belongsTo(Category::class, 'category' ,'code');
    }

    public function getOriginCategoryAttribute(){
        if($this->_category){
            return $this->_category->originCategory;
        }
        return null;
    }


    public function scopePublished($q){
        $q->where('status',self::PUBLISHED);
    }
    
    
    public function getAssetBundleUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        return $this->assetBundle ? Storage::disk($driver)->url($this->assetBundle) : null;
    }


    public function getDownloadUrlAttribute(){
        if(!$this->assetBundle){
            return null;
        }
        if(config('fitin.enabled_encrypt_file')){
            $user = Auth::user();
            $driver = config('fitin.editor.filesystem_driver');
            if($user){
                $path = $this->assetBundle;
                $workspace = $user->getWorkspaceDir();
                $tmp_path = "$workspace/$path";
                if(Storage::disk($driver)->exists($tmp_path)){
                    return Storage::disk($driver)->url($tmp_path);
                }
            }
        }
        
        return Utils::editorStoreRoute('editor.object.download', ['key' => $this->name, 'id' => $this->_id]);
        
        
    }

    public function getDownloadZipUrlAttribute(){
        if(!$this->assetZip){
            return null;
        }
        $user = Auth::user();
        $driver = config('fitin.editor.filesystem_driver');
        if($user){
            $path = $this->assetZip;
            $workspace = $user->getWorkspaceDir();
            $tmp_path = "$workspace/$path";
            if(Storage::disk($driver)->exists($tmp_path)){
                return Storage::disk($driver)->url($tmp_path);
            }
        }
        return Utils::editorStoreRoute('editor.object.zip.download', ['key' => $this->name, 'id' => $this->_id]);
    }

    public function getZipRevisionAttribute(){
        if(!$this->assetZip){
            return 0;
        }
        $array = explode('/', $this->assetZip);
        if(count($array) >= 2){
            return $array[count($array) - 2];
        }else{
            return 0;
        }
        
    }

    public function scopeFilterByBrand($query){
        $user = Auth::user();
        $brand = $user->brand;

        if($brand == 'fitin'){
            return $query;
        }

        return  $query->where(function ($q) use ($brand) {
            $q->where('brand', $brand)->orWhere('brand', 'fitin')
                  ->orWhere('isCommon', true);
        });
    }

    public function scopeFilterByDesigner($query){
        $user = Auth::user();
        $brand = $user->brand;
        $query = $query
                ->where(function ($q) use ($user) {
                    $q->where('status',self::PUBLISHED)->orWhere('metadata.user.id', $user->auth_id);
                });
                
        if($brand !== 'fitin'){
            $query = 
                $query
                ->where(function ($q) use ($brand) {
                    $q->where('brand', $brand)->orWhere('brand', 'fitin')
                        ->orWhere('isCommon', true);
                    });
        }

        return $query;
    }

    public function makeMultiThumb()
    {
        $sizes = self::THUMB_SIZE;
        $driver = config('fitin.editor.filesystem_driver');
        $thumb = $this->thumb;
        $array_info = pathinfo($thumb);
        $file_basename = $array_info['basename'];
        $filename = $array_info['filename'];
        $dirname = $array_info['dirname'];
        $array_explode = explode('/', $dirname);
        array_pop($array_explode);

        $base_path = implode('/', $array_explode);
        
        $image_path = Storage::disk($driver)->path($thumb);
        if(!file_exists($image_path)){
            return;
        }
        $thumbList = [];
        foreach ($sizes as $size => $arrAttr) {
           
           
            $oImageManager = Image::make($image_path);
            $originWidth = $oImageManager->width();
            if($arrAttr['width'] >= $originWidth){
                continue;
            }
            $higher = ($arrAttr['width'] >= $arrAttr['height']) ? $arrAttr['width'] : $arrAttr['height'];
            $oImageManager->fit($higher, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            //resizeCanvas keep the origin shape of image and fill additional blank to get the wanted width-height 
           // $oImageManager->resizeCanvas($arrAttr['width'],$arrAttr['height'], 'center', false, 'FFF');
            $oImageManager->stream();
            
            $folderThumb = $arrAttr['width'];
            $newFileName = $filename . "." . self::THUMB_DEFAULT_MIME;

            $newPathUpload = "$base_path/$folderThumb/$newFileName";
            Storage::disk($driver)->put($newPathUpload, $oImageManager);
            
            $dataFile = [
                'size' => Storage::disk($driver)->size($newPathUpload), 
                'fileType' => 'image',
                'attachmentType' => get_class($this),
                'attachmentKey' => $this->getKey(),
                'path' => $newPathUpload,
                'name' => $newFileName,
                'originName' => $file_basename
            ];
            saveFileHistory($dataFile, $driver, $this);

            //uploadDriver($newFileName, $driver, $newPathUpload , Storage::disk($driver)->url($newPathUpload), $this);
            $thumbList[(string) $arrAttr['width']] = $newPathUpload;
        }

        $this->thumbList = $thumbList; 
        $this->save();
        return $this;
    }

    public function getAssetBundle(){
        $header = request()->header('Version');
        if($header){
            $versionAssetBundle = $this->versionAssetBundle ?? [];
            if(!empty($versionAssetBundle[$header] && is_array($versionAssetBundle[$header]) && isset($versionAssetBundle[$header]['assetBundle']))){
                return $versionAssetBundle[$header]['assetBundle'];
            }else{
                return null;
            }
        }else{
            return $this->assetBundle;
        }
    }

    public function getRevision(){
        $header = request()->header('Version');
        if($header){
            $versionAssetBundle = $this->versionAssetBundle ?? [];
            if(!empty($versionAssetBundle[$header] && is_array($versionAssetBundle[$header]) && isset($versionAssetBundle[$header]['revision']))){
                return $versionAssetBundle[$header]['revision'];
            }else{
                return 0;
            }
        }else{
            return $this->revision;
        }
    }

    public function getVersionAssetBundleAttribute($value = []){
        $driver = config('fitin.editor.filesystem_driver');
        if(empty($value['v2018'])){
            $value['v2018'] = [
                'assetBundle' => null,
                'revision' => 0
            ];
        }
        if(empty($value['v2019'])){
            $value['v2019'] = [
                'assetBundle' => null,
                'revision' => 0
            ];
        }
        if(empty($value['v2020'])){
            $value['v2020'] = [
                'assetBundle' => null,
                'revision' => 0
            ];
        }
        $value['v2018']['assetBundleUrl'] = $value['v2018']['assetBundle'] ?  Storage::disk($driver)->url($value['v2018']['assetBundle'])  : null;
        $value['v2019']['assetBundleUrl'] = $value['v2019']['assetBundle'] ?  Storage::disk($driver)->url($value['v2019']['assetBundle'])  : null;
        $value['v2020']['assetBundleUrl'] = $value['v2020']['assetBundle'] ?  Storage::disk($driver)->url($value['v2020']['assetBundle'])  : null;
        return $value;
    }


    public function getExtensionsAttribute($value = []){
        $driver = config('fitin.editor.filesystem_driver');
        if(!empty($value['zip']) && is_array($value['zip'])){
            $value['zip']['url'] = $value['zip']['path'] ?  Storage::disk($driver)->url($value['zip']['path'])  : null;
        
        }
        return $value;
    }

    public function getOwnerId(){
        if($this->ownerId){
            return $this->ownerId;
        }

        $metadata = $this->metadata ?? [];
        if(isset($metadata['user']) && isset($metadata['user']['id'])){
            return $metadata['user']['id'];
        }

        return null;
    }
}