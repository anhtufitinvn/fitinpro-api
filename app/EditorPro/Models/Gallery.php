<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

class Gallery extends Eloquent {
    
    protected $fillable = [
        "image_path",
        "thumb_path",
        "image_url",
        "thumb_url",
        "index",
        "hidden"
    ];

    const DIR = "gallery";

}