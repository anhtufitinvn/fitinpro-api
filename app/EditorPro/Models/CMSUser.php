<?php

namespace App\EditorPro\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Authenticatable;
use Jenssegers\Mongodb\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use App\EditorPro\Traits\HasRoles;
use App\EditorPro\Traits\CacheTrait;
use Illuminate\Support\Facades\Storage;

class CMSUser extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract {

    use HasRoles, Authenticatable, Authorizable, CacheTrait;

    const CACHE_TAG = "editor-cms-users";
    const WORKSPACE_DIR = "workspace/cms";
    const THUMB_DIR = "thumb";
    
    protected $collection = 'EditorCMSUsers';

    protected $guarded = ['_id'];
    //protected $guard = 'api';
    //protected $guard_name = 'api';
    //protected $primaryKey = '_id';
    protected $fillable = [
        'name',
        'email',
        'password',
        'auth_id',
        'active',
        'avatar'
    ];
    

    protected $hidden = [
        'password',
    ];

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            forgetCacheEditorUsersRolesPermissions();
        });

        

        self::updated(function ($model) {
            forgetCacheEditorUsersRolesPermissions();
        });

        self::deleted(function ($model) {
            $model->roles()->sync([]);
            forgetCacheEditorUsersRolesPermissions();
        });
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'auth_token' => $this->getAuthToken()
        ];
    }

    public function setAuthToken($token){
        $this->auth_token = $token;
    }

    public function getAuthToken(){
        return $this->auth_token;
    }
    
    public function setPasswordAttribute($value){
        $this->attributes['password'] = \Hash::make($value);      
    }

    public function roles(){
        return $this->belongsToMany( Role::class, null ,'user_ids',
        'role_ids' );
    }

    public function getAllPermissionsAttribute(){
        return $this->getAllPermissions();
    }

    public function setAuthIdAttribute($value){
        $last = CMSUser::whereNotNull('auth_id')->latest('_id')->first();
        if(!$last){
            $id = "{$value}_1";
        }else{
            $idArray = explode('_', $last->auth_id);
            $num = (int) $idArray[1];
            $num++;
            $id = "{$value}_$num";
        }
        $this->attributes['auth_id'] = $id;
    }

    public function getAvatarUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        return $this->avatar ? Storage::disk($driver)->url($this->avatar) : config('fitin.default_asset_image');
    }
}