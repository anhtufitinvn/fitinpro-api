<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\EditorPro\Traits\HasPermissions;

class Role extends Eloquent
{
    use HasPermissions;
    
    protected $collection = 'EditorRoles';
    //protected $primaryKey = 'name';
    public const CACHE_TAG = 'editor-role';
    public const CACHE_KEY = 'editor-role';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'user_ids'
    ];
    
    protected $hidden = ['user_ids', 'permission_ids'];

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            forgetCacheEditorUsersRolesPermissions();
        });
     
        self::updated(function ($model) {
            forgetCacheEditorUsersRolesPermissions();
        });

        self::deleting(function ($model) {
            $model->users()->sync([]);
            $model->permissions()->sync([]);
        });

        self::deleted(function ($model) {
            forgetCacheEditorUsersRolesPermissions();
        });
    }

    public function users(){
        return $this->belongsToMany( CMSUser::class, null ,'role_ids',
        'user_ids' );
    }

    public function permissions() {
        return $this->belongsToMany( Permission::class, 'null' ,'role_ids',
        'permission_ids' );
    }
}
