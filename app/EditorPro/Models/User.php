<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

//use App\Traits\HasRoles;
use App\FitIn\CacheManager;
use App\EditorPro\Traits\CacheTrait;
use App\Helpers\Utils;

use Jenssegers\Mongodb\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject {
   // use //HasRoles, 
    use Notifiable;
    use CacheTrait;
    protected $collection = 'EditorUsers';

    protected $guarded = ['_id'];
    protected $guard = 'api';
    protected $guard_name = 'api';
    protected $primaryKey = 'auth_id';
    protected $fillable = [
        'name',
        'full_name',
        'avatar',
        'email',
        'password',
        'phone',
        'active',
        'app_id',
        'auth_id',
        'config',
        'model',
        'jsonBundle',
        'brand'

    ];
    public const CACHE_TAG = 'editor-user';
    const CONFIG_DIR = "config";
    const THUMB_DIR = "thumb";
    const MODEL_DIR = "models";
    const BUNDLE_DIR = "bundles";
    const LAYOUT_DIR = "layouts";
    const WORKSPACE_DIR = "workspace";

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['configFile', 'configHeaders', 'modelFile'];

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            $driver = config('fitin.editor.filesystem_driver');
            $path = self::CONFIG_DIR."/{$model->auth_id}";
            if(!Storage::disk($driver)->has("$path/")){
                Storage::disk($driver)->makeDirectory("$path/");
            }
            self::flushCache();
        });

        

        self::updated(function ($model) {
            self::flushCache();
        });

        self::deleted(function ($model) {
            self::flushCache();
        });
    }

    public function checkExistPath($path = null){
        return "a";
    }

    public function getPrivateKey(){
        $key = ($this->config && is_array($this->config) && isset($this->config['private_key'])) ? $this->config['private_key'] : '123456';
        return $key;
    }

    public function getWorkspaceDir(){
        return self::WORKSPACE_DIR .'/'. $this->auth_id;
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'auth_token' => $this->getAuthToken()
        ];
    }

    /**
     * Determine if the user has verified their email address.
     *
     * @return bool
     */
    public function hasVerifiedEmail()
    {
        if($this->active){
            return true;
        }

        //Check authID
        $oAuthID = EditorAuthID::getInstance();
        $hasConfirmation = $oAuthID->hasConfirmation($oAuthID->getToken());

        //Store
        if($hasConfirmation){
            $this->update([
                'active' => 1
            ]);
        }

        return $hasConfirmation;
    }


    public function setAuthToken($token){
        $this->auth_token = $token;
    }

    public function getAuthToken(){
        return $this->auth_token;
    }

    public function getConfigFileAttribute(){
        $url = Utils::editorStoreRoute('editor.config.download', ['tmp' => $this->_id]);
        return $url;
    }

    public function getModelFileAttribute(){
        $url = Utils::editorStoreRoute('editor.model.download', ['tmp' => $this->_id]);
        return $url;
    }

    public function getConfigHeadersAttribute(){
        if($this->config && !empty($this->config['headers'])){
            return $this->config['headers'];
        }
        return false;
    }

    public function getAvatarUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        return $this->avatar ? Storage::disk($driver)->url($this->avatar) : config('fitin.default_asset_image');
    }

}