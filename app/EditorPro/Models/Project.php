<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\EditorPro\Traits\CacheTrait;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class Project extends Eloquent {

    use CacheTrait;

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            
        });

        self::created(function ($model) {
            self::flushCache();
        });

        self::updating(function ($model) {

        });

        self::updated(function ($model) {
            $changes = $model->getChanges();
            if(isset($changes['isTemplate']) && ($changes['isTemplate'] === 0 || $changes['isTemplate'] === false)){
                $key = $model->code;
                $freshTimestamp = $model->freshTimestamp();
                $layouts = Layout::where('project', $key)->update(['updated_at' => $freshTimestamp]);

            }
            self::flushCache();
        });

        self::deleting(function ($model) {

        });

        self::deleted(function ($model) {
            self::flushCache();
        });
    }

    protected $collection = 'EditorProjects';
    const INDEXES = ['code', 'name'];
    protected $fillable = [
        "name" ,
        "status",
        "code",
        "ownerId",
        "thumbList",
        "image",

        //from fitin
        'fitin_id',
        
        'sort',
        'landing_url',
        'isTemplate',
        'desc',
        'attrs',
        'metadata',
        'location'
    ];
    
    public const CACHE_TAG = 'editor-project';
    const PUBLISHED = "published";
    const PENDING = "pending";
    const MODEL_TYPE = "project";
    const DIR_NAME = "projects";
    const THUMB_DIR = "thumb";

    const THUMB_DEFAULT_MIME = "jpg";
    const THUMB_SIZE = [
        '128x128' => [
            'width' => 128,
            'height' => 128,
        ],
        '256x256' => [
             'width' => 256,
             'height' => 256,
        ]
    ];

    public function _owner(){
        return $this->belongsTo(CMSUser::class, 'ownerId', 'auth_id');
    }

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.editor.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image'); 
    }

    public function getThumb128UrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        $thumbList = $this->thumbList;
        if(is_array($thumbList) && !empty($thumbList["128"])){
            return Storage::disk($driver)->url($thumbList["128"]) ;
        }else{
            return $this->imageUrl;
        }
    }

    public function getThumb256UrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        $thumbList = $this->thumbList;
        if(is_array($thumbList) && !empty($thumbList["256"])){
            return Storage::disk($driver)->url($thumbList["256"]) ;
        }else{
            return $this->imageUrl;
        }
    }

    public function scopePublished($query)
    {
        return $query->where('status', self::PUBLISHED);
    }

    public function layouts(){
        return $this->hasMany(Layout::class, 'project', 'code');
    }

    public function makeMultiThumb()
    {
        $sizes = self::THUMB_SIZE;
        $driver = config('fitin.editor.filesystem_driver');
        $thumb = $this->image;
        $array_info = pathinfo($thumb);
        $file_basename = $array_info['basename'];
        $filename = $array_info['filename'];
        $dirname = $array_info['dirname'];
        $array_explode = explode('/', $dirname);
        array_pop($array_explode);

        $base_path = implode('/', $array_explode);
        
        $image_path = Storage::disk($driver)->path($thumb);
        if(!file_exists($image_path)){
            return;
        }
        $thumbList = [];
        foreach ($sizes as $size => $arrAttr) {
           
           
            $oImageManager = Image::make($image_path);
            $originWidth = $oImageManager->width();
            if($arrAttr['width'] >= $originWidth){
                continue;
            }
            $higher = ($arrAttr['width'] >= $arrAttr['height']) ? $arrAttr['width'] : $arrAttr['height'];
            $oImageManager->fit($higher, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            //resizeCanvas keep the origin shape of image and fill additional blank to get the wanted width-height 
           // $oImageManager->resizeCanvas($arrAttr['width'],$arrAttr['height'], 'center', false, 'FFF');
            $oImageManager->stream();
            
            $folderThumb = $arrAttr['width'];
            $newFileName = $filename . "." . self::THUMB_DEFAULT_MIME;

            $newPathUpload = "$base_path/$folderThumb/$newFileName";
            Storage::disk($driver)->put($newPathUpload, $oImageManager);
            
            $dataFile = [
                'size' => Storage::disk($driver)->size($newPathUpload), 
                'fileType' => 'image',
                'attachmentType' => get_class($this),
                'attachmentKey' => $this->getKey(),
                'path' => $newPathUpload,
                'name' => $newFileName,
                'originName' => $file_basename
            ];
            saveFileHistory($dataFile, $driver, $this);
            //uploadDriver($newFileName, $driver, $newPathUpload , Storage::disk($driver)->url($newPathUpload), $this);
            $thumbList[(string) $arrAttr['width']] = $newPathUpload;
        }

        $this->thumbList = $thumbList; 
        $this->save();
        return $this;
    }
}