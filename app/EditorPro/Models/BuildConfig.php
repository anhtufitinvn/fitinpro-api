<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
class BuildConfig extends Eloquent {


    protected $collection = 'EditorBuildConfig';

    const BUILD_DIR = "system/build";
    public const CACHE_TAG = 'editor-system-config';
    public const PENDING = "pending";
    public const PUBLISHED = "published";
    protected $fillable = [     
        "version",
        "brand",
        "path",
        "status",
        "description",
        "force_update"
    ];

    //public $primaryKey = 'type';
   
    protected $appends = ['url'];

    public static function getVersion($brand = null){
        if(!$brand){
            $configs = BuildConfig::whereNull('brand')->latest()->get();
        }
        else {
            $configs = BuildConfig::where('brand', $brand)->latest()->get();
        }
        return $configs;
    }

    public static function getAllVersion($brand = null){
        if(!$brand){
            $configs = BuildConfig::latest()->get();
        }
        else {
            $configs = BuildConfig::where('brand', $brand)->latest()->get();
        }
        return $configs;
    }

    public static function getLatestVersion($brand = null){
        if(!$brand){
            $configs = BuildConfig::whereNull('brand')->latest()->first();
        }
        else {
            $configs = BuildConfig::where('brand', $brand)->latest()->first();
        }
        
        return $configs;
    }

    public static function getLatestPublishedVersion($brand = null){
        if(!$brand){
            $configs = BuildConfig::whereNull('brand')->where('status', self::PUBLISHED)->latest()->first();
        }
        else {
            $configs = BuildConfig::where('brand', $brand)->where('status', self::PUBLISHED)->latest()->first();
        }
        
        return $configs;
    }

    public static function checkVersion($version, $brand = null){
        if(!$brand){
            $config = BuildConfig::whereNull('brand')->latest()->first();
        }
        else {
            $config = BuildConfig::where('brand', $brand)->latest()->first();
        }

        if(!$config){
            return true;
        }

        // if(strlen($version) <= 10){
        //     return false;
        // }

        //$old_substr = substr($config->version, 0 ,10);
        //$new_substr = substr($version, 0 ,10);
        //$old_remain = str_replace($old_substr, "", $config->version);
        //$new_remain = str_replace($new_substr, "", $version);

        //$pattern = "/^[0-9]{4}.[0-9]{2}.[0-9]{2}[0-9a-zA-Z._]+$/";

        //if(!preg_match($pattern, $config->version)){
        //    return true;
        //}

        // if($new_substr < $old_substr){
        //     return false;
        // }
        // else if($new_substr > $old_substr){
        //     return true;
        // }else { // == case
        //     if($old_remain >= $new_remain){
        //         return false;
        //     }
        // }
        
        // if($config && $config->version >= $version){
        //     return false;
        // }
        $old_version = $config->version;
        $compare = version_compare($version, $old_version);
        if($compare < 1){
            return false;
        }

        return true;
    }


    public function getUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        return $this->path ? Storage::disk($driver)->url($this->path) : null;
    }
}