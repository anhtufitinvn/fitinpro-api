<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FileDriver extends Eloquent {

    protected $collection = 'Editor_FileDrivers';

    protected $fillable = [
        'size',
        'type',
        'origin_name',
        'revision',
        "file" ,
        "driver",
        "path",
        "url" ,
        "key",
        "model"   
    ];
    
}