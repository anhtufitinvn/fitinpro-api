<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Traits\CacheTrait;

class Brand extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'EditorBrands';
    protected $primaryKey = 'code';
    protected $fillable = [
        "name" ,
        "code",
        "status",
        "image"
    ];
    const INDEXES = ['name', 'code'];
    public const PENDING = "pending";
    public const PUBLISHED = "published";
    const DIR_NAME = "brands";
    public const CACHE_TAG = 'editor-brand';
    const THUMB_DIR = "thumb";
   

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.editor.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image');
    }

    public function scopePublished($query)
    {
        return $query->where('status', self::PUBLISHED);
    }
}