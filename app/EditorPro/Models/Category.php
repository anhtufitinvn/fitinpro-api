<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Traits\CacheTrait;

class Category extends Eloquent {

    use CacheTrait;
    const MODEL_TYPE = "category";
    const DIR_NAME = "categories";
    const THUMB_DIR = "thumb";
    protected $collection = 'EditorCategories';
    protected $primaryKey = 'code';
    protected $fillable = [
        "name" ,
        "code",
        "parent",
        "status",
        "priority",
        "image"

    ];
    const INDEXES = ['code', 'name'];
    public const PENDING = "pending";
    public const PUBLISHED = "published";

    public const CACHE_TAG = 'editor-categories';

    public function scopePublished($q){
        $q->whereNotNull('status')->where('status',self::PUBLISHED);
    }

    public function scopeIsParent($q){
        $q->whereNull('parent');
    }

    public function scopeSort($q){
        $q->orderBy('priority', 'DESC');
    }

    public function sub_categories(){
        return $this->hasMany(Category::class, 'parent', 'code')->orderBy('priority', 'DESC');
    }

    public function _parent(){
        return $this->belongsTo(Category::class, 'parent', 'code');
    }
    
    public function getOriginCategoryAttribute(){
        if($this->_parent){
            return $this->_parent->originCategory;
        }
        return $this->code;
    }

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.editor.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image');;
    }

}