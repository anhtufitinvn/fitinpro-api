<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

class Notification extends Eloquent {

    
    protected $collection = 'EditorNotifications';

    protected $fillable = [
        "senderId", //NULLABLE, CAN BE SYSTEM
        "receiverId",
        "type",
        "read",
        "meta"
    ];
   
  

    public const CACHE_TAG = 'editor-notification';

    public function notificationType(){
        return $this->belongsTo(NotificationType::class, 'type', 'code');
    }

    public function receiver(){
        return $this->belongsTo(User::class, 'receiverId', 'auth_id');
    }

    public function getMessageAttribute(){
        if (is_array($this->meta)){
            $meta = $this->meta;
            $message  = $this->notificationType->message;
            foreach ($meta as $key=>$val){
                $message = str_replace(":$key", $meta[$key], $message);
            }
            return $message;
        }
        return null;
    }
}