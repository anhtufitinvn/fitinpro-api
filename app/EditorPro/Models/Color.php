<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Traits\CacheTrait;

class Color extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'EditorColors';

    protected $primaryKey = 'code';

    protected $fillable = [

        "name",
        "slug",
        "group",
        "code",
        "hex",
        "attrs",
        "meta",
        "image",
        "priority",
        "usedCount"
    ];
    const INDEXES = ['slug', 'name'];
    public const CACHE_TAG = 'editor-color';
    const MODEL_TYPE = "color";
    
    const DIR_NAME = 'colors';
    public function getImageUrlAttribute(){
        $driver = config('fitin.asset.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : null;
    }

    public function color_group(){
        return $this->belongsTo(ColorGroup::class, 'group', 'code');
    }
}