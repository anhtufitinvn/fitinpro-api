<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\EditorPro\Traits\CacheTrait;
use \App\Helpers\Utils;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;

class Room extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'EditorRooms';
    const PENDING = 'pending';
    const PUBLISHED = 'published';
    const REJECTED = 'rejected';
    const PRIVATE = 'private';
    const WAITING = 'waiting';

    public const CACHE_TAG = 'editor-room';
    const MODEL_TYPE = "room";
    const DIR_NAME = "rooms";
    const THUMB_DIR = 'thumb';
    const ASSET_DIR = 'asset';
    const PREFIX = "homestyler";
    const THUMB_DEFAULT_MIME = "jpg";
    const THUMB_SIZE = [
        '128x128' => [
            'width' => 128,
            'height' => 128,
        ],
        '256x256' => [
             'width' => 256,
             'height' => 256,
        ]
    ];
    
    const INDEXES = ['displayName', 'keyname'];
    protected $fillable = [
       
        "keyname" ,
        "displayName",
        'category',
        "ownerId",
        "settings",
        "attrs",
        "parent" ,
        "isTemplate",
        "status",
        'image',
        "published_at",
        "description",
        "dataJson",
        "assetBundle",
        "thumbList",
        "revision",

        //USE FOR ASSET BUNDLE VERSION
        'versionAssetBundle'
    ];

    // [
    //     'versionAssetBundle' => [
    //         'v2018' => [
    //             'assetBundle' => 'path',
    //             'revision' => '12345678'
    //         ],
    //         'v2019' => [
    //             'assetBundle' => 'path',
    //             'revision' => '12345678'
    //         ]
    //     ]
    // ]

    protected $attributes_schema = [
        'dataJson' => 'array',
        'settings' => 'array'
    ];


    protected $appends = ['imageUrl'];

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.editor.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image');
    
    }

    public function getThumb128UrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        $thumbList = $this->thumbList;
        if(is_array($thumbList) && !empty($thumbList["128"])){
            return Storage::disk($driver)->url($thumbList["128"]) ;
        }else{
            return $this->imageUrl;
        }
    }

    
    public function scopePublished($query)
    {
        return $query->where('status', self::PUBLISHED);
    }



    public function getDownloadUrlAttribute(){
        if(!$this->assetBundle){
            return null;
        }
        if(config('fitin.enabled_encrypt_file')){
            $user = Auth::user();
            $driver = config('fitin.editor.filesystem_driver');
            if($user){
                $path = $this->assetBundle;
                $workspace = $user->getWorkspaceDir();
                $tmp_path = "$workspace/$path";
                if(Storage::disk($driver)->exists($tmp_path)){
                    return Storage::disk($driver)->url($tmp_path);
                }
            }
        }
        return Utils::editorStoreRoute('editor.room.prefab.download', ['id' => $this->_id, 'name' => $this->keyname]);
    }

    public function owner(){
        return $this->belongsTo(User::class, 'ownerId', 'auth_id');
    }


    public function getAssetBundleUrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        return $this->assetBundle ? Storage::disk($driver)->url($this->assetBundle) : null;
    }

    public function makeMultiThumb()
    {
        $sizes = self::THUMB_SIZE;
        $driver = config('fitin.editor.filesystem_driver');
        $thumb = $this->image;
        $array_info = pathinfo($thumb);
        $file_basename = $array_info['basename'];
        $filename = $array_info['filename'];
        $dirname = $array_info['dirname'];
        $array_explode = explode('/', $dirname);
        array_pop($array_explode);

        $base_path = implode('/', $array_explode);
        
        $image_path = Storage::disk($driver)->path($thumb);
        if(!file_exists($image_path)){
            return;
        }
        $thumbList = [];
        foreach ($sizes as $size => $arrAttr) {
           
           
            $oImageManager = Image::make($image_path);
            $originWidth = $oImageManager->width();
            if($arrAttr['width'] >= $originWidth){
                continue;
            }
            $higher = ($arrAttr['width'] >= $arrAttr['height']) ? $arrAttr['width'] : $arrAttr['height'];
            $oImageManager->fit($higher, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            //resizeCanvas keep the origin shape of image and fill additional blank to get the wanted width-height 
           // $oImageManager->resizeCanvas($arrAttr['width'],$arrAttr['height'], 'center', false, 'FFF');
            $oImageManager->stream();
            
            $folderThumb = $arrAttr['width'];
            $newFileName = $filename . "." . self::THUMB_DEFAULT_MIME;

            $newPathUpload = "$base_path/$folderThumb/$newFileName";
            Storage::disk($driver)->put($newPathUpload, $oImageManager);
            
            $dataFile = [
                'size' => Storage::disk($driver)->size($newPathUpload), 
                'fileType' => 'image',
                'attachmentType' => get_class($this),
                'attachmentKey' => $this->getKey(),
                'path' => $newPathUpload,
                'name' => $newFileName,
                'originName' => $file_basename
            ];
            saveFileHistory($dataFile, $driver, $this);
            //uploadDriver($newFileName, $driver, $newPathUpload , Storage::disk($driver)->url($newPathUpload), $this);
            $thumbList[(string) $arrAttr['width']] = $newPathUpload;
        }

        $this->thumbList = $thumbList; 
        $this->save();
        return $this;
    }

    public function getAssetBundle(){
        $header = request()->header('Version');
        if($header){
            $versionAssetBundle = $this->versionAssetBundle ?? [];
            if(!empty($versionAssetBundle[$header] && is_array($versionAssetBundle[$header]) && isset($versionAssetBundle[$header]['assetBundle']))){
                return $versionAssetBundle[$header]['assetBundle'];
            }else{
                return null;
            }
        }else{
            return $this->assetBundle;
        }
    }

    public function getRevision(){
        $header = request()->header('Version');
        if($header){
            $versionAssetBundle = $this->versionAssetBundle ?? [];
            if(!empty($versionAssetBundle[$header] && is_array($versionAssetBundle[$header]) && isset($versionAssetBundle[$header]['revision']))){
                return $versionAssetBundle[$header]['revision'];
            }else{
                return 0;
            }
        }else{
            return $this->revision;
        }
    }

    public function getVersionAssetBundleAttribute($value = []){
        $driver = config('fitin.editor.filesystem_driver');
        if(empty($value['v2018'])){
            $value['v2018'] = [
                'assetBundle' => null,
                'revision' => 0
            ];
        }
        if(empty($value['v2019'])){
            $value['v2019'] = [
                'assetBundle' => null,
                'revision' => 0
            ];
        }
        if(empty($value['v2020'])){
            $value['v2020'] = [
                'assetBundle' => null,
                'revision' => 0
            ];
        }
        $value['v2018']['assetBundleUrl'] = $value['v2018']['assetBundle'] ?  Storage::disk($driver)->url($value['v2018']['assetBundle'])  : null;
        $value['v2019']['assetBundleUrl'] = $value['v2019']['assetBundle'] ?  Storage::disk($driver)->url($value['v2019']['assetBundle'])  : null;
        $value['v2020']['assetBundleUrl'] = $value['v2020']['assetBundle'] ?  Storage::disk($driver)->url($value['v2020']['assetBundle'])  : null;
        return $value;
    }
}