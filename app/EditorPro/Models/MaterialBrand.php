<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Traits\CacheTrait;

class MaterialBrand extends Eloquent {

    use CacheTrait;
    const MODEL_TYPE = "material-brand";
    const DIR_NAME = "material-brands";
    const THUMB_DIR = "thumb";
    protected $collection = 'EditorMaterialBrands';
    protected $primaryKey = 'code';
    protected $fillable = [
        "name" ,
        "code",
        "image",
        "status"

    ];
  
    public const CACHE_TAG = 'editor-material-brands';

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.editor.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image');;
    }

    
}