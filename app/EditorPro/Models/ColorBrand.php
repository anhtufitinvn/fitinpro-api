<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\EditorPro\Traits\CacheTrait;

class ColorBrand extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'EditorColorBrands';

    protected $primaryKey = 'code';
    
    protected $fillable = [
        "name",
        "code",
        "image",
        "status"
    ];
    const DIR_NAME = "material-brands";
    const THUMB_DIR = "thumb";
    const PENDING = 'pending';
    const PUBLISHED = 'published';

    public const CACHE_TAG = 'editor-color-brand';

    public function scopePublished($query){
        return $query->where('status', self::PUBLISHED);
    }

    public function color_groups(){
        return $this->hasMany(ColorGroup::class, 'brand', 'code');
    }

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.editor.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image');;
    }

}