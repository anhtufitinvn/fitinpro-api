<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\EditorPro\Traits\CacheTrait;
use Illuminate\Support\Facades\Storage;

class Style extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'EditorStyles';

    protected $fillable = [
        "name",
        "status",
        "code",
        "ownerId",
        "image"
    ];
    const INDEXES = ['code', 'name'];
  

    public const CACHE_TAG = 'editor-style';
    const DIR_NAME = "styles";
    const PUBLISHED = "published";
    const PENDING = "pending";

    public function getImageUrlAttribute()
    {
        $driver = config('fitin.editor.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image'); 
    }


    public function scopePublished($query)
    {
        return $query->where('status', self::PUBLISHED);
    }
}