<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

class LayoutRoom extends Eloquent {
    
    protected $fillable = [
        "_id",
        "type",
        "name",
        "area",
        "qty"
    ];

}