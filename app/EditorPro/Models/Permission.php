<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Permission extends Model
{
    protected $collection = 'EditorPermissions';
    //protected $primaryKey = 'name';
    public const CACHE_TAG = 'editor-permission';
    public const CACHE_KEY = 'editor-permission';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'group'   
    ];

    public function roles()
    {
        return $this->belongsToMany(
            Role::class,
            null,
            'permission_ids',
            'role_ids'
        );

    }
}
