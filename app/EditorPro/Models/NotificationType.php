<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

class NotificationType extends Eloquent {

    
    protected $collection = 'EditorNotificationTypes';

    protected $fillable = [
        "code",
        "message"
    ];
   
  

    public const CACHE_TAG = 'editor-notification-type';

}