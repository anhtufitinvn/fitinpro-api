<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Traits\CacheTrait;

class Page extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'EditorPages';
    protected $primaryKey = 'code';
    protected $fillable = [
        "name" ,
        "code",
        "metadata"
    ];
    
   
    public const CACHE_TAG = 'editor-page';
    
}