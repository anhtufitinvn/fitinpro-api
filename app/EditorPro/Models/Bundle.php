<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\EditorPro\Traits\CacheTrait;
use Intervention\Image\Facades\Image;
use \App\Helpers\Utils;
use Illuminate\Support\Facades\Storage;
use App\FitIn\Asset;

class Bundle extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'EditorBundles';
    const PENDING = 'pending';
    const PUBLISHED = 'published';
    const REJECTED = 'rejected';
    const PRIVATE = 'private';
    const WAITING = 'waiting';
    const PREFIX = "homestyler";
    public const CACHE_TAG = 'editor-bundle';
    const MODEL_TYPE = "bundle";
    const DIR_NAME = "bundles";
    const GLTF_DIR = 'gltf';
    const THUMB_DIR = 'thumb';
    const ZIP_DIR = 'zip';

    const THUMB_DEFAULT_MIME = "jpg";
    const THUMB_SIZE = [
        '128x128' => [
            'width' => 128,
            'height' => 128,
        ],
        '256x256' => [
             'width' => 256,
             'height' => 256,
        ]
    ];

    protected $fillable = [
        'displayName',
        'keyname',
        'isTemplate',   
        "brand",
        'name',
        'status',
        'dataJson',
        "published_at",
        "description",
        'image',
        'ownerId',
        'attrs',
        'metaZip',
        'zip_version',
        'price',
        'thumbList',
        "keywords"
        //'revision'
    ];


    const INDEXES = ['keyname', 'displayName', 'name'];
    protected $attributes_schema = [

        'dataJson' => 'array',
        'attrs' => 'array'
    ];

    public function owner(){
        return $this->belongsTo(User::class, 'ownerId', 'auth_id');
    }
    
    public function getImageUrlAttribute()
    {
        $driver = config('fitin.editor.filesystem_driver');
        return $this->image ? Storage::disk($driver)->url($this->image) : config('fitin.default_asset_image');
    }

    public function getMetaZipUrlAttribute()
    {
        $driver = config('fitin.editor.filesystem_driver');
        return $this->metaZip ? Storage::disk($driver)->url($this->metaZip) : null;
    }


    
    public function scopePublished($query)
    {
        return $query->where('status', self::PUBLISHED);
    }

    public function getEcomProductList()
    {
        $dataJson = $this->dataJson;
        if(!$dataJson || !is_array($dataJson) || (empty($dataJson['List_Objects'])) || !is_array($dataJson['List_Objects'])){
            $list = [
                'total_price' => 0,
                'items' => [

                ]
            ];
            $this->productList = $list;
        }
        else{
            $listObjects = $dataJson['List_Objects'];
            $listObjectsKey = array_column($listObjects, 'Key_name');

            $array_values = array_count_values($listObjectsKey);
            $array_values = array_change_key_case( $array_values); //Convert all key to lower case
            $array_unique_objects_key = array_unique($listObjectsKey);
            $stringObjectsKey = implode(',', $array_unique_objects_key);
            $count = count($array_unique_objects_key);
            $asset = new Asset();
            $data = $asset->getByType('product-filter', ['object_key' => $stringObjectsKey, 'limit' => $count + 1]);
            
            $total_price = 0;
            $final_item_list = [];

            foreach ($data as $product){
                $key = $product['object_key'];
                $array_list_count = $array_values[$key];
                $total_price = $total_price + $array_list_count * $product['price'];
                $final_item_list[] = [
                    'count' => $array_list_count,
                    'product' => $product
                ];
            }

            $list = [
                'total_price' => $total_price,
                'items' => $final_item_list
            ];

            $this->productList = $list;
        }

    }

    public function getThumb128UrlAttribute(){
        $driver = config('fitin.editor.filesystem_driver');
        $thumbList = $this->thumbList;
        if(is_array($thumbList) && !empty($thumbList["128"])){
            return Storage::disk($driver)->url($thumbList["128"]) ;
        }else{
            return $this->imageUrl;
        }
    }

    public function makeMultiThumb()
    {
        $sizes = self::THUMB_SIZE;
        $driver = config('fitin.editor.filesystem_driver');
        $thumb = $this->image;
        $array_info = pathinfo($thumb);
        $file_basename = $array_info['basename'];
        $filename = $array_info['filename'];
        $dirname = $array_info['dirname'];
        $array_explode = explode('/', $dirname);
        array_pop($array_explode);

        $base_path = implode('/', $array_explode);
        
        $image_path = Storage::disk($driver)->path($thumb);
        if(!file_exists($image_path)){
            return;
        }
        $thumbList = [];
        foreach ($sizes as $size => $arrAttr) {
           
           
            $oImageManager = Image::make($image_path);
            $originWidth = $oImageManager->width();
            if($arrAttr['width'] >= $originWidth){
                continue;
            }
            $higher = ($arrAttr['width'] >= $arrAttr['height']) ? $arrAttr['width'] : $arrAttr['height'];
            $oImageManager->fit($higher, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            //resizeCanvas keep the origin shape of image and fill additional blank to get the wanted width-height 
           // $oImageManager->resizeCanvas($arrAttr['width'],$arrAttr['height'], 'center', false, 'FFF');
            $oImageManager->stream();
            
            $folderThumb = $arrAttr['width'];
            $newFileName = $filename . "." . self::THUMB_DEFAULT_MIME;

            $newPathUpload = "$base_path/$folderThumb/$newFileName";
            Storage::disk($driver)->put($newPathUpload, $oImageManager);
            
            $dataFile = [
                'size' => Storage::disk($driver)->size($newPathUpload), 
                'fileType' => 'image',
                'attachmentType' => get_class($this),
                'attachmentKey' => $this->getKey(),
                'path' => $newPathUpload,
                'name' => $newFileName,
                'originName' => $file_basename
            ];
            saveFileHistory($dataFile, $driver, $this);
            //uploadDriver($newFileName, $driver, $newPathUpload , Storage::disk($driver)->url($newPathUpload), $this);
            $thumbList[(string) $arrAttr['width']] = $newPathUpload;
        }

        $this->thumbList = $thumbList; 
        $this->save();
        return $this;
    }
}