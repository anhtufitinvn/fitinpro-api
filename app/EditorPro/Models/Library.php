<?php

namespace App\EditorPro\Models;

use App\Models\Ver2\Object3d;
use App\Models\Ver2\Bundle;
use App\Models\Ver2\Material;
use App\Models\Ver2\Layout;
use App\Models\Ver2\ColorGroup;
use App\Models\Ver2\Category;
use App\Models\Ver2\Project;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\EditorPro\Traits\CacheTrait;

class Library extends Eloquent {

    use CacheTrait;
    
    protected $collection = 'EditorLibraries';

    protected $fillable = [
        "key" ,
        "userId",
        "type",
        "model",
        "downloadStatus"
    ];

    protected $models = [
        Object3d::class,
        Bundle::class,
        Material::class,
        Layout::class,
        ColorGroup::class,
        Category::class,
        Project::class
    ];

    public const CACHE_TAG = 'library';
    public const SUCCESS = 'success';
    public const PENDING = 'pending';
    
    public function getList(){
        $arrayList = [];
        foreach ($this->models as $model){
            $type = $model::MODEL_TYPE;
            $arrayList[$type] = $model::getEditorProCollection(); 
        }
        return collect($arrayList);
    }

    public function getListByUser($userId){
        $arrayList = [];
        $data = Library::where('userId', $userId)->where('downloadStatus', self::SUCCESS)->get();

        foreach ($data as $d){
            $type = $d->type;
            $model = $d->model;
            if(empty($arrayList[$type])){
                $arrayList[$type] = collect([]);
            };
            if($model::MODEL_TYPE == Material::MODEL_TYPE){
                $element = Material::where('_id', $d->key)->orWhere('code', $d->key)->first();
            }else{
                $element = $model::where((new $model)->getKeyName(), $d->key)->first();
            }
            
            if($element){
                $arrayList[$type]->push($element);
            }   
        }
        
        return collect($arrayList);
    }

}