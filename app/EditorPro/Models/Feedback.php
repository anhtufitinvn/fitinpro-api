<?php

namespace App\EditorPro\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use App\EditorPro\Traits\CacheTrait;
class Feedback extends Eloquent {

    use CacheTrait;
    protected $collection = 'EditorFeedbacks';

    protected $fillable = [
        "title",
        "content",
        "ownerId"
    ];
   
    protected $dates = ['created_at'];

    public const CACHE_TAG = 'editor-feedback';

    public function owner(){
        return $this->belongsTo(User::class, 'ownerId', 'auth_id');
    }

}