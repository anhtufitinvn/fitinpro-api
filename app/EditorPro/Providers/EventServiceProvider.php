<?php

namespace App\EditorPro\Providers;


use App\EditorPro\Events\ApproveBundle;
use App\EditorPro\Listeners\ApproveBundleListener;
use App\EditorPro\Events\RejectBundle;
use App\EditorPro\Listeners\RejectBundleListener;
use App\EditorPro\Events\ApproveRoom;
use App\EditorPro\Listeners\ApproveRoomListener;
use App\EditorPro\Events\RejectRoom;
use App\EditorPro\Listeners\RejectRoomListener;
use App\EditorPro\Events\ApproveLayout;
use App\EditorPro\Listeners\ApproveLayoutListener;
use App\EditorPro\Events\RejectLayout;
use App\EditorPro\Listeners\RejectLayoutListener;
use App\EditorPro\Events\UserStoreLibrary;
use App\EditorPro\Listeners\UserStoreLibraryListener;
use App\EditorPro\Events\UserDuplicateAsset;
use App\EditorPro\Listeners\UserDuplicateAssetListener;
use App\EditorPro\Events\SendConsultantLayout;
use App\EditorPro\Listeners\SendConsultantLayoutListener;
use App\EditorPro\Events\UploadEvent;
use App\EditorPro\Listeners\UploadEventListener;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ApproveBundle::class => [
            ApproveBundleListener::class
        ],
        RejectBundle::class => [
            RejectBundleListener::class
        ],
        ApproveRoom::class => [
            ApproveRoomListener::class
        ],
        RejectRoom::class => [
            RejectRoomListener::class
        ],
        ApproveLayout::class => [
            ApproveLayoutListener::class
        ],
        RejectLayout::class => [
            RejectLayoutListener::class
        ],
        UserStoreLibrary::class => [
            UserStoreLibraryListener::class
        ],
        UserDuplicateAsset::class => [
            UserDuplicateAssetListener::class
        ],

        SendConsultantLayout::class => [
            SendConsultantLayoutListener::class
        ],

        UploadEvent::class => [
            UploadEventListener::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
