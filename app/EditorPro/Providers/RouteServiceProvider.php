<?php

namespace App\EditorPro\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\EditorPro\Http\Controllers';
    protected $namespace_web = 'App\EditorPro\Http\Controllers\Web';
    protected $namespace_cms = 'App\EditorPro\Http\Controllers\Cms';
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapFrontendRoutes();
        $this->mapCmsRoutes();
        $this->mapWebRoutes();
    }


    protected function mapFrontendRoutes()
    {
        Route::namespace($this->namespace)
             ->group(base_path('app/EditorPro/Routes/editor.php'));
    }

    protected function mapCmsRoutes()
    {
        Route::namespace($this->namespace_cms)
             ->group(base_path('app/EditorPro/Routes/cms.php'));
    }

    protected function mapWebRoutes()
    {
        Route::namespace($this->namespace_web)
             ->group(base_path('app/EditorPro/Routes/web.php'));
    }
}
