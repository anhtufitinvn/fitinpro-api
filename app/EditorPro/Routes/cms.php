<?php

use Illuminate\Http\Request;
use App\Helpers\Utils;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'editor/cms/v1',
], function () {
   
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/logout', 'AuthController@logout');

  
    Route::group(['middleware' => [
         'auth:editor-cms'  // 'jwt.auth' 
        ]], function () {
            //Route::get('auth/me', 'UserController@me');
            Route::post('auth/password', 'AuthController@updatePassword');
            Route::post('auth/profile', 'AuthController@updateProfile');
            Route::post('auth/avatar', 'AuthController@uploadAvatar');
            
            Route::post('upload/image', 'UploadController@uploadImage');

            editorApiResource('users', 'user', 'UserController');
            //Route::apiResource('users', 'UserController')->middleware('editor_role_or_permission:admin');

            Route::post('cms-users/{id}/password', 'CMSUserController@changePassword')->middleware('editor_role_or_permission:admin|cms_user.change_password');
            
            editorApiResource('cms-users', 'cms_user', 'CMSUserController');
            //Route::apiResource('cms-users', 'CMSUserController')->middleware('editor_role_or_permission:admin');
            
            editorApiResource('roles', 'role', 'RoleController');
            //Route::apiResource('roles', 'RoleController')->middleware('editor_role_or_permission:admin');

            editorApiResource('permissions', 'permission', 'PermissionController');
            //Route::apiResource('permissions', 'PermissionController')->middleware('editor_role_or_permission:admin');

            Route::post('upload/layouts/asset-bundle', 'UploadController@uploadLayoutAsset')->middleware('editor_role_or_permission:admin|layout.upload_asset_bundle'); 
            Route::post('layouts/sync/ecom', 'LayoutController@syncDataFromEcom')->middleware('editor_role_or_permission:admin|layout.sync_data');
            Route::post('layouts/{key}/approved', 'LayoutController@approved')->middleware('editor_role_or_permission:admin|layout.approved');
            Route::post('layouts/{key}/rejected', 'LayoutController@rejected')->middleware('editor_role_or_permission:admin|layout.rejected');
            Route::get('layouts/{key}/list-items', 'LayoutController@getListItems')->middleware('editor_role_or_permission:admin|layout.show');
            Route::get('layouts/{key}/data-json', 'LayoutController@getDataJson')->middleware('editor_role_or_permission:admin|layout.show');

            #Specific API for upload json => read JSON => check info in Ecom then sync to HomeStyler
            //Route::post('layouts-by-upload-json', 'LayoutController@storeByUploadJson')->middleware('editor_role_or_permission:admin|designer');

            Route::post('upload/layouts/thumb', 'UploadController@uploadLayoutThumb')->middleware('editor_role_or_permission:admin|layout.upload_thumb');
            Route::post('upload/layouts/floor-plan', 'UploadController@uploadLayoutFloorPlan')->middleware('editor_role_or_permission:admin|layout.upload_thumb');
            Route::post('upload/layouts/panorama-zip', 'UploadController@uploadLayoutPanoramaZip')->middleware('editor_role_or_permission:admin');

            Route::post('upload/layouts/gallery', 'UploadController@uploadLayoutGallery')->middleware('editor_role_or_permission:admin|layout.update');
            Route::delete('layouts/{key}/gallery/{id}', 'LayoutController@destroyGallery')->middleware('editor_role_or_permission:admin|layout.update');
            
            
            editorApiResource('layouts', 'layout', 'LayoutController');
            //Route::apiResource('layouts', 'LayoutController')->middleware('editor_role_or_permission:admin|designer');

            Route::get('objects/3js', 'ObjectController@get3JSObjectNotExist')->middleware('editor_role_or_permission:admin|object.index');
            
            editorApiResource('objects', 'object', 'ObjectController');
            //Route::apiResource('objects', 'ObjectController')->middleware('editor_role_or_permission:admin|designer');
            
            Route::post('upload/objects/asset-bundle', 'UploadController@uploadObjectAsset')->middleware('editor_role_or_permission:admin|object.upload_asset_bundle'); 
            Route::post('upload/objects/asset-zip', 'UploadController@uploadObjectZip')->middleware('editor_role_or_permission:admin|upload_asset_zip');
            Route::post('upload/objects/thumb', 'UploadController@uploadObjectThumb')->middleware('editor_role_or_permission:admin|upload_thumb');

            editorApiResource('materials', 'material', 'MaterialController');
            //Route::apiResource('materials', 'MaterialController')->middleware('editor_role_or_permission:admin|designer'); 
            
            Route::post('upload/materials/asset-bundle', 'UploadController@uploadMaterialAsset')->middleware('editor_role_or_permission:admin|material.upload_asset_bundle'); 
            Route::post('upload/materials/thumb', 'UploadController@uploadMaterialThumb')->middleware('editor_role_or_permission:admin|material.upload_thumb');
            //Create Material by Upload Excel
            Route::post('materials/excel', 'MaterialController@createByExcel')->middleware('editor_role_or_permission:admin|material.upload_excel');

            editorApiResource('material-categories', 'material_category', 'MaterialCategoryController');
            //Route::apiResource('material-categories', 'MaterialCategoryController')->middleware('editor_role_or_permission:admin|designer');
            
            Route::post('upload/material-categories/thumb', 'UploadController@uploadMaterialCategoryThumb')->middleware('editor_role_or_permission:admin|material_category.upload_thumb');

            editorApiResource('material-brands', 'material_brand', 'MaterialBrandController');
            //Route::apiResource('material-brands', 'MaterialBrandController')->middleware('editor_role_or_permission:admin|designer');
            
            Route::post('upload/material-brands/thumb', 'UploadController@uploadMaterialBrandThumb')->middleware('editor_role_or_permission:admin|material_brand.upload_thumb');

            editorApiResource('colors', 'color', 'ColorController');
            //Route::apiResource('colors', 'ColorController')->middleware('editor_role_or_permission:admin|designer');
            //Create colors by Upload Excel
            Route::post('colors/excel', 'ColorController@createByExcel')->middleware('editor_role_or_permission:admin|color.upload_excel');
            
            editorApiResource('color-groups', 'color_group', 'ColorGroupController');
            //Route::apiResource('color-groups', 'ColorGroupController')->middleware('editor_role_or_permission:admin|designer');
            
            editorApiResource('color-brands', 'color_brand', 'ColorBrandController');
            //Route::apiResource('color-brands', 'ColorBrandController')->middleware('editor_role_or_permission:admin|designer');
            
            Route::post('upload/color-brands/thumb', 'UploadController@uploadColorBrandThumb')->middleware('editor_role_or_permission:admin|color_brand.upload_thumb');

            editorApiResource('bundles', 'bundle', 'BundleController');
            //Route::apiResource('bundles', 'BundleController')->middleware('editor_role_or_permission:admin|designer');
            
            Route::post('bundles/{key}/approved', 'BundleController@approved')->middleware('editor_role_or_permission:admin|bundle.approved');
            Route::post('bundles/{key}/rejected', 'BundleController@rejected')->middleware('editor_role_or_permission:admin|bundle.rejected');
            Route::get('bundles/{key}/list-items', 'BundleController@getListItems')->middleware('editor_role_or_permission:admin|bundle.show');
            Route::post('upload/bundles/thumb', 'UploadController@uploadBundleThumb')->middleware('editor_role_or_permission:admin|bundle.upload_thumb');
            
            editorApiResource('rooms', 'room', 'RoomController');
            //Route::apiResource('rooms', 'RoomController')->middleware('editor_role_or_permission:admin|designer');
            
            Route::post('upload/rooms/asset-bundle', 'UploadController@uploadRoomAsset')->middleware('editor_role_or_permission:admin|room.upload_asset_bundle'); 
            Route::post('rooms/{key}/approved', 'RoomController@approved')->middleware('editor_role_or_permission:admin|room.approved');
            Route::post('rooms/{key}/rejected', 'RoomController@rejected')->middleware('editor_role_or_permission:admin|room.rejected');
            Route::get('rooms/{key}/list-items', 'RoomController@getListItems')->middleware('editor_role_or_permission:admin|room.show');
            Route::post('upload/rooms/thumb', 'UploadController@uploadRoomThumb')->middleware('editor_role_or_permission:admin|room.upload_thumb');

            editorApiResource('categories', 'category','CategoryController');
            //Route::apiResource('categories', 'CategoryController')->middleware('editor_role_or_permission:admin|designer');
            
            Route::post('upload/categories/thumb', 'UploadController@uploadCategoryThumb')->middleware('editor_role_or_permission:admin|category.upload_thumb');

            editorApiResource('room-categories', 'room_category','RoomCategoryController');
            //Route::apiResource('room-categories', 'RoomCategoryController')->middleware('editor_role_or_permission:admin|designer');
            
            Route::post('upload/room-categories/thumb', 'UploadController@uploadRoomCategoryThumb')->middleware('editor_role_or_permission:admin|room_category.upload_thumb');

            editorApiResource('brands', 'brand','BrandController');
            //Route::apiResource('brands', 'BrandController')->middleware('editor_role_or_permission:admin|designer');
            
            Route::post('upload/brands/thumb', 'UploadController@uploadBrandThumb')->middleware('editor_role_or_permission:admin|brand.upload_thumb');

            editorApiResource('projects', 'project', 'ProjectController');
            //Route::apiResource('projects', 'ProjectController')->middleware('editor_role_or_permission:admin|designer');
            

            Route::get('styles', 'StyleController@index');


            Route::post('upload/projects/thumb', 'UploadController@uploadProjectThumb')->middleware('editor_role_or_permission:admin|project.upload_thumb');


            editorApiResource('pages', 'page', 'PageController');
            //Route::apiResource('pages', 'PageController')->middleware('editor_role_or_permission:admin');
            
            editorApiResource('feedbacks', 'feedback', 'FeedbackController');
            //Route::apiResource('feedbacks', 'FeedbackController')->middleware('editor_role_or_permission:admin');

            Route::post('config/upload/build', 'ConfigController@uploadBuildConfig')->middleware('editor_role_or_permission:admin|version.upload');
            Route::put('config/build/{id}', 'ConfigController@update')->middleware('editor_role_or_permission:admin|version.update');
            Route::get('config/build/all', 'ConfigController@index')->middleware('editor_role_or_permission:admin|version.index');

            //Dashboard
            Route::get('dashboard/overall', 'DashboardController@getOverall');
            Route::get('dashboard/newest-users', 'DashboardController@getNewestUsers');
            Route::get('dashboard/newest-feedbacks', 'DashboardController@getNewestFeedbacks');

            Route::get('locations', 'LocationController@index');
    });


    Route::get('config/build/latest', 'ConfigController@getBuildConfigLatest')->middleware('editor-access-key');
    Route::get('config/build', 'ConfigController@getBuildConfig')->middleware('editor-access-key');
});

Route::group([
    'prefix' => 'homestyler/v1',
], function () {
    Route::get('projects', 'Consultant\ProjectController@index')->middleware('editor-access-key');
    Route::get('projects/{id}', 'Consultant\ProjectController@show')->middleware('editor-access-key');
    Route::get('layouts', 'Consultant\LayoutController@index')->middleware('editor-access-key');
    Route::get('layouts/{id}', 'Consultant\LayoutController@show')->middleware('editor-access-key');

    Route::get('consultant/images', 'Consultant\FileController@listImages')->middleware('editor-access-key');
});