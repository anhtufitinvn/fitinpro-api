<?php

use Illuminate\Http\Request;
use App\Helpers\Utils;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'editor/v1',
], function () {
    Route::post('auth/user/{social}', 'AuthController@social');

    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/logout', 'AuthController@logout');

    //Login by specific key for BRAND ACCOUNT
    Route::post('auth/key/login', 'AuthController@loginByKey');


    Route::post('auth/register', 'AuthController@register'); #get token active from email
    Route::post('auth/resend-confirmation', 'AuthController@resendConfirmation'); #get token confirm from email
    Route::post('auth/verify', 'AuthController@verifyAccount');#post token to active or validate

    Route::post('auth/verify-otp', 'AuthController@verifyOtpCode');#verify if token is valid

    Route::post('auth/forgot-password', 'AuthController@forgotPassword'); #get token from email
    Route::post('auth/reset-password', 'AuthController@resetPassword'); #post token to reset password
    
   
    
    Route::group(['middleware' => [
            'auth:editor' 
        ]], function () {
            Route::get('auth/me', 'UserController@me');
            Route::post('auth/profile/change', 'UserController@changeProfile');
            Route::post('auth/password/change', 'AuthController@changePassword');
            Route::post('auth/upload/avatar', 'AuthController@uploadAvatar');

            Route::post('user/account/delete', 'UserController@removeAccount');
            
           

            Route::post('upload/config', 'UploadController@uploadConfig');
            Route::post('upload/model', 'UploadController@uploadModel');
            //Route::post('upload/bundle/json/{key}', 'UploadController@uploadBundleJson');
            //Route::post('upload/layout/json/{key}', 'UploadController@uploadLayoutJson');
            
            Route::post('upload/objects/thumb', 'UploadController@uploadObjectThumb');
            Route::post('upload/layouts/thumb', 'UploadController@uploadLayoutThumb');
            Route::post('upload/bundles/thumb', 'UploadController@uploadBundleThumb');
            Route::post('upload/rooms/thumb', 'UploadController@uploadRoomThumb');
            Route::post('upload/materials/thumb', 'UploadController@uploadMaterialThumb');

            Route::post('upload/layouts/asset-bundle', 'UploadController@uploadLayoutAsset');    
            Route::post('upload/objects/asset-bundle', 'UploadController@uploadObjectAsset'); 
            Route::post('upload/rooms/asset-bundle', 'UploadController@uploadRoomAsset');

            Route::post('upload/objects/asset-zip', 'UploadController@uploadObjectZip');
            
            Route::post('upload/objects/extensions/zip', 'UploadController@uploadObjectExtensionZip');


            Route::post('upload/bundles/meta-zip', 'UploadController@uploadBundleZip');

            Route::post('upload/materials/extensions/zip', 'UploadController@uploadMaterialExtensionZip');


            Route::any('download/layouts/asset-bundle/{name}/{id}', 'DownloadController@downloadLayoutPrefab')->name('editor.layout.prefab.download');
            Route::any('download/rooms/asset-bundle/{name}/{id}', 'DownloadController@downloadRoomPrefab')->name('editor.room.prefab.download');
           
            Route::any('download/{tmp}/config', 'DownloadController@downloadConfig')->name('editor.config.download');
            Route::any('download/{tmp}/model', 'DownloadController@downloadModel')->name('editor.model.download');
                
            Route::any('download/object/{id}/{key}', 'DownloadController@downloadObject')->name('editor.object.download');
            Route::any('download/object/asset-zip/{id}/{key}', 'DownloadController@downloadObjectZip')->name('editor.object.zip.download');

            Route::any('download/bundle/json/{id}/{key}', 'DownloadController@downloadBundleJson')->name('editor.bundle.json.download');
            Route::any('download/bundle/editor/json/{rev}/{id}', 'DownloadController@downloadBundleEditorJson')->name('editor.bundle-editor.download');
            
            Route::any('download/material/{name}/{id}', 'DownloadController@downloadMaterial')->name('editor.material.download');
            //Route::any('download/layout/json/{name}/{id}', 'DownloadController@downloadLayoutJson')->name('editor.layout.download');
            //Route::any('download/layout/editor/json/{rev}/{id}', 'DownloadController@downloadLayoutEditorJson')->name('editor.layout-editor.download');
            //Route::any('download/layout/editor/prefab/{rev}/{id}', 'DownloadController@downloadLayoutEditorPrefab')->name('editor.layout-editor.prefab.download');

            Route::post('library/success', 'LibraryController@confirmDownload');
            Route::get('library/all', 'LibraryController@all');
            Route::delete('library', 'LibraryController@deleteUserLibrary');
            Route::get('library', 'LibraryController@getByUser');

            Route::get('categories', 'CategoryController@index');

            Route::get('room-categories', 'RoomCategoryController@index');

            Route::get('objects/filters', 'ObjectController@filter');
            Route::post('objects/filters-v2', 'ObjectController@filterV2');

            Route::get('objects/mine', 'ObjectController@indexByUser');
            
            Route::get('objects/keys', 'ObjectController@indexByKeys');
            Route::post('objects/keys-v2', 'ObjectController@indexByKeysV2');

            Route::get('objects/{key}/related', 'ObjectController@getRelatedObjects');

            Route::get('objects/not-exists', 'ObjectController@getNotExistObject');

            Route::apiResource('objects', 'ObjectController');
            
           // Route::post('layouts/user/{id}/duplicate', 'LayoutController@duplicateLayoutUser');
            Route::post('layouts/{id}/duplicate', 'LayoutController@duplicate');
            Route::post('layouts/{key}/request/approved', 'LayoutController@requestApproved');
            Route::get('layouts/template', 'LayoutController@indexTemplate');
            Route::apiResource('layouts', 'LayoutController');

            Route::post('rooms/{id}/duplicate', 'RoomController@duplicate');
            Route::post('rooms/{key}/request/approved', 'RoomController@requestApproved');
            Route::get('rooms/template', 'RoomController@indexTemplate');
            Route::apiResource('rooms', 'RoomController');

            
            Route::post('bundles/{key}/duplicate', 'BundleController@duplicate');
            Route::post('bundles/{key}/request/approved', 'BundleController@requestApproved');
            Route::get('bundles/template', 'BundleController@indexTemplate');
            Route::apiResource('bundles', 'BundleController')->names([
                'update' => 'editor.bundles.update'
            ]);;

            //Route::get('materials', 'MaterialController@index');
            
            //Route::get('materials/{key}', 'MaterialController@show');
            Route::apiResource('materials', 'MaterialController');
        

            Route::get('material-categories', 'MaterialCategoryController@index');
            Route::get('material-brands', 'MaterialBrandController@index');

            Route::get('projects', 'ProjectController@index');

            Route::get('styles', 'StyleController@index');
            
            Route::get('color-groups', 'ColorGroupController@index');

            Route::get('color-brands', 'ColorBrandController@index');

            Route::get('colors/most-used', 'ColorController@getMostUsed');
            Route::get('colors', 'ColorController@index');

            Route::get('notifications', 'NotificationController@index');
            Route::get('notifications/unread/count', 'NotificationController@indexUnreadCount');
            Route::get('notifications/unread', 'NotificationController@indexUnread');
            Route::post('notifications/read/{key}', 'NotificationController@read');

            Route::get('library/objects', 'LibraryController@getObjects');

            Route::post('feedbacks', 'FeedbackController@store');

            Route::get('brands', 'BrandController@index');

            //User create and build Model Feature
            
            Route::post('upload/objects', 'ObjectController@createByUpload');
            
    });

    Route::get('brands/{id}', 'BrandController@show');
    Route::get('test/encrypt', 'TestController@testEncrypt');
});


Route::group([
    'prefix' => 'v1',
], function () {
    Route::get('materials', 'MaterialController@specificIndex');
    Route::post('objects/keys', 'ObjectController@indexByKeysV2');
    Route::get('layouts/keys', 'LayoutController@listKeys');
});