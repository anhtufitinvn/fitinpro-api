<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', 'HomeController@index');

Route::get('editor/tutorial', 'PageController@tutorial');

Route::group([
    'prefix' => 'editor/web/v1/',
], function () {
    Route::get('bundles/{key}', 'BundleController@getForm');

    Route::get('objects/{key}', 'ObjectController@getDetail');
    
    Route::get('pages/{key}', 'PageController@show');
});