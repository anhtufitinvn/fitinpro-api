<?php

namespace App\GraphQL\Queries;

use Closure;
use App\Models\Ver2\Object3d;
use Rebing\GraphQL\Support\Facades\GraphQL;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class ObjectsQuery extends Query
{
    protected $attributes = [
        'name' => 'objects query'
    ];

    public function type(): Type
    {
        return Type::listOf(GraphQL::type('object'));
    }

    public function args(): array
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::string()],
            'name' => ['name' => 'name', 'type' => Type::string()]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        if (isset($args['id'])) {
            return Object3d::where('id' , $args['id'])->get();
        }

        if (isset($args['name'])) {
            return Object3d::where('name', $args['name'])->get();
        }

        return Object3d::all();
    }
}