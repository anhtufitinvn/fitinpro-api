<?php

namespace App\GraphQL\Types;

use App\Models\Ver2\Object3d;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ObjectType extends GraphQLType
{    
    protected $attributes = [
        'name'          => 'Object',
        'description'   => 'A Object3d',
        'model'         => Object3d::class,
    ];

    public function fields() :array
    {
        return [
            '_id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The _id of the object'
                // Use 'alias', if the database column is different from the type name.
                // This is supported for discrete values as well as relations.
                // - you can also use `DB::raw()` to solve more complex issues
                // - or a callback returning the value (string or `DB::raw()` result)
                //'alias' => '_id',
            ],
            'id' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The id of the object',
                // Use 'alias', if the database column is different from the type name.
                // This is supported for discrete values as well as relations.
                // - you can also use `DB::raw()` to solve more complex issues
                // - or a callback returning the value (string or `DB::raw()` result)
                'alias' => 'name',
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The name of the object'
            ],
            'thumbUrl' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The thumb url of the object'
            ],

            'ar' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The ar of the object'
            ],
        ];
    }

    // If you want to resolve the field yourself, you can declare a method
    // with the following format resolve[FIELD_NAME]Field()
    protected function resolveEmailField($root, $args)
    {
        return strtolower($root->email);
    }    

    protected function resolveArField($root, $args)
    {
        return ['value' => $root->ar];
    }   
}