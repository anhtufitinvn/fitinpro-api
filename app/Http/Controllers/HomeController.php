<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return response()->json([
            'message' => 'Welcome to Object 3D CMS Api',
            'version' => '2.0.1',
            'date' => '19-09-2019'
        ]);
    }
}
