<?php
namespace App\Http\Controllers\Editor;


use App\Http\Resources\Bundle\BundleResource;
use App\Http\Resources\Bundle\BundleCollection;

use App\Http\Resources\Layout\LayoutResource;
use App\Helpers\Utils;
use Illuminate\Support\Facades\Validator;
use App\Models\Ver2\FileDriver;
use App\Models\Ver2\Bundle;
use App\Models\Ver2\Object3d;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Storage;
use App\Repositories\Bundle\BundleRepository;
use App\Repositories\Object3d\Object3dRepository;
use App\Http\Controllers\Api_V2\ApiController;

class BundleController extends ApiController
{


    protected $bundleRepository, $object3dRepository;

    public function __construct(BundleRepository $bundleRepository, Object3dRepository $object3dRepository){
        $this->bundleRepository = $bundleRepository;
        $this->object3dRepository = $object3dRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $bundles = $this->bundleRepository->getAll($params);
        return new BundleCollection($bundles);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        
        $dataJson = $input['dataJson'];
        $list_objects =  $dataJson['List_Objects'];
        $item_list = [];
        foreach($list_objects as $obj){
            $key = strtolower($obj['Key_name']);
            $object = $this->object3dRepository->find($key);
            $threeJSObj = $this->unityToThreeJS($obj);
            if($object){
                $item_list[] = [
                    'position' => $threeJSObj['Position'],
                    'rotation' => $threeJSObj['Rotation'],
                    'name' => $object->name,
                    //'brand' => $object->brand,
                    'vendor' => $object->vendor,
                    'thumb' => $object->thumb,
                    'thumbUrl' => $object->thumbUrl,
                    'fbx' => $object->fbx,
                    'fbxUrl' => $object->fbxUrl,
                    'attributes' => $object->getOriginal('attributes'),
                    'gltf' => $object->gltf,
                    'gltfUrl' => $object->gltfUrl,
                    'materials' => $object->materials
                ];
    
            }
        }
        


        $origin_key_name = $input['keyname'];
        $key_name = $origin_key_name. '_'. \Carbon\Carbon::now()->timestamp;
        

        $bundle_data = [
            'item_list' => $item_list,
            'status' => Bundle::PUBLISHED,
            'displayName' => $input['displayName'],
            'name' => $input['displayName'],
            //'brand' => $bundle->brand,
            'key_name' => $key_name,
            'homestylerReference' => $origin_key_name,
            //'ownerId' => Auth::user()->auth_id,
            'attrs' => ["isTemplate" => 1],
            'settings' => [
                "camera" => [
                    "position" => [
                        "x" => 0, 
                        "y" => 0, 
                        "z" => 0
                    ], 
                    "target" => [
                        "x" => 0, 
                        "y" => 0, 
                        "z" => 0
                    ]
                ]
            ],
            'imagePath' => $this->copyImageTo3js($input)
        ];
        
        $bundle = Bundle::where('homestylerReference', $origin_key_name)->first();
        if($bundle){
            $bundle->update($bundle_data);
        }
        else{
            $bundle = $this->bundleRepository->create($bundle_data);
        }
       
        
        return new BundleResource($bundle);
    }


    protected function copyImageTo3js($input){
        $url = $input['imageUrl'];
        if(!$url){
            return null;
        }
        $asset_driver = config('fitin.asset.filesystem_driver');
        if(!file_valid($url)){
            return null;
        }

        $time = \Carbon\Carbon::now()->timestamp;      
        $new_name = pathinfo($url)['basename'];
        $new_path = Bundle::DIR_NAME ."/{$input['keyname']}/" . Bundle::THUMB_DIR ."/$time/$new_name";
        $file_content = file_get_contents($url);
        Storage::disk($asset_driver)->put($new_path, $file_content);
        return $new_path;
    }


    protected function unityToThreeJS($obj){
        $newObj['Position'] = $this->positionToThreeJS($obj['Position']);
        $newObj['Rotation'] = $this->rotationToThreeJS($obj['Rotation']);
        return $newObj;
    }

    protected function positionToThreeJS($position){
        $newPosition = [
            'x' => $position['x'] * 100 * -1,
            'y' => $position['y'] * 100,
            'z' => $position['z'] * 100
        ];
        return $newPosition;
    }

    protected function rotationToThreeJS($rotation){
        $newRotation = [
            'x' => deg2rad($rotation['x']),
            'y' => deg2rad($rotation['y']),
            'z' => deg2rad($rotation['z'])
        ];
        return $newRotation;
    }
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return LayoutResource|JsonResponse
     */
    public function show(Request $request, $key)
    {
        $key = strtolower($key);
        $bundle = $this->bundleRepository->find($key);
        if(!$bundle){
            return $this->responseError('Bundle not found'); 
        }
        return new BundleResource($bundle);
    }



    
}
