<?php
namespace App\Http\Controllers\Editor;


use App\Http\Resources\Room\RoomResource;
use App\Http\Resources\Room\RoomCollection;

use App\Http\Resources\Layout\LayoutResource;
use App\Helpers\Utils;
use Illuminate\Support\Facades\Validator;
use App\Models\Ver2\FileDriver;
use App\Models\Ver2\Room;
use App\Models\Ver2\Object3d;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Storage;
use App\Repositories\Room\RoomRepository;
use App\Repositories\Object3d\Object3dRepository;
use App\Http\Controllers\Api_V2\ApiController;

class RoomController extends ApiController
{


    protected $roomRepository, $object3dRepository;

    public function __construct(RoomRepository $roomRepository, Object3dRepository $object3dRepository){
        $this->roomRepository = $roomRepository;
        $this->object3dRepository = $object3dRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $rooms = $this->roomRepository->getAll($params);
        return new RoomCollection($rooms);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        return $this->responseError('Under construction'); 
    
    }


    protected function copyImageTo3js($input){
        $url = $input['imageUrl'];
        if(!$url){
            return null;
        }
        $asset_driver = config('fitin.asset.filesystem_driver');
        if(!file_valid($url)){
            return null;
        }

        $time = \Carbon\Carbon::now()->timestamp;      
        $new_name = pathinfo($url)['basename'];
        $new_path = Room::DIR_NAME ."/{$input['keyname']}/" . Room::THUMB_DIR ."/$time/$new_name";
        $file_content = file_get_contents($url);
        Storage::disk($asset_driver)->put($new_path, $file_content);
        return $new_path;
    }


    
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return LayoutResource|JsonResponse
     */
    public function show(Request $request, $key)
    {
        $key = strtolower($key);
        $room = $this->roomRepository->find($key);
        if(!$room){
            return $this->responseError('Room not found'); 
        }
        return new RoomResource($room);
    }



    
}
