<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Resources\Layout\LayoutCollection;
use App\Http\Resources\Layout\LayoutResource;
use App\Http\Resources\Asset\ObjectResourceCollection;

use App\Models\Ver2\Layout;
use App\Models\Ver2\Object3d;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Layout\LayoutRepository;

class LayoutController extends ApiController
{
    protected $layoutRepository;

    public function __construct(LayoutRepository $layoutRepository){
        $this->layoutRepository = $layoutRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return LayoutCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $layouts = $this->layoutRepository->getAll($params);
        return new LayoutCollection($layouts);
    }

    public function getListName()
    {
        $layouts = $this->layoutRepository->getListName();
        return $this->responseData($layouts);
    }

    public function getListObject(Request $request, $name)
    {
        $objects = $this->layoutRepository->getListObject($name);
        return new ObjectResourceCollection($objects);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateLayoutRequest $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $input = $request->all();
        $input['ownerId'] = $user->auth_id;
        $input['isTemplate'] = true;
        $name = $request->get('name');
        if($layout = Layout::where('name', $name)->first()) {
            return $this->responseError('Layout already exists');
        }
       
        if($file = $request->file('file')){
            $validator = Validator::make($request->all(), [
                'file' =>  "required"
            ]);
    
            if($validator->fails()){
                return $this->responseError('File Or Name Invalid', "INVALID");   
            }
    
            $content = json_decode(file_get_contents($file));
            if($content === null){
                return $this->responseError('File content is not JSON', "INVALID");  
            }

            if ($file && $file->isValid()) {
                $layout = $this->layoutRepository->create($input);
                return $this->uploadJsonFile($layout, $file, 'create');
                
            } else {
                return $this->responseError('Lỗi quá trình upload');   
                           
            }
        }else{
            $layout = $this->layoutRepository->create($input);
            $this->webhook("layout.create", [
                'id' => $layout->_id,
                'name' => $layout->name,
                'dataJson' => ($layout->dataJson)
            ]);
            
            return new LayoutResource($layout);
        }

        
        
       
       
    }

    
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return LayoutResource|JsonResponse
     * @throws AuthorizationException
     */
    public function show(Request $request, $key)
    {
        $layout = $this->layoutRepository->find($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        return new LayoutResource($layout);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return LayoutResource|JsonResponse
     * @throws AuthorizationException
     */
    public function update(Request $request, $key)
    {
        $input = $request->all();
        $layout = $this->layoutRepository->update($key, $input);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }
        
        if($file = $request->file('file')){
            $content = json_decode(file_get_contents($file));
            if($content === null){
                return $this->responseError('File content is not JSON', "INVALID");  
            }

            if ($file && $file->isValid()) {
                return $this->uploadJsonFile($layout, $file, 'update');
                
            }
        }else {
            $this->webhook("layout.update", [
                'id' => $layout->_id,
                'name' => $layout->name,
                'dataJson' => ($layout->dataJson)
            ]);
        }
        return new LayoutResource($layout);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return JsonResponse|Response
     * @throws AuthorizationException
     */
    public function destroy(Request $request,$key)
    {
        $layout = $this->layoutRepository->delete($key);
        if(!$layout){
            return $this->responseError('Layout not found' );
        }

        $this->webhook("layout.delete", [
            'id' => $layout->_id,
            'name' => $layout->name,
            'dataJson' => []
        ]);

        return $this->respondSuccess();
    }


    public function uploadJsonFile($layout, $file, $method = 'create'){
        $driver = config('fitin.asset.filesystem_driver');
        $content = json_decode(file_get_contents($file));
        if($content === null){
            return $this->responseError('File content is not JSON', "INVALID");  
        }

        $type = Layout::DIR_NAME;
        $arrFileName = pathinfo($file->getClientOriginalName());
        $file_name = strtolower($arrFileName['filename']);
        $file_base_name = strtolower($arrFileName['basename']);
        $revision = \Carbon\Carbon::now()->timestamp;
        $path = "$type/{$layout->name}/$revision";
        $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
            
        $this->uploadDriver($file_base_name, $driver, "$path/$file_base_name", Storage::disk($driver)->url("$path/$file_base_name"));
            
        $layout->update([
            'revision' =>  $revision,
            'json' =>  "$path/$file_base_name"
        ]);
           
        $this->webhook("layout.$method", [
                'id' => $layout->_id,
                'name' => $layout->name,
                'dataJson' => (file_get_contents($file))
        ]);
            
        return response()->json([
            'code' => 0,
            'message' => "Success",
            'data' => [
                '_id' => $layout->_id,
                'path' => "$path/$file_base_name",
                'dataJson' => $content
            ]
        ]);
    }
}
