<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Resources\Color\ColorResource;
use App\Http\Resources\Color\ColorResourceCollection;
use Illuminate\Support\Facades\Validator;
use App\Models\Ver2\Color;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

use App\Repositories\Color\ColorRepository;

class ColorController extends ApiController
{

    protected $colorRepository;

    public function __construct(ColorRepository $colorRepository){
        $this->colorRepository = $colorRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        
        $colors = $this->colorRepository->getAll();

        return new ColorResourceCollection($colors);
    }

    public function getListForEcom(Request $request)
    {
        $colors = $this->colorRepository->getListForEcom();
        return new ColorResourceCollection($colors);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name'   => 'required',
            'code' => 'required',
            'group' => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('name, group, code required', "INVALID");   
        }

        $input['slug'] = \App\Helpers\Utils::slugifyFromModel(new Color, $request->get('name')); 
        $input['code'] = \App\Helpers\Utils::slugifyFromModel(new Color, $request->get('code')); 
        $color = $this->colorRepository->create($input);

        return new ColorResource($color);
    }
  

    /**
     * Display the specified resource.
     *
     * @param Brand $category
     * @return BrandResource
     */
    public function show($key)
    {
        $key = strtolower($key);
        $color = $this->colorRepository->find($key);
        if(!$color) {
            return $this->responseError('Color not found');
        }
        return new ColorResource($color);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        $key = strtolower($key);
        $color = $this->colorRepository->update($key, $request->all());
        if(!$color) {
            return $this->responseError('Color not found');
        }
        
        return (new ColorResource($color));
    }

    public function countUse(Request $request){
        $key = strtolower($request->get('code'));
        $color = $this->colorRepository->updateCount($key);
        if(!$color) {
            return $this->responseError('Color not found');
        }
        
        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        $key = strtolower($key);
        $color = $this->colorRepository->delete($key);
        if(!$color) {
            return $this->responseError('Color not found');
        }
        return $this->respondObjectDeleted($color->_id);
    }
}
