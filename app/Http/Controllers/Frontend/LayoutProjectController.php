<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Resources\Project\ProjectResource;
use App\Http\Resources\Project\ProjectCollection;

use App\Models\Ver2\Project;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Repositories\Project\ProjectRepository;

class LayoutProjectController extends ApiController
{
    protected $projectRepository;

    public function __construct(ProjectRepository $projectRepository){
        $this->projectRepository = $projectRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $projects = $this->projectRepository->getAll();
        return new ProjectCollection($projects);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        
        $input = $request->all();
        $code = $request->get('code');       
        if($code){
            if(Project::where('code', $code)->first()){
                return $this->responseError('Code has already existed', 'DATA_EXISTS');
            }
        }else {
            $code = \App\Helpers\Utils::slugifyFromModel(new Project, $request->get('name'));
        }
       
        $input['code'] = $code;
        $project = $this->projectRepository->create($input);
        return (new ProjectResource($project));
    }


    /**
     * Display the specified resource.
     *
     * @param Project $category
     * @return ProjectResource
     */
    public function show($id)
    {
        $project = $this->projectRepository->find($id);
        if(!$project) {
            return $this->responseError('Project not found');
        }
        return new ProjectResource($project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Project $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        
        $project = $this->projectRepository->update($id, $input);

        return (new ProjectResource($project));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $project = $this->projectRepository->delete($id);
        if(!$project) {
            return $this->responseError('Project not found');
        }

        return $this->respondSuccess();
    }
}
