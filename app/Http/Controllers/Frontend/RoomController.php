<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Resources\Room\RoomResource;
use App\Http\Resources\Room\RoomResourceCollection;

use App\Models\Ver2\Room;
use App\Models\Ver2\Attachment;

use App\Repositories\Room\RoomRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;


class RoomController extends ApiController
{
    protected $roomRepository;

    public function __construct(RoomRepository $roomRepository){
        $this->roomRepository = $roomRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    
    public function index(Request $request)
    {
        $params = $request->all();
        $room = $this->roomRepository->getAll($params);
        return new RoomResourceCollection($room);

    }

    public function getMyRooms(Request $request)
    {
        $room = $this->roomRepository->getMyRooms();
        return new RoomResourceCollection($room);

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $code = $request->get('code');       
        // if($code){
        //     if(Room::where('code', $code)->first()){
        //         return $this->responseError('Code has already existed', 'DATA_EXISTS');
        //     }
        // }else {
        //     $code = \App\Helpers\Utils::slugifyFromModel(new Room, $request->get('name'));
        // }
        $user = Auth::user();

        $input = $request->all();

        if($code){      
            if($room = Room::where('code', $code)->where('owner_id', $user->auth_id)->first()){           
                $updateRoom = $this->update($request, $room->getKey());       
                return $updateRoom;
            }
        }

        $input['owner_id'] = $user->auth_id;
        $room = $this->roomRepository->create($input);
        return (new RoomResource($room));
            
    }


    /**
     * Display the specified resource.
     *
     * @param Room $category
     * @return RoomResource
     */
    public function show(Request $request, $key)
    {
        $room = $this->roomRepository->find($key);
        if(!$room) {
            return $this->responseError('Room not found');
        }
        return new RoomResource($room);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Room $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        $room = $this->roomRepository->update($key, $request->all());
        if(!$room) {
            return $this->responseError('Room not found');
        }
        
        return (new RoomResource($room));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Room $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        $room = $this->roomRepository->delete($key);
        if(!$room) {
            return $this->responseError('Room not found');
        }
        return $this->respondObjectDeleted($room->_id);
    }
}
