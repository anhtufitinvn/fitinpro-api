<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Resources\User\UserResource;
use App\Http\Resources\User\UserResourceCollection;
use App\FitIn\AuthID;
use App\Models\Ver2\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

use App\Services\UserService;

/**
 * @group 1. Authentication
 */
class AuthController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    protected $userService;

    public function __construct(UserService $userService)
    {
        //$this->middleware('auth:api', ['except' => ['login', 'social', 'register']]);
        $this->userService = $userService;
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        // $credentials = $request->only('email', 'password');
        // $oAuthID = new AuthID();
        // $resultAuth = $oAuthID->login($credentials['email'], $credentials['password']);
        
        // if ($resultAuth) {
        //     $userInfo = $oAuthID->getInfo($resultAuth['token']);
        //     $user = User::where('auth_id', $userInfo['id'])->first();
        //     if (!$user) {
        //         $arrEmail = explode('@', $resultAuth['email']);
        //         $user = User::create([
        //             'auth_id' => $resultAuth['id'],
        //             'name' => $arrEmail[0],
        //             'email' => $userInfo['email'],
        //             'app_id' => $userInfo['id'],
        //             'active' => $userInfo['status'] > 0 ? 1 : 0
        //         ]);
        //     }
        //     else{
        //         $user->active = $userInfo['status'] > 0 ? 1 : 0;
        //         $user->save();
        //     }
            
        //     if($user->is_user_normal){
        //         return response()->json([
        //             'data' => [
        //                 'error' => 1,
        //                 'error_code' => 'UNAUTHORIZED',
        //             ],
                    
        //             'message' => __('Normal accounts cannot login to the CMS!'),
        //             'code' => -401
        //         ]);
        //     }


        //     if(!$user->active){
        //         return response()->json([
        //             'data' => [
        //                 'error' => 1,
        //                 'error_code' => 'UNAUTHORIZED',
        //             ],
                    
        //             'message' => __('Account not active!'),
        //             'code' => -401
        //         ]);
        //     }
            
            
        //     Auth::loginUsingId($user->_id);
            
        //     $token = auth()->check() ? JWTAuth::fromUser(auth()->user()) : false;
            
        //     $arrToken = [
        //         'access_token' => $token,
        //         'ecommerce_token' => $resultAuth['jwt'],
        //         'auth_token' => $resultAuth['token']
        //     ];
            
        //     return $this->respondWithToken($arrToken);
        // }

        // return response()->json([
        //     'data' => [
        //         'error' => 1,
        //         'error_code' => 'UNAUTHORIZED',
        //     ],
            
        //     'code' => -401,
        //     'message' => __('Email or password incorrect!')
        // ]);

        $inputs = request()->input();

        $arResult = $this->userService->login($inputs['email'], $inputs['password']);

        if (!$arResult){
            return response()->json([
                'code' => -1,
                'message' => 'Thông tin không hợp lệ hoặc chưa kích hoạt'
            ]);
        }

        return response()->json([
            'code' => 0,
            'message' => 'Login successfully!',
            'data' => $arResult
        ]);
    }

    public function loginByToken(Request $request)
    {   
        $token = $request->get('access_token');
        if(!$token){
            return response()->json([
                'code' => -401,
                'message' => __('access_token is required!')
            ]);
        }

        $arResult = $this->userService->loginByToken( $token);
        
        return response()->json([
            'code' => 0,
            'message' => 'Login successfully!',
            'data' => $arResult
        ]);
    }

    /**
     * Get a JWT token via merchant code.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loginByMerchantCode(Request $request)
    {
        $user = User::where('merchant_code', $request->get('merchant_code'))->first();
        if(!$user){
            return response()->json(['error' => true, 'message' => __('Invalid user!')]);
        }
        Auth::loginUsingId($user->_id);
        $token = auth()->check() ? JWTAuth::fromUser(auth()->user()) : false;
        $arrToken = [
            'access_token' => $token
        ];
        return $this->respondWithToken($arrToken);
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => __('Successfully logged out')]);
    }



    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($arrToken)
    {
        return response()->json([
            'code' => 0,
            'data' => [
                'access_token' => $arrToken['access_token'],
                'token_type' => 'bearer',
                'expires_in' => $this->guard()->factory()->getTTL() * 60,
                'ecommerce_token' => $arrToken['ecommerce_token'] ?? NULL,
                'auth_token' => $arrToken['id_token'] ?? NULL,
            ],
            
            'message' => 'Login success!'
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard('api');
    }

    // public function loginBySocial($social, $access_token)
    // {
    //     $resultAuth = $this->authID->socialLogin($social, $access_token);

    //     if (!$resultAuth) {
    //         return [
    //             'error' => true,
    //             'code' => 401,
    //             'error_code' => 'AuthorizationException',
    //             'message' => 'Access token not valid!'
    //         ];
    //     }

    //     $customer = $this->createOrUpdateUser($resultAuth);

    //     return $this->loginByUser($customer, $resultAuth);
    // }

    public function social($social)
    {
        $access_token = request()->input('access_token', false);      
        if (!$access_token) {
            return response()->json([
                'data' => [
                    'error_code' => 'TOKEN_MISSING',
                ],
                
                'code' => -401,
                'message' => __('access_token is required!')
            ]);
        }

        $arrResult = $this->userService->loginBySocial($social, $access_token);
        
        if (!$arrResult){
            return response()->json([
                'code' => -1,
                'message' => 'Token not valid'
            ]);
        }

        return response()->json([
            'code' => 0,
            'message' => 'Login successfully!',
            'data' => $arrResult
        ]);

        // $oAuthID = new AuthID();
        // $resultAuth = $oAuthID->socialLogin($social, $access_token);
        
        // if ($resultAuth) {
        //     $user = User::where('auth_id', $resultAuth['id'])->first();
        //     if (!$user) {
        //         $userInfo = $oAuthID->getInfo($resultAuth['token']);
        //         $arrEmail = explode('@', $resultAuth['email']);
        //         $user = User::create([
        //             'auth_id' => $resultAuth['id'],
        //             'name' => $arrEmail[0],
        //             'email' => $userInfo['email'],
        //             'app_id' => $userInfo['id'],
        //             'active' => 1
        //         ]);
        //     }
        //     Auth::loginUsingId($user->_id);
        //     $token = auth()->check() ? JWTAuth::fromUser(auth()->user()) : false;
        //     $arrToken = [
        //         'access_token' => $token,
        //         'ecommerce_token' => $resultAuth['jwt'],
        //         'id_token' => $resultAuth['token']
        //     ];
        //     return $this->respondWithToken($arrToken);
        // }

        // return response()->json([
        //     'data' => [
        //         'error' => 1,
        //         'error_code' => 'UNAUTHORIZED',
        //     ],
            
        //     'code' => -401,
        //     'message' => __('Login failed please try again!'),
            
        // ]);
    }

    public function register(Request $request)
    {
        $arrUser = $request->all();
        $validator = Validator::make(
            $arrUser,
            [
                'name' => 'required|max:255',
                'password' => 'required|min:6',
                'email' => 'required|email|unique:users'
            ]
        );

        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());

            return response()->json([
                         
                'code' => -1,
                'message' => $errorString
            ]);
        }


        $oAuthID = new AuthID();
        $existEmail = $oAuthID->hasUniqueEmail($arrUser['email']);

        if ($existEmail) {
            return response()->json([     
                'code' => -1,
                'message' => __('That email has already been taken')
            ]);
        }

        User::create([
            'auth_id' => $arrUser['id'],
            'name' => $arrUser['name'],
            'email' => $arrUser['email'],
            'active' => 0,
            'role' => User::ROLE_USER
        ]);

        $oAuthID->register($arrUser['email'], $arrUser['password']);

        return response()->json(
            ['code' => 0, 'data' => [], 'message' => __('Register successfully! Please activate email!')]
        );
    }



    public function verifyOtp(Request $request){
        $code = request()->input('code');
        $email = request()->input('email');
        if(empty($code)){
            return response()->json([
                'data' => [
                    'error' => 1,
                    'error_code' => 'CODE_MISSING',
                ],
                
                'code' => -401,
                'message' => __('Code is required!')
            ]);
        }

        //TODO
        $result = $this->userService->verifyOtp($email, $code);

        return response()->json(
            $result
        );
    }


    public function registerAR(Request $request)
    {
        $arrUser = $request->all();
        $validator = Validator::make(
            $arrUser,
            [
                'name' => 'required|max:255',
                'password' => 'required|min:6',
                'password_confirmation' => 'required|min:6|same:password',
                'email' => 'required|email|unique:users',
                'phone' => 'required|min:10|max:20'
            ]
        );

        if ($validator->fails()) {
            $errorString = implode(",",$validator->messages()->all());

            return response()->json([
                'code' => -1,
                'message' => $errorString
            ]);
        }


        $oAuthID = new AuthID();
        $existEmail = $oAuthID->hasUniqueEmail($arrUser['email']);

        if ($existEmail) {
            return response()->json([
                'code' => -1,
                'message' => __('That email has already been taken')
            ]);
        }

        User::create([
            'auth_id' => $arrUser['id'],
            'name' => $arrUser['name'],
            'phone' => $arrUser['phone'],
            'email' => $arrUser['email'],
            'active' => 0,
            'role' => User::ROLE_USER
        ]);

        $oAuthID->register($arrUser['email'], $arrUser['password'],$arrUser['phone']);


        return response()->json(
            ['code' => 0, 'data' => [], 'message' => __('Register successfully! Please activate email!')]
        );
    }


    public function forgotPassword(){
        //

        return response()->json(
            ['code' => 0, 'data' => [], 'message' => __('Register successfully! Please activate email!')]
        );
    }

    public function changePassword(){
        return response()->json(
            ['code' => 0, 'data' => [], 'message' => __('Register successfully! Please activate email!')]
        );
    }

    public function validateAccount(Request $request){
        $credentials = $request->only('email', 'password');
        $oAuthID = new AuthID();
        $resultAuth = $oAuthID->login($credentials['email'], $credentials['password']);
        if(!$resultAuth){
            return $this->responseError('Credential not correct', 'INVALID');
        }else{
            return response()->json([
                'code' => 0,
                'data' => [],
                'message' => 'Account Available'
            ]);
        }
    }

    public function me()
    {
        $me = Auth::user();
        return new UserResource($me);
    }
}
