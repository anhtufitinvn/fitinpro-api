<?php
namespace App\Http\Controllers\Frontend;

use App\FitIn\CacheManager;
use App\Http\Resources\Bundle\BundleResource;
use App\Http\Resources\Bundle\BundleCollection;

use App\Http\Resources\Bundle\NewBundleResource;

use App\Http\Resources\Layout\LayoutResource;
use App\Helpers\Utils;
use Illuminate\Support\Facades\Validator;
use App\Models\Ver2\FileDriver;
use App\Models\Ver2\Bundle;
use App\Models\Ver2\Object3d;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Storage;
use App\Repositories\Bundle\BundleRepository;

class BundleController extends ApiController
{


    protected $bundleRepository;

    public function __construct(BundleRepository $bundleRepository){
        $this->bundleRepository = $bundleRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $params = $request->all();
        $params['type'] = 'published';
        $bundles = $this->bundleRepository->getAll($params);
        return new BundleCollection($bundles);
    }

    public function myBundles(Request $request)
    {
        $params = $request->all();
        $params['type'] = 'my-bundles';
       
        $bundles = $this->bundleRepository->getAll($params);
        return new BundleCollection($bundles);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return LayoutResource|JsonResponse
     */
    public function show(Request $request, $key)
    {
        $key = strtolower($key);
        $bundle = $this->bundleRepository->find($key);
        if(!$bundle){
            return $this->responseError('Bundle not found'); 
        }
        return new NewBundleResource($bundle);
    }
   
}
