<?php

namespace App\Http\Controllers\Frontend\Upload;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\Models\Ver2\Attachment;
use App\Models\Ver2\Object3d;
use App\Models\Ver2\File;
use App\Models\Ver2\Bundle;
use App\Http\Controllers\Frontend\ApiController;

class UploadBundleController extends ApiController
{


    /** *****************************************************************************
     * Upload GLTF.
     *
     * @param Request $request
     * 
     * @return Array
     */
    public function uploadGLTF(Request $request){
        $driver = config('fitin.asset.filesystem_driver');

        $type = Bundle::DIR_NAME;
        //$name = $request->get('name');
        $file = $request->file('gltf');
        $brand =  strtolower($request->get('brand'));
        $room =  strtolower($request->get('room'));
        $key_name =  strtolower($request->get('key_name'));

        $validator = Validator::make($request->all(), [
            'gltf' =>  'required|file',
            'key_name'   => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('File Or Name Invalid', "INVALID");   
        }

        $obj = Bundle::where('key_name', $key_name)->first();
        if(!$obj){
            return $this->responseError('Bundle not found');
        }

        if ($file && $file->isValid()) {
            $arrFileName = pathinfo($file->getClientOriginalName());
            
            $file_name = strtolower($arrFileName['filename']);
            if(!$brand){
                $brand = explode('_', $file_name)[0];
            }

            if($room){
                $folder_name = "$key_name/$room";
            }else{
                $folder_name = "{$key_name}_room";
            }
            
            $time = Carbon::now()->timestamp;
            $new_file_base_name = "$key_name.glb";
            $path = "$type/$brand/$folder_name/" . Bundle::GLTF_DIR ."/$time";
            
            $upload = Storage::disk($driver)->putFileAs($path, $file, $new_file_base_name);
            $data = [
                "file_name" => $new_file_base_name,
                "path" => "$path/$new_file_base_name",
                "url" =>  Storage::disk($driver)->url($upload)
            ];       

            // $obj->gltf = "$path/$new_file_base_name";
            // $obj->save();


            /**
             * Save History
             */
            $dataFile = [
                'size' => Storage::disk($driver)->size($upload), 
                'fileType' => 'gltf',
                'attachmentType' => get_class($obj),
                'attachmentKey' => $obj->key_name,
                'path' => $upload,
                'name' => $new_file_base_name,
                'originName' => $file_name,
                'revision' => $time
            ];
            $this->saveFileHistory($dataFile, $driver, $obj);


            return response()->json([
                'code' => 0,
                'data' => $data,
                'message' => 'Upload success'
            ]);
            
        } else {
            return $this->responseError('Lỗi quá trình upload');   
                       
        }
    }

}
