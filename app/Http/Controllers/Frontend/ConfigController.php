<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Ver2\SystemConfig;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class ConfigController extends ApiController
{

    public function getConfig(Request $request)
    {
        $configs = SystemConfig::getConfig('frontend');
        return $this->responseObjectData($configs);
    }

    
    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return LayoutResource|JsonResponse
     */
    public function show(Request $request, $id)
    {
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return LayoutResource|JsonResponse
     */
    public function update(Request $request)
    {
        $input = $request->only(['version']);
        foreach ($input as $key=>$value){
            $conf = SystemConfig::where('type', $key)->where('environment', 'frontend')->first();
            if($conf){
                $conf->update(['value' =>  $value]);
            }
           
        }
        
        return $this->respondSuccess();
    }

}
