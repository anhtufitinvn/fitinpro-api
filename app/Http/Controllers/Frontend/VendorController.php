<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Resources\Vendor\VendorResource;
use App\Http\Resources\Vendor\VendorResourceCollection;
use App\Http\Resources\Project\ProjectCollection;

use App\Models\Ver2\Vendor;
use App\Models\Ver2\Project;
use App\Models\Ver2\Object3d;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Repositories\Vendor\VendorRepository;

class VendorController extends ApiController
{
    protected $vendorRepository;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function __construct(VendorRepository $vendorRepository){
        $this->vendorRepository = $vendorRepository;
    }

    public function index()
    {
        $categories = $this->vendorRepository->getAll();
        
        return new VendorResourceCollection($categories);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {      
        $slug = $request->get('code');       
        if($slug){
            if(Vendor::where('code', $slug)->first()){
                return $this->responseError('Code has already existed', 'DATA_EXISTS');
            }
        }else {
            $slug = \App\Helpers\Utils::slugifyFromModel(new Vendor, $request->get('name'));
        }
        $input = $request->all();
        $input['code'] = $slug;

        $vendor = $this->vendorRepository->create($input);

        return (new VendorResource($vendor));
    }

    /**
     * Display the specified resource.
     *
     * @param Vendor $vendor
     * @return VendorResource
     */
    public function show($key)
    {
        $vendor = $this->vendorRepository->find($key);
        if(!$vendor) {
            return $this->responseError('Vendor not found');
        }
        return new VendorResource($vendor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Vendor $vendor
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        $input =  $request->only(['name', 'image64', 'status', 'image', "email"]);
        $vendor = $this->vendorRepository->update($key, $input);
        if(!$vendor) {
            return $this->responseError('Vendor not found');
        }

        return (new VendorResource($vendor));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Vendor $vendor
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        $vendor = $this->vendorRepository->delete($key);
        if(!$vendor) {
            return $this->responseError('Vendor not found');
        }

        return $this->respondSuccess();
    }

    public function getObjectByName(Request $request, $name)
    {
        
        $vendors = Object3d::where('vendor', $name)->get();
        $vendor = Vendor::where('code', $name)->first();
        return response()->json([
            'code' => 0,
            'message' => 'Success',
            'data' => [
                'vendor' => $vendor->only(['name', 'code', 'status', 'email']),
                'list' => $vendors->map->only(['thumbUrl', 'name'])
            ]
        ]);

    }
}

