<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Resources\Material\MaterialResource;
use App\Http\Resources\Material\MaterialResourceCollection;

use App\Models\Ver2\Material;

use App\Repositories\Material\MaterialRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Support\Facades\Storage;

class MaterialCategoryController extends ApiController
{
    protected $materialRepository;

    public function __construct(MaterialRepository $materialRepository){
        $this->materialRepository = $materialRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    
    public function index(Request $request)
    {
        $data = [
            ['name' => 'Floor', 'code' => 'floor'],
            ['name' => 'Wall', 'code' => 'wall'],
        ];

        return response()->json([
            'code' => 0,
            "message" => "success",
            'data' => [
                'list' => $data
            ]
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        return response()->json(['message' => 'In construction....']);
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'name' =>  'required'
        ]);
        if($validator->fails()){
            return $this->responseError('Name required', "INVALID");   
        }
        $type = $request->get('attrs')['type'] ?? null;
        
        $category = $request->get('attrs')['category'] ?? null;
        $code = \App\Helpers\Utils::slugifyFromModel(new Material, $type . ' ' . $category . ' ' . $request->get('name'));
        $input['code'] = $code;
        $material = $this->materialRepository->create($input);

        return (new MaterialResource($material));
    }


    /**
     * Display the specified resource.
     *
     * @param Material $category
     * @return MaterialResource
     */
    public function show($key)
    {
        return response()->json(['message' => 'In construction....']);
        $key = strtolower($key);
        $material = Material::where('_id', $key)->orWhere('code', $key)->first();
        if(!$material) {
            return $this->responseError('Material not found');
        }
        return new MaterialResource($material);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Material $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        return response()->json(['message' => 'In construction....']);
        $key = strtolower($key);
        $material = $this->materialRepository->update($key, $request->except(['code']));
        if(!$material) {
            return $this->responseError('Material not found');
        }
        
        return (new MaterialResource($material));
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Material $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        return response()->json(['message' => 'In construction....']);
        $material = $this->materialRepository->delete($key);
        if(!$material) {
            return $this->responseError('Material not found');
        }
        return $this->respondObjectDeleted($material->_id);
    }
}
