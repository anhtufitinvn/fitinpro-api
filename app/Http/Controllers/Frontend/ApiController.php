<?php

namespace App\Http\Controllers\Frontend;

use App\Traits\JsonRespondController;
use App\Http\Controllers\Controller;

use App\Helpers\Utils;
use Illuminate\Support\Facades\Cache;
use App\Models\Ver2\Attachment;
use App\Models\Ver2\Object3d;
use App\Models\Ver2\Bundle;
use \Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Models\Ver2\File;
use App\Events\UploadEvent;

class ApiController extends Controller
{
    use JsonRespondController;


    /**
     * Return generic json response with the given data.
     *
     * @param $data
     * @param int $statusCode
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respond($data, $statusCode = 200, $headers = [])
    {
        return response()->json($data, $statusCode, $headers);
    }

    /**
     * Respond with success.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondSuccess($message = 'Sucess')
    {
        return $this->respond([
            'code' => 0,
            'data' => [
                   
            ],
            'message' => 'Success'
        ]);
    }

    /**
     * Respond with created.
     *
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondCreated($data)
    {
        return $this->respond($data, 200);
    }

    /**
     * Respond with no content.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondNoContent()
    {
        return $this->respond(null, 204);
    }

    /**
     * Respond with error.
     *
     * @param $message
     * @param $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondError($message, $statusCode)
    {
        return $this->respond([
            'errors' => [
                'message' => $message,
                'status_code' => $statusCode
            ]
        ], $statusCode);
    }

    /**
     * Respond with unauthorized.
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondUnauthorized($message = 'Unauthorized')
    {
        return $this->respondError($message, -401);
    }

    /**
     * Respond with forbidden.
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondForbidden($message = 'Forbidden')
    {
        return $this->respondError($message, -1);
    }

    /**
     * Respond with not found.
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondNotFound($message = 'Not Found')
    {
        return $this->respondError($message, -1);
    }

    /**
     * Respond with failed login.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondFailedLogin()
    {
        return $this->respond([
            'errors' => [
                'email or password' => 'is invalid',
            ]
        ], -401);
    }

    /**
     * Respond with internal error.
     *
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondInternalError($message = 'Internal Error')
    {
        return $this->respondError($message, 500);
    }

    public function heartbreak(){
        return response()->json(['error' => false]);
    }

    public function sortHandle($oQuery,$sorts){
        if(empty($sorts)) return $oQuery;

        $arrSorts = explode(',',$sorts);
        foreach ($arrSorts as $sort) {
            $arrKeys = explode(':',$sort);

            $sort_condition = !empty($arrKeys[1]) ?  $arrKeys[1] : 'desc';

            $oQuery = $oQuery->orderBy($arrKeys[0],$sort_condition);
        }


        return $oQuery;
    }

    /**
     * Sends a response that the object has been deleted, and also indicates
     * the id of the object that has been deleted.
     *
     * @param  int $id
     * @return JsonResponse
     */
    public function respondObjectDeleted($id)
    {
        return response()->json([
            'code' => 0,
            'message' => 'success',
            'data' => [
                'id' => $id,
                'deleted' => true
            ]
        ]);
    }



    public function responseData($data, $code = 0){
        
        return response()->json([
            'code' => $code,
            'message' => 'success',
            'data' => [
                'list' => $data
            ]
        ]);
    }

    public function responseObjectData($data, $code = 0){
        
        return response()->json([
            'code' => $code,
            'message' => 'success',
            'data' => $data
        ]);
    }

    public function responseError($message, $error_code = 'NOT_FOUND', $code = -1){
        return response()->json([        
            'code' => $code,
            'message' => $message,
            'data' => [
                'error_code' => $error_code
            ]
        ]);
    }

    public function clearCache($class){
        Cache::tags($class::CACHE_TAG)->flush();
    }

    protected function saveFileHistory($data, $driver = NULL, $obj = NULL){
        $data['owner'] = Auth::user()->email;
        $file = File::create($data);

        if($driver && $obj){
            $this->uploadDriver($data['name'] ?? NULL, $driver, $data['path'], Storage::disk($driver)->url($data['path']),$obj );
        }
        return $file;
    }


    public function uploadDriver($file, $driver, $path, $url , $instance = NULL){
        return uploadDriver($file, $driver, $path, $url , $instance);
    }
}
