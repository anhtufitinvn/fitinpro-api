<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Resources\Brand\BrandResource;
use App\Http\Resources\Brand\BrandResourceCollection;
use App\Http\Resources\Project\ProjectCollection;

use App\Models\Ver2\Brand;
use App\Models\Ver2\Project;


use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Repositories\Brand\BrandRepository;

class BrandController extends ApiController
{
    protected $brandRepository;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function __construct(BrandRepository $brandRepository){
        $this->brandRepository = $brandRepository;
    }

    public function index()
    {
        $categories = $this->brandRepository->getAll();
        
        return new BrandResourceCollection($categories);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {      
        $slug = $request->get('code');       
        if($slug){
            if(Brand::where('code', $slug)->first()){
                return $this->responseError('Code has already existed', 'DATA_EXISTS');
            }
        }else {
            $slug = \App\Helpers\Utils::slugifyFromModel(new Brand, $request->get('name'));
        }
        $input = $request->all();
        $input['code'] = $slug;

        $brand = $this->brandRepository->create($input);

        return (new BrandResource($brand));
    }

    /**
     * Display the specified resource.
     *
     * @param Brand $brand
     * @return BrandResource
     */
    public function show($key)
    {
        $brand = $this->brandRepository->find($key);
        if(!$brand) {
            return $this->responseError('Brand not found');
        }
        return new BrandResource($brand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Brand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        $input =  $request->only(['name', 'image64', 'status', 'image']);
        $brand = $this->brandRepository->update($key, $input);
        if(!$brand) {
            return $this->responseError('Brand not found');
        }

        return (new BrandResource($brand));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $brand
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        $brand = $this->brandRepository->delete($key);
        if(!$brand) {
            return $this->responseError('Brand not found');
        }

        return $this->respondSuccess();
    }
}
