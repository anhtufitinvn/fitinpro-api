<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\CMS\CategoryStoreRequest;
use App\Http\Requests\CMS\CategoryUpdateRequest;
use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\Category\CategoryResourceCollection;
use App\Http\Resources\Project\ProjectCollection;

use App\Models\Ver2\Category;
use App\Models\Ver2\Project;
use App\Models\Ver2\Attachment;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Repositories\Category\CategoryRepository;

class CategoryController extends ApiController
{
    protected $categoryRepository;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function __construct(CategoryRepository $categoryRepository){
        $this->categoryRepository = $categoryRepository;
    }

    public function index(Request $request)
    {
        $params = $request->all();
        $params['published'] = true;
        $categories = $this->categoryRepository->getAll($params);
        
        return new CategoryResourceCollection($categories);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {      
        $slug = $request->get('code');       
        if($slug){
            if(Category::where('code', $slug)->first()){
                return $this->responseError('Code has already existed', 'DATA_EXISTS');
            }
        }else {
            $slug = \App\Helpers\Utils::slugifyFromModel(new Category, $request->get('name'));
        }
        $input = $request->all();
        $input['code'] = $slug;

        $category = $this->categoryRepository->create($input);

        return (new CategoryResource($category));
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return CategoryResource
     */
    public function show($key)
    {
        $category = $this->categoryRepository->find($key);
        if(!$category) {
            return $this->responseError('Category not found');
        }
        return new CategoryResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        $input =  $request->only(['name', 'image64', 'status', 'image']);
        $category = $this->categoryRepository->update($key, $input);
        if(!$category) {
            return $this->responseError('Category not found');
        }

        return (new CategoryResource($category));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        $category = $this->categoryRepository->delete($key);
        if(!$category) {
            return $this->responseError('Category not found');
        }

        return $this->respondSuccess();
    }
}
