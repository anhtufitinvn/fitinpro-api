<?php

namespace App\Http\Controllers\Frontend;


use App\Http\Resources\Color\ColorGroupResource;
use App\Http\Resources\Color\ColorGroupResourceCollection;
use Illuminate\Support\Facades\Validator;
use App\Models\Ver2\ColorGroup;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use App\Repositories\Color\ColorGroupRepository;

class ColorGroupController extends ApiController
{
    protected $colorGroupRepository;

    public function __construct(ColorGroupRepository $colorGroupRepository){
        $this->colorGroupRepository = $colorGroupRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $colors = $this->colorGroupRepository->getAll();

        return new ColorGroupResourceCollection($colors);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
       
        $input = $request->all();
        $validator = Validator::make($input, [
            'name'   => 'required'
        ]);
        if($validator->fails()){
            return $this->responseError('File Required', "INVALID");   
        }
        $code = \App\Helpers\Utils::slugifyFromModel(new ColorGroup, $request->get('name')); 
        $input['code'] = $code;
        $color = $this->colorGroupRepository->create($input);

        return new ColorGroupResource($color);
    }
  

    /**
     * Display the specified resource.
     *
     * @param Brand $category
     * @return BrandResource
     */
    public function show($key)
    {
        $key = strtolower($key);
        $color = $this->colorGroupRepository->find($key);
        if(!$color) {
            return $this->responseError('Color Group not found');
        }
        return new ColorGroupResource($color);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $key)
    {
        $key = strtolower($key);
        $color = $this->colorGroupRepository->update($key, $request->all());
        if(!$color) {
            return $this->responseError('Color Group not found');
        }
        
        return (new ColorGroupResource($color));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Brand $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($key)
    {
        $key = strtolower($key);
        $color = $this->colorGroupRepository->delete($key);
        if(!$color) {
            return $this->responseError('Color Group not found');
        }
        return $this->respondObjectDeleted($color->_id);
    }
}
