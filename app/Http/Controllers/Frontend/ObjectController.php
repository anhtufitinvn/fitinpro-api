<?php

namespace App\Http\Controllers\Frontend;

use App\FitIn\CacheManager;
use App\Http\Resources\CMS\ObjectResource;
use App\Http\Resources\CMS\SkuResource;
use App\Http\Resources\Layout\LayoutResource;
use App\Models\Ver2\Object3d;
use App\Models\Ver2\V360;
use Illuminate\Support\Facades\Storage;
use App\Models\Ver2\Sku;
use App\Models\Ver2\User;
use App\Models\Ver2\Vendor;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Object3d\Object3dRepository;

use App\Services\UploadService;


class ObjectController extends ApiController
{

   protected $upload_service, $object3dRepository;


   public function __construct(UploadService $service, Object3dRepository $object3dRepository)
   {
      $this->upload_service = $service;
      $this->object3dRepository = $object3dRepository;
   }

   /**
    * Display a listing of the resource.
    *
    * @return AnonymousResourceCollection
    */
    public function index(Request $request)
    {
       $params = $request->all();
       $params['type'] = $request->get('type');
       $params['published'] = true;
       $objects = $this->object3dRepository->getAllForPlugin($params);
       return new \App\Http\Resources\Asset\ObjectResourceCollection($objects);
    }
 
    public function getItems(Request $request)
    {
       $params = $request->all();
       $params['type'] = 'item';
       $params['published'] = true;
       $objects = $this->object3dRepository->getAll($params);
       return new \App\Http\Resources\Asset\ObjectResourceCollection($objects);
    }
 
    public function getTTS(Request $request)
    {
       $params = $request->all();
       $params['type'] = 'tts';
       $params['published'] = true;
       $objects = $this->object3dRepository->getAll($params);
       return new \App\Http\Resources\Asset\ObjectResourceCollection($objects);
    }

   // public function myObjects(Request $request)
   // {
   //     $objects = $this->object3dRepository->getMyObjects();
   //     return new \App\Http\Resources\Asset\ObjectResourceCollection($objects);
   // }

   /**
    * Store a newly created resource in storage.
    *
    * @param Request $request
    * @return Response
    */
   public function store(Request $request)
   {
      $input = request()->all();

      $name = strtolower($request->get('name'));
      $exists = Object3d::where('name', $name)->first();
      if ($exists) {
         return $this->responseError('Key name duplicated', 'DATA_EXISTS');
      }
      if (auth()->id()) {
         $input['ownerID'] = auth()->user()->auth_id;
      }
      $object = $this->object3dRepository->create($input);

      return new \App\Http\Resources\Asset\ObjectResource($object);
   }

   public function createByUploadPrefab(Request $request)
   {
      $driver = Object3d::UNITY_DRIVER;

      $type = Object3d::DIR_NAME;
      $user = Auth::user();

      $file = $request->file('file');
      $validator = Validator::make($request->all(), [
         'file' => 'required|file'
      ]);
      if ($validator->fails()) {
         return $this->responseError('File Invalid', "INVALID");
      }
      $unity_type = $request->get('type');
      if (!$unity_type) {
         return $this->responseError('You must choose type "layout" or "furniture"', "INVALID");
      }

      $arrFileName = pathinfo($file->getClientOriginalName());
      $filename = strtolower($arrFileName['filename']);
      $filenameArray = explode('_', $filename);

      $brand = $filenameArray[0];
      $name = $filename;
      $vendor = $brand;
      $groupName = $filenameArray[1] ? $brand . '_' . $filenameArray[1] : $brand;
      $object = Object3d::where('name', $name)->first();
      if (!$object) {
         $input = [
            'ownerID' => $user->auth_id,
            'name' => $name,
            'brand' => $brand,
            'vendor' => $vendor,
            'status' => Object3d::PUBLISHED,
            'groupName' => $groupName,
            'isPrimary' => true,
            'dimension' => [
               'w' => 0,
               'h' => 0,
               'd' => 0
            ],
            'attributes' => [],
            "ar" => [
               "ios" => null,
               "android" => null
            ],
            'materials' => [],
            "view360" => []

         ];
         $object = $this->object3dRepository->create($input);
      }

      if ($file && $file->isValid()) {

         $file_base_name = strtolower($arrFileName['basename']);
         $time = Carbon::now()->timestamp;


         if ($unity_type == 'furniture') {
            $path = Object3d::UNITY_WEB_DIR . "/furniture/$brand";
         } elseif ($unity_type == 'layout') {
            $path = Object3d::UNITY_WEB_DIR . "/layout/$brand/" . $filenameArray[1];
         } else {
            return $this->responseError('type must be "layout" or "furniture"', "INVALID");
         }

         $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
         $object->prefab = "$path/$file_base_name";
         $object->prefabType = $unity_type;
         $object->save();

         /**
          * Save History
          */
         $dataFile = [
            'size' => Storage::disk($driver)->size($upload),
            'fileType' => 'prefab',
            'attachmentType' => get_class($object),
            'attachmentKey' => $object->name,
            'path' => $upload,
            'name' => $file_base_name,
            'originName' => $file_base_name,
            'revision' => $time
         ];
         $this->saveFileHistory($dataFile, $driver, $object);

         return new \App\Http\Resources\Asset\ObjectResource($object);

      } else {
         return $this->responseError('Lỗi quá trình upload');

      }

   }


   public function createByUploadEditorPro(Request $request)
   {
      $driver = config('fitin.editor.filesystem_driver');
      $type = Object3d::DIR_NAME;
      $user = Auth::user();

      $file = $request->file('file');
      $validator = Validator::make($request->all(), [
         'file' => 'required|file'
      ]);
      if ($validator->fails()) {
         return $this->responseError('File Invalid', "INVALID");
      }

      $arrFileName = pathinfo($file->getClientOriginalName());
      $filename = strtolower($arrFileName['filename']);
      $filenameArray = explode('_', $filename);

      $brand = $filenameArray[0];
      $name = $filename;
      $vendor = $brand;
      $groupName = $filenameArray[1] ? $brand . '_' . $filenameArray[1] : $brand;
      $object = Object3d::where('name', $name)->first();
      if (!$object) {
         $input = [
            'ownerID' => $user->auth_id,
            'name' => $name,
            'brand' => $brand,
            'vendor' => $vendor,
            'status' => Object3d::PUBLISHED,
            'groupName' => $groupName,
            'isPrimary' => true,
            'dimension' => [
               'w' => 0,
               'h' => 0,
               'd' => 0
            ],
            'attributes' => [],
            "ar" => [
               "ios" => null,
               "android" => null
            ],
            'materials' => [],
            "view360" => []

         ];
         $object = $this->object3dRepository->create($input);
      }

      if ($file && $file->isValid()) {
         $time = Carbon::now()->timestamp;
         $file_base_name = strtolower($arrFileName['basename']);
         $path = Object3d::ASSET_DIR . "/$type/$brand/$name/$time/" . md5_file($file);

         $upload = Storage::disk($driver)->putFileAs($path, $file, $file_base_name);
         $object->editorProAsset = "$path/$file_base_name";
         $object->save();

         /**
          * Save History
          */
         $dataFile = [
            'size' => Storage::disk($driver)->size($upload),
            'fileType' => 'prefab',
            'attachmentType' => get_class($object),
            'attachmentKey' => $object->name,
            'path' => $upload,
            'name' => $file_base_name,
            'originName' => $file_base_name,
            'revision' => $time
         ];
         $this->saveFileHistory($dataFile, $driver, $object);

         return new \App\Http\Resources\Asset\ObjectResource($object);

      } else {
         return $this->responseError('Lỗi quá trình upload');

      }

   }

   /**
    * Display the specified resource.
    *
    * @param Request $request
    * @param int $id
    * @return LayoutResource|JsonResponse
    */
   public function show(Request $request, $key)
   {
      $key = strtolower($key);
      $object = $this->object3dRepository->find($key);
      if (!$object) {
         return $this->responseError('Object3D not found', 'NOT_FOUND');
      }
      return new \App\Http\Resources\Asset\ObjectResource($object);

   }

   public function showGLTF(Request $request, $key)
   {
      $key = strtolower($key);
      $object = $this->object3dRepository->find($key);
      if (!$object) {
         return $this->responseError('Object3D not found', 'NOT_FOUND');
      }
      return new \App\Http\Resources\Asset\ObjectGLTFResource($object);

   }

   public function getObjectForEcom(Request $request, $key)
   {
      $key = strtolower($key);
      $object = $this->object3dRepository->find($key);
      if (!$object) {
         return $this->responseError('Object3D not found', 'NOT_FOUND');
      }
      return new \App\Http\Resources\Asset\FitinObjectResource($object);

   }

   public function getListObjectKeyWhereHas360ForEcom(Request $request)
   {
      $v360s = V360::groupBy('sku')->get();
      $array_sku = $v360s->pluck('sku')->toArray();
      $objects = Object3d::whereIn('name', $array_sku)->get();
      $object_keys = $objects->pluck('name')->toArray();
      return response()->json([
         'code' => 0,
         "message" => "success",
         'data' => $object_keys
      ]);
   }

   public function getListObjectWhereHasIOS(Request $request)
   {
      $objects = Object3d::whereNotNull('ar.ios')->get();
      $data = $objects->map->only(['name', 'ar']);
      return response()->json([
         'code' => 0,
         "message" => "success",
         'data' => $data
      ]);
   }


   public function showNew(Request $request, $key)
   {
      $key = strtolower($key);
      $object = $this->object3dRepository->find($key);
      if (!$object) {
         return $this->responseError('Object3D not found', 'NOT_FOUND');
      }
      return new \App\Http\Resources\Asset\NewObjectResource($object);

   }


   /**
    * Update the specified resource in storage.
    *
    * @param Request $request
    * @param int $id
    * @return LayoutResource|JsonResponse
    */
   public function update(Request $request, $key)
   {
      $input = request()->except(['name', 'sku', 'fbx', 'ownerID', 'unity', 'editorPro', 'ar']);
      $key = strtolower($key);
      $object = $this->object3dRepository->update($key, $input);

      if (!$object) {
         return $this->responseError('Object3d not found');
      }
      return new \App\Http\Resources\Asset\ObjectResource($object);
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param int $id
    * @return JsonResponse|Response
    */
   public function destroy($key)
   {
      $key = strtolower($key);
      $object = $this->object3dRepository->delete(strtolower($key));

      if (!$object) {
         return $this->responseError('Object3d not found');
      }


      return $this->respondObjectDeleted($object->id);
   }


   public function getObjectByMerchant(Request $request)
   {
      $object_key = $request->input('object_key');
      $merchant_code = $request->input('merchant_code');

      $limit = (int)$request->get('limit');

      $oQuery = Object3d::orderBy('updated_at', 'DESC');

      if ($object_key) {
         $arrObjectKey = explode(',',$object_key);
         $oQuery->whereIn('name',$arrObjectKey);
      }

      if($limit){
         $oQuery->limit($limit);
      }

      if($merchant_code && $merchant_code != 'all'){
         $oQuery::where('vendor', $merchant_code);
      }

      $oResult = $oQuery->get();

      $arrResult = [];

      $list = $oResult ? $oResult->map->only(['vendor', 'imageUrl', 'name', 'updated_at'])->toArray() : [];

      $new_list = array_map(function ($e) {
         return [
            'merchant' => $e['vendor'],
            'image' => $e['imageUrl'],
            'key_name' => $e['name'],
            'updated_at' => $e['updated_at']
         ];
      }, $list);

      $arrResult['list'] = $new_list;

      return $this->responseObjectData($arrResult);
   }

   public function getObjectIosFile(Request $request, $key)
   {
      $object = $this->object3dRepository->find($key);
      if (!$object) {
         return $this->responseError('Object3D not found', 'NOT_FOUND');
      }
      if (
         empty($object->ar)
         || (is_array($object->ar) && empty($object->ar['ios']))
         || (!is_array($object->ar))
      ) {
         return $this->responseError('File not exists', 'NOT_FOUND');
      }

      $path = $object->ar['ios'];
      $driver = config('fitin.asset.filesystem_driver');
      $exists = Storage::disk($driver)->exists($path);
      if (!$exists) {
         return $this->responseError('File not exists', 'NOT_FOUND');
      }
      return response()->json([
         'code' => 0,
         'message' => 'Success',
         'data' => [
            'url' => Storage::disk($driver)->url($path)
         ]
      ]);
   }

   public function getListForEcom()
   {
      $objects = $this->object3dRepository->getAll();
      return new \App\Http\Resources\Asset\NewObjectResourceCollection($objects);
   }
}
