<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;


class IsMerchant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $oJWT = JWTAuth::parseToken()->getPayload();


        if(!empty($oJWT['merchant_id'])){
            $request->attributes->add(['merchant_id' => $oJWT['merchant_id']]);
        }

        return $next($request);
    }
}
