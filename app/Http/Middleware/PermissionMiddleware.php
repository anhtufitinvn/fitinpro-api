<?php

namespace App\Http\Middleware;

use Closure;
use App\Exceptions\UnauthorizedException;

class PermissionMiddleware
{
    public function handle($request, Closure $next, $permission)
    {
        if (app('auth')->guest()) {
            return response()->json([
                'error' => 1,
                'error_code' => 'UNAUTHORIZED_REQUEST',
                'message' => __('Vui lòng đăng nhập để truy cập tài nguyên!'),
                'code' => 401
            ]);
            //throw UnauthorizedException::notLoggedIn();
        }

        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);
        
        $user = app('auth')->user();   
        foreach ($permissions as $permission) {
            
            $user_permissions = $user->getAllPermissions()->pluck('name')->toArray();
            
            if (in_array($permission, $user_permissions)) {
                return $next($request);
            }
        }
        return response()->json([
            'error' => 1,
            'error_code' => 'UNAUTHORIZED_REQUEST',
            'message' => __('Bạn không có quyền truy cập thao tác này!'),
            'code' => 401
        ]);
        //throw UnauthorizedException::forPermissions($permissions);
    }
}
