<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;


class LoginMerchant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(request()->header('partner-token') && request()->header('partner-token') == config()->get('fitin.partner_token')){         
            return $next($request);
        }else{
            return response()->json([
                'error' => true,
                'message' => 'Unauthorized access!'
            ],401);
        }
        
    }
}
