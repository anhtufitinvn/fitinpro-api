<?php

namespace App\Http\Middleware;

use Closure;


class EditorPrivateToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(request()->header('editor-token') && request()->header('editor-token') == config()->get('fitin.editor_token')){         
            
            return $next($request);
        }else{
            return response()->json([
                'code' => -1,
                'message' => 'Unauthorized!',
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        }
        
    }
}
