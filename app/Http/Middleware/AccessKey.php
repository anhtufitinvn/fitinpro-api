<?php

namespace App\Http\Middleware;

use Closure;

class AccessKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(request()->header('access-key') &&  in_array(request()->header('access-key'), config('external-token') )){         
           
            return $next($request);
        }else{
            return response()->json([
                'code' => -1,
                'message' => 'Unauthorized!',
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ]);
        }
        
    }
}
