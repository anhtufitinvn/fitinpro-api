<?php

namespace App\Http\Requests\Layout;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLayoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:255',
            'layout_json' => '',
            'tags' => 'max:255',
            'image' => 'max:255',
            'parent' => 'boolean',
            //'status' => 'boolean',
            'description' => 'max:500',
            'project_id' => 'max:11',
            'style_id'=>'max:11',
            'estimated_price',
            'attachment_id'
        ];
    }
}
