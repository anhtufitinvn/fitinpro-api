<?php

namespace App\Http\Resources\EditorPro;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;

class LayoutResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
           '_id' => $this->_id,
            'project' => $this->project,
            'style' => $this->style,
            'display_name' => $this->display_name,
            'name' => $this->name,
            'imageUrl' => $this->imageUrl,
            'fbx' => $this->fbx,
            'fbxUrl' => $this->fbxUrl,
            'json' => $this->json,
            'jsonUrl' => $this->jsonUrl,
            'prefab' => $this->prefab,
            'prefabUrl' => $this->prefabUrl,
            'dataJson'  => $this->dataJson

        ];
        
        if(!empty($this->dependencies) && count($this->dependencies)){
            $data['dependencies'] = $this->dependencies;
        }
        return $data;
    }

    /**
     * Get additional data that should be returned with the resource array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function with($request)
    {
        return [
            
            'code' => 0,
            'message' => 'Success'
        ];
    }
}
