<?php

namespace App\Http\Resources\CMS;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SkuResourceCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\CMS\SkuResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,     
            'message' => 'Success', 
        ];
    }
}
