<?php

namespace App\Http\Resources\CMS;

use Illuminate\Http\Resources\Json\JsonResource;

class SkuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        //$arrDefault = (array)$this;

//        $arrResult = [
//            'material_image' => $this->materialImage ? $this->materialImage->getUrl() : null,
//            'material_image_thumbs' => $this->materialImage ? $this->materialImage->getMultiThumb() : null,
//            'color_image' => $this->colorImage ? $this->colorImage->getUrl() : null,
//            'color_image_thumbs' => $this->colorImage ? $this->colorImage->getMultiThumb() : null,
//            'image' => $this->attachment ? $this->attachment->getUrl() : null,
//            'thumbs' => $this->attachment ? $this->attachment->getMultiThumb() : null,
//        ];
//
//        $arrResult = array_merge($arrDefault, $arrResult);

        return parent::toArray($request);
    }

}
