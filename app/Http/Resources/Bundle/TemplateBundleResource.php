<?php

namespace App\Http\Resources\Bundle;

use Illuminate\Http\Resources\Json\JsonResource;

class TemplateBundleResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [     
            '_id' => $this->_id,
            'userId' => $this->userId,  
            'name' => $this->name,
            'dataJson' => $this->dataJson,
            'image' => $this->image
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
