<?php

namespace App\Http\Resources\Bundle;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BundleCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\Bundle\BundleResource';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
