<?php

namespace App\Http\Resources\Bundle;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Support\Facades\Storage;

class BundleRenderResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        $data = [       
           
            'key_name' => $this->key_name,
            'dataRender' => $this->dataRender
    
        ];


        return $data;
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }

}
