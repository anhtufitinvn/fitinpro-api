<?php

namespace App\Http\Resources\Bundle;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Support\Facades\Storage;

class FitinBundleResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        $data = [       
            '_id' => $this->_id,
            'name' => $this->name,
            'category' => $this->category,
            'vendor' => $this->vendor,
            'room' => $this->room,
            'brand' => $this->brand,
            'key_name' => $this->key_name,
            'status' => $this->status,
            //'data_json' => $this->data_json,
            'item_list' => $this->item_list,
            //'sort' => $this->sort,
            //'isAndroid' => $this->isAndroid,
            //'isEditor' => $this->isEditor,
            //'isIos' => $this->isIos,
            //'imagePath' => $this->imagePath,
            'image' => $this->image,
            //'gltfUrl' => $this->gltfUrl,
            //'gltf' => $this->gltf,
            //'position' => $this->position,
            //'positionXYZ' => $this->positionXYZ,
            //'rotation' => $this->rotation,
            //'rotationXYZ' => $this->rotationXYZ,
            //'scale' => $this->scale,
            //'scaleXYZ' => $this->scaleXYZ,
            'settings' => $this->settings,
            'attrs' => $this->attrs
            //'editorPro' => $this->editorPro,
           // 'owner' => $this->owner? $this->owner->only(['name', 'email', 'auth_id']) : null
        ];

        // if(is_array($this->attrs)){
        //     $data = array_merge($data, $this->attrs);
        // }

        return $data;
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }

}
