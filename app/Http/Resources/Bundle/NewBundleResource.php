<?php

namespace App\Http\Resources\Bundle;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Support\Facades\Storage;

class NewBundleResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        $item_list = $this->item_list;
        if(is_array($item_list)){
            foreach ($item_list as &$item){
                if(!empty($item['gltf'])){
                    $driver = config('fitin.asset.filesystem_driver');
                    $item['gltfUrl'] = Storage::disk($driver)->url($item['gltf']);  
                }

                if(!empty($item['fbx'])){
                    $driver = config('fitin.asset.filesystem_driver');
                    $item['fbxUrl'] = Storage::disk($driver)->url($item['fbx']);  
                }
                
            }
        }
        
        return [       
            '_id' => $this->_id,
            'name' => $this->name,
            'category' => $this->category,
            'vendor' => $this->vendor,
            'room' => $this->room,
            'brand' => $this->brand,
            'key_name' => $this->key_name,
            'status' => $this->status,
            'data_json' => $this->data_json,
            'item_list' => $item_list,
            //'sort' => $this->sort,
            //'isAndroid' => $this->isAndroid,
            //'isEditor' => $this->isEditor,
            //'isIos' => $this->isIos,
            'imagePath' => $this->imagePath,
            'image' => $this->image,
            'gltfUrl' => $this->gltfUrl,
            'gltf' => $this->gltf,
            //'position' => $this->position,
            //'positionXYZ' => $this->positionXYZ,
            //'rotation' => $this->rotation,
            //'rotationXYZ' => $this->rotationXYZ,
            //'scale' => $this->scale,
            //'scaleXYZ' => $this->scaleXYZ,
            'settings' => $this->settings,
            'attrs' => $this->attrs,
            'editorPro' => $this->editorPro
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }

}
