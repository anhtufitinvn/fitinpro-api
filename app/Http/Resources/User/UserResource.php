<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [     
            'userId' => $this->auth_id,  
            'name' => $this->name,
            'email' => $this->email,
            'avatar' => $this->avatar,

            'phone' => $this->phone,
            
            'isAdmin' => $this->hasRole('admin'),
            'roles' => $this->roles->map->only(['name', 'title'])
           // 'templateBundle' => $this->user_template_bundle->count() ?  $this->user_template_bundle->map->only(['dataJson', 'bundle', 'image', '_id']) : null
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
