<?php

namespace App\Http\Resources\Room;

use Illuminate\Http\Resources\Json\JsonResource;

class RoomResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data =  [
            '_id' => $this->_id,
            'version' => $this->version,
            //'room_id' => $this->room_id,
            'parent_id' => $this->parent_id,
            //'owner_id' => $this->owner_id,
            'name' => $this->name,
            'type' => $this->type,
            'code' => $this->code,
            'width' => $this->width,
            'length' => $this->length,
            'height' => $this->height,
            'geometry' => $this->geometry,
            'wall_thickness' => $this->wall_thickness,
            'shape_id' => $this->shape_id,
            'shape_name' => $this->shape_name,
            'wall_materials' => $this->wall_materials,
            'floor_materials' => $this->floor_materials,
            'doors' => $this->doors,
            'windows' => $this->windows,
            // 'dataJson' => $this->dataJson,
            'imageUrl' => $this->imageUrl,
            'settings' => $this->settings,
            'gltfUrl' => $this->gltfUrl
        ];

        return $data;
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
