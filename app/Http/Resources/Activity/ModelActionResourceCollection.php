<?php

namespace App\Http\Resources\Activity;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ModelActionResourceCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\Activity\ModelActionResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,     
            'message' => 'Success', 
        ];
    }
}
