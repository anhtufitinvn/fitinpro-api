<?php

namespace App\Http\Resources\Asset;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Support\Facades\Storage;

class NewObjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        // $driver_ar = config('fitin.asset.filesystem_driver');
        // $ar = $this->ar;
        // if(!empty($this->ar) && is_array($this->ar)){
            
        //     $ar['iosUrl'] = !empty($ar['ios']) ? Storage::disk($driver_ar)->url($ar['ios']) : null;
        //     $ar['androidUrl'] = !empty($ar['android']) ? Storage::disk($driver_ar)->url($ar['android']) : null;
        // }
    
        return [
        //    "_id" => $this->_id,
        //    "dimension" => $this->dimension,
            "keywords" => $this->keywords,
            "vendor" => $this->vendor,
            "category" => $this->category,
            "project" => $this->project,
            "layout" => $this->layout,
            "attributes" => $this->attributes,
            "ownerID" => $this->ownerID,
            "owner" => $this->_owner ? $this->_owner->name : null,
            "name" => $this->name,
            "groupName" => $this->groupName,
            "brand" => $this->brand,
            "fbx" => $this->fbx,
            "fbxUrl" => $this->fbxUrl,
            //"prefab" => $this->prefab,
            //"prefabType" => $this->prefabType,
            //"prefabUrl" => $this->prefabUrl,
            "colorRange" => $this->colorRange,
            "ar" => $this->ar,
            "webAr" => $this->webAr,
            "unity" => $this->unity,
            "editorPro" => $this->editorPro,
            "desc" => $this->desc,
            "status" => $this->status,
        //    "isPrimary" => $this->isPrimary,
            "thumb" => $this->thumb,
            "thumbUrl" => $this->thumbUrl,
        //    "thumbMaterial" => $this->thumbMaterial,
            "thumbMaterialUrl" => $this->thumbMaterialUrl,
            "hdrp" => $this->hdrp,
        //    "sku" => $this->sku,
            "materials" => $this->materials,
        //    "view360" => $this->view360,
            "gltf" => $this->gltf,
            "gltfUrl" => $this->gltfUrl,
            //"dependency" => $this->dependency,
            //"enable_dimension" => $this->enable_dimension,
           // "isFloat" => $this->isFloat,
           // "isHole" => $this->isHole,
            //"isTTS" => $this->isTTS,
            //"anchor" => $this->anchor,
            //"editorProAsset" => $this->editorProAsset,
            //"editorProAssetUrl" => $this->editorProAssetUrl,
            //'position' => $this->position,
            //'rotation' => $this->rotation,
            //'scale' => $this->scale,
            //"renderable" => $this->renderable,
            "updated_at" => $this->updated_at
        ];

    }
    public function with($request)
    {
        return [
            'message' => 'Success',
            'code' => 0
        ];
    }
}
