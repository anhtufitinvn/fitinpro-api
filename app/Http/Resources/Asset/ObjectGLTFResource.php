<?php

namespace App\Http\Resources\Asset;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Support\Facades\Storage;

class ObjectGLTFResource extends JsonResource
{
    

    public function toArray($request)
    {   
        
        return [
            'object_key' => $this->name,
            'image' => $this->thumbUrl,
            'poster' => $this->thumbUrl,
            'glb' => $this->gltfUrl,
            'iosGlb' => ($this->webAr && isset($this->webAr['iosUrl'])) ? $this->webAr['iosUrl'] : null,
            'androidGlb' => ($this->webAr && isset($this->webAr['androidUrl'])) ? $this->webAr['androidUrl'] : null
        ];

    }
    
    public function with($request)
    {
        return [
            'message' => 'Success',
            'code' => 0
        ];
    }
}
