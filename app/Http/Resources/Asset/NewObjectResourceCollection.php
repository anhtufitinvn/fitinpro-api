<?php

namespace App\Http\Resources\Asset;

use Illuminate\Http\Resources\Json\ResourceCollection;

class NewObjectResourceCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\Asset\NewObjectResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'message' => 'Success',
            'code' => 0
        ];
    }
}
