<?php

namespace App\Http\Resources\Asset;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Support\Facades\Storage;

class FitinObjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        
        return [
            
                "dimension" => (is_array($this->attributes) && isset($this->attributes['dimension']) ) ? $this->attributes['dimension'] : null,
                "category" => $this->category,
                "name" => $this->name,
                "attributes" => $this->getAttribute('attributes'),
                "brand" => $this->brand,
                "ar" => $this->ar,        
                "webAr" => $this->webAr,
                "materials" => $this->materials,
            
                "v360" => $this->v360,
                "v360s" => $this->v360s
            ];
    }
    public function with($request)
    {
        return [
            'message' => 'Success',
            'code' => 0
        ];
    }
}
