<?php

namespace App\Http\Resources\Role;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RoleResourceCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\Role\RoleResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
                  
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,     
            'message' => 'Success', 
        ];
    }
}
