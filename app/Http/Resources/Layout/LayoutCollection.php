<?php

namespace App\Http\Resources\Layout;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LayoutCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\Layout\LayoutResource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,    
            'message' => 'Success',  
        ];
    }
}
