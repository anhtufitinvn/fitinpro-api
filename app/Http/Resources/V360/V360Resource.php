<?php

namespace App\Http\Resources\V360;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class V360Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'revision' => $this->revision,
            'brand' => $this->brand,
            'filename' => $this->filename,
            'vendor' => $this->vendor,
           // 'url' => Storage::url("view360/" . $this->brand . "/" . $this->sku . '/' . $this->revision. '/' . $this->filename)
           'url' => $this->imageUrl
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
