<?php

namespace App\Http\Resources\V360;

use Illuminate\Http\Resources\Json\ResourceCollection;

class V360ResourceCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\V360\V360Resource';

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,   
            'message' => 'Success',   
        ];
    }
}
