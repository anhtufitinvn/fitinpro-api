<?php

namespace App\Http\Resources\Material;

use Illuminate\Http\Resources\Json\JsonResource;

class MaterialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data =  [
            '_id' => $this->_id,
            'name' => $this->name,
            "code" => $this->code,
            "status" => $this->status,
            "hexColor" => $this->hexColor,
            "map" => $this->map ?? [],
            "normalMap" => $this->normalMap ?? [],
            "editorPro" => $this->editorPro ?? [],
            'attrs'  => $this->attrs,
            'imageUrl' => $this->imageUrl
        ];

        return $data;
    }

    public function with($request)
    {
        return [
            'code' => 0,  
            'message' => 'Success',    
        ];
    }
}
