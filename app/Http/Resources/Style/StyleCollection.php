<?php

namespace App\Http\Resources\Style;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StyleCollection extends ResourceCollection
{
    public $collects = 'App\Http\Resources\Style\StyleResource';
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'list' => $this->collection
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }
}
