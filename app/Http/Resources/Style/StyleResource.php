<?php

namespace App\Http\Resources\Style;

use Illuminate\Http\Resources\Json\JsonResource;

class StyleResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [       
            '_id' => $this->_id,
            'name' => $this->name,
            'status' => $this->status,
            'slug' => $this->slug,
            
            'image' => $this->image
        ];
    }

    public function with($request)
    {
        return [
            'code' => 0,
            'message' => 'Success',
        ];
    }

}
