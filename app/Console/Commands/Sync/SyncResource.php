<?php

namespace App\Console\Commands\Sync;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Models\Ver2\Project; 
use App\Models\Ver2\Style; 

use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

use App\Models\Ver2\FileDriver; 
use App\FitIn\Asset;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class SyncResource extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:resource {--type=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->option('type') ?? NULL;
        
        if($type == 'project'){
            $this->syncProject();
        }
        if($type == 'style'){
            $this->syncStyle();
        }
        
       
    }

    protected function syncProject(){
        $asset = new Asset();
        $ecom_projects = $asset->getEcomProjects();
        $driver = config('fitin.asset.filesystem_driver');
        foreach ($ecom_projects as $project){
            if(empty($project['name'])){
                continue;
            }
            $name = $project['name'];
            $code = $project['slug'] ?? \Illuminate\Support\Str::slug($name, "-");
            $data = [
                'name' => $name,
                'code' => $code,
                'status' => $project['status'] ?? 'pending'
            ];
            $project_model = Project::updateOrCreate(['code' => $code], $data);
            if(!$project_model->image && !empty($project['image'])){
                $thumb_url = $project['image'];
                $pathinfo = pathinfo($thumb_url);
                $basename = $pathinfo['basename'];
                if(file_valid($thumb_url)){
                    $file_ecom_content = file_get_contents($thumb_url);
                    $type = Project::DIR_NAME;
                    $time = \Carbon\Carbon::now()->timestamp;
                    $path = "$type/$code/$time/$basename";
                    $download = Storage::disk($driver)->put($path, $file_ecom_content);

                    $url = Storage::disk($driver)->url($path);
                    $project_model->image = $path;
                    $project_model->save();
                    uploadDriver($basename, $driver, $path, $url , $project_model);
                }else{
                    continue;
                }
            }
        }
    }

    protected function syncStyle(){
        $asset = new Asset();
        $ecom_styles = $asset->getEcomStyles();
        
        $driver = config('fitin.asset.filesystem_driver');
        foreach ($ecom_styles as $style){
            if(empty($style['name'])){
                continue;
            }
            $name = $style['name'];
            $code = $style['slug'] ?? \Illuminate\Support\Str::slug($name, "-");
            $data = [
                'name' => $name,
                'code' => $code,
                'status' => $style['status'] ?? 'pending'
            ];
            $style_model = Style::updateOrCreate(['code' => $code], $data);
            if(!$style_model->image && !empty($style['image'])){
                $thumb_url = $style['image'];
                $pathinfo = pathinfo($thumb_url);
                $basename = $pathinfo['basename'];
                if(file_valid($thumb_url)){
                    $file_ecom_content = file_get_contents($thumb_url);
                    $type = Style::DIR_NAME;
                    $time = \Carbon\Carbon::now()->timestamp;
                    $path = "$type/$code/$time/$basename";
                    $download = Storage::disk($driver)->put($path, $file_ecom_content);

                    $url = Storage::disk($driver)->url($path);
                    $style_model->image = $path;
                    $style_model->save();
                    uploadDriver($basename, $driver, $path, $url , $style_model);
                }else{
                    continue;
                }
            }
        }
    }
    
    
}
