<?php

namespace App\Console\Commands\Sync;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Models\Ver2\Object3d; 

use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

use App\Models\Ver2\FileDriver; 
use App\FitIn\Asset;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class SyncObjectThumb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:object:thumb {--page=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$this->syncFileFromExternalServerWithTime();
        $this->syncFileFromExternalServerWithPaginate();
    }

    protected function syncFileFromExternalServerWithTime(){
        $driver = config('fitin.editor.filesystem_driver');
        $object_collection  = (new Object3d)->getTable();
        $asset = new Asset();
        
        $from = \Carbon\Carbon::now()->subDays(1)->format('Y-m-d H:i:s');
        $to = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        $query = ['from' => $from, 'to' => $to];
        $ecom_objects = $asset->getEcomItemsWithThumb($query);

        foreach ($ecom_objects as $obj){

            $name = strtolower($obj['object_key']);
            $file_ecom_thumb_url = $obj['image'];
            
            $pathinfo = pathinfo($file_ecom_thumb_url);
            $basename = $pathinfo['basename'];

            $obj3d = Object3d::where('name',$name)->first();
            if(!$obj3d){
                continue;
            }
            
            $editorPro = $obj3d->getOriginal('editorPro') ?? [];
            if(!isset($ecom['thumb'])){
                              
                if(file_valid($file_ecom_thumb_url)){
                    $file_ecom_content = file_get_contents($file_ecom_thumb_url);
                    $type = Object3d::DIR_NAME;
                    $brand = $obj3d->brand ?? explode('_', $name)[0];
                    $time = \Carbon\Carbon::now()->timestamp;
                    $path = "$type/$brand/$name/" . Object3d::THUMB_EDITORPRO_DIR. "/$time/$basename";
                    $download = Storage::disk($driver)->put($path, $file_ecom_content);

                    $url = Storage::disk($driver)->url($path);
                    $editorPro['thumb'] = $path;
                    \DB::collection($object_collection)->where('name', $name)->update(['editorPro' => $editorPro]);
                    uploadDriver($basename, $driver, $path, $url , $obj3d);
                }else{
                    continue;
                }
                

            }else{
                if(!file_valid($file_ecom_thumb_url)){
                    continue;    
                }else{
                    $current_path = $editorPro['thumb'];
                    $file_ecom_content = file_get_contents($file_ecom_thumb_url);

                    if(Storage::disk($driver)->exists($current_path) && (md5($file_ecom_content) == md5(Storage::disk($driver)->get($current_path)))){
                        continue;
                    }else{
                        $type = Object3d::DIR_NAME;
                        $brand = $obj3d->brand ?? explode('_', $name)[0];
                        $time = \Carbon\Carbon::now()->timestamp;
                        $path = "$type/$brand/$name/" . Object3d::THUMB_EDITORPRO_DIR. "/$time/$basename";
                        $download = Storage::disk($driver)->put($path, $file_ecom_content);

                        $url = Storage::disk($driver)->url($path);
                        $editorPro['thumb'] = $path;
                        \DB::collection($object_collection)->where('name', $name)->update(['editorPro' => $editorPro]);
                        uploadDriver($basename, $driver, $path, $url , $obj3d);
                    }
                }
                

            }
        }
        
    }

    protected function syncFileFromExternalServerWithPaginate(){
        $driver = config('fitin.editor.filesystem_driver');
        $object_collection  = (new Object3d)->getTable();
        $asset = new Asset();
        
        $page = $this->option('page') ?? 1;
        $query = ['limit' => 50, 'page' => $page];
        $ecom_objects = $asset->getEcomItemsWithThumb($query);
        
        foreach ($ecom_objects as $obj){

            $name = strtolower($obj['object_key']);
            $file_ecom_thumb_url = $obj['image'];
            
            $pathinfo = pathinfo($file_ecom_thumb_url);
            $basename = $pathinfo['basename'];

            $obj3d = Object3d::where('name',$name)->first();
            if(!$obj3d){
                continue;
            }
            
            $editorPro = $obj3d->getOriginal('editorPro') ?? [];
            if(!isset($editorPro['thumb'])){
                              
                if(file_valid($file_ecom_thumb_url)){
                    $file_ecom_content = file_get_contents($file_ecom_thumb_url);
                    $type = Object3d::DIR_NAME;
                    $brand = $obj3d->brand ?? explode('_', $name)[0];
                    $time = \Carbon\Carbon::now()->timestamp;
                    $path = "$type/$brand/$name/" . Object3d::THUMB_EDITORPRO_DIR. "/$time/$basename";
                    $download = Storage::disk($driver)->put($path, $file_ecom_content);

                    $url = Storage::disk($driver)->url($path);
                    $editorPro['thumb'] = $path;
                    \DB::collection($object_collection)->where('name', $name)->update(['editorPro' => $editorPro]);
                    uploadDriver($basename, $driver, $path, $url , $obj3d);

                    echo "Updated editor pro image for $name \n";
                }else{
                    continue;
                }
                

            }else{
                if(!file_valid($file_ecom_thumb_url)){
                    continue;    
                }else{
                    $current_path = $editorPro['thumb'];
                    $file_ecom_content = file_get_contents($file_ecom_thumb_url);

                    if(Storage::disk($driver)->exists($current_path) && (md5($file_ecom_content) == md5(Storage::disk($driver)->get($current_path)))){
                        continue;
                    }else{
                        $type = Object3d::DIR_NAME;
                        $brand = $obj3d->brand ?? explode('_', $name)[0];
                        $time = \Carbon\Carbon::now()->timestamp;
                        $path = "$type/$brand/$name/" . Object3d::THUMB_EDITORPRO_DIR. "/$time/$basename";
                        $download = Storage::disk($driver)->put($path, $file_ecom_content);

                        $url = Storage::disk($driver)->url($path);
                        $editorPro['thumb'] = $path;
                        \DB::collection($object_collection)->where('name', $name)->update(['editorPro' => $editorPro]);
                        uploadDriver($basename, $driver, $path, $url , $obj3d);
                        echo "Updated editor pro image for $name \n";
                    }
                }
                

            }
        }
        
    }
    
}
