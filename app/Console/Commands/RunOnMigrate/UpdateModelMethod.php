<?php

namespace App\Console\Commands\RunOnMigrate;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Activity\ModelAction; 

use \Illuminate\Support\Facades\Storage;
use GuzzleHttp\Client;

use GuzzleHttp\Exception\ClientException;

class UpdateModelMethod extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activity:model-method:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model_methods = config('model-method');
        foreach($model_methods as $m){
            foreach($m['actions'] as $action=>$des){
                $cre = [
                    'model' => $m['model'],
                    'type' => $m['type'],
                    'action' => $action
                ];

                $data = array_merge($cre, ['param' => $des['param'], 'description' => $des['description']]);

                ModelAction::updateOrCreate($cre, $data);
            }
        }
        echo "Update model method successfully \n";
    }

    
}
