<?php

namespace App\Console\Commands\RunOnMigrate;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\Bundle; 
use App\Models\Ver2\Object3d; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

class AssetConvertItemMaterial extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:map:item:material';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$this->changeUrl();
        $this->mapMaterialPath();
        
    }

    public function changeUrl(){
        $objects = Object3d::all();
        $old_static = "https://cdn-dev-3d.fitin.vn/webgl";
        $new_static = config('fitin.asset.static_url');
        foreach ($objects as $object){
            $materials = $object->materials;
            if(is_array($materials)){
                foreach ($materials as &$item){
                    if(!empty($item['map'])){
                        $item['map'] = Utils::UrlReplace($item['map'], $old_static, $new_static);
                
                    }

                    if(!empty($item['normalMap'])){
                        $item['normalMap'] = Utils::UrlReplace($item['normalMap'], $old_static, $new_static);
                    }

                }
                $object->materials = $materials;
                $object->save();
            }
            
        }
        
        return 'Successfully!';
    }

    protected function mapMaterialPath(){
        $static_url = [
            "https://cdn-dev-3d.fitin.vn/webgl/",
            "https://cdn-demo-3d.fitin.vn/webgl/",
            "https://cdn-3d.fitin.dev/webgl/",
            "https://cdn.fitin.dev/webgl/",
            "http://localhost:9020/storage/",
            "https://static-3d.fitin.vn/webgl/",
            "https://static-dev.fitin.vn/webgl/",
            "https://static-demo.fitin.vn/webgl/",
            "https://static-demo.fitin3d.com/webgl/"
        ];

        $objects = Object3d::all();
        foreach ($objects as $object){
            $materials = $object->getOriginal('materials');
            if(is_array($materials)){

                foreach ($materials as &$material){   

                    if(!empty($material['aoMap'])){
                        $material['aoMapPath'] = $this->array_str_replace($static_url, "", $material['aoMap']);
                    }
                    if(!empty($material['map'])){
                        $material['mapPath'] = $this->array_str_replace($static_url, "", $material['map']);
                    }
                    if(!empty($material['normalMap'])){
                        $material['normalMapPath'] = $this->array_str_replace($static_url, "", $material['normalMap']);
                    }
                }

                $object->update([
                    'materials' => $materials
                ]);
            }
        }
    }

    protected function array_str_replace($array, $replace, $string){
        foreach ($array as $str){
            $string = str_replace($str, $replace, $string);
            $string = str_replace("webgl/webgl/", "webgl/", $string);
        }
        return $string;
    }
}
