<?php

namespace App\Console\Commands\RunOnMigrate;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\Bundle; 
use App\Models\Ver2\Object3d; 
use App\Models\Ver2\Texture; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

use App\Models\Ver2\FileDriver; 
use GuzzleHttp\Client;

use GuzzleHttp\Exception\ClientException;

class SyncFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    const DRIVERS = [
        'asset' => "https://cdn-dev-3d.fitin.vn/webgl",
        'unity' => "https://cdn-dev-3d.fitin.vn/unity",
        'editor' => "https://editor-dev-store.fitin.vn"
    ];
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$this->fillMisssingFileDriverData();
        $this->syncFileFromExternalServer();
    }

    protected function syncFileFromExternalServer(){
        $list_drivers = self::DRIVERS;
        $file_drivers = FileDriver::all();
        foreach ($file_drivers as $file){
            $driver = $file->driver;
            $path = $file->path;
            if(Storage::disk($driver)->exists($path)){
                continue;
            }

            $url = $list_drivers[$driver] . "/$path";
            if(file_valid($url)){
                $content = file_get_contents($url);
                $download = Storage::disk($driver)->put($path, $content);
                echo "Successfully download $path \n";
            }      

        }
    }
    
    protected function fillMisssingFileDriverData(){
        $this->syncObjects();
        $this->syncTextures();
    }

    protected function syncObjects(){
        $objects = Object3d::all();
        $driver_unity = config('fitin.unity.filesystem_driver');
        $driver_asset = config('fitin.unity.filesystem_driver');
        foreach ($objects as $object){
            if($object->prefab){
                $this->firstOrCreatePath($driver_unity, $object->prefab, $object);
            }
            if($object->fbx){
                $this->firstOrCreatePath($driver_asset, $object->fbx, $object);
            }
            if($object->gltf){
                $this->firstOrCreatePath($driver_asset, $object->gltf, $object);
            }
            if($object->ar && is_array($object->ar) && !empty($object->ar['ios'])){

                $this->firstOrCreatePath($driver_asset, $object->ar['ios'], $object);
            }
            if($object->thumb){
                $this->firstOrCreatePath($driver_asset, $object->thumb, $object);
            }
        }
    }

    protected function syncTextures(){
        $textures = Texture::all();
        $driver_asset = config('fitin.unity.filesystem_driver');
        foreach ($textures as $texture){
            $brand = $texture->brand;
            $object = $texture->group;
            $path = $texture->imagePath;
            $file_name = $texture->file;
            if(!$brand || !$object){
                continue;
            }
            if(!$path){
                $path = Object3d::DIR_NAME."/$brand/$object/".Object3d::TEXTURE_DIR . "/$file_name";
                $texture->imagePath = $path;
                $texture->save();
            }

            $this->firstOrCreatePath($driver_asset, $path, $texture);

            foreach (Texture::THUMB_SIZE as  $arrAttr){
                $width =  $arrAttr['width'];
                $pathsub = Object3d::DIR_NAME."/$brand/$object/".Object3d::TEXTURE_DIR .  "/$width/$file_name";
                $this->firstOrCreatePath($driver_asset, $pathsub, $texture);
            }
        }
    }


    protected function firstOrCreatePath($driver, $path, $instance){
        if(!Storage::disk($driver)->exists($path)){
            return false;
        }

        $file_driver = FileDriver::where('driver', $driver)->where('path', $path)->first();
        if($file_driver){
            if(!$file_driver->key){
                $file_driver->key = $instance->getKey();
                $file_driver->save();
            }
            if(!$file_driver->model){
                $file_driver->model = get_class($instance);
                $file_driver->save();
            }
        }else{
            $array_path = explode('/', $path);
            $file = $array_path[count($array_path) - 1];
            $data = [
                'file' => $file,
                'driver' => $driver,
                'path' => $path,
                'url' => Storage::disk($driver)->url($path),
                'key' => $instance->getKey(),
                'model' => get_class($instance)
            ];
            
            $file = FileDriver::create($data);
        }
    }
}
