<?php

namespace App\Console\Commands\RunOnMigrate;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
//use Illuminate\Database\Schema\Blueprint;
use Jenssegers\Mongodb\Schema\Blueprint;

class CreateCollectionIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'collection:index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (config('model-custom-key') as $modelClass){
            $model = new $modelClass;
            $primaryKey = $model->getKeyName();
            $table = $model->getTable();
            
            if(defined("$modelClass::INDEXES")){
                $indexes = $modelClass::INDEXES;
                Schema::collection($table, function(Blueprint $collection) use ($indexes) {
                    foreach($indexes as $index){
                        $collection->index($index);
                    }          
                });
            }
            if($primaryKey == "_id"){
                continue;
            } 
            // Schema::collection($table, function(Blueprint $collection) use ($primaryKey) {
            //     $collection->unique($primaryKey);
            // });
        }
    }

    
}
