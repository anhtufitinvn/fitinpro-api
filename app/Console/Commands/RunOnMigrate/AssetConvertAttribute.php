<?php

namespace App\Console\Commands\RunOnMigrate;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\Object3d; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

class AssetConvertAttribute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:map:item:attribute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->mapAttribute();
        
    }

    public function mapAttribute(){
        $objects = Object3d::all();
        foreach ($objects as $object){
            $attributes = $object->getOriginal('attributes');
            if(!is_array($attributes) && $attributes == null ){
                $attributes = [
                    "dimension" => $object->dimension,
                    "colorRange" => $object->colorRange,
                    "isPrimary" => $object->isPrimary,
                    "isFloat" => $object->isFloat,
                    "isHole" => $object->isHole,
                    "dependency" => $object->dependency,
                    "enable_dimension" => $object->enable_dimension,
                    "anchor" => $object->anchor,
                    "renderable" => $object->renderable,
                    "isTTS" => $object->isTTS,
                    'position' => $object->position,
                    'positionXYZ' => $object->positionXYZ,
                    'rotation' => $object->rotation,
                    'rotationXYZ' => $object->rotationXYZ,
                    'scale' => $object->scale,
                    'scaleXYZ' => $object->scaleXYZ
                ];
                $object->update([
                    'attributes' => $attributes
                ]);
            }
            
        }
        
        return 'Successfully!';
    }

    
}
