<?php

namespace App\Console\Commands\Asset;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\V2\Texture; 
use App\Models\V2\Object3d; 
use App\FitIn\AuthID;

class AssetMappingTextureObjectId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:map:texture:object-id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $textures = Texture::all();
        foreach ($textures as $texture){
            $object = Object3d::where('name', $texture->group)->first();
            if($object){
                $texture->object3d_id = ($object->_id);
                $texture->save();
            }
        }
        
        return 'Successfully!';
    }
}
