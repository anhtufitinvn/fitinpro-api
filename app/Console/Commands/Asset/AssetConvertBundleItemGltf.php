<?php

namespace App\Console\Commands\Asset;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\Bundle; 
use App\Models\Ver2\Object3d; 
use \Illuminate\Support\Facades\Storage;

class AssetConvertBundleItemGltf extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:map:bundle:item:gltf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bundles = Bundle::all();
        $static = "https://static-dev.fitin3d.com/webgl";
        foreach ($bundles as $bundle){
            $item_list = $bundle->item_list;
            if(is_array($item_list)){
                foreach ($item_list as &$item){
                    if(!empty($item['gltf'])){
                        $item['gltf'] = str_replace($static, "", $item['gltf']);
                    }

                    if(!empty($item['fbx'])){
                        $item['fbx'] = str_replace($static, "", $item['fbx']);
                    }

                    if(!empty($item['gltfPath'])){
                        unset($item['gltfPath']);
                    }
                }
                $bundle->item_list = $item_list;
                $bundle->save();
            }
            
        }
        
        return 'Successfully!';
    }
}
