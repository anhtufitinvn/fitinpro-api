<?php

namespace App\Console\Commands\Asset;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\User; 


class AssetInitUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:user:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $credentials = [
            'name' => 'Admin',
            'email' => 'admin@fitin.vn',
            'password' => \Hash::make('Admin@123xyz!'),
            'active' => 1
        ];

        $user = User::updateOrCreate(['email' => $credentials['email']], $credentials);
        
        return 'Successfully!';
    }
}
