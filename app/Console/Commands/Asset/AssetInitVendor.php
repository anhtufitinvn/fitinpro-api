<?php

namespace App\Console\Commands\Asset;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\Vendor; 


class AssetInitVendor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:vendor:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    protected $vendors = [
        'fitin',
        'fullhouse',
        'fitis',
        'alpha2',
        'casanha',
        'cassina',
        'fraser',
        'jysk',
        'library',
        'sunavenue',
        'sunwah',
        'sunwahpp',
        'stanley',
    ];
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->vendors as $vendor){
            $credentials = [
                'name' => $vendor,
                'email' => "$vendor@fitin.vn",
                'code' => $vendor,
                'password' => md5("$vendor@123"),
                'active' => 1,
                'status' => 'published'
            ];
    
            $user = Vendor::firstOrCreate(['name' => $vendor], $credentials);
        }   
        
        
        return 'Successfully!';
    }
}
