<?php

namespace App\Console\Commands\Asset;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Models\V2\Object3d; 


class AssetMappingObjectType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:map:object:type';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $objects = \DB::collection('Objects')->get();

        foreach ($objects as $object){
            $fbx = $object['fbx'] ?? NULL;
            if(!isset($object['name'])){
                continue;
            }
            if(!$fbx ||  strpos($fbx, 'default') !== false){
                $attributes = $object['attributes'] ?? [];
                $attributes['type'] = '2d';
                \DB::collection('Objects')->where('name', $object['name'])->update(['attributes' => $attributes]);
            }else{
                $attributes = $object['attributes'] ?? [];
                $attributes['type'] = '3d';
                \DB::collection('Objects')->where('name', $object['name'])->update(['attributes' => $attributes]);
            }
        }
        
        return 'Successfully!';
    }
}
