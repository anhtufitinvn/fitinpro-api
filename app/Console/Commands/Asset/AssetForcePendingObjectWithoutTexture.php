<?php

namespace App\Console\Commands\Asset;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\Object3d; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

class AssetForcePendingObjectWithoutTexture extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:object:pending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkTexture();
          
    }

    public function checkTexture(){
        $objects = Object3d::all();
        $list = [];
        foreach ($objects as $object){
            $materials = $object->getOriginal('materials');
            $forcePending = 0;
            if(!$materials){
                $forcePending++;
            }

            if(is_array($materials) && !count($materials)){
                $forcePending++;
            }
            if(is_array($materials)){
                $hasTexture = 0;
                foreach ($materials as $item){
                    if(!empty($item['mapPath']) || !empty($item['normalMapPath']) || !empty($item['aoMapPath'])){
                        $hasTexture++;
                    }             

                }
               if(!$hasTexture){
                $forcePending++;
               }
            }
            
            if($forcePending){
                $list[] = $object->name;
            }
        }

        dd($list);
        
        return 'Successfully!';
    }

    
}
