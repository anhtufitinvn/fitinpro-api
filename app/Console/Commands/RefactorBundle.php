<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\RefactorModel\BundleMap; 
use App\Models\Ver2\Bundle; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

class RefactorBundle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:refactor:bundle';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $bundles = Bundle::all();

        foreach ($bundles as $bundle){
            $input = $bundle->toArray();
            $input = (new BundleMap())->buildAttributeStructure( $input);

           
            BundleMap::create($input);
        }
        
        return 'Successfully!';
    }
}
