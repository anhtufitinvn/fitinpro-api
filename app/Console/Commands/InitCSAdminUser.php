<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Plugin\CS\Models\User;
use Illuminate\Support\Facades\Hash;

class InitCSAdminUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:cs:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $user = [
            'name' => 'CS Admin',
            'username' => 'cs',
            'email' => 'cs@fitin.vn',
            'password' => Hash::make('123456'),
            'active' => true
        ];
        User::updateOrCreate(['email' => $user['email']],$user);

        $user = [
            'name' => 'Fitin',
            'username' => 'fitin',
            'email' => 'admin@fitin.vn',
            'password' => Hash::make('123456'),
            'active' => true
        ];
        User::updateOrCreate(['email' => $user['email']],$user);
    }
}
