<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Models\Ver2\Layout; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

class RefactorLayoutDataJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:refactor:layout:json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $layouts = Layout::all();

        foreach ($layouts as $layout){
            $dataJson = $layout->dataJson;
            if(is_array($dataJson) && isset($dataJson['TTS']) && isset($dataJson['RoomId'])){
                $newJson = parseLayoutFromUnity($dataJson);
                $layout->dataJson = $newJson;
                $layout->save();
            }
        }
        
        return 'Successfully!';
    }
}
