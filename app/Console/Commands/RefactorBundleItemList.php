<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\RefactorModel\BundleMap; 
use App\Models\Ver2\Bundle; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

class RefactorBundleItemList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:refactor:bundle:item';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $list = [
            "dimension",
            "colorRange",
            "isPrimary" ,
            "isFloat",
            "isHole",
            "dependency",
            "enable_dimension",
            "anchor",
            "renderable",
            "isTTS",
            'position',
            'positionXYZ',
            'rotation',
            'rotationXYZ',
            'scale',
            'scaleXYZ',
        ];
        $bundles = Bundle::all();

        foreach ($bundles as $bundle){
            $itemList = $bundle->getOriginal('item_list');
            if(is_array($itemList)){
                foreach ($itemList as &$item){
                    $attributes = $item['attributes'] ?? [];
                    foreach ($list as $pr){
                        if(isset($item[$pr])){
                            $attributes[$pr] = $item[$pr];
                        }
                    }
                    $item['attributes'] = $attributes;
                }

                $bundle->update(['item_list' => $itemList]);
            }
        }
        
        return 'Successfully!';
    }
}
