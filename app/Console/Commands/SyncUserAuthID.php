<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\V2\User; 
use App\Models\User as MySQLUser; 
use App\FitIn\AuthID;

class SyncUserAuthID extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:auth-id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Snyc User Auth ID from Id-dev';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mysql_users = MySQLUser::all();
        foreach ($mysql_users as $mysql_user){
            $user = User::where('email', $mysql_user->email)->first();
            if($user){
                $user->auth_id = $mysql_user->id;
                $user->save();
            }

        }
        
        return 'Successfully!';
    }
}
