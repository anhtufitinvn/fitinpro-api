<?php

namespace App\Console\Commands\Tool;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Models\Ver2\Layout; 
use App\Models\Ver2\Material; 

use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

use App\Models\Ver2\FileDriver; 
use App\FitIn\Asset;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class CopyEditorProFileFromAssetDriverToEditorDriver extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy:driver:asset:editor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //$this->copyLayout();
        $this->copyMaterial();
    }

    public function copyLayout()
    {
        $assetDriver = config('fitin.asset.filesystem_driver');
        $editorDriver = config('fitin.editor.filesystem_driver');
        
        $layouts = Layout::all();
        foreach ($layouts as $layout)
        {
            $editorPro = $layout->getOriginal('editorPro') ?? [];
            if(!empty($editorPro['prefab'])){
                $path = $editorPro['prefab'];
                if( Storage::disk($assetDriver)->exists($path) &&  !Storage::disk($editorDriver)->exists($path)){
                    Storage::disk($editorDriver)->put($path, Storage::disk($assetDriver)->get($path));
                    $pathinfo = pathinfo($path);
                    $basename = $pathinfo['basename'];
                    $url = Storage::disk($editorDriver)->url($path);
                    uploadDriver($basename, $editorDriver, $path, $url , $layout);
                }
                
            }

            if(!empty($editorPro['json'])){
                $path = $editorPro['json'];
                if( Storage::disk($assetDriver)->exists($path) &&  !Storage::disk($editorDriver)->exists($path)){
                    Storage::disk($editorDriver)->put($path, Storage::disk($assetDriver)->get($path));
                    $pathinfo = pathinfo($path);
                    $basename = $pathinfo['basename'];
                    $url = Storage::disk($editorDriver)->url($path);
                    uploadDriver($basename, $editorDriver, $path, $url , $layout);
                }
            }
        }
       
        echo "Done! \n";
    }


    public function copyMaterial()
    {
        $assetDriver = config('fitin.asset.filesystem_driver');
        $editorDriver = config('fitin.editor.filesystem_driver');
        
        $materials = Material::all();
        foreach ($materials as $material)
        {
            $editorPro = $material->getOriginal('editorPro') ?? [];
            if(!empty($editorPro['asset'])){
                $path = $editorPro['asset'];
                if( Storage::disk($assetDriver)->exists($path) &&  !Storage::disk($editorDriver)->exists($path)){
                    Storage::disk($editorDriver)->put($path, Storage::disk($assetDriver)->get($path));
                    $pathinfo = pathinfo($path);
                    $basename = $pathinfo['basename'];
                    $url = Storage::disk($editorDriver)->url($path);
                    uploadDriver($basename, $editorDriver, $path, $url , $material);
                }
                
            }

        }
       
        echo "Done! \n";
    }
    
    
}
