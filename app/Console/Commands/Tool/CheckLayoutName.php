<?php

namespace App\Console\Commands\Tool;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\RefactorModel\Objects; 
use App\Models\Ver2\LayoutName; 
use App\Models\Ver2\Object3d; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

class CheckLayoutName extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:layout:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $layouts = LayoutName::all();
        $total = []; 
        foreach ($layouts as $layout){
            $dataJson = $layout->layout_json;
            $json = json_decode($dataJson, true);
            if(isset($json['TTS'])){
                $tts = $json['TTS'];
                $items = $json['Items'];
                $tts_name = array_column($tts , 'Name');
                $items_name = array_column($items , 'Name');
                $total = array_merge($total, $tts_name, $items_name);
            }

            if(isset($json['tts'])){
                $tts = $json['tts'];
                $tts_name = array_column($tts , 'name');
                $total = array_merge($total, $tts_name);
            }
        }

        $total = array_values(array_unique($total));
        
        $list_exist_but_no_unity = Object3d::whereIn('name', $total)->whereNull('unity.prefab')->get()->pluck('name');
        $list_exist = Object3d::whereIn('name', $total)->get()->pluck('name');
        
        $list_not_exist = array_diff($total, $list_exist->toArray());
        dd($list_exist_but_no_unity->toArray(), array_values($list_not_exist));
        return 'Successfully!';
    }
}
