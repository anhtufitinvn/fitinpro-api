<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\FileDriver; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

class RefactorKeyFileDriver extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'file:refactor:key';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $files = FileDriver::all();

        foreach ($files as $file){
            $model = $file->model;
            if($model && $file->key){
                $item = $model::where('_id', $file->key)->first();
                if($item){
                    $file->key = $item->getKey();
                    $file->save();
                }
            }
            
        }
        
        return 'Successfully!';
    }
}
