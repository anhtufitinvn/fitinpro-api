<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\Layout; 
use App\Models\Ver2\Object3d; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

class RemappingLayout extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:mapping:layout';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $layouts = Layout::all();

        foreach ($layouts as $layout){
            $dataJson = $layout->dataJson;
            if(!$dataJson || empty($dataJson['tts']) || !is_array($dataJson['tts']) || !count($dataJson['tts'])){
                continue;
            }
            $tts = $dataJson['tts'];
            foreach ($tts as $item){
                $object_name = $item['name'] ?? NULL;
                if($object_name){
                    $object = Object3d::whereNull('layout')->where('name', $object_name)->first();
                    if($object){
                        $object->layout = $layout->name;
                        $object->save();
                    }
                }
            }
        }
        
        return 'Successfully!';
    }
}
