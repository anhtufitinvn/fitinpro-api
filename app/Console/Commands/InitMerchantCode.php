<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\V2\User;

class InitMerchantCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init-merchant-code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init merchant code for specific users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $user = User::where('email', 'admin@fitin.vn')->first();
        if($user){
            $user->merchant_code = 'fitin';
            $user->save();
        }

        $user = User::where('email', 'admin@fitin3d.com')->first();
        if($user){
            $user->merchant_code = 'fitin3d';
            $user->save();
        }

        $user = User::where('email', 'admin@uma.vn')->first();
        if($user){
            $user->merchant_code = 'uma';
            $user->save();
        }
        return 'Successfully!';
    }
}
