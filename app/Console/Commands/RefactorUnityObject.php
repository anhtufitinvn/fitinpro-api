<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Models\Ver2\Object3d; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

class RefactorUnityObject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:refactor:object:unity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        //$this->mapNullUnity();
        $this->mapWrongUnity();
    }

    public function mapNullUnity()
    {

        $objects = Object3d::all();
        $driver = Object3d::UNITY_DRIVER;
        foreach ($objects as $object){
            $unity = $object->getOriginal('unity');
            if($unity && is_array($unity) && isset($unity['prefab']) && ($unity['prefab']) ){
                continue;
            }
            $name = $object->name;
            $brand = $object->brand;
            if(!$brand) {
                $brand = "fitin";
            }
            $file_name_array = explode('_', $name);
            $prename1 = $file_name_array[0] ?? 'fitin';
            $prename2 = $file_name_array[1] ?? 'fitin';
            $path_layout = Object3d::UNITY_WEB_DIR."/layout/$prename1/$prename2/$name";
            $path_furniture = Object3d::UNITY_WEB_DIR."/furniture/$prename1/$name";

            if(Storage::disk($driver)->exists($path_furniture)){
                $unity = [
                    'prefab' => $path_furniture,
                    'prefabType' => 'furniture'
                ];
                $originUnity = $object->getOriginal('unity');
                if(!$originUnity || (is_array($originUnity) && !isset($originUnity['prefab'])) || !$originUnity['prefab']){
                    $object->unity = $unity;
                    $object->save();
                }
                
            }else if (Storage::disk($driver)->exists($path_layout)){
                $unity = [
                    'prefab' => $path_layout,
                    'prefabType' => 'layout'
                ];
                $originUnity = $object->getOriginal('unity');
                if(!$originUnity || (is_array($originUnity) && !isset($originUnity['prefab']) ) || !$originUnity['prefab']){
                    $object->unity = $unity;
                    $object->save();
                }
            }
        }
        
        return 'Successfully!';
    }

    public function mapWrongUnity()
    {

        $objects = Object3d::all();
        $driver = Object3d::UNITY_DRIVER;
        foreach ($objects as $object){
            $unity = $object->getOriginal('unity');
            if($unity && is_array($unity) && isset($unity['prefab']) && ($unity['prefab']) && isset($unity['prefabType'])){
                $prefab = $unity['prefab'];
                if (strpos($prefab, 'webgl-assets') !== false) {
                    continue;
                }
                $prefabType = $unity['prefabType'];
                $name = $object->name;
                $file_name_array = explode('_', $name);
                $prename1 = $file_name_array[0] ?? 'fitin';
                $prename2 = $file_name_array[1] ?? 'fitin';
                $path_layout = Object3d::UNITY_WEB_DIR."/layout/$prename1/$prename2/$name";
                $path_furniture = Object3d::UNITY_WEB_DIR."/furniture/$prename1/$name";

                if($prefabType == 'layout'){
                    $unity['prefab'] = $path_layout;
                    $object->unity = $unity;
                    $object->save();

                }elseif($prefabType == 'furniture'){
                    $unity['prefab'] = $path_furniture;
                    $object->unity = $unity;
                    $object->save();
                }
            }
        }
        
        return 'Successfully!';
    }
}
