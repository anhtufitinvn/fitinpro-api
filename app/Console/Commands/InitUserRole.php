<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Ver2\User;
use App\Models\Ver2\Role;

class InitUserRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:role';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init roles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrRoles = [
            [
                'name' => 'admin',
                'title' => 'Admin'
            ],                      
            [
                'name' => 'user',
                'title' => 'User'
            ]
        ];

        foreach ($arrRoles as $item) {
            $role = Role::updateOrCreate(['name' => $item['name']], [
                'name' => $item['name'],
                'title' => $item['title'] ?? NULL      
            ]);
        }
        
        $user = \App\Models\Ver2\User::where('email','admin@fitin.vn')->first();
        if($user){
            $user->syncRoles('admin');
        }

        $user = \App\Models\Ver2\User::where('email','admin@fitin3d.com')->first();
        if($user){
            $user->syncRoles('admin');
        }

        return 'Successfully!';
    }
}
