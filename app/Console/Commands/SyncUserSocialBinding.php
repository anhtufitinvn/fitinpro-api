<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\V2\User; 
use App\Models\ID\User as MySQLUser; 
use App\FitIn\AuthID;

class SyncUserSocialBinding extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:social';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Snyc User from user_socials on Id-dev';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mysql_users = MySQLUser::with(['user_profile', 'user_socials'])->get();
        foreach ($mysql_users as $mysql_user){
            $user = User::where('email', $mysql_user->email)->first();
            if($user){
                $user->auth_id = $mysql_user->id;

                if(!empty($mysql_user->user_profile)){
                    $user->name = $mysql_user->user_profile->full_name;
                }

                if(!empty($mysql_user->user_socials)){
                    foreach ($mysql_user->user_socials as $social){
                        if ($social->user_social_name) $user->name = $social->user_social_name;
                        if($social->type == 'facebook') $user->facebook_id = $social->user_social_id;
                        if($social->type == 'google') $user->google_id = $social->user_social_id;
                    }
                }
              
                //if(!$user->avatar) $user->avatar = $mysql_user->avatar;
                if(!$user->phone) $user->phone = $mysql_user->phone;

                $user->save();
            }else{
                $data = [
                    'auth_id' => $mysql_user->id,
                    'user_parent_id' => 0,
                    'email' => $mysql_user->email,
                    'role' => 0,
                    'active' => $mysql_user->status,
                    'phone' =>  $mysql_user->phone,
                 //   'avatar' =>  $mysql_user->avatar
                ];

                if ($mysql_user->user_profile) $data['name'] = $mysql_user->user_profile->full_name;
                $user = User::create($data);
            }

        }
        
        return 'Successfully!';
    }
}
