<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Models\Ver2\Layout; 
use App\EditorPro\Models\Layout as LayoutUser; 
use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

class RefactorLayoutEditorPro extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'asset:refactor:layout:unity:editor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $layouts = Layout::all();

        foreach ($layouts as $layout){
            $editorPro = $layout->getOriginal('editorPro');
            if(!$editorPro){
                $editorPro = [];
                $editorPro['revision'] = $layout->revision;
                $editorPro['prefab'] = $layout->prefab;
                $editorPro['json'] = $layout->json;
                
                \DB::collection('Layouts')->where('name', $layout->name)->update(['editorPro' => $editorPro]);
            }
        }
        
        $layoutusers = LayoutUser::all();

        foreach ($layoutusers as $layout){
            $editorPro = $layout->getOriginal('editorPro');
            if(!$editorPro){
                $editorPro = [];
                $editorPro['revision'] = $layout->revision;
                $editorPro['prefab'] = $layout->prefab;
                $editorPro['json'] = $layout->json;
                
                \DB::collection('EditorLayouts')->where('name', $layout->name)->update(['editorPro' => $editorPro]);
            }
        }
        return 'Successfully!';
    }
}
