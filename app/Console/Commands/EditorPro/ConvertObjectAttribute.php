<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\EditorPro\Models\Object3d; 

use \Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;

class ConvertObjectAttribute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:object:attribute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //1st : canColor => canCook
        //2nd : init TTS canDelete

        //$this->canColorToCanCook();
        $this->initTTSCanDelete();
        

    }

    protected function canColorToCanCook(){
        $objects = Object3d::all();
        foreach ($objects as $object){
            $attrs = $object->getOriginal('attributes');
            if(is_array($attrs) && isset($attrs['canColor'])){
                $attrs['canCook'] = $attrs['canColor'];
                unset($attrs['canColor']);
                $object->update(['attributes' => $attrs]);
            }
        }
    }

    protected function initTTSCanDelete(){
        $objects = Object3d::withTTS()->get();
        foreach ($objects as $object){
            $attrs = $object->getOriginal('attributes');
            if(is_array($attrs)){
                $attrs['canDelete'] = 1;
                $object->update(['attributes' => $attrs]);
            }
        }
    }
    
    
}
