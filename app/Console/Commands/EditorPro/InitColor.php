<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\ColorGroup;
use App\EditorPro\Models\Color;
use App\EditorPro\Models\ColorBrand;

use App\Models\Ver2\ColorGroup as ThreeJSColorGroup;
use App\Models\Ver2\Color as ThreeJSColor;

use Illuminate\Support\Facades\Hash;

class InitColor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:editor:color';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [
            'name' => 'Nippon',
            'code' => 'nippon'
        ];

        $brand = ColorBrand::firstOrCreate(['code' => $data['code']], $data);

        $threeJSColorGroups = ThreeJSColorGroup::published()->get();
        foreach ($threeJSColorGroups as $group){
            $data = [
                'name' => $group->name,
                'code' => $brand->code . '-' .$group->code,
                'brand' => $brand->code,
                'hex' => $group->hex,
                'status' => $group->status
            ];

            $newgroup = ColorGroup::firstOrCreate(['code' => $data['code']], $data);

            $threeJSColors = $group->colors;

            foreach ($threeJSColors as $color){
                $data = [
                    'name' => $color->name,
                    'group' => $newgroup->code,
                    'code' => $color->code,
                    'hex' => $color->hex,
                    'priority' => $color->priority
                ];
                $newcolor = Color::firstOrCreate(['code' => $data['code']], $data);
            }
        }
        
    }
}
