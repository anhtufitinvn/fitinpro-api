<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\ColorGroup;
use App\EditorPro\Models\Color;
use App\EditorPro\Models\ColorBrand;

use Illuminate\Support\Facades\Hash;

class InitDuluxColor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:editor:dulux:color';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json = file_get_contents(public_path().'/dulux_colors');
        $json = json_decode($json, true);

        $arr = [];

        foreach ($json as $key => $item) {
            if(!isset($arr[$item['name']])){
                $arr[$item['name']] = [
                    'info' => [],
                    'list' => []
                ];
            }
            if(!isset($arr[$item['name']]['info']['name']) && !empty($item['name'])){
                $arr[$item['name']]['info']['name'] = $item['name'];
            }
            if(!isset($arr[$item['name']]['info']['title']) && !empty($item['title'])){
                $arr[$item['name']]['info']['title'] = $item['title'];
            }
            if(!isset($arr[$item['name']]['info']['color']) && !empty($item['color'])){
                $arr[$item['name']]['info']['color'] = $item['color'];
            }
            
            $arr[$item['name']]['list'][] = $item;
        }

        //ksort($arr, SORT_NUMERIC);
        //dd($arr);
        
        $data = [
            'name' => 'Dulux',
            'code' => 'dulux',
            'status' => ColorBrand::PUBLISHED
        ];

        $brand = ColorBrand::firstOrCreate(['code' => $data['code']], $data);

        foreach ($arr as $groupColor){
            $data = [
                'name' => $groupColor['info']['name'],
                'code' => $brand->code . '-' .slug($groupColor['info']['name']),
                'brand' => $brand->code,
                'hex' => str_replace('#', '', $groupColor['info']['color']),
                'description' => $groupColor['info']['title'],
                'status' => ColorGroup::PUBLISHED
            ];

            $newgroup = ColorGroup::firstOrCreate(['code' => $data['code']], $data);
            $list = $groupColor['list'];
            
            foreach ($list as $childList){
                $colorList = $childList['colorList'];
                foreach ($colorList as $color){
                    $colorRGB = explode(',', $color['RGB']);
                    $colorHex = sprintf("%02x%02x%02x", (int) $colorRGB[0], (int) $colorRGB[1], (int) $colorRGB[2]);
                    
                    $data = [
                        'name' => $color['name'],
                        'group' => $newgroup->code,
                        'code' => slug($color['title']),
                        'hex' => $colorHex,
                        'meta' => [
                            'rgb' =>  $color['RGB'],
                            'cmyk' => $color['CMYK'],
                            'hsb' => $color['HSB']
                        ]
                    ];
                    $newcolor = Color::firstOrCreate(['code' => $data['code']], $data);
                }
            }
        }
    }
}
