<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\Project;
use App\EditorPro\Models\Object3d;
use Illuminate\Support\Facades\Hash;

class InitStatusObjectAndProject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:editor:object:project:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Project::whereNotNull('name')->update(['status' => Project::PUBLISHED]);
        Object3d::whereNotNull('name')->update(['status' => Object3d::PUBLISHED]);
    }
}
