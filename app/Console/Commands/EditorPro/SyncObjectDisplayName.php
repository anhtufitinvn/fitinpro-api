<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\Object3d;
use Illuminate\Support\Facades\Hash;
use App\FitIn\Asset;

class SyncObjectDisplayName extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sync:editor:object:displayname';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $asset = new Asset();
        $object3d = Object3d::all();
        foreach ($object3d as $obj){
            if (!$obj->displayName || $obj->displayName == $obj->name){
                $fitin_obj = $asset->getByKey('product-by-key', $obj->name);
                if(!$fitin_obj || empty($fitin_obj['name'])){
                    $new_name = $obj->name;
                    $new_name = strtoupper(str_replace("_", " ", $new_name));
                    $obj->displayName = $new_name;
                    $obj->save();
                }else{
                    $new_name = $fitin_obj['name'];
                    $obj->displayName = strtoupper($new_name);
                    $obj->save();
                }
                
            }
        }
    }
}
