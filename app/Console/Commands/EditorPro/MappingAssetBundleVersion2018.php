<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\Object3d;
use App\EditorPro\Models\Room;
use App\EditorPro\Models\Layout;
use App\EditorPro\Models\Material;
use Illuminate\Support\Facades\Hash;

class MappingAssetBundleVersion2018 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:mapping:editor:asset:version:2018';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->mapObjects();
        $this->mapRooms();
        $this->mapLayouts();
        $this->mapMaterials();
        return 1;
    }

    protected function mapObjects(){
        $objects = Object3d::whereNotNull('assetBundle')->get();
        foreach ($objects as $obj){
            $versionAssetBundle = $obj->getOriginal('versionAssetBundle') ?? [];
            $versionAssetBundle['v2018'] = [
                'assetBundle' => $obj->assetBundle,
                'revision' => $obj->revision
            ];
            $obj->versionAssetBundle = $versionAssetBundle;
            $obj->save();
        }
        echo "Done mapping Objects". PHP_EOL;
    }

    protected function mapRooms(){
        $instances = Room::whereNotNull('assetBundle')->get();
        foreach ($instances as $obj){
            $versionAssetBundle = $obj->getOriginal('versionAssetBundle') ?? [];
            $versionAssetBundle['v2018'] = [
                'assetBundle' => $obj->assetBundle,
                'revision' => $obj->revision
            ];
            $obj->versionAssetBundle = $versionAssetBundle;
            $obj->save();
        }
        echo "Done mapping Rooms". PHP_EOL;
    }

    protected function mapLayouts(){
        $instances = Layout::whereNotNull('assetBundle')->get();
        foreach ($instances as $obj){
            $versionAssetBundle = $obj->getOriginal('versionAssetBundle') ?? [];
            $versionAssetBundle['v2018'] = [
                'assetBundle' => $obj->assetBundle,
                'revision' => $obj->revision
            ];
            $obj->versionAssetBundle = $versionAssetBundle;
            $obj->save();
        }
        echo "Done mapping Layouts". PHP_EOL;
    }

    protected function mapMaterials(){
        $instances = Material::whereNotNull('assetBundle')->get();
        foreach ($instances as $obj){
            $versionAssetBundle = $obj->getOriginal('versionAssetBundle') ?? [];
            $versionAssetBundle['v2018'] = [
                'assetBundle' => $obj->assetBundle,
                'revision' => $obj->revision
            ];
            $obj->versionAssetBundle = $versionAssetBundle;
            $obj->save();
        }
        echo "Done mapping Materials". PHP_EOL;
    }
}
