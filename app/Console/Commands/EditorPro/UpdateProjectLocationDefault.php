<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\Project;

use App\EditorPro\Repositories\Object3dRepository;
use Illuminate\Support\Facades\Hash;

class UpdateProjectLocationDefault extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update:project:location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    protected $objectRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Object3dRepository $objectRepository)
    {
        $this->objectRepository = $objectRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $location = [
            'city_id' => 79,
            'city_name' => "Hồ Chí Minh",
            'district_id' => 760,
            'district_name' => "Quận 1",
            'address' => '',
            'lat' => '',
            'long' => ''
        ];

        Project::whereNotNull('name')->update(['location' => $location]);

    }
}
