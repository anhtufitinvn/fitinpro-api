<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\EditorPro\Models\Material;

class ChangeMaterialCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:change:material:categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        Material::where('category', 'gach')->update(['category' => 'ceramic']);
        Material::where('category', 'nhua')->update(['category' => 'plastic']);
        Material::where('category', 'go')->update(['category' => 'wood']);
    }
}
