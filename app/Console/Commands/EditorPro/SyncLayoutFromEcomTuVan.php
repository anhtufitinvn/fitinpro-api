<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


use App\EditorPro\Models\Project; 
use App\EditorPro\Models\Style; 
use App\EditorPro\Models\Category; 

use Illuminate\Support\Facades\Storage;
use App\FitIn\Asset;
use App\Helpers\Utils;
use App\EditorPro\Services\ResourceTuVanService;

class SyncLayoutFromEcomTuVan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sync:editor:layout:from:ecom:tuvan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';
    
    protected $resourceTuvanService;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ResourceTuVanService $resourceTuvanService)
    {
        $this->resourceTuvanService = $resourceTuvanService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {          
        return $this->resourceTuvanService->syncFitinProjectAndLayouts();
    }

    
    
}
