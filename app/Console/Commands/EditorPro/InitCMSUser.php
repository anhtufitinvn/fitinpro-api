<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\CMSUser;
use Illuminate\Support\Facades\Hash;

class InitCMSUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:editor:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $user = [
            'name' => 'Admin',
            'email' => 'admin@fitin.vn',
            'password' => 'Admin@123xyz!',
            'active' => true,
            'auth_id' => "cms_1"
        ];
        CMSUser::updateOrCreate(['email' => $user['email']], $user);

        $user = [
            'name' => 'Lam Tran',
            'email' => 'lam.tran@fitin.vn',
            'password' => '123456',
            'active' => true,
            'auth_id' => "cms_2"
        ];
        CMSUser::updateOrCreate(['email' => $user['email']], $user);

        $user = [
            'name' => 'Editor Admin',
            'email' => 'editor@fitin.vn',
            'password' => 'Editor@123xyz!',
            'active' => true,
            'auth_id' => "cms_3"
        ];
        CMSUser::updateOrCreate(['email' => $user['email']], $user);
    }
}
