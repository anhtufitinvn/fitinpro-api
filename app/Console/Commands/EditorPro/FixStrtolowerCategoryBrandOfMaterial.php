<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\Material;
use Illuminate\Support\Facades\Hash;

class FixStrtolowerCategoryBrandOfMaterial extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:strtolower:material';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $materials = Material::all();
        foreach ($materials as $material){
            if($material->category){
                $material->category = strtolower($material->category);
                $material->save();
            }
            if($material->brand){
                $material->brand = strtolower($material->brand);
                $material->save();
            }

        }
    }
}
