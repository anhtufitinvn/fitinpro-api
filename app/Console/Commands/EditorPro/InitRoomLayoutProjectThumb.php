<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\Room;
use App\EditorPro\Models\Layout;
use App\EditorPro\Models\Project;
use Illuminate\Support\Facades\Hash;

class InitRoomLayoutProjectThumb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:editor:room-layout-project:thumb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $instances = Room::whereNotNull('image')->get();
        foreach ($instances as $inst){
            $inst->makeMultiThumb();
        }

        $instances = Layout::whereNotNull('image')->get();
        foreach ($instances as $inst){
            $inst->makeMultiThumb();
        }

        $instances = Project::whereNotNull('image')->get();
        foreach ($instances as $inst){
            $inst->makeMultiThumb();
        }
    }
}
