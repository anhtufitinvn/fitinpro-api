<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\RoomCategory;
use Illuminate\Support\Facades\Hash;

class InitRoomCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:editor:room-category';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $cat = [
            'name' => 'Phòng ngủ',
            'code' => 'bedroom'
        ];
        RoomCategory::firstOrCreate(['code' => $cat['code']], $cat);

        $cat = [
            'name' => 'Phòng khách',
            'code' => 'livingroom'
        ];
        RoomCategory::firstOrCreate(['code' => $cat['code']], $cat);

        $cat = [
            'name' => 'Phòng ăn',
            'code' => 'diningroom'
        ];
        RoomCategory::firstOrCreate(['code' => $cat['code']], $cat);
    }
}
