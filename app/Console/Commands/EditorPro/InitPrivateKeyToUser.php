<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\User;
use Illuminate\Support\Facades\Hash;

class InitPrivateKeyToUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:editor:user:private-key';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $users = User::all();
        foreach ($users as $user){
            $config = $user->config ?? [];
            $config = array_merge($config, [
                'private_key' => gen_uuid()
            ]);

            $user->config = $config;
            $user->save();
        }
    }
}
