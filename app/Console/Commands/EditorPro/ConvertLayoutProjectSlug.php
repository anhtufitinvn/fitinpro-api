<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\EditorPro\Models\Layout; 

use \Illuminate\Support\Facades\Storage;
use App\Helpers\Utils;

use App\Models\Ver2\FileDriver; 
use Illuminate\Support\Str;

class ConvertLayoutProjectSlug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:layout:slug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $layouts = Layout::all();
        foreach ($layouts as $layout){
            $layout->project = Str::slug($layout->project, "-");
            $layout->save();
        }

    }


    
    
}
