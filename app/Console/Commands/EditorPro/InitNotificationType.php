<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\NotificationType;
use Illuminate\Support\Facades\Hash;

class InitNotificationType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:editor:notification-type';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $type = [
            'code' => 'bundle.approved',
            'message' => 'Your bundle :key has been approved'
        ];
        NotificationType::updateOrCreate(['code' => $type['code']], $type);

        $type = [
            'code' => 'bundle.rejected',
            'message' => 'Your bundle :key has been rejected'
        ];
        NotificationType::updateOrCreate(['code' => $type['code']], $type);

        $type = [
            'code' => 'room.approved',
            'message' => 'Your room :key has been approved'
        ];
        NotificationType::updateOrCreate(['code' => $type['code']], $type);

        $type = [
            'code' => 'room.rejected',
            'message' => 'Your room :key has been rejected'
        ];
        NotificationType::updateOrCreate(['code' => $type['code']], $type);

        $type = [
            'code' => 'layout.approved',
            'message' => 'Your layout :key has been approved'
        ];
        NotificationType::updateOrCreate(['code' => $type['code']], $type);

        $type = [
            'code' => 'layout.rejected',
            'message' => 'Your layout :key has been rejected'
        ];
        NotificationType::updateOrCreate(['code' => $type['code']], $type);
       
    }
}
