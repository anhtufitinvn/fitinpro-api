<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\Brand;
use Illuminate\Support\Facades\Hash;

class InitBrand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init:editor:brand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $brand = [
            'name' => 'JYSK',
            'code' => 'jysk',
            'status' => 'published'
        ];
        Brand::firstOrCreate(['code' => $brand['code']], $brand);

        $brand = [
            'name' => 'Casa Nha',
            'code' => 'casanha',
            'status' => 'published'
        ];
        Brand::firstOrCreate(['code' => $brand['code']], $brand);
    }
}
