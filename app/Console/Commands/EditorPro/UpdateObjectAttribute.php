<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\Object3d;

use App\EditorPro\Repositories\Object3dRepository;
use Illuminate\Support\Facades\Hash;

class UpdateObjectAttribute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update:object:attribute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    protected $objectRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Object3dRepository $objectRepository)
    {
        $this->objectRepository = $objectRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
        $params = ['category' => 'module', 'type' => 'item', 'user' => 360];
        $objects = $this->objectRepository->getAll($params);
        foreach ($objects as $obj) {
            $attrs = $obj->getOriginal('attributes') ?? [];
            $attrs['canArray'] = 1;
            $attrs['canScale'] = 1;
            $attrs['canCook'] = 1;
            $obj->update(['attributes' => $attrs]);
        }

    }
}
