<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\CMSUser;
use App\EditorPro\Models\Role;
use App\EditorPro\Models\Permission;

class InitUserRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:editor:role:permission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init roles';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $permission_list = config()->get('editor-permissions');


        foreach ($permission_list as $group_name => $group_permissions) {
            foreach($group_permissions as $name => $title){
                Permission::updateOrCreate(['name' => "$group_name.$name"], [
                    'name' => "$group_name.$name",
                    'title' => $title,
                    'group' =>  $group_name
                ]);
                
            } 
            
        }

        $arrRoles = [
            [
                'name' => 'admin',
                'title' => 'Admin'
            ],                      
            [
                'name' => 'designer',
                'title' => 'Designer'
            ]
        ];

        foreach ($arrRoles as $item) {
            $role = Role::updateOrCreate(['name' => $item['name']], [
                'name' => $item['name'],
                'title' => $item['title'] ?? NULL      
            ]);

            if( $item['name'] == 'admin'){
                foreach ($permission_list as $group_name => $group_permissions){       
                    foreach ($group_permissions as $p=>$title){
                        $permission_name = "$group_name.$p";
                        $role->givePermissionTo($permission_name);
                    }
                }
                
            }
        }
        
        $user = CMSUser::where('email','admin@fitin.vn')->first();
        if($user){
            $user->syncRoles('admin');
        }

        

        return 'Successfully!';
    }
}
