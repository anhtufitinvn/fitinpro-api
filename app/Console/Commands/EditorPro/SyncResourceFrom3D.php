<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Models\Ver2\Object3d; 
use App\EditorPro\Models\Object3d as EditorObject3d; 

use App\Models\Ver2\Material; 
use App\EditorPro\Models\Material as EditorMaterial; 

use App\EditorPro\Models\Project; 
use App\EditorPro\Models\Style; 
use App\EditorPro\Models\Category; 

use Illuminate\Support\Facades\Storage;
use App\FitIn\Asset;
use App\Helpers\Utils;

use App\Models\Ver2\FileDriver; 


class SyncResourceFrom3D extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'editor:sync:3d';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {          
        $this->syncObjects();
        $this->syncMaterials();
        $this->syncProjects();
        $this->syncStyles();
        $this->syncCategories();
    }

    protected function syncObjects(){           
        $objects = Object3d::all();
        $assetDriver = config('fitin.asset.filesystem_driver');
        $editorDriver = config('fitin.editor.filesystem_driver');

        foreach ($objects as $object){
            if($object->editorProAsset){
                $thumb = ($object->editorPro && is_array($object->editorPro) && !empty($object->editorPro['thumb']))? $object->editorPro['thumb'] : $object->thumb;
                $data = [
                    "displayName" => $object->displayName,
                    "vendor" => $object->vendor ,
                    "attributes" => $object->getOriginal('attributes') ?? [],
                    "ownerId" => $object->ownerID,
                    "category" => $object->category,
                    "name" => $object->name,
                    "brand" => $object->brand,
                    "thumb" => $thumb,
                    "status" => $object->status,
                    'assetBundle' => $object->editorProAsset,
                    'revision'  => $object->editorProUploadVersion ?? 0
                ];

                $editorObj = EditorObject3d::firstOrCreate(['name' => $object->name], $data);
                if($editorObj->wasRecentlyCreated){
                    $path = $thumb;
                    if($path){
                        if( Storage::disk($assetDriver)->exists($path) &&  !Storage::disk($editorDriver)->exists($path)){
                            Storage::disk($editorDriver)->put($path, Storage::disk($assetDriver)->get($path));
                            $pathinfo = pathinfo($path);
                            $basename = $pathinfo['basename'];
                            $url = Storage::disk($editorDriver)->url($path);
                            uploadDriver($basename, $editorDriver, $path, $url , $editorObj);
                        }
                    }

                    echo "Created {$object->name} \n";
                }
                
            }
            
        }
    }

    protected function syncMaterials()
    {
        $materials = Material::all();
        $assetDriver = config('fitin.asset.filesystem_driver');
        $editorDriver = config('fitin.editor.filesystem_driver');
        foreach ($materials as $material){
            if($material->editorPro && !empty($material->editorPro['asset'])){
                $path = $material->image;
                $data = [
                    'name'  => $material->name, 
                    'code'  => $material->code,
                    'status'  => $material->status,
                    'hexColor'  => $material->hexColor,
                    'image' => $material->image,
                    'assetBundle' => $material->editorPro['asset'],
                    'revision' => \Carbon\Carbon::now()->timestamp,
                    'attrs'  => $material->getOriginal('attrs'),
                    'type' => $material->getOriginal('attrs')['type'] ?? null
                ];

                $editorMtl = EditorMaterial::firstOrCreate(['code' => $material->code], $data);
                if($editorMtl->wasRecentlyCreated){
                    if($path){
                        if( Storage::disk($assetDriver)->exists($path) &&  !Storage::disk($editorDriver)->exists($path)){
                            Storage::disk($editorDriver)->put($path, Storage::disk($assetDriver)->get($path));
                            $pathinfo = pathinfo($path);
                            $basename = $pathinfo['basename'];
                            $url = Storage::disk($editorDriver)->url($path);
                            uploadDriver($basename, $editorDriver, $path, $url , $editorMtl);
                        }
                    }
                }
                
            }
        }
    }
    
    protected function syncProjects(){
        $asset = new Asset();
        $ecom_projects = $asset->getEcomProjects();
        $driver = config('fitin.editor.filesystem_driver');
        foreach ($ecom_projects as $project){
            if(empty($project['name'])){
                continue;
            }
            $name = $project['name'];
            $code = $project['slug'] ?? \Illuminate\Support\Str::slug($name, "-");
            $data = [
                'name' => $name,
                'code' => $code,
                'status' => $project['status'] ?? 'pending'
            ];
            $project_model = Project::firstOrCreate(['code' => $code], $data);
            if($project_model->wasRecentlyCreated){
                if(!$project_model->image && !empty($project['image'])){
                    $thumb_url = $project['image'];
                    $pathinfo = pathinfo($thumb_url);
                    $basename = $pathinfo['basename'];
                    if(file_valid($thumb_url)){
                        $file_ecom_content = file_get_contents($thumb_url);
                        $type = Project::DIR_NAME;
                        $time = \Carbon\Carbon::now()->timestamp;
                        $path = "$type/$code/$time/$basename";
                        $download = Storage::disk($driver)->put($path, $file_ecom_content);
    
                        $url = Storage::disk($driver)->url($path);
                        $project_model->image = $path;
                        $project_model->save();
                        uploadDriver($basename, $driver, $path, $url , $project_model);
                    }else{
                        continue;
                    }
                }
            }
            
        }
    }

    protected function syncStyles(){
        $asset = new Asset();
        $ecom_styles = $asset->getEcomStyles();
        
        $driver = config('fitin.editor.filesystem_driver');
        foreach ($ecom_styles as $style){
            if(empty($style['name'])){
                continue;
            }
            $name = $style['name'];
            $code = $style['slug'] ?? \Illuminate\Support\Str::slug($name, "-");
            $data = [
                'name' => $name,
                'code' => $code,
                'status' => $style['status'] ?? 'pending'
            ];
            $style_model = Style::firstOrCreate(['code' => $code], $data);
            if($style_model->wasRecentlyCreated){
                if(!$style_model->image && !empty($style['image'])){
                    $thumb_url = $style['image'];
                    $pathinfo = pathinfo($thumb_url);
                    $basename = $pathinfo['basename'];
                    if(file_valid($thumb_url)){
                        $file_ecom_content = file_get_contents($thumb_url);
                        $type = Style::DIR_NAME;
                        $time = \Carbon\Carbon::now()->timestamp;
                        $path = "$type/$code/$time/$basename";
                        $download = Storage::disk($driver)->put($path, $file_ecom_content);
    
                        $url = Storage::disk($driver)->url($path);
                        $style_model->image = $path;
                        $style_model->save();
                        uploadDriver($basename, $driver, $path, $url , $style_model);
                    }else{
                        continue;
                    }
                }
            }
            
        }
    }

    protected function syncCategories(){
        $asset = new Asset();
        $ecom_cats = $asset->getEcomCategories();
        
        $driver = config('fitin.editor.filesystem_driver');
        foreach ($ecom_cats as $cat){
            if(empty($cat['name'])){
                continue;
            }
            $name = $cat['name'];
            $code = $cat['slug'] ?? \Illuminate\Support\Str::slug($name, "-");
            $data = [
                'name' => $name,
                'code' => $code,
                'status' => 'published'
            ];
            $cat_model = Category::firstOrCreate(['code' => $code], $data);
            if($cat_model->wasRecentlyCreated || !$cat_model->image){
                if(!$cat_model->image && !empty($cat['image'])){
                    $thumb_url = $cat['image'];
                    $pathinfo = pathinfo($thumb_url);
                    $basename = $pathinfo['basename'];
                    if(file_valid($thumb_url)){
                        $file_ecom_content = file_get_contents($thumb_url);
                        $type = Category::DIR_NAME;
                        $time = \Carbon\Carbon::now()->timestamp;
                        $path = "$type/$code/$time/$basename";
                        $download = Storage::disk($driver)->put($path, $file_ecom_content);
    
                        $url = Storage::disk($driver)->url($path);
                        $cat_model->image = $path;
                        $cat_model->save();
                        uploadDriver($basename, $driver, $path, $url , $cat_model);
                    }else{
                        continue;
                    }
                }
            }

            foreach ($cat['children'] as $sub_cat){
                if(empty($sub_cat['name'])){
                    continue;
                }
                $name = $sub_cat['name'];
                $code = $sub_cat['slug'] ?? \Illuminate\Support\Str::slug($name, "-");
                $data = [
                    'name' => $name,
                    'code' => $code,
                    'status' => 'published',
                    'parent' => $cat_model->code
                ];
                $sub_cat_model = Category::updateOrCreate(['code' => $code], $data);
                if($sub_cat_model->wasRecentlyCreated){
                    if(!$sub_cat_model->image && !empty($sub_cat['image'])){
                        $thumb_url = $sub_cat['image'];
                        $pathinfo = pathinfo($thumb_url);
                        $basename = $pathinfo['basename'];
                        if(file_valid($thumb_url)){
                            $file_ecom_content = file_get_contents($thumb_url);
                            $type = Category::DIR_NAME;
                            $time = \Carbon\Carbon::now()->timestamp;
                            $path = "$type/$code/$time/$basename";
                            $download = Storage::disk($driver)->put($path, $file_ecom_content);
        
                            $url = Storage::disk($driver)->url($path);
                            $sub_cat_model->image = $path;
                            $sub_cat_model->save();
                            uploadDriver($basename, $driver, $path, $url , $sub_cat_model);
                        }else{
                            continue;
                        }
                    }
                }
            }
            
        }
    }
    
}
