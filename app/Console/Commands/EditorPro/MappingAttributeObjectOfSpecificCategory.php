<?php

namespace App\Console\Commands\EditorPro;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\EditorPro\Models\Object3d;
use Illuminate\Support\Facades\Hash;

class MappingAttributeObjectOfSpecificCategory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:mapping:editor:object:attribute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mappings = [
            'giuong' => [
                'putable' => 1
            ],
            'ban' => [
                'putable' => 1
            ],
            'sofa' => [
                'putable' => 1
            ],
            'ghe' => [
                'putable' => 1
            ],
            'sofa-don' => [
                'putable' => 1
            ],
            'tu' => [
                'hangable' => 1
            ],
            'tham' => [
                'isCarpet' => 1
            ]
        ];
        $objects = Object3d::all();
        foreach ($objects as $object){
            $attributes = $object->getOriginal('attributes');
            
            foreach ($mappings as $cat=> $array_atts){
                if($object->originCategory == $cat){
                    $attributes = array_merge($attributes, $array_atts);
                }
            }

            if (strpos($object->name, 'carpet') !== false) {
                $attributes['isCarpet'] = 1;
            }

            if (strpos($object->name, 'picture') !== false) {
                $attributes['hookable'] = 1;
                $attributes['anchor'] = 'wall';
            }

            $object->update(['attributes' => $attributes]);
        }
        echo 'Done';
        return 0;
    }
}
