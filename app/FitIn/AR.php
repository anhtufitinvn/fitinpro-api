<?php

namespace App\FitIn;


use Illuminate\Support\Facades\Log;

class AR
{

    public static function getFileInfoUrl($key_name, $platform)
    {
        $arrResult = self::getFileInfo($key_name, $platform);
        $url = !empty($arrResult['data']['url']) && $arrResult['data']['url'] != "N/A" ? $arrResult['data']['url'] : false;
        return $url;
    }

    public static function getFileInfo($key_name, $platform)
    {
        $arrResult = self::requestAPI('v1/file/' . $key_name . '/' . $platform);
        return $arrResult;
    }

    public static function approveFile($key_name, $platform)
    {
        $arrResult = self::requestAPI('v1/file/approve/' . $key_name . '/' . $platform, 'POST');
        return $arrResult;
    }


    public static function requestAPI($url, $method = 'GET')
    {
        try{
            $arrData = [
                'headers' => [
                    'X-Access-Token' => config('fitin.ar.AR_FILE_API_TOKEN'),
                    'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
                    'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                    'Upgrade-Insecure-Requests' => 1

                ]
            ];

            $url = config('fitin.ar.AR_FILE_API') . '/' . $url;

            $client = new \GuzzleHttp\Client();
            $response = $client->request($method, $url, $arrData);

            if ($response->getStatusCode() == 200) {
                $jsonResult = $response->getBody()->getContents();
                $result = json_decode($jsonResult, 1);

                return $result;
            }
            return false;
        }
        catch (\Exception $e){
            Log::error($e);

            return false;
        }
    }


}
