<?php
namespace App\FitIn;


class Helper
{
    public static function unlimit_time() {
        set_time_limit( 0 );
        ini_set( 'max_execution_time', - 1 );
    }

    public static function getORMKey($oQuery)
    {
        $sqlRaw = $oQuery->toSql();
        $key = md5(serialize($sqlRaw));
        return $key;
    }
}
