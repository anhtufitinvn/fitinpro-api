<?php

namespace App\FitIn;

use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\Log;
use VladimirYuldashev\LaravelQueueRabbitMQ\Queue\Connectors\RabbitMQConnector;
use Psr\Log\NullLogger;

class RabbitMQ
{

    private $queue;

    public function __construct()
    {
        $this->setUpQueue();
    }

    public function setUpQueue()
    {
        $connector = new RabbitMQConnector(new Dispatcher());
        $queue = $connector->connect(config('queue.connections.rabbitmq'));
        $queue->setContainer($this->createContainer());
        $this->queue = $queue;
    }

    private function createContainer()
    {
        $container = new Container();
        $container['log'] = new NullLogger;
        return $container;
    }

    /**
     * @param $json_data array
     * @return mixed
     */
    public function sendMessages($json_data)
    {
        try{

            return $this->queue->pushRaw(
                $json_data
            );
        }
        catch (\Exception $e){
            $arrError = [
                'api' => config('app.url'),
                'error' => class_basename($e) . ' | ' .$e->getMessage(),
                'stack' => mb_substr($e->getTraceAsString(), 0, 100)  
             ];
             $channel = env('LOG_CHANNEL', 'telegram');
             Log::channel($channel)->error($arrError);
            return null;
        }
    }
}
