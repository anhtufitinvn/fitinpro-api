<?php
namespace App\FitIn;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Asset
{
    protected $api_url;
    
    public function __construct()
    {
        $this->api_url = [
            'category' => 'api/frontend/v1/product-categories/filters', 
            'layout' => 'api/frontend/v1/layouts',
            'resource' => 'api/frontend/v1/resources',
            'objects' => 'api/frontend/v1/object-keys', //Object đang bán
            'projects' => 'api/frontend/v1/layout-projects',
            'styles' => 'api/frontend/v1/layout-styles',
            'object_thumbs' => 'api/frontend/v1/products-3d',
            'product-filter' => 'api/frontend/v1/products/filters',
            'product-by-key' => 'api/frontend/v1/products/by-object-key-name/{key}',
            'layout-by-key' => 'api/frontend/v1/layouts/{key}'
        ];
    }

    public function getByType($type, $query = false)
    {
        if(!isset($this->api_url[$type])){
            $string = implode(", ", array_keys($this->api_url));
            return [         
                "code" => -1,
                "message" => "Only support $string",
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "Only support $string",
                        "stack" => []
                    ]
                ]
            
            ];
        }
        $arrResult = self::requestGET($this->api_url[$type], $query);
        
        return $arrResult['data'];    
    }

    public function getByKey($type, $key, $query = false)
    {
        if(!isset($this->api_url[$type])){
            $string = implode(", ", array_keys($this->api_url));
            return [         
                "code" => -1,
                "message" => "Only support $string",
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "Only support $string",
                        "stack" => []
                    ]
                ]
            ];
        }
        $url = str_replace('{key}', $key, $this->api_url[$type]);
        $arrResult = self::requestGET($url, $query);
        
        if(isset($arrResult['code']) && $arrResult['code'] == -1){
            return null;
        }
        return $arrResult['data'];    
    }

    public function checkItem($array_keys){
        $resource = $this->getByType('objects');
        $object_keys = $resource['object_keys'] ?? [];
        return array_intersect($object_keys, $array_keys);
        
    }

    public function getEcomItems(){
        $resource = $this->getByType('objects');
        $object_keys = $resource['object_keys'] ?? [];
        return $object_keys;
        
    }

    public function getEcomCategories(){
        $resource = $this->getByType('category');
        return $resource ?? [];  
    }

    public function getEcomBundles(){
        return [];
        
    }

    public function getEcomProjects(){
        $resource = $this->getByType('projects') ?? [];
        return $resource;
    }

    public function getEcomStyles(){
        $resource = $this->getByType('styles') ?? [];
        return $resource;
    }

    public function getEcomItemsWithThumb($query){
       
        $resource = $this->getByType('object_thumbs', $query);
        
        return $resource;
        
    }

    public static function requestGET($url, $query = false)
    {
        try {
            $query_params = $query ?  ['query' => $query] : [];
            $url = config('fitin.ecom.production_api_endpoint') . '/' . $url;
            $client = new Client();
            
            $response = $client->request('GET',  $url , $query_params);
            $stream = $response->getBody();
            $contents = $stream->getContents();
            $result = json_decode($contents, 1);
            return $result;
            
        } catch (ClientException $e) {
            return [
                "code" => -1,
                "message" => $e->getMessage(),
                "errors" => [
                    [
                        "error" => 1,
                        "message" => $e->getMessage(),
                        "stack" => []
                    ]
                ]
            ];
        }
    }

    public static function requestPOST($url, $dataPost)
    {
        try {
            $arrData = [
                'headers' => []
            ];
            $arrData['json'] = $dataPost;
            $client = new \GuzzleHttp\Client();
            $url = config('fitin.ecom.api_endpoint') . '/' . $url;
            
            $response = $client->request('POST', $url, $arrData);

            if ($response->getStatusCode() == 200) {
                $jsonResult = $response->getBody()->getContents();
                $result = json_decode($jsonResult, 1);

                return $result;
            }
            return false;
        } catch (ClientException $e) {
            return [
                "code" => -1,
                "message" => $e->getMessage(),
                "errors" => [
                    [
                        "error" => 1,
                        "message" => "",
                        "stack" => []
                    ]
                ]
            ];
        }
    }

}
