<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use App\Models\V2\Permission;
use App\FitIn\CacheManager;

trait HasPermissions
{
    private $permissionClass;

    public function getPermissionClass(){
        $this->permissionClass = Permission::class;   
        return $this->permissionClass;
    }

    public static function bootHasPermissions()
    {
        static::deleting(function ($model) {
            if (method_exists($model, 'isForceDeleting') && ! $model->isForceDeleting()) {
                return;
            }

            //$model->permissions()->detach();
            $model->permissions()->sync([]);
        });
    }


    /**
     * A model may have multiple direct permissions.
     */
    // public function permissions()
    // {
    //     return $this->morphToMany(
    //         Permission::class,
    //         'model',
    //         null,
    //         'permission_ids'
    //     );
    // }


    /**
     * Return all the permissions the model has via roles.
     */
    public function getPermissionsViaRoles()
    {
        return $this->load('roles', 'roles.permissions')
            ->roles->flatMap(function ($role) {
                return $role->permissions;
            })->sort()->values();
    }

    /**
     * Return all the permissions the model has, both directly and via roles.
     *
     * @throws \Exception
     */
    public function getAllPermissions()
    {   
        $data = CacheManager::getAllPermissions($this);     
        return $data;
    }

    /**
     * Grant the given permission(s) to a role.
     *
     * @param string|array|\Spatie\Permission\Contracts\Permission|\Illuminate\Support\Collection $permissions
     *
     * @return $this
     */
    public function givePermissionTo(...$permissions)
    {
        $permissions = collect($permissions)
            ->flatten()
            ->map(function ($permission) {
                if (empty($permission)) {
                    return false;
                }

                return $this->getStoredPermission($permission);
            })
            ->filter(function ($permission) {
                return $permission instanceof Permission;
            })           
            ->map->_id
            ->all();

        $model = $this->getModel();
            
        if ($model->exists) {
            $this->permissions()->sync($permissions, false);
            $model->load(['permissions']);
        }
       
        return $this;
    }

    /**
     * Remove all current permissions and set the given ones.
     *
     * @param string|array|\Spatie\Permission\Contracts\Permission|\Illuminate\Support\Collection $permissions
     *
     * @return $this
     */
    public function syncPermissions(...$permissions)
    {
        
        //$this->permissions()->detach();
        $this->permissions()->sync([]);
        
        return $this->givePermissionTo($permissions);
    }

    /**
     * Revoke the given permission.
     *
     * @param \Spatie\Permission\Contracts\Permission|\Spatie\Permission\Contracts\Permission[]|string|string[] $permission
     *
     * @return $this
     */
    public function revokePermissionTo($permission)
    {
        $this->permissions()->detach($this->getStoredPermission($permission));

        $this->load('permissions');

        return $this;
    }

    public function getPermissionNames()
    {
        return $this->permissions->pluck('name');
    }

    /**
     * @param string|array|\Spatie\Permission\Contracts\Permission|\Illuminate\Support\Collection $permissions
     *
     * @return \Spatie\Permission\Contracts\Permission|\Spatie\Permission\Contracts\Permission[]|\Illuminate\Support\Collection
     */
    protected function getStoredPermission($permissions)
    {
        $permissionClass = $this->getPermissionClass();
        // if (is_numeric($permissions)) {
        //     return $permissionClass::where('_id', $permissions)->first();
        // }

        if (is_string($permissions)) {
            return $permissionClass::where('name',$permissions)->orWhere('_id', $permissions)->first();
        }

        if (is_array($permissions)) {
            return $permissionClass::whereIn('name', $permissions)
                ->get();
        }

        return $permissions;
    }

}
