<?php 

namespace App\Traits;

trait CastString
{
    public function setAttribute($key, $value)
    {
        parent::setAttribute($key, $value);
        if (!empty($this->cast_string)){
            foreach ($this->cast_string as $key){
                if(!empty($this->attributes[$key])){
                    $this->attributes[$key] = (string) $this->attributes[$key];
                }
            }
            
        }
    }
}