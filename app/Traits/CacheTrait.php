<?php

namespace App\Traits;

use Illuminate\Support\Facades\Cache;

trait CacheTrait
{
    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            
        });

        self::created(function ($model) {
            self::flushCache();
        });

        self::updating(function ($model) {

        });

        self::updated(function ($model) {
            self::flushCache();
        });

        self::deleting(function ($model) {

        });

        self::deleted(function ($model) {
            self::flushCache();
        });
    }

    public static function flushCache()
    {   
        $cacheModel = [
            \App\Models\Ver2\Object3d::class,
            \App\Models\Ver2\Bundle::class,
            \App\Models\Ver2\Layout::class,
            \App\Models\Ver2\Room::class,
            \App\Models\Ver2\Project::class,
            \App\Models\Ver2\Material::class,
            \App\Models\Ver2\Category::class,
            \App\Models\EditorPro\Library::class,
            \App\Models\EditorPro\Layout::class
        ];

         
        if(in_array(self::class, $cacheModel)){
            foreach($cacheModel as $model){           
                if(defined("$model::CACHE_TAG")){   
                    Cache::tags($model::CACHE_TAG)->flush();
                }   
            } 
        }else{
            if(defined("self::CACHE_TAG")){   
                Cache::tags(self::CACHE_TAG)->flush();
            }  

            if(defined("self::ADDITIONAL_CACHE_TAG")){   
                Cache::tags(self::ADDITIONAL_CACHE_TAG)->flush();
            }  
        }
        
    }
}
