<?php

namespace App\Providers;

use App\Events\UploadEvent;
use App\Listeners\UploadEventListener;
use App\Events\DownloadEvent;
use App\Listeners\DownloadEventListener;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UploadEvent::class => [
            UploadEventListener::class
        ],
        DownloadEvent::class => [
            DownloadEventListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
