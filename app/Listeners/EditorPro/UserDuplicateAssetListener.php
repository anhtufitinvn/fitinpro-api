<?php

namespace App\Listeners\EditorPro;

use App\EditorPro\Events\UserDuplicateAsset;
use App\Models\EditorPro\User;
use App\Models\EditorPro\Library;

class UserDuplicateAssetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Handle the event.
     *
     * @param  customerOrder  $event
     * @return void
     */
    public function handle(UserDuplicateAsset $event)
    {
        $user = $event->user;
        $instance = $event->instance;
        $key = $event->key;
        $class = get_class($instance);

        $data_reference = [
            'userId' => $user->auth_id,
            'key' => $key,
            'model' => $class,
            'type' => $class::MODEL_TYPE  
        ];

        $data = array_merge($data_reference, [ 'downloadStatus' => Library::SUCCESS]);

        $libraryCreate = Library::firstOrCreate($data_reference, $data);

        return $libraryCreate;
        
    }
}
