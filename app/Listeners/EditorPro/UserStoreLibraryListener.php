<?php

namespace App\Listeners\EditorPro;

use App\EditorPro\Events\UserStoreLibrary;
use App\Models\EditorPro\User;
use App\Models\EditorPro\Library;

class UserStoreLibraryListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Handle the event.
     *
     * @param  customerOrder  $event
     * @return void
     */
    public function handle(UserStoreLibrary $event)
    {
        $key = $event->key;
        $user = $event->user;
        $instance = $event->instance;
        $class = get_class($instance);

        $data_reference = [
            'userId' => $user->auth_id,
            'key' => $key,
            'model' => $class,
            'type' => $class::MODEL_TYPE  
        ];

        $data = array_merge($data_reference, [ 'downloadStatus' => Library::PENDING]);


        $libraryCreate = Library::firstOrCreate($data_reference, $data);

        return $libraryCreate;
        
    }
}
