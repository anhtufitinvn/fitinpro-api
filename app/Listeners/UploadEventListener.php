<?php

namespace App\Listeners;

use App\Events\UploadEvent;
use App\Models\Ver2\FileDriver;

class UploadEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Handle the event.
     *
     * @param  UploadEvent  $event
     * @return void
     */
    public function handle(UploadEvent $event)
    {
        $file = $event->file;
        $driver = $event->driver;
        $path = $event->path;
        $url = $event->url;
        $instance = $event->instance;

        $data = [
            'file' => $file,
            'driver' => $driver,
            'path' => $path,
            'url' => $url
        ];
        if($instance){
            $data['key'] = $instance->getKey();
            $data['model'] = get_class($instance);
        }
        $file = FileDriver::create($data);
        return $file;
        
    }
}
