<?php

namespace App\Listeners;

use App\Events\DownloadEvent;
use App\Models\Log\UserDownload;

class DownloadEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Handle the event.
     *
     * @param  DownloadEvent  $event
     * @return void
     */
    public function handle(DownloadEvent $event)
    {
        $user = $event->user;
        $ip = $event->ip;
        $file = $event->file;
        $driver = $event->driver;
        $path = $event->path;
        $url = $event->url;
        $instance = $event->instance;

        $data = [
            'userId' => $user->auth_id,
            'address' => $ip,
            'file' => $file,
            'driver' => $driver,
            'path' => $path,
            'url' => $url
        ];
        if($instance){
            $data['key'] = $instance->getKey();
            $data['model'] = get_class($instance);
        }
        return UserDownload::create($data);
        
    }
}
