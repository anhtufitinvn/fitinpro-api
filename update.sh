#!/usr/bin/env bash
git pull

chown -R apache:apache /data/php/cms-editor
chmod -R 775 bootstrap/cache/
chmod -R 777 storage/

php artisan activity:model-method:update
php artisan command:init:editor:notification-type
php artisan command:init:editor:room-category

php artisan collection:index
php artisan route:clear
php artisan cache:clear
php artisan config:clear

​php artisan queue:restart

supervisorctl reread
supervisorctl update

supervisorctl restart cms-editor-queue-3dresource:*
