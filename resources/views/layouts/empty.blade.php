<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
   <meta charset="utf-8">
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
   <meta name="description" content="Bootstrap Admin App">
   <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
   <link rel="icon" type="image/x-icon" href="{{asset('favicon.ico')}}">
   <title>FitIn 3D Dashboard Enterprice</title>
   <!-- =============== VENDOR STYLES ===============-->
   <!-- FONT AWESOME-->
   <link rel="stylesheet" href="{{asset('vendor/@fortawesome/fontawesome-free/css/brands.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/@fortawesome/fontawesome-free/css/regular.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/@fortawesome/fontawesome-free/css/solid.css')}}">
   <link rel="stylesheet" href="{{asset('vendor/@fortawesome/fontawesome-free/css/fontawesome.css')}}">
   <!-- SIMPLE LINE ICONS-->
   <link rel="stylesheet" href="{{asset('vendor/simple-line-icons/css/simple-line-icons.css')}}">
   <!-- =============== BOOTSTRAP STYLES ===============-->
   <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}" id="bscss">
   <!-- =============== APP STYLES ===============-->
   <link rel="stylesheet" href="{{asset('css/theme.css')}}" id="maincss">
   <link rel="stylesheet" href="{{asset('css/app.css')}}" id="maincss">
   <script>
    window.appConfigs = @json($appConfigs)
  </script>
</head>

<body>
   <div class="wrapper">
      <div class="block-center wd-xxl">
         <div id="app">
           @yield('page')
         </div>

      </div>
   </div>

    <!-- sweetalert-->
    <script src="{{asset('vendor/sweetalert/dist/sweetalert.min.js')}}"></script>
   <!-- =============== APP SCRIPTS ===============-->
   <script src="{{asset('js/app.js')}}"></script>
</body>

</html>
