<div class="dataTables_paginate paging_simple_numbers" id="table-log_paginate">
<ul class="pagination justify-content-end">
        <li class="{!! $items->previousPageUrl() ? '' : 'disabled' !!} btn-page btnprev page-item">
            <a class="page-link" href="{!! $items->previousPageUrl() !!}" aria-label="Previous">
                <i class="fa fa-angle-double-left"></i> Previous
            </a>
        </li>
                                @if(ceil($items->total() / $items->perPage()) < 6)
                                
                                            @for($i = 1; $i <= ceil($items->total() / $items->perPage()); $i++)
                                            <li class="page-item {{($items->currentPage() == $i)? 'active' : ''}}"><a class="page-link" href="{{$items->url($i)}}">{!! $i !!}</a></li>    
                                            @endfor
                                
                                        @else
                                            <li class="page-item {{($items->currentPage() == 1)? 'active' : ''}}"><a class="page-link" href="{{$items->url(1)}}">{!! 1 !!}</a></li>    
                                            @if($items->currentPage() > 3)
                                            <li class="disabled"><a>...</a></li>
                                            @endif
                                
                                            @for($i = 2; $i <= ceil($items->total() / $items->perPage()) - 1; $i++)
                                                @if(in_array($i , [$items->currentPage()-1, $items->currentPage(), $items->currentPage() +1]))
                                                <li class="page-item {{($items->currentPage() == $i)? 'active' : ''}}"><a class="page-link" href="{{$items->url($i)}}">{!! $i !!}</a></li>    
                                                @endif
                                            @endfor
                                
                                            @if($items->currentPage() < ceil($items->total() / $items->perPage()) - 2)
                                            <li class="disabled"><a class="page-link">...</a></li>
                                            @endif
                                
                                            <li class="page-item {{($items->currentPage() == ceil($items->total() / $items->perPage()))? 'active' : ''}}"><a class="page-link" href="{{$items->url(ceil($items->total() / $items->perPage()))}}">{!! ceil($items->total() / $items->perPage()) !!}</a></li>    
                                        @endif
        <li class="{!! $items->nextPageUrl() ? '' : 'disabled' !!}  page-item btn-page btnnext">
            <a class="page-link" href="{!! $items->nextPageUrl() !!}" aria-label="Next">
                Next <i class="fa fa-angle-double-right"></i>
            </a>
        </li>
    </ul>
</div>