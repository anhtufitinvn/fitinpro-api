<!-- START sidebar nav-->
<ul class="sidebar-nav">
  <!-- START user info-->
  <li class="has-user-block">
    <div class="collapse show" id="user-block">
      <div class="item user-block">
        <!-- User picture-->
        <div class="user-block-picture">
          <div class="user-block-status">
            <img class="img-thumbnail rounded-circle" src="{{ Auth::user()->avatar() }}" alt="{{ Auth::user()->name }}" width="60" height="60">
            <div class="circle bg-success circle-lg"></div>
          </div>
        </div>
        <!-- Name and Job-->
        <div class="user-block-info">
          <span class="user-block-name">Hi {{ Auth::user()->name }}</span>
          <!-- <span class="user-block-role">Designer</span> -->
        </div>
      </div>
    </div>
  </li>
  <!-- END user info-->
  <!-- Iterates over all sidebar items-->
  <li class="nav-heading ">
    <span data-localize="sidebar.heading.HEADER">Menu Navigation</span>
  </li>
  <li class="{{ request()->is('/') ? 'active' : '' }}">
    <a href="{{ route('home') }}" title="Dashboard">
    <em class="far icon-speedometer"></em>
    <span>Dashboard</span>
    </a>
  </li>
  <li class="{{ (request()->is('layout*') || request()->is('editor/{id}')) ? 'active' : '' }}">
    <a href="{{ route('layout') }}" title="Layouts">
    <em class="far icon-layers"></em>
    <span>Layouts</span>
    </a>
  </li>
  <li class="{{ request()->is('project*') ? 'active' : '' }}">
    <a href="{{ route('project') }}" title="projects">
    <em class="far fa-clipboard"></em>
    <span>Projects</span>
    </a>
  </li>
  <li class="{{ request()->is('style*') ? 'active' : '' }}">
    <a href="{{ route('style') }}" title="styles">
    <em class="fab fa-foursquare"></em>
    <span>Styles</span>
    </a>
  </li>
  <li class="{{ request()->is('example') ? 'active' : '' }}">
    <!-- <a href="/example" title="Example">
    <em class="far fa-file"></em>
    <span data-localize="sidebar.nav.EXAMPLE">Example</span>
    </a> -->
    <!-- <a href="#submenu" title="Menu" data-toggle="collapse">
    <em class="icon-speedometer"></em>
    <span data-localize="sidebar.nav.menu.MENU">Menu</span>
    </a>
    <ul class="sidebar-nav sidebar-subnav collapse" id="submenu">
      <li class="sidebar-subnav-header">Menu</li>
      <li class=" ">
        <a href="submenu.html" title="Submenu">
        <span data-localize="sidebar.nav.menu.SUBMENU">Submenu</span>
        </a>
      </li>
    </ul> -->
  </li>
</ul>
<!-- END sidebar nav-->
