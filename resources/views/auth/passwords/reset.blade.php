@extends('layouts.app')

@section('page')
<!-- START card-->
<div class="card card-flat">
  <div class="card-header text-center">
      <a href="/">
        <img class="block-center rounded" src="{{asset('img/logo.png')}}" alt="Image">
      </a>
  </div>
  <div class="card-body">
      <p class="text-center py-2">{{ __('Reset Password') }}</p>
      <form method="POST" action="{{ route('password.update') }}">
          @csrf

          <input type="hidden" name="email" value="{{$email}}">
          <div class="form-group row">
              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Token code') }}</label>

              <div class="col-md-8">
                  <input id="token" type="token" class="form-control{{ $errors->has('token') ? ' is-invalid' : '' }}" name="token" value="{{ $token ?? old('token') }}" required autofocus>

                  @if ($errors->has('token'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('token') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group row">
              <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

              <div class="col-md-8">
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                  @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                  @endif
              </div>
          </div>

          <div class="form-group row">
              <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

              <div class="col-md-8">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
              </div>
          </div>

          <div class="form-group row mb-0">
              <div class="col-md-8 offset-md-4">
                  <button type="submit" class="btn btn-primary">
                      {{ __('Reset Password') }}
                  </button>
              </div>
          </div>
      </form>
  </div>
</div>

@endsection
