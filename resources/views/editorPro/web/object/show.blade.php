<!DOCTYPE html>
<html lang="en">
<head>
	<title>Product Detail</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400"> 
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/editor/detail/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/editor/detail/fonts/font-awesome-4.7.0/css/font-awesome.min.css">


	
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/editor/detail/css/util.css">
    <link rel="stylesheet" type="text/css" href="/editor/detail/css/main.css">
    
    <style>
        .dropdown-content img{
            width:100% !important;
            height: inherit!important;
        }
        .dropdown-content p{
            color:black;
        }
        span, p, a {
            font-family:Open Sans, sans-serif;   
            color:black!important;
        }

        .slick3 {
            width:100%;
        }
        </style>
<!--===============================================================================================-->
</head>
<body>

	<!-- Product Detail -->
	<div class="container bgwhite p-t-35 p-b-80">
		<div class="flex-w flex-sb">
			<div class="w-size13 p-t-30 respon5">
				<div class="wrap-slick3 flex-sb flex-w">
				
					<div class="slick3">
						<div class="item-slick3" data-thumb="images/thumb-item-01.jpg">
							<div class="wrap-pic-w">
								<img src="{{$data['image']}}" alt="IMG-PRODUCT">
							</div>
						</div>

						
					</div>
				</div>
			</div>

			<div class="w-size14 p-t-30 respon5">
				<h4 class="product-detail-name m-text16 p-b-13">
                {{$data['name']}}
				</h4>

				<span class="m-text17">
					 {{number_format($data['price'], 0, '.', ',')}} đ
                </span>
                
                @if($data['percent_discount'])
                <div class="m-t-5">
                    <span class="s-text20">
                        Tiết kiệm: {{$data['percent_discount']}}% - Giá gốc: {{number_format($data['market_price'], 0, '.', ',')}} đ
                    </span>
                </div>
                @endif

				<!--  -->
                <div class="dis-block m-t-10">
					<span class="s-text3 m-r-35">SKU: {{$data['sku']}}</span>
					<span class="s-text3 float-r">Thương hiệu: {{$data['product_brand']['name']}}</span>
				</div>
				<div>
					<span class="s-text3">Loại: {{$data['product_category']['name']}}</span>
				</div>

                <div class="p-b-45">
					<span class="s-text3">Chi tiết xem tại: <br><a href="{{$url}}">{{$url}}</a></span>
				</div>
				<!--  -->
				<div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Thông tin sản phẩm
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						
                            {!!$data['description']!!}
                        
					</div>
				</div>

				<div class="wrap-dropdown-content bo7 p-t-15 p-b-14">
					<h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
						Hướng dẫn bảo quản
						<i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
						<i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
					</h5>

					<div class="dropdown-content dis-none p-t-15 p-b-23">
						
                            {!!$data['storage_instructions'] !!}
                        
					</div>
				</div>

				
			</div>
		</div>
	</div>



	


<!--===============================================================================================-->
	<script type="text/javascript" src="/editor/detail/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->

<!--===============================================================================================-->
<script type="text/javascript" src="/editor/detail/vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="/editor/detail/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->



<!--===============================================================================================-->
	<script src="/editor/detail/js/main.js"></script>

</body>
</html>
