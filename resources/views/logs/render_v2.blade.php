<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="noindex, nofollow">
  <title>Render Logs</title>
  <link rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
  <style>
    body {
      padding: 25px;
    }

    h1 {
      font-size: 1.5em;
      margin-top: 0;
    }

    #table-log {
        font-size: 0.85rem;
    }

    .sidebar {
        font-size: 0.85rem;
        line-height: 1;
    }

    .btn {
        font-size: 0.7rem;
    }

    .stack {
      font-size: 0.85em;
    }

    .date {
      min-width: 75px;
    }

    .text {
      word-break: break-all;
    }

    a.llv-active {
      z-index: 2;
      background-color: #f5f5f5;
      border-color: #777;
    }

    .list-group-item {
      word-wrap: break-word;
    }

    .folder {
      padding-top: 15px;
    }

    .div-scroll {
      height: 80vh;
      overflow: hidden auto;
    }
    .nowrap {
      white-space: nowrap;
    }

  </style>
</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col sidebar mb-3">
      <h1><i class="fa fa-calendar" aria-hidden="true"></i> Render Logs</h1>
      <p class="text-muted"><i>by lam.tran</i></p>
      <div class="list-group div-scroll">
       
        
      </div>
    </div>
    <div class="col-10 table-container">
      
        <table id="table-log" class="table table-striped" data-ordering-index="2">
          <thead>
          <tr>
            <th width="7%">Type</th>
            <th width="13%">Payload</th>
            <th width="7%">User Id</th>
            <th width="9%">Email</th>
            <th width="6%">Env</th>
            <th width="10%">Server</th>
            <th width="12%">Date</th>
            <th width="10%">Version</th>
            <th>Content</th>
          </tr>
          </thead>
          <tbody>

          @foreach($logs as $key => $log)
            <tr data-display="stack{{{$key}}}">
                <td class="nowrap {{(strtolower($log['type']) == 'error') ? 'text-danger' : 'text-info'}}">
                    <span class="fa {{(strtolower($log['type']) == 'error') ? 'fa-exclamation' : 'fa-info'}}" aria-hidden="true"></span>&nbsp;&nbsp;{{$log['type']}}
                </td>
                <td class="text">{{$log['payloadId']}}</td>
                <td class="text">{{$log['userId'] ?? ''}}</td>
                <td class="text">{{$log['email'] ?? ''}}</td>
                <td class="text">{{$log['environment'] ?? ''}}</td>
                <td class="text">{{$log['serverName']}}</td>
                <td class="date">{{\Carbon\Carbon::createFromFormat('Y:m:d_H:i:s.u', $log['time'])->format('Y-m-d H:i:s')}}</td>
                <td class="text">{{$log['versionBuild'] ?? ''}}</td>
                <td class="text">
              
                Click to collapse
                @if ($log['content'])
                  <div class="stack" id="stack{{{$key}}}"
                       style="display: none; white-space: pre-wrap;">{{{ trim($log['content']) }}}
                  </div>
                @endif
              </td>
            </tr>
          @endforeach

          </tbody>
        </table>
      
        <div class="row">
            <div class="col-sm-12 col-md-7"></div>
            <div class="col-sm-12 col-md-5">
                @include('layouts.pagination', ['items' => $logs])
            </div>
        </div>
    </div>

    
  </div>
</div>
<!-- jQuery for Bootstrap -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<!-- FontAwesome -->
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<!-- Datatables -->

<script>
  $(document).ready(function () {
    $('.table-container tr').on('click', function () {
      $('#' + $(this).data('display')).toggle();
    });
    
  });
</script>
</body>
</html>
