
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

var BootstrapVue = require('bootstrap-vue');
import 'bootstrap-vue/dist/bootstrap-vue.css';
window.Vue.use(BootstrapVue);

window.Vue.filter('toCurrency', function (value) {
    if (typeof value !== "number") {
      value = parseInt(value);
    }
    var formatter = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' });
    return formatter.format(parseInt(value));
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./pages', true, /\.vue$/i);
files.keys().map(key => {
  let nameP = key.split('/').pop().split('.')[0];
  nameP = nameP.toLowerCase() + '-page';
  Vue.component(nameP, files(key).default);
  return key;
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
